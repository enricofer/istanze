from django.db import models
from django.contrib.gis.db import models

from select_multiple_field.models import SelectMultipleField


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Create your models here.
class OCCUPAZIONE(models.Model):

    SCELTA_GIORNI = (
        ('0', 'LUN'),
        ('1', 'MAR'),
        ('2', 'MER'),
        ('3', 'GIO'),
        ('4', 'VEN'),
        ('5', 'SAB'),
        ('6', 'DOM'),
    )
    
    concessione = models.ForeignKey('CONCESSIONE',related_name="concessioni_dataset")
    delimitazione = models.MultiPolygonField(srid=3857)
    area = models.FloatField()
    dimensioni = models.CharField(max_length=50)
    data_from = models.DateField()
    data_to = models.DateField()
    giorni = SelectMultipleField(max_length=15, choices=SCELTA_GIORNI)
    ora_from = models.TimeField()
    ora_end = models.TimeField()
    
    
class CONCESSIONE(models.Model):
    
    def update_filename(instance, filename):
        path = "osap/%s" % instance.ditta
        return os.path.join(path, filename)
    
    ditta = models.CharField(max_length=50)
    localizzazione = models.CharField(max_length=50)
    sup_somministrazione = models.FloatField(blank=True, null=True) 
    note = models.CharField(max_length=50,blank=True, null=True)
    documento = models.FileField(upload_to=update_filename,blank=True, null=True)
