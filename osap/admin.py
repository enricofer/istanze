from django.contrib import admin

from .models import OCCUPAZIONE, CONCESSIONE

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Register your models here.
#admin.site.unregister(User)
#admin.site.unregister(Group)
#admin.site.register(User, UserAdmin)
#admin.site.register(Group, GroupAdmin)
admin.site.register(OCCUPAZIONE)
admin.site.register(CONCESSIONE)
