from django.apps import AppConfig

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

class OsapConfig(AppConfig):
    name = 'osap'
