from django_tables2 import Table,SingleTableView
from django_filters.views import FilterView
import django_tables2 as tables
from django_tables2.utils import A
from .models import corrispondenza,sottoscrittori,repertorioIstanze,catIstanze,settori

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

class ComunicazioniTable(tables.Table):
    
    id = tables.LinkColumn('comunicazione', args=[A('pk')], orderable=False, empty_values=())

    class Meta:
        model = corrispondenza
        # add class="paleblue" to <table> tag
        attrs = {"class": "paleblue"}
        exclude = ("", )
        
    def render_edit(self):
        return 'Edit'

class ModelTable(Table):
    class Meta:
        pass
        
def table_factory(model, categoria,filtro):
    meta_class = type('Meta', (ModelTable.Meta,), {'model':model, 'attrs':{"class": "paleblue"}, "exclude": ("geom", )})
    if filtro == "":
        filtro = '###'
    table_class = type(model._meta.object_name + 'Table',
                        (ModelTable,), {
                                'Meta':meta_class,
                                'categoria':categoria,
                                'filtro':filtro,
                                'id': tables.LinkColumn('istanza_filtrata', args=[A('pk'),categoria,filtro], orderable=True, empty_values=())
                            }
                        
                    )
    return table_class


        