var targetExtent = [1716014, 5023919, 1737137, 5038662];

var targetProjection = new ol.proj.Projection({
  code: 'EPSG:3003',
  extent: targetExtent,
  units: 'm'
});
ol.proj.addProjection(targetProjection);

var UTM32Extent = [719459.961,5024860.053, 733523.921,5038269.875];
proj4.defs('EPSG:32632', '+proj=utm +zone=32 +datum=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
var UTM32proj = new ol.proj.Projection({
  code: 'EPSG:32632',
  extent: UTM32Extent
});
ol.proj.addProjection(UTM32proj);

var UTM33Extent = [166021.4431, 0.0000, 833978.5569, 9329005.1825];
proj4.defs('EPSG:32633', '+proj=utm +zone=33 +datum=WGS84 +towgs84=128.8,17.85,0,0,0,0,0 +units=m +no_defs');
var UTM33proj = new ol.proj.Projection({
  code: 'EPSG:32633',
  extent: UTM33Extent
});
ol.proj.addProjection(UTM33Extent);

var ETRS89Extent = [719459.961,5024860.053, 733523.921,5038269.875];
proj4.defs('EPSG:25832', '+proj=utm +zone=32 +ellps=GRS80 +towgs84=128.8,17.85,0,0,0,0,0 +units=m +no_defs');
var ETRS89proj = new ol.proj.Projection({
  code: 'EPSG:25832',
  extent: ETRS89Extent
});
ol.proj.addProjection(ETRS89proj);

var mappeDiBase_supporto = new ol.layer.Group({
              'title': 'Mappe di base',
              'fold': 'close',
              layers: [
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'ortofoto 2007',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileArcGISRest({
                          url: 'https://oscar.comune.padova.it/server/rest/services/foto/MapServer',
                          projection: targetProjection
                        })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'agea 2015',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileArcGISRest({
                          url: 'https://oscar.comune.padova.it/server/rest/services/agea_2015/MapServer',
                          projection: targetProjection
                        })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'agea 2018',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileArcGISRest({
                          url: 'https://oscar.comune.padova.it/server/rest/services/agea_2018/MapServer',
                          projection: targetProjection
                        })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'ortofoto 2021',
                        visible: true,
                        type: 'base',
                        source: new ol.source.TileArcGISRest({
                          url: 'https://oscar.comune.padova.it/server/rest/services/foto_2021/MapServer',
                          projection: targetProjection
                        })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'BLOM ortho *',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileWMS({
                          url: 'http://Padova:P4d0v4_2013@www.blomurbex.com/v02/WMSService?usertoken=0ef1451624f1e268baf59b265c211f62',
                          projection: ETRS89proj,
                          params: {layers: 'ortho',format:'image/jpeg',CRS:'EPSG:25832'}
                        })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'BLOM north *',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileWMS({
                          url: 'http://Padova:P4d0v4_2013@www.blomurbex.com/v02/WMSService?usertoken=0ef1451624f1e268baf59b265c211f62',
                          projection: ETRS89proj,
                          params: {layers: 'north',format:'image/jpeg',CRS:'EPSG:25832'}
                        })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'catasto (servizio comune PD)',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileArcGISRest({
                          url: 'https://oscar.comune.padova.it/server/rest/services/catasto/MapServer',
                          projection: targetProjection
                        })
                      }),
                      new ol.layer.Tile({
                          title: 'catasto (servizio agenzia del territorio) *',
                          visible: false,
                          type: 'base',
                          source: new ol.source.TileWMS({
                              projection: ETRS89proj,
                              url: 'https://wms.cartografia.agenziaentrate.gov.it/inspire/wms/ows01.php',
                              params: {
                                  'LAYERS': 'CP.CadastralParcel,fabbricati,vestizioni',
                                  format:'image/png'
                              },
                              serverType: 'mapserver'
                          })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'DBT 2007',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileArcGISRest({
                          url: 'https://oscar.comune.padova.it/server/rest/services/dbt/MapServer',
                          projection: targetProjection
                        })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'CRT 1996',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileArcGISRest({
                          url: 'https://oscar.comune.padova.it/server/rest/services/ctr_1996/MapServer',
                          params: {
                            'LAYERS':"show:0,2,3,4,5,6,7,8,10,11,13",
                            'TRANSPARENT': 'true'
                            },
                          projection: targetProjection
                        })
                      }),
                      new ol.layer.Tile({
                        extent: targetExtent,
                        title: 'Nessuno sfondo',
                        visible: false,
                        type: 'base',
                        source: new ol.source.TileArcGISRest({
                          url: 'https://oscar.comune.padova.it/server/rest/services/nessuno_sfondo/MapServer',
                          projection: targetProjection
                        })
                      }),
              ]
          })

var mappeDiBase_PI = new ol.layer.Group({
              'title': 'Piano degli Interventi',
              'fold': 'close',
              layers: [
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'PI Adottato DCC 49/2022',
                  visible: false,
                  source: new ol.source.TileWMS({
                    url: 'https://rapper.comune.padova.it/mapproxy/',
                    params: {layers: 'PI2030',CRS:'EPSG:3003','DPI':150}
                  })
                }),
                /*new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'Piano degli interventi lavorazione',
                  visible: false,
                  source: new ol.source.TileArcGISRest({
                    url: 'https://oscar.comune.padova.it/server/rest/services/pi_work/MapServer',
                    projection: targetProjection,
                    params: {gdbVersion: 'FERREGUTIE.VAR_OSPEDALE_GIUSTINIANEO_2'}
                  })
                }),*/
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'PI centro storico vigente',
                  visible: true,
                  source: new ol.source.TileArcGISRest({
                    url: 'https://oscar.comune.padova.it/server/rest/services/pr1000/MapServer/export',
                    params: {layers:'show:3,4,6,7,9,10,11,12,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30'},
                    projection: targetProjection,
                  })
                }),
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'Piano degli interventi vigente',
                  visible: true,
                  source: new ol.source.TileArcGISRest({
                    url: 'https://oscar.comune.padova.it/server/rest/services/prg/MapServer',
                    projection: targetProjection
                  })
                }),
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'Piano degli interventi DECADUTO',
                  visible: true,
                  source: new ol.source.TileWMS({
                    url: 'https://rapper.comune.padova.it/qgisserver/?MAP=/dati/archiviazione/servizi/PI/PI_DECADUTO.qgs',
                    params: {layers: 'pi_decaduto',format:'image/png',CRS:'EPSG:3003'}
                  })
                })
              ]
          })

var mappeDiBase_PAT = new ol.layer.Group({
              'title': 'Piano di Assetto del territorio',
              'fold': 'close',
              layers: [
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'Ambiti di urbanizzazione consolidata ex L14/2017',
                  visible: false,
                  source: new ol.source.TileArcGISRest({
                    url: 'https://oscar.comune.padova.it/server/rest/services/pat/MapServer/export',
                    params: {layers:'show:70'},
                    projection: targetProjection,
                  })
                }),
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'PAT trasformabilità',
                  visible: false,
                  source: new ol.source.TileArcGISRest({
                    url: 'https://oscar.comune.padova.it/server/rest/services/pat/MapServer/export',
                    params: {layers:'show:36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68'},
                    projection: targetProjection,
                  })
                }),
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'PAT fragilità',
                  visible: false,
                  source: new ol.source.TileArcGISRest({
                    url: 'https://oscar.comune.padova.it/server/rest/services/pat/MapServer/export',
                    params: {layers:'show:30,31,32,33,34'},
                    projection: targetProjection,
                  })
                }),
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'PAT invarianti',
                  visible: false,
                  source: new ol.source.TileArcGISRest({
                    url: 'https://oscar.comune.padova.it/server/rest/services/pat/MapServer/export',
                    params: {layers:'show:21,22,23,24,25,26,27,28'},
                    projection: targetProjection,
                  })
                }),
                new ol.layer.Tile({
                  extent: targetExtent,
                  title: 'PAT vincoli',
                  visible: false,
                  source: new ol.source.TileArcGISRest({
                    url: 'https://oscar.comune.padova.it/server/rest/services/pat/MapServer/export',
                    params: {layers:'show:1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19'},
                    projection: targetProjection,
                  })
                })
              ]
          })

var mappeDiBase_PNRR = new ol.layer.Group({
  'title': 'PGRA',
  'fold': 'close',
  layers: [
    new ol.layer.Tile({
      extent: targetExtent,
      title: 'Tirante acqua TR300',
      visible: false,
      opacity: 0.8,
      source: new ol.source.TileWMS({
        url: 'https://sigma.distrettoalpiorientali.it/sigma/geo/sigma/wms',
        params: {layers: 'TR300_H',CRS:'EPSG:3003'},
        projection: targetProjection
      })
    }),
    new ol.layer.Tile({
      extent: targetExtent,
      title: 'Tirante acqua TR100',
      visible: false,
      opacity: 0.8,
      source: new ol.source.TileWMS({
        url: 'https://sigma.distrettoalpiorientali.it/sigma/geo/sigma/wms',
        params: {layers: 'TR100H',CRS:'EPSG:3003'},
        projection: targetProjection
      })
    }),
    new ol.layer.Tile({
      extent: targetExtent,
      title: 'Tirante acqua TR30',
      visible: false,
      opacity: 0.8,
      source: new ol.source.TileWMS({
        url: 'https://sigma.distrettoalpiorientali.it/sigma/geo/sigma/wms',
        params: {layers: 'TR30_H',CRS:'EPSG:3003'},
        projection: targetProjection
      })
    }),
    new ol.layer.Tile({
      extent: targetExtent,
      title: 'pericolo idraulico',
      visible: false,
      opacity: 0.8,
      source: new ol.source.TileWMS({
        url: 'https://rapper.comune.padova.it/mapproxy/',
        params: {layers: 'PGRA_pericolo',CRS:'EPSG:3003'},
        projection: targetProjection
      })
    }),
    new ol.layer.Tile({
      extent: targetExtent,
      title: 'rischio idraulico',
      visible: false,
      opacity: 0.8,
      source: new ol.source.TileWMS({
        url: 'https://rapper.comune.padova.it/mapproxy/',
        params: {layers: 'PGRA_rischio',CRS:'EPSG:3003'},
        projection: targetProjection
      })
    }),
  ]
})

var mappeDiBase_altro = new ol.layer.Group({
              'title': 'Supporto',
              'fold': 'close',
              layers: [
                  new ol.layer.Tile({
                    extent: targetExtent,
                    title: 'vecchi PdL',
                    visible: false,
                    source: new ol.source.TileArcGISRest({
                      url: 'https://oscar.comune.padova.it/server/rest/services/Vecchi_PDL/MapServer',
                      projection: targetProjection
                    })
                  }),
                  new ol.layer.Tile({
                    extent: targetExtent,
                    title: 'patrimonio',
                    visible: false,
                    source: new ol.source.TileArcGISRest({
                      url: 'https://oscar.comune.padova.it/server/rest/services/patrimonio/MapServer',
                      params: {
                        'LAYERS':"show:1,2",
                        'TRANSPARENT': 'true',
                        },
                      projection: targetProjection
                    })
                  }),
                  new ol.layer.Tile({
                    extent: targetExtent,
                    title: 'vie',
                    visible: false,
                    source: new ol.source.TileArcGISRest({
                      url: 'https://oscar.comune.padova.it/server/rest/services/base/MapServer',
                      params: {
                        'LAYERS':"show:0,1,2",
                        'TRANSPARENT': 'true',
                        },
                      projection: targetProjection
                    })
                  }),
                  new ol.layer.Vector({
                      title: 'PUA *',
                      visible: false,
                      source: new ol.source.Vector({
                        url: '/pua/geojson/',
                        format: new ol.format.GeoJSON(),
                        projection: targetProjection,
                      }),
                      style: function (feature, resolution) {
                                  return new ol.style.Style({
                                      stroke: new ol.style.Stroke({
                                          color: '#ff0000',
                                          width: 3
                                      }),
                                      fill: new ol.style.Fill({
                                          color: 'rgba(255, 255, 255, 0.3)'
                                      }),
                                      text: new ol.style.Text({
                                          textAlign: "center",
                                          textBaseline: "middle",
                                          font: "Bold 12px Arial",
                                          text: feature.get('id_pua')+"\n"+feature.get('ditta'),
                                          fill: new ol.style.Fill({
                                              color: "#ff0000"
                                          }),
                                          stroke: new ol.style.Stroke({
                                              color: "#ffffff",
                                              width: 3
                                          })
                                      })
                                  });
                              }
                  }),
                  new ol.layer.Vector({
                      title: 'RICHIESTE *',
                      visible: false,
                      source: new ol.source.Vector({
                        url: '/repertorio/geojson/',
                        format: new ol.format.GeoJSON(),
                        projection: targetProjection,
                      }),
                      style: function (feature, resolution) {
                                  return new ol.style.Style({
                                      stroke: new ol.style.Stroke({
                                          color: '#36a92e',
                                          width: 3
                                      }),
                                      fill: new ol.style.Fill({
                                          color: 'rgba(255, 255, 255, 0.3)'
                                      }),
                                      text: new ol.style.Text({
                                          textAlign: "center",
                                          textBaseline: "middle",
                                          font: "Bold 12px Arial",
                                          text: feature.get('idIstanza')+"/"+feature.get('catIstanza')+"\n"+feature.get('ditta'),
                                          fill: new ol.style.Fill({
                                              color: "#36a92e"
                                          }),
                                          stroke: new ol.style.Stroke({
                                              color: "#ffffff",
                                              width: 3
                                          })
                                      })
                                  });
                              }
                  }),
                  new ol.layer.Vector({
                      title: 'CERTIFICATI *',
                      visible: false,
                      source: new ol.source.Vector({
                        url: '/certificati/geojson/',
                        format: new ol.format.GeoJSON(),
                        projection: targetProjection,
                      }),
                      style: function (feature, resolution) {
                                  return new ol.style.Style({
                                      stroke: new ol.style.Stroke({
                                          color: '#2799e5',
                                          width: 3
                                      }),
                                      fill: new ol.style.Fill({
                                          color: 'rgba(255, 255, 255, 0.3)'
                                      }),
                                      text: new ol.style.Text({
                                          textAlign: "center",
                                          textBaseline: "middle",
                                          font: "Bold 12px Arial",
                                          text: feature.get('pk')+"\n"+feature.get('richiedente'),
                                          fill: new ol.style.Fill({
                                              color: "#2799e5"
                                          }),
                                          stroke: new ol.style.Stroke({
                                              color: "#ffffff",
                                              width: 3
                                          })
                                      })
                                  });
                              }
                  }),

              ]
          })