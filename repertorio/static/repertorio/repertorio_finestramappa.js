console.log('FINESTRAMAPPA LOADING')

var targetExtent = [1716014, 5023919, 1737137, 5038662];

var map_glob, getfeature_popup;
var polyObject, zoningObject //polyJSON, zoningJSON, 
var polyFeatures, polyOverlay, zoningFeatures, zoningOverlay;

var targetProjection = new ol.proj.Projection({
    code: 'EPSG:3003',
    // The extent is used to determine zoom level 0. Recommended values for a
    // projection's validity extent can be found at http://epsg.io/.
    extent: targetExtent,
    units: 'm'
});
ol.proj.addProjection(targetProjection);

var UTM32Extent = [719459.961,5024860.053, 733523.921,5038269.875];
proj4.defs('EPSG:32632', '+proj=utm +zone=32 +datum=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
var UTM32proj = new ol.proj.Projection({
    code: 'EPSG:32632',
    extent: UTM32Extent
});
ol.proj.addProjection(UTM32proj);

var ETRS89Extent = [719459.961,5024860.053, 733523.921,5038269.875];
proj4.defs('EPSG:25832', '+proj=utm +zone=32 +ellps=GRS80 +towgs84=128.8,17.85,0,0,0,0,0 +units=m +no_defs');
var ETRS89proj = new ol.proj.Projection({
    code: 'EPSG:25832',
    extent: ETRS89Extent
});
ol.proj.addProjection(ETRS89proj);

var geoJson_convertitore = new ol.format.GeoJSON();

var WKT_convertitore = new ol.format.WKT();

    /**
     * @param {number} n The max number of characters to keep.
     * @return {string} Truncated string.
     */
String.prototype.trunc = String.prototype.trunc ||
    function(n) {
    return this.length > n ? this.substr(0, n - 1) + '...' : this.substr(0);
    };

// var pj = document.getElementById("finestramappa").innerHTML

// loadFinestraMappa(polyJSON)

function hideGeom( ) {
    console.log("hideGeom è ORA")
    $( "h2:contains('posizione_mod')" ).parent().hide()
}

function loadFinestraMappa( polyJSON ) { //zoningJSON, WMSRif

    console.log(polyJSON)

    console.log("ONLOAD è ORA")
    $( "h2:contains('posizione_mod')" ).parent().hide()

    var polyFeatures = new ol.Collection()
    var multipolygon = new ol.format.WKT().readGeometry(polyJSON)
    
    var currentID = 99999

    console.log("loadFinestraMappa",polyJSON, multipolygon.getPolygons(), multipolygon.getPolygons().length)

    if (multipolygon.getPolygons().length > 1) {
        for (var i = 0; i < multipolygon.getPolygons().length; i++) {
            let newFeat = new ol.Feature({
                geometry: multipolygon.getPolygon(i),
                part: i,
                id: i,
            });
            polyFeatures.push(newFeat)
        }
        var currentID = i
    } 

    var multiID = currentID

    polyFeatures.push(new ol.Feature({
        geometry: multipolygon,
        part: currentID,
        id: currentID
    }))

    class toolbar extends ol.control.Control {

        constructor(opt_options) {
          const options = opt_options || {main:true};

          const toolbar = document.createElement('div');
          toolbar.className = 'bar';

          let strong = document.createElement('strong');
          strong.className = 'etic';
          strong.innerHTML = 'MULTI GEOMETRIA ';
          toolbar.appendChild(strong);
      
          const button1 = document.createElement('button');
          button1.className = 'btn';
          button1.innerHTML = '<';
          let span = document.createElement('span');
          span.className = 'tool';
          span.appendChild(button1);
          toolbar.appendChild(span);
      
          const button2 = document.createElement('button');
          button2.className = 'btn';
          button2.innerHTML = '>';
          span = document.createElement('span');
          span.className = '';
          span.appendChild(button2);

          toolbar.appendChild(span);
      
          const element = document.createElement('div');
          element.className = 'toolbar btn-toolbar ol-unselectable ol-control';
          element.appendChild(toolbar);
      
          super({
            element: element,
            target: options.target,
          });

          this.button = button1
          button1.addEventListener('click', this.handleclick.bind(this), false);
          this.button = button2
          button2.addEventListener('click', this.handleclick.bind(this), false);

          this.element = element
        }

        handleclick(evt) {
            console.log("handleclick",this)
            if (this.button.innerHTML == "<") {
                currentID -= 1
                if (currentID < 0) {
                    currentID = multiID
                }
            } else {
                currentID += 1
                if (currentID > multiID) {
                    currentID = 0
                }
            }
            polyOverlay.getSource().refresh()
            let extent = multipolygon.getExtent();
            if (currentID != multiID) {
                extent = multipolygon.getPolygon(currentID).getExtent();
            }
            map_glob.getView().fit(extent, map_glob.getSize());
            evt.preventDefault();
        }
      }

    var maxResolution = 10;

    function getText(feature, resolution) {
        var text;
        if (resolution > maxResolution) {
            text = '';
        } else {
            text = feature.get('id_pua').toString()+"\n"+feature.get('ditta').trunc(20);;
        }
        return text;
    }

    function polygonStyleFunction(feature, resolution) {
        console.log(feature.get('idIstanza'));
        console.log(resolution);
        if (feature.get('part') == currentID) {
            return new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#ff0000',
                    width: 3
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.3)'
                }),
            });
        }
    }

    var layerSet = [
        mappeDiBase_supporto,
        mappeDiBase_PNRR,
        mappeDiBase_PAT,
        mappeDiBase_PI,
        mappeDiBase_altro
    ]

    //particelle overlay
    if (polyJSON) {

        polyOverlay = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: polyFeatures
            }),
            title: 'perimetro relativo alla richiesta',
            style: polygonStyleFunction
        });
    
        console.log("polyOverlay",polyOverlay.getSource().getExtent());

        layerSet.push(polyOverlay)
    } 

    map_glob = new ol.Map({
        layers: layerSet,
        controls: ol.control.defaults().extend([
            //new ol.control.ScaleLine(),
            new ol.control.OverviewMap()
        ]),
        target: 'map',
        view: new ol.View({
            projection: targetProjection,
            center: ol.extent.getCenter(targetExtent),
            zoom: 2
        })
    });

    var layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Legenda' // Optional label for button
    });

    map_glob.getView().on('propertychange', function(e) {
        switch (e.key) {
            case 'resolution':
            console.log('resolution');
            console.log(e.oldValue);
            break;
        }
    });

    map_glob.addControl(layerSwitcher);

    if (currentID != 99999) {
        map_glob.addControl(new toolbar());
    }

    extent = polyOverlay.getSource().getExtent();
    map_glob.getView().fit(extent, map_glob.getSize());
    enable_layer_state_tracking(map_glob)
}