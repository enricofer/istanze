from django.conf.urls import url,include
import django.contrib.auth.views
from django.conf import settings
from django.conf.urls.static import static
from . import views

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

urlpatterns = [
    url(r'^$', views.istanze, name='istanze_list'),
    url(r'^cat/(?P<categoria>\d+)/', views.istanze, name='istanze_list_cat'),
    url(r'^(?P<categoria>\d+)/$', views.istanze),
    url(r'^(?P<categoria>\d+)/(?P<filtro>.*)/$', views.istanze, name='istanze_list_query'),
    url(r'^map/$', views.mapIstanze, name='istanze_map'),
    url(r'^map/(?P<categoria>\d+)/$', views.mapIstanze, name='istanze_map_cat'),
    url(r'^map/(?P<categoria>\d+)/(?P<filtro>.*)/$', views.mapIstanze, name='istanze_map_query'),
    url(r'^dettaglio/(\d+)/$', views.dettaglio_istanza, name='istanza'),
    url(r'^sv/(\d+)/$', views.streetview_link, name='istanza_streetview_link'),
    url(r'^dettaglio/(?P<n>\d+)/(?P<categoria>\d+)/$', views.dettaglio_istanza, name='istanza_filtrata1'),
    url(r'^dettaglio/(?P<n>\d+)/(?P<categoria>\d+)/(?P<filtro>.*)/$', views.dettaglio_istanza, name='istanza_filtrata'),
    url(r'^sportello/$', views.sportello, name='sportello'),
    url(r'^segnalazione/$', views.segnalazione, name='segnalazione'),
    url(r'^istanza/', include([
        url(r'^nuova/$', views.nuova_istanza, name='nuova_istanza'),
        ])),
    url(r'^com/', include([
        url(r'^$', views.lista_comunicazioni, name="comunicazioni"),
        url(r'^dettaglio/(\d+)/$', views.dettaglio_comunicazione, name='comunicazione'),
        ])),

    url(r'^odt/(\d+)/(\d+)/$', views.genera_odt,name='genera_odt'),
    url(r'^proxyimg/(\d+\.\d+)/(\d+\.\d+)/(\d+\.\d+)/(\d+\.\d+)/(\d+)/wms.png$', views.proxy_img,name='proxy_img'),

    url(r'^sub/dettaglio/(\d+)/', views.dettaglio_sottoscrizione, name='sottoscrizione'),
    url(r'^istruttoria/dettaglio/(\d+)/', views.dettaglio_istruttoria, name='istruttoria'),
    url(r'^loaddb/$', views.load_db_richieste, name='loaddb'),
    url(r'^geojson/$', views.as_geojson, name='as_geojson'),
    url(r'^aggiornamento/$', views.uploadBandi, name='aggiornamento'),
    #url(r'^catasto/$', views.cerca_catasto, name='cerca_catasto'),
    #url(r'^rebuild/$', views.rebuild, name='rebuild'),
    url(r'^list$', views.istanze_list.as_view(), name='list'),
    url(r'^login/', django.contrib.auth.views.LoginView),
    url(r'^logout/', django.contrib.auth.views.LogoutView),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
