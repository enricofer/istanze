from django.contrib.gis import forms
from .models import corrispondenza,sottoscrittori,repertorioIstanze,catIstanze,settori,istruttorie,catIstruttoria


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

class MyBaseWidget(forms.OpenLayersWidget):
    map_width = 1000
    map_height = 700
    map_srid = 3857


class mapForm(forms.Form):
    geom = forms.MultiPolygonField(widget=MyBaseWidget)
    
class searchForm(forms.Form):
    categoria = forms.ModelChoiceField(queryset=catIstanze.objects.all())
    chiave = forms.CharField(max_length=100)


class searchCatasto(forms.Form):
    coordinateCatastali = forms.CharField(max_length=200)


class uploadForm(forms.Form):
    file_csv = forms.FileField(allow_empty_file=False)
