from grappelli.dashboard.apps import DashboardConfig

class PatchedGrappelliDashboard(DashboardConfig):
    label = "grappelli_dashboard"