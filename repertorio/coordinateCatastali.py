import sys
from django.db import connection
from django.contrib.gis.geos import GEOSGeometry
from django.core.serializers import serialize

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

estrazioneTemplate = '''
SELECT
	MIN(estrazione.nome) as nome,
	SUM(ST_Area(estrazione.the_geom)) AS super,
	row_number() OVER () AS ogc_fid,
	ST_Union(estrazione.the_geom) AS the_geom

FROM (
	SELECT "prg_blob"."ogc_fid",
	       "prg_blob"."gid",
	       "prg_blob"."layer",
	       "prg_blob"."interno_pe",
	       "prg_blob"."note",
	       "prg_blob"."super",
	       "prg_blob"."nome",
	       "prg_blob"."gruppo",
	       "prg_blob"."url",
	       "prg_blob"."articolo",
	       ST_Intersection("prg_blob".the_geom,ST_GeomFromText('%s',3003)) AS the_geom
	FROM "zone_vigente"."prg_blob"
	WHERE NOT ST_Disjoint("prg_blob".the_geom,ST_GeomFromText('%s',3003))
) AS estrazione

GROUP BY estrazione.nome
ORDER BY estrazione.nome
'''

destinazioniTemplate = '''
SELECT
	string_agg(estrazione.nome, ', ')

FROM (

SELECT
	       SUM (St_Area(ST_Intersection("prg_blob".the_geom,ST_GeomFromText('%s',3003))))AS area,
	       "prg_blob"."nome",
	       ST_Union(ST_Intersection("prg_blob".the_geom,ST_GeomFromText('%s',3003))) AS the_geom
	FROM "zone_vigente"."prg_blob"
	WHERE NOT ST_Disjoint("prg_blob".the_geom,ST_GeomFromText('%s',3003))
GROUP BY nome
ORDER BY area DESC

) AS estrazione

WHERE estrazione.nome <> 'Area comunale'
'''

def QueryFromCoordinateCatastali(coordinateCatastali):
    annorif = coordinateCatastali.split(":")
    if len(annorif) > 1:
        annocat = "_"+annorif[0]
        coordinateCatastali = annorif[1]
    else:
        annocat = ""
    sqlWhere = ""
    fogliMappali = coordinateCatastali.split(" ")
    for foglioMappali in fogliMappali:
        listaFoglio = foglioMappali.split("/")
        foglio = listaFoglio[0]
        if foglio[-1:].isalpha():
            foglio = "000"[:3-len(foglio[:-1])]+foglio[:-1]+foglio[-1:].upper()
        else:
            foglio = "000"[:3-len(foglio)]+foglio+"0"
        listaMappali = listaFoglio[1]
        mappali = listaMappali.split("-")
        for mappale in mappali:
            sqlWhere += "(foglio = '%s' AND mappale ='%s') OR " % (foglio,mappale)
        #sqlWhere += " OR "
    sqlWhere = "("+sqlWhere[:-4]+")"
    psql=   'SELECT row_number() OVER () AS ogc_fid, '+\
            'ST_SetSRID(ST_Multi(ST_Union(catasto.particelle_cat%s.the_geom)),3003) AS the_geom, ' % (annocat) +\
            'ST_AsText(ST_SetSRID(ST_Multi(ST_Union(catasto.particelle_cat%s.the_geom)),3003)) AS wkt ' % (annocat) +\
            'FROM catasto.particelle_cat%s WHERE %s' % (annocat,sqlWhere)

    print (psql, file=sys.stderr)
    return psql

def GeomFromCoordinateCatastali(coordinateCatastali, formato='WKT', srid_origine = None, srid_destinazione = None):
    query = QueryFromCoordinateCatastali(coordinateCatastali)
    if query:
        cursor = connection.cursor()
        cursor.execute(query)
        row = cursor.fetchone()
        geom = GEOSGeometry( row[1] )
        if srid_destinazione:
            geom.transform(srid_destinazione)
        if formato == 'WKT':
            return geom.ewkt
        elif formato == 'geojson':
            return geom.geojson
        elif formato == 'KML':
            return geom.kml
        elif formato == 'ogr':
            return geom.ogr
    return None


def queryPRG(featureWKT):
    #clear dock widget
    resultArray = []
    cursor = connection.cursor()

    cursor.execute('SELECT nome FROM "zone_vigente"."prg_blob_zoning_zone"')
    zoning = []
    for row in cursor.fetchall():
        zoning.append(row[0])

    queryDef = estrazioneTemplate % (featureWKT,featureWKT)
    #print (queryDef, file=sys.stderr)
    cursor.execute(queryDef)
    #row = cursor.fetchone()

    #self.dockwidget.tableWidget.setRowCount(query.size())
    areaPianificata = 0
    for row in cursor.fetchall():
        if row[0] == 'Area comunale':
            areaComunale = row[1]/2 #nb la query ritorna doppie entità (a causa di una duplicazione delle entità), quindi le aree sono da dimezzare
        elif row[0] in zoning:
            areaPianificata += row[1]/2 #nb la query ritorna doppie entità (a causa di una duplicazione delle entità), quindi le aree sono da dimezzare

    cursor.execute(queryDef)
    table = []
    for row in cursor.fetchall():
        if row[0] == 'Area comunale':
            dest = 'Totale'
        else:
            dest = row[0]
        table.append([dest,round(row[1]/2,2),round(row[1]/2/areaComunale*100,2)]) #nb la query ritorna doppie entità (a causa di una duplicazione delle entità), quindi le aree sono da dimezzare

    table.append(["Area non pianificata",round(areaComunale-areaPianificata,2),round((areaComunale-areaPianificata)/areaComunale*100,2)])
    #print (table, file=sys.stderr)
    return table

def destinazioniPRG(featureWKT):
    cursor = connection.cursor()
    queryDef = destinazioniTemplate % (featureWKT,featureWKT,featureWKT)
    #print (queryDef, file=sys.stderr)
    cursor.execute(queryDef)
    row = cursor.fetchone()
    return row[0]
