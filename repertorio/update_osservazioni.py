from repertorio.models import corrispondenza,sottoscrittori,repertorioIstanze,catIstanze,settori,istruttorie,catIstruttoria,modelli
from django.conf import settings

import os
from datetime import datetime

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

var_500 = catIstanze.objects.get(pk=2)
oss_var_500 = repertorioIstanze.objects.filter(categoriaIstanza=var_500)


for oss in oss_var_500:
    if oss.idIstanza in (26,57):
        new_oss = corrispondenza()
        new_oss.recipiente = 'e'
        new_oss.mittente = oss.ditta
        new_oss.protComunicazione = '20169999999'
        new_oss.dataComunicazione = datetime(2016, 5, 9) #datetime.date.fromisoformat('2016-05-09')
        new_oss.note = "osservazione alla variante piano degli interventi n 10/2016 caricata automaticamente"
        new_oss.istanza = oss
        new_oss.documento = os.path.join('corrispondenza', 'osservazioni', "12_%s.pdf" % str(oss.idIstanza).zfill(3))
        new_oss.save()

    new_oss = corrispondenza()
    new_oss.recipiente = 'u'
    new_oss.mittente = "CONTRODEDUZIONE"
    new_oss.protComunicazione = '20169999999'
    new_oss.dataComunicazione = datetime(2016, 5, 9)
    new_oss.note = "controdeduzione all'osservazione alla variante piano degli interventi n 10/2016 caricata automaticamente"
    new_oss.istanza = oss
    new_oss.documento = os.path.join('corrispondenza', 'controdeduzioni', "output_%s.pdf" % str(oss.idIstanza).zfill(3))
    new_oss.save()