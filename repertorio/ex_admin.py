from django.contrib import admin
from django import forms
from django.contrib.gis import admin
from django.db import connection
from django.contrib.gis.geos import GEOSGeometry
from django.utils.html import format_html
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from datetime import datetime
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from ajax_select.fields import AutoCompleteSelectField, AutoCompleteSelectMultipleField #testing
from ajax_select.admin import AjaxSelectAdmin #testing

from selectable.forms import AutoCompleteSelectWidget,AutoComboboxSelectWidget,AutoCompleteSelectField, AutoCompleteSelectMultipleWidget

from .models import corrispondenza,sottoscrittori,repertorioIstanze,catIstanze,settori,istruttorie,catIstruttoria
from .lookups import settoreLookup, tipoIstanzaLookup
import logging
import sys

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


from .coordinateCatastali import QueryFromCoordinateCatastali, destinazioniPRG

logger = logging.getLogger(__name__)

class corrispondenzaForm(forms.ModelForm):
    #mittente = AutoComboboxSelectWidget (lookup_class=settoreLookup, allow_new=True)


    class Meta(object):
        model = corrispondenza
        #widgets = {'mittente': AutoComboboxSelectWidget(lookup_class=settoreLookup, allow_new=True),}
        exclude = ('', )

    '''
    def __init__(self, *args, **kwargs):
        super(corrispondenzaForm, self).__init__(*args, **kwargs)
        eventuale controllo congruenza
        if self.instance and self.instance.pk and self.instance.owner:
            self.initial['owner'] = self.instance.owner.pk
    '''

    def save(self, *args, **kwargs):
        print (self.cleaned_data['protComunicazione'], file=sys.stderr)
        if len(self.cleaned_data['protComunicazione'])==7:
            self.instance.protComunicazione = str(self.cleaned_data['dataComunicazione'].year)+self.cleaned_data['protComunicazione']
        print (self.instance.protComunicazione, file=sys.stderr)
        '''
        Qua sotto serve a salvare l'eventuale lookup del mittente
        mittente = self.cleaned_data['mittente'].clean()
        print (self.cleaned_data, file=sys.stderr)
        mittente = self.data['mittente_0']
        print (mittente, file=sys.stderr)
        #eventuale controllo if tipoIstanza:
        self.instance.mittente = mittente
        '''
        return super(corrispondenzaForm, self).save(*args, **kwargs)



class corrispondenzaAdmin(admin.ModelAdmin):
    form = corrispondenzaForm
    search_fields = ['protComunicazione','mittente']
    ordering = ['-id']


class sottoscrittoriInlineFormset(forms.models.BaseInlineFormSet):


    def applicaModifiche(self,obj):
        #setattr(obj, self.cognome_field, self.cognome_field.upper())
        #setattr(obj, self.nome_field, self.nome_field.title())
        obj.cognomeDenominazione = obj.cognomeDenominazione.upper()
        obj.nome = obj.nome.title()

    def save_new(self, form, commit=True):
        obj = super(sottoscrittoriInlineFormset, self).save_new(form, commit=False)
        self.applicaModifiche(obj)
        if commit:
            obj.save()
        return obj

    def save_existing(self, form, instance, commit=True):
        obj = super(sottoscrittoriInlineFormset, self).save_existing(form, instance, commit=False)
        self.applicaModifiche(obj)
        if commit:
            obj.save()
        return obj


class sottoscrittoriFormsetMixin(object):
    formset = sottoscrittoriInlineFormset
    #cognome_field = "cognomeDenominazione"
    #nome_field = "nome"

    def get_formset(self, request, obj=None, **kwargs):
        formset = super(sottoscrittoriFormsetMixin, self).get_formset(request, obj, **kwargs)
        formset.request = request
        formset.can_delete
        formset.can_order
        #formset.cognome_field = self.cognome_field
        #formset.nome_field = self.nome_field
        return formset


class sottoscrittoriInline(sottoscrittoriFormsetMixin,admin.StackedInline):
    model = sottoscrittori
    extra = 0
    #classes = ('collapse open',)
    #inline_classes = ('collapse open',)


class istruttorieInlineFormset(forms.models.BaseInlineFormSet):
    def save_new(self, form, commit=True):
        obj = super(istruttorieInlineFormset, self).save_new(form, commit=False)
        setattr(obj, self.user_field, self.request.user.username)
        setattr(obj, self.date_field, datetime.now())
        if commit:
            obj.save()
        return obj

    def save_existing(self, form, instance, commit=True):
        obj = super(istruttorieInlineFormset, self).save_existing(form, instance, commit=False)
        setattr(obj, self.user_field, self.request.user.username)
        setattr(obj, self.date_field, datetime.now())
        if commit:
            obj.save()
        return obj


class istruttorieFormsetMixin(object):
    formset = istruttorieInlineFormset
    user_field = "istruttore"
    date_field = "data"

    def get_formset(self, request, obj=None, **kwargs):
        formset = super(istruttorieFormsetMixin, self).get_formset(request, obj, **kwargs)
        formset.request = request
        formset.user_field = self.user_field
        formset.date_field = self.date_field
        return formset


class istruttorieInline(istruttorieFormsetMixin,admin.StackedInline):

    model = istruttorie
    extra = 0

class istanzeForm(forms.ModelForm):

    #comunicazioni = AutoCompleteSelectMultipleField('corrispondenza', required=True, help_text=None),

    def __init__(self, *args, **kwargs):
        super(istanzeForm, self).__init__(*args, **kwargs)
        self.backupCatasto = self.instance.coordCatastali
        #print (self.backupCatasto, file=sys.stderr)

    class Meta(object):
        model = repertorioIstanze
        exclude = ('', )

    def clean(self):
        '''
        metodo per fare elaborazioni prima della validazione
        '''
        pass

    def save(self, force_insert=False, force_update=False, commit=True):
        print ("SAVE", file=sys.stderr)

        #procedura per compilare il campo per il reverse link delle comunicazioni correlate all'istanza (DUPLICATA)
        for comunicazione in self.cleaned_data['comunicazioni']:
            print ("COM",comunicazione, file=sys.stderr)
            comunicazione.istanza = self.instance
            comunicazione.istanza.save()

        #
        m = super(istanzeForm, self).save(commit=False)
        if self.cleaned_data['geom']:
            #se geometria cambiata aggiorna destVigente e Istanze Correlate
            print ("GEOM:",self.cleaned_data['geom'].wkt, file=sys.stderr)
            #nuove destinazioni di PRG che intersecano la geomtria di istanza
            self.instance.destVigente = destinazioniPRG(self.cleaned_data['geom'].wkt)[:250]
            #altre istanze correlate che si intersecano fra loro
            correlate = repertorioIstanze.objects.filter(geom__intersects=self.cleaned_data['geom'])
            self.instance.istanzeCorrelate.clear()
            for correlata in correlate:
                if correlata != self.instance:
                    self.instance.istanzeCorrelate.add(correlata)
            print (self.instance,self.instance.istanzeCorrelate.all(), file=sys.stderr)

        if self.cleaned_data['destVigente']:
            self.instance.destVigente = self.cleaned_data['destVigente']

        if self.cleaned_data['coordCatastali'] != self.backupCatasto:
            query = QueryFromCoordinateCatastali(self.cleaned_data['coordCatastali'])
            cursor = connection.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            self.instance.geom = GEOSGeometry(row[1],3003)
            print (self.instance.destVigente, file=sys.stderr)


        if commit:
            m.save()

        #procedura per compilare il campo per il reverse link delle comunicazioni correlate all'istanza (DUPLICATA)
        if self.instance.comunicazioni.all():
            for comunicazione in self.instance.comunicazioni.all():
                print ("COM",comunicazione, file=sys.stderr)
                comunicazione.istanza = self.instance
                comunicazione.istanza.save()

        return m

def istanze_accolte(modeladmin, request, queryset):
    queryset.update(stato='si')
istanze_accolte.short_description = "Filtra solo istanze accolte"

@admin.register(repertorioIstanze)
class padovaGeoAdmin(admin.OSMGeoAdmin,AjaxSelectAdmin):
    default_lon = 1725155
    default_lat = 5032083
    default_zoom = 13
    max_zoom = 20
    min_zoom = 10
    map_width = 700
    map_height = 500
    map_srid = 3003
    #wms_url = "http://localhost/cgi-bin/DBT/qgis_mapserv.fcgi?MAP=DBT.qgs"
    wms_url = "http://172.25.193.167:8080/service?"
    wms_layer = 'DBT'
    wms_name = 'DBT'
    wms_options = {'format': 'image/png'}
    #map_template = '/home/urb/Dropbox/dev/istanze/repertorio/templates/admin/repertorio/openlayers_extralayers.html'
    map_template = 'admin/repertorio/openlayers_extralayers.html'
    fieldsets = (
        ("intestazione", {
            'classes': ('grp-collapse grp-open',),
            'fields': ('idIstanza', 'categoriaIstanza')#,'ditta',
        }),
        ("corrispondenza", {
            'classes': ('grp-collapse',),
            'fields': ('comunicazioni',)
        }),
        ("inlines", {
            'classes': ("grp-collapse grp-open placeholder sottoscrittori_set-group",),
            'fields': ()
        }),
        ("posizione", {
            'classes': ('grp-collapse grp-open',),
            'fields': ('coordCatastali', 'geom')
        }),
        ('Altri campi', {
            'classes': ('grp-collapse grp-open',),
            'fields': (
                'rif',
                'destVigente',
                'destRichiesta',
                'note',
                'accordo',
                'istanzeCorrelate',
                'stato',
                'valutazione',
                'destAssegnata',
                'variazioneCartografica',)
        }),
    )
    form = istanzeForm
    filter_horizontal = ('istanzeCorrelate',) #('comunicazioni','istanzeCorrelate',)
    inlines = [sottoscrittoriInline,istruttorieInline]
    search_fields = ['idIstanza','ditta','note'] #,'comunicazioni__mittente'
    list_display = ('pk', 'categoriaIstanza', 'idIstanza','ditta','stato')
    list_filter = ('categoriaIstanza','stato')
    ordering = ['-id']
    actions = ['istanze_accolte','istanze_in_sospeso','istanze_non_valutate']
    class Media:
        css = {
             'all': ('repertorio/css/my_admin.css',)
        }

    def istanze_accolte(modeladmin, request, queryset):
        print ("ciao nina",request, file=sys.stderr)
        HttpResponseRedirect("/admin/repertorio/repertorioistanze/?stato__startswith=s")
    istanze_accolte.short_description = "Filtra solo istanze accolte e parzialmente accolte"

    def istanze_in_sospeso(modeladmin, request, queryset):
        HttpResponseRedirect("/admin/repertorio/repertorioistanze/?stato__endswith=%3F")
    istanze_in_sospeso.short_description = "Filtra solo istanze in sospeso"

    def istanze_non_valutate(modeladmin, request, queryset):
        HttpResponseRedirect("/admin/repertorio/repertorioistanze/?stato__exact=nv")
    istanze_non_valutate.short_description = "Filtra solo istanze non valutate"

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('stato','valutazione','istanzeCorrelate','destAssegnata','variazioneCartografica', 'accordo')
        return self.readonly_fields

    def save_model(self, request, obj, form, change):
        '''
        metodo per fare controlli ed elaborazioni prima del salvataggio
        '''
        pass

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        if instances:
            print ("ISTANCES:", file=sys.stderr)

            for instance in instances:
                print (instance.__dict__, file=sys.stderr)
                if 'cognomeDenominazione' in instance.__dict__:
                    instance.save()
                    instance.istanza.sottoscrizioni.add(instance)

                    sottoscrizioniCorrenti = instance.istanza.sottoscrizioni

                    if sottoscrizioniCorrenti.count() > 1:
                        #nuovaDitta += ' e altri (%s)' % str(sottoscrizioniCorrenti.count()-1)
                        sottoscrittori_dict = {}
                        sottoscrittori_list = []
                        print (sottoscrizioniCorrenti, file=sys.stderr)
                        for s in sottoscrizioniCorrenti.all():
                            sottoscrittori_list.append([s.cognomeDenominazione,s.nome])
                        #print (sottoscrittori_list, file=sys.stderr)

                        for s in sottoscrittori_list:
                            if s[0] in sottoscrittori_dict:
                                sottoscrittori_dict[s[0]].append(s[1])
                            else:
                                sottoscrittori_dict[s[0]]= [s[1]]

                        cognomi = sorted(sottoscrittori_dict.keys())

                        ditta_complessa = ""
                        for cogn in cognomi:
                          ditta_complessa += cogn + " "
                          for nome in sottoscrittori_dict[cogn]:
                            ditta_complessa += nome + ","
                          ditta_complessa = ditta_complessa[:-1] + " "
                        #print ("OKK",ditta_complessa[:100], file=sys.stderr)

                        instance.istanza.ditta = ditta_complessa[:100]
                    else:
                        nuovaDitta = sottoscrizioniCorrenti.values('cognomeDenominazione')[0]['cognomeDenominazione'] + " " + sottoscrizioniCorrenti.values('nome')[0]['nome']
                        instance.istanza.ditta = nuovaDitta[:100]

                    #print ("NUOVA DITTA",instance.istanza.ditta, file=sys.stderr)

                    instance.istanza.save()

                if 'catIstruttoria_id' in instance.__dict__:
                    #è una formset di istruttorie
                    print ("ISTRUTTORIA", instance.id, instance.__dict__, file=sys.stderr)
                    if instance.id:
                        changed = []
                        dbInstance = istruttorie.objects.get(pk=instance.id)
                        for field in instance.__dict__:

                            try:
                                print (field,getattr(instance, field),getattr(dbInstance, field), file=sys.stderr)
                                if getattr(instance, field) != getattr(dbInstance, field):
                                    changed.append (field)
                            except:
                                pass
                        print ("CAMBIATI",changed, file=sys.stderr)
                    else:
                        changed = ['accoglimento','destAssegnata','dettaglio','variazioneCartografica',]

                    if 'accoglimento' in changed:
                        instance.istanza.stato = instance.accoglimento
                        if not instance.accoglimento in ('si','sp','s?'):
                            instance.istanza.destAssegnata = ''
                            instance.istanza.variazioneCartografica = None
                        else:
                            instance.istanza.destAssegnata = instance.destAssegnata
                        instance.istanza.save()
                    if 'destAssegnata' in changed:
                        instance.istanza.destAssegnata = instance.destAssegnata
                        instance.istanza.save()
                    if 'dettaglio' in changed:
                        instance.istanza.valutazione = instance.dettaglio
                        instance.istanza.save()
                    if 'variazioneCartografica' in changed:
                        instance.istanza.variazioneCartografica = instance.variazioneCartografica
                        instance.istanza.save()


                instance.save()


        formset.save_m2m()


class sottoscrittoriForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(sottoscrittoriForm, self).__init__(*args, **kwargs)

    class Meta(object):
        model = sottoscrittori
        exclude = ('', )

    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(sottoscrittoriForm, self).save(commit=False)
        self.instance.cognomeDenominazione = self.cleaned_data['cognomeDenominazione'].upper()
        self.instance.nome = self.cleaned_data['nome'].title()
        if commit:
            m.save()
        return m

class sottoscrittoriAdmin(admin.ModelAdmin):
    #inlines = [sottoscrittoriInline,]
    form = sottoscrittoriForm


class istruttorieForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(istruttorieForm, self).__init__(*args, **kwargs)

    class Meta(object):
        model = istruttorie
        exclude = ('', )

    def save(self, *args, **kwargs):
        print (kwargs, file=sys.stderr)
        self.instance.data = datetime.now()
        self.instance.istruttore = self.request.user.username
        return super(istruttorieForm, self).save(*args, **kwargs)

class istruttorieAdmin(admin.ModelAdmin):
    form = istruttorieForm

    def get_form(self, request, *args, **kwargs):
        form = super(istruttorieAdmin, self).get_form(request, *args, **kwargs)
        form.request = request
        return form

admin.site.unregister(User)
#admin.site.unregister(Group)
admin.site.register(User, UserAdmin)
#admin.site.register(Group, GroupAdmin)
#admin.site.register(repertorioIstanze,padovaGeoAdmin,)
admin.site.register(corrispondenza,corrispondenzaAdmin)
admin.site.register(sottoscrittori,sottoscrittoriAdmin)
admin.site.register(catIstanze)
admin.site.register(settori)
admin.site.register(istruttorie, istruttorieAdmin)
admin.site.register(catIstruttoria)
