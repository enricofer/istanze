from django.shortcuts import render,get_object_or_404,redirect
from django.template.loader import render_to_string
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import requires_csrf_token,csrf_exempt
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseServerError
from django.template import RequestContext
from django.conf import settings
import django_tables2 as tables
from django_tables2   import RequestConfig, SingleTableView
from django.db.models import Q,ManyToManyField
from django.db import connection
from django.contrib.gis.forms import modelform_factory
from django_tables2.utils import A
from .models import corrispondenza,sottoscrittori,repertorioIstanze,catIstanze,settori,istruttorie,catIstruttoria,modelli
from .forms import mapForm,searchForm,searchCatasto, uploadForm
from .tables import table_factory, ComunicazioniTable
from secretary import Renderer
import django_filters
from urllib.parse import urlparse

from django.contrib.gis.geos import GEOSGeometry, MultiPolygon, Polygon
from django.core.serializers import serialize
from django.http import HttpResponseRedirect
#from .coordinateCatastali import GeomFromCoordinateCatastali, queryPRG, destinazioniPRG
from .cartawebImportExport import cartawebExport
from certificati.generaCertificato import decodificaCatasto
from certificati.coordinateCatastali import SV, GeomFromCoordinateCatastali, queryPRG, destinazioniPRG

#from .admin import prendi_primo_id_utile

#import repertorio.tables as mytables
import urllib
import sys
import datetime
import json
import os
import tempfile
import requests
import csv
from io import StringIO


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


def prendi_primo_id_utile(cat):
    istanze_x_categoria = repertorioIstanze.objects.filter(categoriaIstanza=cat).order_by('idIstanza')
    if istanze_x_categoria:
        primo_seq_libero = None
        for seq,istanza in enumerate (istanze_x_categoria):
            if istanza.idIstanza != seq + 1:
                primo_seq_libero = seq + 1
                break
        free_id = primo_seq_libero or istanze_x_categoria.latest('idIstanza').idIstanza  + 1
        return free_id
    else:
        return  1

def JsonFromQueryset(features):
    features_geojson = {
        "type": "FeatureCollection",
        "crs": {"type": "name", "properties": {"name": "EPSG:3003"}},
        "features": []
    }
    for feature in features:
        if feature.geom:
            geometry = json.loads(feature.geom.json)
            properties = {
                "pk": feature.pk,
                "idIstanza": feature.idIstanza,
                "catIstanza": str(feature.categoriaIstanza),
                "ditta": feature.ditta,
            }
            feature = {
                "type": "Feature",
                "geometry": geometry,
                "properties": properties,
            }
            features_geojson["features"].append(feature)
    return features_geojson

def as_geojson(request):
    json = JsonFromQueryset(repertorioIstanze.objects.all())
    return JsonResponse(json)

def get_next_or_prev(models, item, direction):
    '''
    Returns the next or previous item of
    a query-set for 'item'.

    'models' is a query-set containing all
    items of which 'item' is a part of.

    direction is 'next' or 'prev'
    '''
    getit = False
    if direction == 'prev':
        models = models.reverse()
    for m in models:
        if getit:
            return m
        if item == m:
            getit = True
    if getit:
        # This would happen when the last
        # item made getit True
        return models[0]
    return False

def check_filtro(queryset,filtro):
    try:
        id = int(filtro)
        queryset = queryset.filter(Q(idIstanza__exact = filtro))
    except:
        if filtro.lower() in ("si","sp","no","nv","np","?","re","ok"):
            queryset = queryset.filter(Q(stato__iexact = filtro))
        else:
            queryset = queryset.filter(Q(valutazione__icontains = filtro) | Q(ditta__icontains = filtro) | Q(note__icontains = filtro) | Q(riassunto__icontains = filtro) | Q(riferimento__icontains = filtro))
    return queryset


@login_required
def istanze(request,categoria = "0", filtro = ""):
    if request.method == 'POST':
        if request.POST.get('categoria', ''):
            categoria = request.POST.get('categoria', '')
        if request.POST.get('filtro', ''):
            filtro = request.POST.get('filtro', '')
    if categoria == "0":
        currentQueryset = repertorioIstanze.objects.all()
    else:
        currentQueryset = repertorioIstanze.objects.filter(categoriaIstanza = int(categoria))

    if filtro:
        currentQueryset = check_filtro(currentQueryset,filtro)

    table = table_factory(repertorioIstanze, categoria,filtro)(currentQueryset)
    RequestConfig(request).configure(table)
    return render(request, "istanze.html", {"istanze": table,"categorie":catIstanze.objects.all(),"categoria":int(categoria),"filtro":filtro, "searchForm":searchForm})

@login_required
def mapIstanze(request,categoria = "0", filtro = "", qs=None):
    if request.method == 'POST':
        if request.POST.get('categoria', ''):
            categoria = request.POST.get('categoria', '')
        if request.POST.get('filtro', ''):
            filtro = request.POST.get('filtro', '')
    if qs:
        currentQueryset = qs
    else:
        if categoria == "0":
            currentQueryset = repertorioIstanze.objects.all()
        else:
            currentQueryset = repertorioIstanze.objects.filter(categoriaIstanza = int(categoria))

        if filtro:
            currentQueryset = check_filtro(currentQueryset,filtro)

    features_json = JsonFromQueryset(currentQueryset)
    return render(request, "mappaIstanze.html", {"features": features_json,"categorie":catIstanze.objects.all(),"categoria":int(categoria),"filtro":filtro})

@login_required
def dettaglio_istanza(request,n,categoria = "0", filtro = ""):
    if request.method == 'POST':
        if request.POST.get('categoria', ''):
            categoria = request.POST.get('categoria', '')
        if request.POST.get('filtro', ''):
            filtro = request.POST.get('filtro', '')
    if filtro == '###':
        filtro = ''
    if categoria == "0":
        currentQueryset = repertorioIstanze.objects.all()
    else:
        currentQueryset = repertorioIstanze.objects.filter(categoriaIstanza = int(categoria))

    if filtro:
        currentQueryset = check_filtro(currentQueryset,filtro)

    nav={}
    nav["primo"]=currentQueryset.first().pk
    nav["ultimo"]=currentQueryset.last().pk
    try:
        nav["precedente"] = get_next_or_prev(currentQueryset,currentQueryset.get(pk=n),"prev").pk
    except:
        nav["precedente"] = nav["primo"]
    try:
        nav["successivo"] = get_next_or_prev(currentQueryset,currentQueryset.get(pk=n),"next").pk
    except:
        nav["successivo"] = nav["ultimo"]

    #tgeom = currentQueryset.get(pk=n)
    #geom3857 = currentQueryset.get(pk=n).geom.transform(3857,clone=True)
    #feature = geom3857.wkt
    geometry = currentQueryset.get(pk=n).geom
    if geometry:
        feature = geometry.wkt
        if feature:
            table = queryPRG(feature)
        else:
            table = []
        print (table, file=sys.stderr)
    else:
        feature = None
        table = None
    #tgeom.srid(3003)
    #geomForm = mapForm(initial={'geom': tgeom.transform(3857,clone=True) })
    #geomForm = mapForm(initial={'geom': currentQueryset.get(pk=n).geom })

    istanza = get_object_or_404(repertorioIstanze, pk=n)
    if istanza.categoriaIstanza.tipoIstanza == 'Osservazioni alla variante del Piano degli Interventi ai sensi L.R. 11/2004':
        schede_dir = os.path.join(settings.MEDIA_ROOT,'corrispondenza','controdeduzioni')
        controdeduzioni = []
        for root, dirs, files in os.walk(schede_dir):
            for file in files:
                print ("FILE:",file, file=sys.stderr)
                codiceIstanza = ('000')[:3-len(str(istanza.idIstanza))]+str(istanza.idIstanza)+str(istanza.sub_page)
                if file.startswith("output_"+codiceIstanza):
                    controdeduzioni.append(file)
        schede_dir = os.path.join(settings.MEDIA_ROOT,'corrispondenza','osservazioni')
        osservazioni = []
        for root, dirs, files in os.walk(schede_dir):
            for file in files:
                codiceIstanza = ('000')[:3-len(str(istanza.idIstanza))]+str(istanza.idIstanza)
                if file.startswith("12_"+codiceIstanza):
                    osservazioni.append(file)
        print ("osservazioni:",osservazioni, file=sys.stderr)
    else:
        controdeduzioni = None
        osservazioni = None
    istruttorie_relative = istruttorie.objects.filter(istanza_id=istanza.pk)
    return render(request, 'dettaglioIstanza.html', {   'istanza': istanza,
                                                        'controdeduzioni': controdeduzioni,
                                                        'osservazioni': osservazioni,
                                                        'istruttorie': istruttorie_relative,
                                                        'nav': nav,
                                                        'feature': feature,
                                                        'categoria':categoria,
                                                        'filtro':filtro,
                                                        'table':table})

@login_required
def lista_comunicazioni(request,filtro = ""):
    if request.method == 'POST':
        filtro = request.POST.get('filtro', '')
    print ("LISTA COMUNICAZIONI filtro:",filtro, file=sys.stderr)
    if filtro != "":
        currentQueryset = corrispondenza.objects.filter(Q(mittente__icontains = filtro) | Q(note__icontains = filtro))
    else:
        currentQueryset = corrispondenza.objects.all()

    table = ComunicazioniTable(currentQueryset)
    RequestConfig(request).configure(table)
    return render(request, "comunicazioni.html", {"comunicazioni": table,"filtro":filtro})

@login_required
def dettaglio_comunicazione(request,n):
    currentQueryset = repertorioIstanze.objects.all()
    currentItem = currentQueryset.get(pk=n)

    nav={}
    nav["primo"]=currentQueryset.first().pk
    nav["ultimo"]=currentQueryset.last().pk
    try:
        nav["precedente"] = get_next_or_prev(currentQueryset,currentItem,"prev").pk
    except:
        nav["precedente"] = nav["primo"]
    try:
        nav["successivo"] = get_next_or_prev(currentQueryset,currentItem,"next").pk
    except:
        nav["successivo"] = nav["ultimo"]

    currentId = currentItem.pk
    istanzaQueryset = repertorioIstanze.objects.filter(comunicazioni = currentId)
    comunicazione = get_object_or_404(corrispondenza, pk=n)
    return render(request, 'dettaglioComunicazione.html', {'comunicazione': comunicazione, 'nav': nav, "documenti_url":settings.MEDIA_URL, "istanza":istanzaQueryset[0]})

def dettaglio_istruttoria(request,n):
    istruttoria = get_object_or_404(istruttorie, pk=n)
    return render(request, 'dettaglioIstruttoria.html', {'istruttoria': istruttoria, "documenti_url":settings.MEDIA_URL})

@login_required
def load_db_richieste(request):
    cursor = connection.cursor()
    cursor.execute('SELECT id,ditta,the_geom,foglio,mappali,note,prg_vigente,prg_richiesta,prg_assegnata,rif FROM public.db_richieste ORDER BY id')
    richieste=[]
    for row in cursor:
        print (row, file=sys.stderr)
        _categoriaIstanza = catIstanze(pk = 3)
        _sottoscrizioni = sottoscrittori(pk = 12)
        _comunicazioni = corrispondenza(pk = 12)
        _istruttoria = istruttorie(pk = 2)
        catasto = str(row[3])+"/"+str(row[4])
        riga=repertorioIstanze(categoriaIstanza=_categoriaIstanza, idIstanza=row[0], ditta =row[1][0:100] , geom=row[2], coordCatastali=catasto[:250], note=str(row[5])+"\nDEST. VIGENTE: "+str(row[6])+"\nDEST. RICHIESTA: "+str(row[7])+"\nDEST. ASSEGNATA: "+str(row[8]), riferimento=row[9] or "")
        riga.save()
        richieste.append({"id":row[0],"ditta":row[1]})
    return render(request, "load_db.html", {"richieste": richieste})

@login_required
def dettaglio_sottoscrizione(request,n):
    currentQueryset = sottoscrittori.objects.all()
    currentItem = currentQueryset.get(pk=n)

    nav={}
    nav["primo"]=currentQueryset.first().pk
    nav["ultimo"]=currentQueryset.last().pk
    try:
        nav["precedente"] = get_next_or_prev(currentQueryset,currentItem,"prev").pk
    except:
        nav["precedente"] = nav["primo"]
    try:
        nav["successivo"] = get_next_or_prev(currentQueryset,currentItem,"next").pk
    except:
        nav["successivo"] = nav["ultimo"]

    currentId = currentItem.pk
    istanzaQueryset = repertorioIstanze.objects.filter(sottoscrizioni = currentId)
    sottoscrizione = get_object_or_404(sottoscrittori, pk=n)
    return render(request, 'dettaglioSottoscrizione.html', {'sottoscrizione': sottoscrizione, 'nav': nav, "documenti_url":settings.MEDIA_URL, "istanze":istanzaQueryset})

@login_required
def nuova_istanza(request):
    form = modelform_factory(repertorioIstanze, exclude=('',))
    return render(request, 'nuovaIstanza.html', {'FORM': form})


def cerca_catasto(request):
    return redirect("/certificati/catasto/")


@login_required
def sportello(request):
    if request.method == 'POST':
        istanza_sportello = repertorioIstanze.objects.get(pk=request.POST.get("pk"))

        if istanza_sportello.idIstanza == 9999999:
            istanza_sportello.idIstanza = prendi_primo_id_utile(catIstanze.objects.get(pk=0))

        if istanza_sportello.sottoscrittori_set.first():
            richiedente = istanza_sportello.sottoscrittori_set.first()
        else:
            richiedente = sottoscrittori()
            richiedente.istanza = istanza_sportello

        richiedente.cognomeDenominazione = request.POST.get("richiedente_cognome",'').upper()
        richiedente.nome = request.POST.get("richiedente_nome",'').capitalize()
        richiedente.email = request.POST.get("email",'')
        richiedente.titolo = request.POST.get("titolo",'')
        richiedente.note = request.POST.get("telefono",'') + " \n " + request.POST.get("note",'')
        richiedente.save()

        istanza_sportello.sottoscrizioni.add(richiedente)
        istanza_sportello.ditta = request.POST.get("richiedente_cognome",'').upper() + " " + request.POST.get("richiedente_nome",'').capitalize()
        istanza_sportello.coordCatastali = request.POST.get("coordinateCatastali",'')
        if request.POST.get("poligono",''):
            try:
                istanza_geom = GEOSGeometry(request.POST.get("poligono",''))
                if isinstance(istanza_geom, Polygon): 
                    istanza_sportello.geom = MultiPolygon(istanza_geom)
                else:
                    istanza_sportello.geom = istanza_geom
            except:
                pass
        rapporto_sportello = "<Incontro Sportello di %s con %s %s del %s>\n\n" % (
            request.user.username,
            request.POST.get("richiedente_cognome",'').capitalize(),
            request.POST.get("richiedente_nome",'').capitalize(), 
            datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S") 
        )
        rapporto_sportello += request.POST.get("richiesta",'') + '\n\n'
        rapporto_sportello += request.POST.get("risposta",'') + '\n\n'
        istanza_sportello.note = rapporto_sportello


        if istanza_sportello.istruttorie_set.first():
            istruttoria = istanza_sportello.istruttorie_set.first()
        else:
            istruttoria = istruttorie()
            istruttoria.istanza = istanza_sportello
            istruttoria.tipoIstruttoria = 5

        istruttoria.istruttore = request.user.username
        istruttoria.note = request.POST.get("richiesta",'')
        istruttoria.dettaglio = request.POST.get("risposta",'')
        istruttoria.data = datetime.datetime.now()
        if request.FILES:
            istruttoria.allegati = request.FILES['allegato']
        istruttoria.save()

        istanza_sportello.save()
        istanze_temporanee = repertorioIstanze.objects.filter(idIstanza=9999999)
        for istanza in istanze_temporanee:
            istanza.delete()
        return JsonResponse({"esito": "OK"})

    else:
        nuova_istanza = repertorioIstanze()
        nuova_istanza.idIstanza = 9999999
        nuova_istanza.categoriaIstanza = catIstanze.objects.get(pk=0)
        nuova_istanza.ditta = "sportello_temporaneo_da_cancellare"
        nuova_istanza.save()
        return render(request, 'report_sportello.html', {'pk': nuova_istanza.pk}) #'form': form

@login_required
def segnalazione(request):
    if request.method == 'POST' and \
        request.POST.get("segnalazione"):
        nuova_segnalazione = repertorioIstanze()

        cat_segnalazioni = catIstanze.objects.get(pk=8)
        nuova_segnalazione.categoriaIstanza = cat_segnalazioni
        nuova_segnalazione.idIstanza = prendi_primo_id_utile(cat_segnalazioni)
        nuova_segnalazione.ditta = request.user.username.upper()
        
        nuova_segnalazione.coordCatastali = request.POST.get("coordinateCatastali",'')
        if request.POST.get("poligono",''):
            try:
                istanza_geom = GEOSGeometry(request.POST.get("poligono",''))
                if isinstance(istanza_geom, Polygon): 
                    nuova_segnalazione.geom = MultiPolygon(istanza_geom)
                else:
                    nuova_segnalazione.geom = istanza_geom
            except:
                pass
        else:
            if request.POST.get("coordinateCatastali",''):
                ambito_wkt, nuova_segnalazione.coordCatastali =  GeomFromCoordinateCatastali(request.POST.get("coordinateCatastali",''), check=True)
                nuova_segnalazione.geom = GEOSGeometry(ambito_wkt)
        
        nuova_segnalazione.note = request.POST.get("segnalazione",'')

        nuova_segnalazione.save()

        istruttoria = istruttorie()
        istruttoria.istanza = nuova_segnalazione
        istruttoria.tipoIstruttoria = 3

        istruttoria.istruttore = request.user.username
        istruttoria.note = request.POST.get("segnalazione",'')
        istruttoria.data = datetime.datetime.now()
        if request.FILES:
            istruttoria.allegati = request.FILES['allegato']
        istruttoria.save()

        return JsonResponse({"esito": "OK", "pk": nuova_segnalazione.pk})

    else:
        return HttpResponseServerError()

@login_required
def rebuild(request):
    if request.method == 'GET':
        sez_istanze = request.GET.get('instanze', '')
        sez_note = request.GET.get('note', '')
        sez_correlate = request.GET.get('correlate', '')
        sez_valutazione = request.GET.get('valutate', '')
    else:
        sez_istanze = None
        sez_note = None
        sez_correlate = None
        sez_valutazione = None
    print ("STR",sez_istanze,sez_note, file=sys.stderr)
    for istanza in repertorioIstanze.objects.all():
        if sez_istanze:
            try:
                istanza.destVigente = destinazioniPRG(istanza.geom.wkt)[:250]
                print (istanza, file=sys.stderr)
                istanza.save()
            except:
                pass
        if sez_note:
            try:
                if "DEST. " in istanza.note:
                    note = istanza.note.split("DEST. ")
                    #istanza.note = note[0]
                    print (note, file=sys.stderr)
                    if note[0] != "None":
                        istanza.note = note[0]
                    else:
                        istanza.note = ""
                    if note[3][11:] != "None":
                        istanza.destAssegnata = note[3][11:]
                    else:
                        istanza.destAssegnata = ""
                    istanza.destRichiesta = note[2][11:]
                    istanza.save()
            except:
                pass
        if sez_correlate and istanza.geom:
            correlate = repertorioIstanze.objects.filter(geom__intersects=istanza.geom)
            istanza.istanzeCorrelate.clear()
            for correlata in correlate:
                if correlata != istanza:
                    istanza.istanzeCorrelate.add(correlata)
            print (istanza,istanza.istanzeCorrelate.all(), file=sys.stderr)
            istanza.save()

    if sez_valutazione:
        ultima = datetime.date.min
        istruttorie_related = istruttorie.objects.all().order_by('data')
        for istruttoria in istruttorie_related:
            istruttoria_related = repertorioIstanze.objects.get(pk=istruttoria.istanza.id)
            istruttoria_related.stato = istruttoria.accoglimento
            istruttoria_related.valutazione = istruttoria.dettaglio
            if not istruttoria_related.destAssegnata:
                if istruttoria.accoglimento in ('si','sp','s?'):
                    istruttoria_related.destAssegnata = istruttoria.destAssegnata
            else:
                if not istruttoria.destAssegnata:
                    print ("REVERSE destAssegnata",istruttoria, file=sys.stderr)
                    istruttoria.destAssegnata = istruttoria_related.destAssegnata
                    istruttoria.save()
            istruttoria_related.save()
            print (istruttoria_related,istruttoria_related.stato,istruttoria_related.destAssegnata, file=sys.stderr)

    response = istanze(request)
    return response

def genera_odt(request, modello_pk, comunicazione_pk):
    modello = modelli.objects.get(pk=modello_pk)
    comunicazione = corrispondenza.objects.get(pk=comunicazione_pk)
    return ReportGenerator().create_report(modello, comunicazione, "istanza-%d-%s" % (comunicazione.istanza.pk,comunicazione.mittente.replace(" ","_")), request)

@csrf_exempt
def proxy_img(request,left_extent,bottom_extent,right_extent,top_extent,scale):
    base_url = "http://172.25.193.167/cgi-bin/qgis_mapserv.fcgi?MAP=/usr/lib/cgi-bin/PI/STAMPE.qgs&SERVICE=WMS&REQUEST=GetPrint&TEMPLATE=IMMAGINE&version=1.3.0&FORMAT=png&DPI=150&map0:EXTENT={left},{bottom},{right},{top}&map0:SCALE={scale}&CRS=EPSG:3003"
    img_url = base_url.format(left=left_extent,bottom=bottom_extent,right=right_extent,top=top_extent,scale=scale)
    r = requests.get(img_url, stream=True)
    tmpfile = tempfile.NamedTemporaryFile()
    if r.status_code == 200:
        with open(tmpfile.name, 'wb') as f:
            for chunk in r:
                f.write(chunk)

    with open(tmpfile.name, "rb") as f:
        return HttpResponse(f.read(), content_type="image/png")

def streetview_link(request,istanza_id):
    x = request.GET.get('x',None)
    y = request.GET.get('y',None)
    if x and y:
        return SV([x,y])
    else:
        rep  = repertorioIstanze.objects.get(pk=istanza_id)
        return SV(rep.geom.centroid)

class ReportGenerator():
   """ Class ReportGenerator """

   @staticmethod
   def create_report(modello_obj, comunicazione_obj, doc_name, request):
       parsed_url = urlparse(request.build_absolute_uri())
       host_complete_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_url)
       engine = Renderer()
       data = {
            "destinatario": comunicazione_obj.mittente.upper(),
            "protocollo": comunicazione_obj.protComunicazione,
            "data": comunicazione_obj.dataComunicazione,
            "indirizzo": comunicazione_obj.indirizzo or "",
            "email": comunicazione_obj.email or "",
            "parametri": modello_obj.parametri,
            "url": "%s/admin/repertorio/repertorioistanze/%d/change/" % (host_complete_url, comunicazione_obj.istanza.pk),
            "catasto": decodificaCatasto(comunicazione_obj.istanza.coordCatastali),
            "oggetto": "Istanza %d/%d - %s - %s" % (
                comunicazione_obj.istanza.idIstanza,
                comunicazione_obj.istanza.categoriaIstanza.pk,
                modello_obj.descrizione, 
                decodificaCatasto(comunicazione_obj.istanza.coordCatastali)
            ),
       }
       #root = os.path.dirname(__file__)
       #document = root + '/templates/bedjango/template.odt'
       template = modello_obj.modello_odt.path #os.path.join(settings.MEDIA_ROOT,modello_obj.modello_odt)
       result = engine.render(template, **data)
       response = HttpResponse(content_type='application/vnd.oasis.opendocument.text')
       response['Content-Disposition'] = 'inline; filename=%s.odt' % doc_name

       with tempfile.NamedTemporaryFile() as output:
           output.write(result)
           output.flush()
           output = open(output.name, 'rb')
           response_content = output.read()
           response.write(response_content)

       return response

class istanzeFilter(django_filters.FilterSet):
    class Meta:
        model = repertorioIstanze
        fields = {
                'categoriaIstanza': ['exact'],
                'idIstanza': ['exact'],
                'ditta': ['icontains'],
                'note': ['icontains']
            }

class istanzeTableView(istanzeFilter, SingleTableView):
    def get_table_data(self):
        return self.object_list

class istanze_list(istanzeTableView):
    model = repertorioIstanze
    table_class = table_factory(repertorioIstanze, "0","")
    filterset_class = istanzeFilter
    template_name='istanze_filter.html' ,


def nomina_istanza(istanza):
    sottoscrizioniCorrenti = istanza.sottoscrizioni
    if sottoscrizioniCorrenti.count() > 1:
        #nuovaDitta += ' e altri (%s)' % str(sottoscrizioniCorrenti.count()-1)
        sottoscrittori_dict = {}
        sottoscrittori_list = []
        for s in sottoscrizioniCorrenti.all().exclude(titolo='professionista incaricato'):
            sottoscrittori_list.append([s.cognomeDenominazione,s.nome])

        for s in sottoscrittori_list:
            if s[0] in sottoscrittori_dict:
                sottoscrittori_dict[s[0]].append(s[1])
            else:
                sottoscrittori_dict[s[0]]= [s[1]]

        cognomi = sorted(sottoscrittori_dict.keys())

        ditta_complessa = ""
        for cogn in cognomi:
            ditta_complessa += cogn + " "
            for nome in sottoscrittori_dict[cogn]:
                ditta_complessa += nome + ","
                ditta_complessa = ditta_complessa[:-1] + " "

        return ditta_complessa[:100]
    else:
        nuovaDitta = sottoscrizioniCorrenti.values('cognomeDenominazione')[0]['cognomeDenominazione'] + " " + sottoscrizioniCorrenti.values('nome')[0]['nome']
        return nuovaDitta[:100]

def uploadBandi(request):

    desc_mapping = {
        24: "l'individuazione di manufatto incongruo (%s mq, %s mc) con la seguente motivazione: \"%s\"",
        25: 'la perimetrazione di ambito urbano da riqualificare (%s mq, %s mc) con la seguente motivazione: \"%s\"',
        26: 'la perimetriazione di un ambito da rigenerare (%s mq, %s mc) con la seguente motivazione: \"%s\"',
        27: "l'individuazione di un nuovo ambito di nuova edificazione (%s mq, %s mc) con la seguente motivazione: \"%s\" con il seguente vantaggio pubblico: \"%s\"",
        28: "l'individuazione di un nuovo ambito di riclassificazione di area a servizi (%s mq, %s mc) con la seguente motivazione: \"%s\" con il seguente vantaggio pubblico: \"%s\"",
        29: "l'individuazione di lotto singolo per esigenze familiari (\"%s\") con la seguente destinazione: \"%s\"",
    }

    delete = False # mettere a false in produzione
    update_geom_only = True

    newForm = uploadForm()
    if request.method == 'POST':
        upForm = uploadForm(request.POST, request.FILES)
        if "file_csv" in request.FILES:
            csv_file = request.FILES["file_csv"]
            csv_file.seek(0)
            f = StringIO(csv_file.read().decode('utf-8'))
            csv_reader = csv.DictReader(f, delimiter=',')
            istanze_processate = 0
            error = None
            istanze_inserite = 0
            if update_geom_only:
                for i,row in enumerate(csv_reader):
                    record = json.loads(row["json"])
                    data_prot = datetime.datetime.strptime(record["data_protocollo"], "%Y-%m-%d").date()
                    protocollo_completo = str(data_prot.year)+record["nr_protocollo"]
                    verifica_comunicazione = corrispondenza.objects.filter(protComunicazione=protocollo_completo).first()
                    if not verifica_comunicazione:
                        return render(request, 'upload.html', {"msg": 'SOLO AGGIORNAMENTO CATASTO E GEOMETRIE: ERRORE PROTOCOLLO NON RICONOSCIUTO' ,"form": newForm})
                    else:
                        istanza_update = verifica_comunicazione.istanza
                    fmdict = {}
                    fmsource= record["immobili"][0]
                    for fmsource in record["immobili"]:
                        for key in sorted(record["immobili"][0].keys()):
                            if key[:3] == 'fog':
                                if not fmsource[key] in fmdict:
                                    fmdict[fmsource[key]] = []
                            if key[:3] == 'map':
                                fg_related = "fog" + key.replace("map","").split("_")[0]
                                if fmsource[key]:
                                    fmdict[fmsource[fg_related]].append(fmsource[key])
                    
                    fmtarget = ""
                    for fg,mplist in fmdict.items():
                        fmtarget += (fg + "/"+ ('-'.join(mplist))) + " "

                    try:
                        ambito_wkt, feedback =  GeomFromCoordinateCatastali(fmtarget.strip(), check=True)
                        fmtarget = feedback
                        if ambito_wkt:
                            ambito_geom = GEOSGeometry(ambito_wkt,3003)
                            destinazioni_vigenti_rilevate = destinazioniPRG(ambito_geom.wkt)[0]
                        else:
                            ambito_geom = None
                            fmtarget = feedback + " [Coordinate catastali non valide]"
                            destinazioni_vigenti_rilevate = ""
                    except:
                        ambito_geom = None
                        fmtarget = "[Coordinate catastali non valide]"
                        destinazioni_vigenti_rilevate = ""
                    
                    #istanza_update.coordCatastali = fmtarget[:250],
                    #istanza_update.destVigente = destinazioni_vigenti_rilevate[:250],
                    #istanza_update.geom = ambito_geom,
                    #istanza_update.save()
                    if istanza_update.coordCatastali != fmtarget[:250]:
                        print (istanza_update.pk,",",istanza_update.idIstanza,",",istanza_update.categoriaIstanza,",", istanza_update.ditta,",", protocollo_completo,",", istanza_update.coordCatastali,",", fmtarget)#, istanza_update.geom.wkt if istanza_update.geom else "nn", ambito_geom.wkt if ambito_geom else "nn")
                return render(request, 'upload.html', {"msg": 'SOLO AGGIORNAMENTO CATASTO E GEOMETRIE' ,"form": newForm})

            for i,row in enumerate(csv_reader):
                record = json.loads(row["json"])
                data_prot = datetime.datetime.strptime(record["data_protocollo"], "%Y-%m-%d").date()
                protocollo_completo = str(data_prot.year)+record["nr_protocollo"]
                verifica_comunicazione = corrispondenza.objects.filter(protComunicazione=protocollo_completo).first()
                if delete and verifica_comunicazione:
                    for com in corrispondenza.objects.filter(protComunicazione=protocollo_completo):
                        print ("delete",com)
                        if com.istanza:
                            com.istanza.delete()
                        print ("delete",com.istanza)
                        com.delete()

                if not verifica_comunicazione: #verifica se l'istanza è già inserita
                    nome_proponente = record["e00_ar_nome"]
                    cognome_proponente = record["e00_ar_cognome"]
                    cf_proponente = record["e00_ar_codicefiscale"]
                    mail_proponente = record["e00_ar_mail"]
                    pec_proponente = record["pec"]
                    tel_proponente = record["e00_ar_tel"]
                    superficie = record["superficie"]
                    volume_esistente = record["volume_esistente"]
                    if record["destinazioni_vigenti"]:
                        destinazioni_vigenti_dichiarate = [ value for key,value in record["destinazioni_vigenti"][0].items() ]
                    else:
                        destinazioni_vigenti_dichiarate = []
                    destinazione_pi_richiesta = record["destinazione_pi_richiesta"] or "NULL"
                    chk_oggetto1 = record["chk_oggetto1"]
                    chk_oggetto2 = record["chk_oggetto2"]
                    chk_oggetto3 = record["chk_oggetto3"]

                    motivazioni_richiesta = record["motivazioni_richiesta"] or ""
                    convenienza_pubblica = record["convenienza_pubblica"] or ""
                    destinazione_residenziale_proposta = record["destinazione_residenziale_proposta"] or ""
                    esigenze_familiari = record["esigenze_familiari"] or ""

                    desc = "Istanza per "
                    if record["modulo"] == 'RIQUALIFICAZIONE':
                        if chk_oggetto1:
                            cat_id = 24
                        elif chk_oggetto2:
                            cat_id = 25
                        elif chk_oggetto3:
                            cat_id = 26
                        desc += desc_mapping[cat_id] % (superficie, volume_esistente, motivazioni_richiesta)
                    elif record["modulo"] == 'URBANIZZAZIONE':
                        if chk_oggetto1: #VERIFICARE IN PRODUZIONE
                            cat_id = 27
                        elif chk_oggetto2: #VERIFICARE IN PRODUZIONE
                            cat_id = 28
                        desc += desc_mapping[cat_id] % (superficie, volume_esistente, motivazioni_richiesta, convenienza_pubblica) 
                    elif record["modulo"] == 'LOTTI':
                        cat_id = 29
                        desc += desc_mapping[cat_id] % (esigenze_familiari, destinazione_residenziale_proposta) 
                    
                    categoria = catIstanze.objects.get(pk=cat_id)

                    fmdict = {}
                    fmsource= record["immobili"][0]
                    for fmsource in record["immobili"]:
                        for key in sorted(record["immobili"][0].keys()):
                            if key[:3] == 'fog':
                                if not fmsource[key] in fmdict:
                                    fmdict[fmsource[key]] = []
                            if key[:3] == 'map':
                                fg_related = "fog" + key.replace("map","").split("_")[0]
                                if fmsource[key]:
                                    fmdict[fmsource[fg_related]].append(fmsource[key])
                    
                    fmtarget = ""
                    for fg,mplist in fmdict.items():
                        fmtarget += (fg + "/"+ ('-'.join(mplist))) + " "

                    new_istanza = new_comunicazione = new_istruttoria = None

                    try:
                        ambito_wkt, feedback =  GeomFromCoordinateCatastali(fmtarget.strip(), check=True)
                        fmtarget = feedback
                        if ambito_wkt:
                            ambito_geom = GEOSGeometry(ambito_wkt,3003)
                            destinazioni_vigenti_rilevate = destinazioniPRG(ambito_geom.wkt)[0]
                        else:
                            ambito_geom = None
                            fmtarget = feedback + " [Coordinate catastali non valide]"
                            destinazioni_vigenti_rilevate = ""
                    except:
                        ambito_geom = None
                        fmtarget = "[Coordinate catastali non valide]"
                        destinazioni_vigenti_rilevate = ""

                    try:

                        new_istanza = repertorioIstanze.objects.create(
                            ditta = "temp",
                            idIstanza = prendi_primo_id_utile(categoria),
                            categoriaIstanza = categoria,
                            coordCatastali = fmtarget[:250],
                            destVigente = destinazioni_vigenti_rilevate[:250],
                            destRichiesta = destinazione_pi_richiesta,
                            geom = ambito_geom,
                            note = desc
                        )


                    
                        new_comunicazione = corrispondenza.objects.create(
                            recipiente = 'e',
                            mittente = nome_proponente + ' ' + cognome_proponente,
                            protComunicazione = protocollo_completo,
                            dataComunicazione = data_prot,
                            email = pec_proponente or mail_proponente,
                            note = json.dumps({
                                "cf":  cf_proponente,
                                "mail": mail_proponente,
                                "tel": tel_proponente
                            }, indent=4),
                            istanza = new_istanza
                        )

                        new_istanza.comunicazioni.add(new_comunicazione)

                        titolo = 'unico proprietario' if (not record["comproprietari"] and not record["deleganti"]) else 'comproprietario'

                        if not record["deleganti"]:
                            new_sottoscrittore = sottoscrittori.objects.create(
                                cognomeDenominazione = cognome_proponente.upper(),
                                nome = nome_proponente.capitalize(),
                                codicefiscale = cf_proponente.upper(),
                                titolo = titolo,
                                istanza = new_istanza
                            )
                        else:
                            new_sottoscrittore = sottoscrittori.objects.create(
                                cognomeDenominazione = cognome_proponente.upper(),
                                nome = nome_proponente.capitalize(),
                                codicefiscale = cf_proponente.upper(),
                                titolo = 'professionista incaricato',
                                istanza = new_istanza
                            )
                        new_istanza.sottoscrizioni.add(new_sottoscrittore)

                        if titolo == 'comproprietario':

                            if record["comproprietari"]:
                                repertorio_prop = record["comproprietari"]
                                prop_map = {
                                    "cognome": "cognome_sub",
                                    "nome": "nome_sub",
                                    "cf": "cf_sub",
                                }
                            elif record["deleganti"]:
                                titolo = 'unico proprietario' if len(record["deleganti"]) == 1 else 'comproprietario'
                                repertorio_prop = record["deleganti"]
                                prop_map = {
                                    "cognome": "cognome_delegante_sub",
                                    "nome": "nome_delegante_sub",
                                    "cf": "cf_delegante_sub",
                                }
                            # un altro caso non è possibile quindi deve andare in errore perchè non trova repertorio_prop

                            for proprietario in repertorio_prop:
                                new_sottoscrittore = sottoscrittori.objects.create(
                                    cognomeDenominazione = proprietario[prop_map["cognome"]].upper(),
                                    nome = proprietario[prop_map["nome"]].capitalize(),
                                    codicefiscale = proprietario[prop_map["cf"]].upper(),
                                    titolo = titolo,
                                    istanza = new_istanza
                                )
                                new_istanza.sottoscrizioni.add(new_sottoscrittore)
                            
                            new_istanza.save()
                        
                        new_istanza.ditta = nomina_istanza(new_istanza)
                        new_istanza.save()

                        new_istruttoria = istruttorie.objects.create(
                            tipoIstruttoria = 7,
                            istruttore = 'istanze online',
                            data = datetime.date.today(),
                            note = json.dumps({
                                "categoria": str(categoria),
                                "coordinateCatastali": fmtarget,
                                "superficie": superficie,
                                "volume_esistente": volume_esistente,
                                "destinazioni_vigenti_dichiarate":  ' '.join(destinazioni_vigenti_dichiarate),
                                "destinazioni_vigenti_rilevate":  destinazioni_vigenti_rilevate,
                                "destinazione_pi_richiesta": destinazione_pi_richiesta,
                                "motivazioni_richiesta": motivazioni_richiesta,
                                "esigenze_familiari": esigenze_familiari,
                                "destinazione_residenziale_proposta": destinazione_residenziale_proposta,
                                "esigenze_familiari": esigenze_familiari,
                            }, indent=4),
                            destRichiesta = destinazione_pi_richiesta,
                            istanza = new_istanza
                        )

                        istanze_inserite += 1
                    
                    except Exception as e:
                        error = e
                        if new_istruttoria: 
                            new_istruttoria.delete()
                        if new_comunicazione: 
                            new_comunicazione.delete()
                        if new_istanza:
                            new_istanza.delete()
                        break

        if error:
            msg = "Errore: %s riga %s" % (str(error), error.__traceback__.tb_lineno)
        else:
            msg = "Processati %d record. Inseriti %d record" % (i+1, istanze_inserite)

        return render(request, 'upload.html', {"msg": msg ,"form": newForm})
    else:
        return render(request, 'upload.html', {"msg":"","form": newForm})