# -*- coding: utf-8 -*-
"""
/***************************************************************************
 cartawebImportExport

 PROCEDURA per importare ed esportare dati da cartaweb
                              -------------------
        begin                : 2015-06-18
        git sha              : $Format:%H$
        copyright            : (C) 2015 by Enrico Ferreguti
        email                : ferregutie@comune.padova.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from django.contrib.gis.geos import GEOSGeometry
import sys

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

punti = 1
linee = 2
aree = 3
boh = 99

template_txt = '''
{
    "symcolor3": -1,
    "gid": "%(gid)s",
    "symcolor": 0,
    "symangle": 0,
    "symtypename": "TextSymbol",
    "attrib": {
        "labeled": false,
        "wkid": 3003
    },
    "geomtype": "MAPPOINT",
    "symtext": "%(symtext)s",
    "cmenu": {
        "link": null,
        "builtInItems": {
            "quality": false,
            "play": false,
            "loop": false,
            "rewind": false,
            "forwardAndBack": false,
            "save": false,
            "print": false,
            "zoom": false
        },
        "clipboardMenu": false,
        "customItems": [
            {
                "enabled": true,
                "caption": "Cancella",
                "separatorBefore": false,
                "visible": true
            }
        ],
        "clipboardItems": {
            "cut": false,
            "paste": false,
            "selectAll": true,
            "copy": false,
            "clear": false
        }
    },
    "symltype": "",
    "gname": "%(gname)s",
    "symtype": "Arial",
    "geomarray": [
        {
            "x": %(x)s,
            "y": %(y)s,
            "spatialReference": {
                "wkid": %(geomsr)s
            }
        }
    ],
    "symfonte": "",
    "symalpha2": 1,
    "geomsr": %(geomsr)s,
    "symfontp": "middle",
    "symwidth": 1,
    "symsize": 15,
    "symcolor2": -1,
    "symalpha1": 1
}
'''

template_poligon = '''
{
    "geomarray": %(geomarray)s,
    "symltype": "solid",
    "cmenu": {
        "clipboardItems": {
            "copy": false,
            "paste": false,
            "clear": false,
            "selectAll": true,
            "cut": false
        },
        "link": null,
        "clipboardMenu": false,
        "builtInItems": {
            "print": false,
            "zoom": false,
            "quality": false,
            "save": false,
            "play": false,
            "loop": false,
            "rewind": false,
            "forwardAndBack": false
        },
        "customItems": [
            {
                "caption": "Cancella",
                "separatorBefore": false,
                "visible": true,
                "enabled": true
            }
        ]
    },
    "symwidth": %(symwidth)s,
    "geomtype": "%(geomtype)s",
    "symtypename": "%(symtypename)s",
    "symcolor2": %(symcolor2)s,
    "symalpha1": %(symalpha1)s,
    "symfontp": "middle",
    "symsize": %(symsize)s,
    "symfonte": "",
    "attrib": {
        "wkid": %(geomsr)s,
        "labeled": true
    },
    "symcolor": %(symcolor)s,
    "symangle": 0,
    "geomsr": %(geomsr)s,
    "symtype": "%(symtype)s",
    "gid": "%(gid)s",
    "gname": "%(gname)s",
    "symtext": "%(gname)s",
    "symcolor3": %(symcolor3)s,
    "symalpha2": %(symalpha2)s
}
'''


def cartawebExport(wkt):
    parametri = {}
    parametri["symwidth"] = 1.5
    parametri["symcolor"] = 16777215
    parametri["symcolor2"] = 16711680
    parametri["symcolor3"] = 0
    parametri["symalpha1"] = 0
    parametri["symalpha2"] = 1
    parametri["symsize"] = 1.5

    fa = "["


    parametri["geomtype"] = 'POLYGON'
    parametri["symtypename"] ="SimpleFillSymbol"
    parametri["symtype"] = "solid"
    parametri["geomsr"] = "3003"

    multigeom = GEOSGeometry(wkt)

    #print pointCollection
    for geom in multigeom:
        for ring in geom:
            ga = "["
            for point in ring:
                print ("VERTICE:",point, file=sys.stderr)
                vString = '{"x":%s,"y":%s,"spatialReference":{"wkid":%s}},' % (point[0],point[1],parametri["geomsr"])
                ga += vString
            ga = ga[:-1] + "]"
            
            print ("GA:",ga, file=sys.stderr)
        
            parametri["gid"] = "export wkt"
            parametri["gname"] = "export wkt"
            parametri["geomarray"] = ga
        
            #print parametri
            featDef = template_poligon % parametri
            fa += featDef+',\n'
    fa = fa[:-2]+"]"

    print ("FA:",fa, file=sys.stderr)
    
    return fa