from django import forms
from .models import repertorioIstanze
from leaflet.forms.widgets import LeafletWidget



class mapForm(forms.Form):
	
    
    class Meta:
        model = repertorioIstanze
        fields = ('geom',)
        widgets = {'geom': LeafletWidget()}

