from django.conf import settings # import the settings file

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

def documenti_url(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'DOCUMENTI_URL': settings.MEDIA_URL}