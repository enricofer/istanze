#from dataclasses import fieldslink_genera_odtrepo
from django.contrib import admin
from django import forms
from django.db import models
from django.contrib.gis import admin
from django.db import connection, connections
from django.contrib.gis.geos import GEOSGeometry
from django.utils.html import format_html
from django.urls import reverse
from django.http import FileResponse, HttpResponse, HttpResponseRedirect, Http404
from django.forms.models import model_to_dict
from django.conf import settings
from datetime import datetime
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.admin import SimpleListFilter
from ajax_select.fields import AutoCompleteSelectField, AutoCompleteSelectMultipleField #testing
from ajax_select.admin import AjaxSelectAdmin #testing
from django_json_widget.widgets import JSONEditorWidget
from collections import OrderedDict

from secretary import Renderer

from selectable.forms import AutoCompleteSelectWidget,AutoComboboxSelectWidget,AutoCompleteSelectField, AutoCompleteSelectMultipleWidget

#Export to shapefile
from shapely.geometry import mapping
from shapely import wkt
from fiona import collection
from fiona.crs import from_epsg
from io import BytesIO
from zipfile import ZipFile

from taggit.models import Tag
from taggit.forms import TagField
from taggit_labels.widgets import LabelWidget
from taggit_bulk.actions import tag_wizard

from massadmin.massadmin import MassEditMixin

from certificati.generaCertificato import decodificaCatasto
from .models import corrispondenza,sottoscrittori,repertorioIstanze,catIstanze,settori,istruttorie,catIstruttoria, valutazione, modelli as modelli_doc, accoglimento_avanzamento_choice, accoglimento_valutazione_choice, tipoIstruttoria_ridotta
from .views import mapIstanze, nomina_istanza
from .lookups import settoreLookup, tipoIstanzaLookup
import logging
import sys
from urllib.parse import quote,urlencode, quote_plus
#import urllib.parse
import tempfile
import requests
import shutil
import re
import os
import uuid
import csv
import json
import nested_admin
import certifi


from certificati.coordinateCatastali import QueryFromCoordinateCatastali, destinazioniPRG
from certificati.views import getDestinazioniHTMLFromGeometry, ubicazione, riferimenti_incrociati

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

logger = logging.getLogger(__name__)

accoglimento = accoglimento_avanzamento_choice + accoglimento_valutazione_choice

def shapefileFromQueryset(dataset, exportComunicazioni=False):
    """
    Returns format representation for given dataset.
    """

    accoglimento = dict(accoglimento_choiches)

    attribs = (
        ('id_istanza', 'int'),
        ('cat_id', 'int'),
        ('cat_txt', 'str:100'),
        ('ditta', 'str:100'),
        ('coordCatastali', 'str:250'),
        ('stato_avan', 'str:30'),
        ('stato_val', 'str:30'),
        ('note', 'str'),
        ('valutazione', 'str'),
        ('riassunto', 'str:250'),
        ('destVigente', 'str:100'),
        ('destRich', 'str:100'),
        ('destAss', 'str:100'),
        ('accordo', 'str:4'),
    )

    schema = {'geometry': 'MultiPolygon', 'properties': attribs}
    #TODO: it would be much better to save the artifacts to an in memory buffer
    temp_dir = os.path.join(settings.MEDIA_ROOT,'tmp',str(uuid.uuid1()))
    os.makedirs(temp_dir)
    temp_shp = os.path.join(temp_dir,"istanze.shp")

    if exportComunicazioni and len(dataset) > 50:
        raise Http404("Limitare l'esportazione con comunicazioni a massimo 50 istanze.")  

    with collection(temp_shp, "w", "ESRI Shapefile", schema, crs=from_epsg(3003), encoding='utf-8') as output:

        for thing in dataset:
            if thing.geom:
                data = dict()
                data['id_istanza'] = thing.idIstanza
                data['cat_id'] = thing.categoriaIstanza.pk
                data['cat_txt'] = thing.categoriaIstanza.tipoIstanza #
                data['ditta'] = thing.ditta if thing.ditta else ''
                data['coordCatastali'] = thing.coordCatastali if thing.coordCatastali else 'ISTANZA DI CARATTERE GENERALE'
                data['stato_avan'] = accoglimento[thing.stato]
                data['stato_val'] = accoglimento[thing.stato_valutazione]
                data['note'] = thing.note if thing.note else ''
                data['valutazione'] = thing.valutazione if thing.valutazione else ''
                data['riassunto'] = thing.riassunto if thing.riassunto else ''
                data['destVigente'] = thing.destVigente if thing.destVigente else ''
                data['destRich'] = thing.destRichiesta if thing.destRichiesta else ''
                data['destAss'] = thing.destAssegnata if thing.destAssegnata else ''
                data['accordo'] = thing.accordo if thing.accordo else ''

                the_geom = wkt.loads(thing.geom.wkt)
                output.write({'properties': data, 'geometry': mapping(the_geom)})

            if exportComunicazioni:
                comunicazioni_related = corrispondenza.objects.filter(istanza__pk=thing.pk).order_by('protComunicazione')
                print ('comunicazioni_related', comunicazioni_related)
                order = 1
                for com in comunicazioni_related:
                    filename, ext = os.path.splitext(com.documento.path)
                    shutil.copy(com.documento.path,os.path.join(temp_dir,str(thing.idIstanza).zfill(4)+"-"+str(order)+ext))
                    order += 1


    #zipFileName = os.path.join(temp_dir,'pua.zip')
    zipMemory = BytesIO()
    zip = ZipFile(zipMemory, 'w')

    for filename in os.listdir(temp_dir):
        zip.write(os.path.join(temp_dir,filename),filename)
        os.remove(os.path.join(temp_dir,filename))

    os.rmdir(temp_dir)

    # fix for Linux zip files read in Windows
    for filename in zip.filelist:
        filename.create_system = 0

    zip.close()
    zipMemory.seek(0)
    return zipMemory

def prendi_primo_id_utile(cat):
    istanze_x_categoria = repertorioIstanze.objects.filter(categoriaIstanza=cat).order_by('idIstanza')
    if istanze_x_categoria:
        primo_seq_libero = None
        for seq,istanza in enumerate (istanze_x_categoria):
            print ("SEQ", seq, istanza.idIstanza, file=sys.stderr)
            if istanza.idIstanza != seq + 1:
                primo_seq_libero = seq + 1
                break
        free_id = primo_seq_libero or istanze_x_categoria.latest('idIstanza').idIstanza  + 1
        print ("NEW ID", free_id, file=sys.stderr)
        return free_id
    else:
        return  1

class corrispondenzaForm(forms.ModelForm):
    #mittente = AutoComboboxSelectWidget (lookup_class=settoreLookup, allow_new=True)
    dataComunicazione = forms.SelectDateWidget()

    class Meta(object):
        model = corrispondenza
        exclude = ('', )
        readonly_fields = ( 'MODELLO',)

    MODELLO = forms.ModelChoiceField(
        queryset=modelli_doc.objects.filter(multiplo=False).filter(abilitato=True).order_by('pk'),
        label='Modelli',
        required=False,
    )

    '''
    def __init__(self, *args, **kwargs):
        super(corrispondenzaForm, self).__init__(*args, **kwargs)
        eventuale controllo congruenza
        if self.instance and self.instance.pk and self.instance.owner:
            self.initial['owner'] = self.instance.owner.pk
    '''

    def save(self, *args, **kwargs):
        print (self.cleaned_data['protComunicazione'], file=sys.stderr)
        if len(self.cleaned_data['protComunicazione'])==7:
            self.instance.protComunicazione = str(self.cleaned_data['dataComunicazione'].year)+self.cleaned_data['protComunicazione']
        print (self.instance.protComunicazione, file=sys.stderr)
        '''
        Qua sotto serve a salvare l'eventuale lookup del mittente
        mittente = self.cleaned_data['mittente'].clean()
        print (self.cleaned_data, file=sys.stderr)
        mittente = self.data['mittente_0']
        print (mittente, file=sys.stderr)
        #eventuale controllo if tipoIstanza:
        self.instance.mittente = mittente
        '''
        return super(corrispondenzaForm, self).save(*args, **kwargs)

class corrispondenzaAdmin(admin.ModelAdmin):
    form = corrispondenzaForm
    search_fields = ['protComunicazione','mittente']
    ordering = ['-id']

#--------------------------------------------------------------

class corrispondenzaInlineFormset(forms.models.BaseInlineFormSet):

    def applicaModifiche(self,obj):
        #setattr(obj, self.cognome_field, self.cognome_field.upper())
        #setattr(obj, self.nome_field, self.nome_field.title())
        obj.cognomeDenominazione = obj.cognomeDenominazione.upper()
        obj.nome = obj.nome.title()

    def save_new(self, form, commit=True):
        obj = super(corrispondenzaInlineFormset, self).save_new(form, commit=False)
        #self.applicaModifiche(obj)
        if commit:
            obj.save()
        return obj

    def save_existing(self, form, instance, commit=True):
        obj = super(corrispondenzaInlineFormset, self).save_existing(form, instance, commit=False)
        #self.applicaModifiche(obj)
        if commit:
            obj.save()
        return obj

class corrispondenzaFormsetMixin(object):
    formset = corrispondenzaInlineFormset

    def __get_formset(self, request, obj=None, **kwargs):
        formset = super(corrispondenzaFormsetMixin, self).get_formset(request, obj, **kwargs)
        formset.request = request
        formset.can_delete
        formset.can_order
        return formset

class corrispondenzaInline(corrispondenzaFormsetMixin,nested_admin.NestedStackedInline):
    model = corrispondenza
    form = corrispondenzaForm
    extra = 0
    readonly_fields = ( 'link_genera_odt',)
    fieldsets = (
        ('', {
            'classes': ('grp-collapse grp-open',),
            'fields': (
                ('recipiente',),
                ('mittente',),
                ('indirizzo',),
                ('email',),
                ('protComunicazione'),
                ('dataComunicazione'),
                ('note'),
                ('documento'),
            )
        }),
        ('Risposta', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (
                ('MODELLO', 'link_genera_odt')
            )
        }),
    )

    def link_genera_odt(self, obj):
        link = '<a target="_blank" style="cursor: pointer;" target="_blank" onclick="genera_odt(%s,this)">Genera risposta</a>' % obj.pk
        return format_html(link)
    link_genera_odt.short_description = ''
    link_genera_odt.allow_tags = True

#--------------------------------------------------------------
class sottoscrittoriInlineFormset(forms.models.BaseInlineFormSet):

    def applicaModifiche(self,obj):
        #setattr(obj, self.cognome_field, self.cognome_field.upper())
        #setattr(obj, self.nome_field, self.nome_field.title())
        obj.cognomeDenominazione = obj.cognomeDenominazione.upper()
        obj.nome = obj.nome.title()

    def save_new(self, form, commit=True):
        obj = super(sottoscrittoriInlineFormset, self).save_new(form, commit=False)
        self.applicaModifiche(obj)
        if commit:
            obj.save()
        return obj

    def save_existing(self, form, instance, commit=True):
        obj = super(sottoscrittoriInlineFormset, self).save_existing(form, instance, commit=False)
        self.applicaModifiche(obj)
        if commit:
            obj.save()
        return obj


class sottoscrittoriFormsetMixin(object):
    formset = sottoscrittoriInlineFormset
    #cognome_field = "cognomeDenominazione"
    #nome_field = "nome"

    def get_formset(self, request, obj=None, **kwargs):
        formset = super(sottoscrittoriFormsetMixin, self).get_formset(request, obj, **kwargs)
        formset.request = request
        formset.can_delete
        formset.can_order
        #formset.cognome_field = self.cognome_field
        #formset.nome_field = self.nome_field
        return formset


class sottoscrittoriInline(sottoscrittoriFormsetMixin,nested_admin.NestedStackedInline):
    model = sottoscrittori
    verbose_name = "Titolo"
    extra = 0
    fieldsets = (
        ('', {
            'classes': ('grp-collapse grp-open',),
            'fields': (
                ('cognomeDenominazione',),
                ('nome',),
                ('titolo'),
                ('codicefiscale'),
                ('note'),
            )
        }),
    )
    #classes = ('collapse open',)
    #inline_classes = ('collapse open',)


class istruttorieInlineFormset(forms.models.BaseInlineFormSet):

    def save_new(self, form, commit=True):
        obj = super(istruttorieInlineFormset, self).save_new(form, commit=False)
        setattr(obj, self.user_field, self.request.user.username)
        setattr(obj, self.date_field, datetime.now())
        if commit:
            obj.save()
        return obj

    def save_existing(self, form, instance, commit=True):
        obj = super(istruttorieInlineFormset, self).save_existing(form, instance, commit=False)
        setattr(obj, self.user_field, self.request.user.username)
        setattr(obj, self.date_field, datetime.now())
        if commit:
            obj.save()
        return obj

class istruttorieFormsetMixin(object):
    formset = istruttorieInlineFormset
    user_field = "istruttore"
    date_field = "data"
    tipo_field = "tipoIstruttoria"
    accoglimento_field = "tipoIstruttoria"

    def get_formset(self, request, obj=None, **kwargs):

        #def default_matrice(obj):
        #    if not obj:
        #        return forms.HiddenInput(),{}
        #    check_modello_val = valutazione.objects.filter(catIstanza__pk=obj.categoriaIstanza.pk)
        #    if check_modello_val: 
        #        wdgt = JSONEditorWidget(height="200px",options={"mode":"form", "mainMenuBar": False, "navigationBar": False, "statusBar": False})
        #        return wdgt, { regole["label"]: regole["default"] for regole in check_modello_val.first().matrice }
        #    else:
        #        return forms.HiddenInput(),{}

        formset = super(istruttorieFormsetMixin, self).get_formset(request, obj, **kwargs)
        #if "matrice_valutazione" in formset.form.base_fields:
        #    formset.form.base_fields["matrice_valutazione"].widget, formset.form.base_fields["matrice_valutazione"].initial = default_matrice(obj)

        self.parent_obj = obj
        if self.parent_obj and not self.parent_obj.valutazione_ok:
            formset.form.base_fields["accoglimento"].initial = "np"
            formset.form.base_fields["note"].initial = self.parent_obj.descrizione_sintetica

        formset.request = request
        formset.user_field = self.user_field
        formset.date_field = self.date_field
        return formset

class istruttorieInline(istruttorieFormsetMixin,nested_admin.NestedStackedInline):

    model = istruttorie
    readonly_fields = ('rapporto_istruttoria',) #'descrizione_sintetica',
    extra = 0
    fieldsets = (
        ('', {
            'classes': ('grp-collapse grp-open',),
            'fields': (
                ('tipoIstruttoria', 'istruttore', 'data'),
                ('accoglimento','stato_valutazione'),
                ('note',),
                #('matrice_valutazione'),
                ('sintesi', ),
                ('dettaglio',),
                #('descrizione_sintetica'),
                ('rapporto_istruttoria'),
                ('destAssegnata'),
                ('allegati'),
            )
        }),

        ('altre informazioni', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (
                ('vas',),
                ('cartografia_variazione',),
                ('cartografia_versione',),
                ('normativa_versione'),
            )
        }),
    )

    def _formfield_for_dbfield(self, db_field, request, **kwargs):
            print ("formfield_for_dbfield",db_field, request, **kwargs)
            if db_field.name == 'bb':
                kwargs['choices'] =  tipoIstruttoria_ridotta
            return super(istruttorieInline,self).formfield_for_dbfield(db_field, request, **kwargs)


    def _formfield_for_choice_field(self, db_field, request, **kwargs):
            if db_field.name == 'tipoIstruttoria':
                kwargs['choices'] =  tipoIstruttoria_ridotta
            return super(istruttorieInline,self).formfield_for_choice_field(db_field, request, **kwargs)

        

class ControlIstanze(SimpleListFilter):
    title = 'controllo istanze' 
    parameter_name = 'controllo_istanze'

    def lookups(self, request, model_admin):
        return (
            ('avvisi', 'avvisi'),
            ('di_carattere_generale', 'di carattere generale' ),
            ('PI2030', 'PI2030'),
            ('di_carattere_ambientale','di carattere ambientale')
        )

    def queryset(self, request, qs):
        if self.value() == 'avvisi':
            qs = qs.filter(categoriaIstanza__pk__in=[24,25,26,27,28,29])
        elif self.value() == 'PI2030':
            qs = qs.filter(categoriaIstanza__pk__in=[24,25,26,27,28,29,7,22,21])
        elif self.value() == 'di_carattere_ambientale':
            pk_di_carattere_ambientale = []
            for item in qs:
                print (item,item.vas_piu_recente,item.contiene_vas)
                if item.contiene_vas:
                    pk_di_carattere_ambientale.append(item.pk)
            print (pk_di_carattere_ambientale)
            qs = qs.filter(pk__in=pk_di_carattere_ambientale)
        elif self.value() == 'di_carattere_generale':
            qs = qs.exclude(coordCatastali__contains='/')
        return qs

class TutteCategorie(SimpleListFilter):
    title = 'categoria istanza' 
    parameter_name = 'categoriaIstanza'

    def lookups(self, request, model_admin):
        return [[cat.pk,str(cat)] for cat in catIstanze.objects.all()]

    def queryset(self, request, qs):
        if self.value():
            return qs.filter(categoriaIstanza__pk=self.value())
        else:
            return qs

class istanzeForm(forms.ModelForm):

    wkt_geom = forms.CharField(required=False, widget=forms.Textarea( attrs={'rows':'2', 'cols': '10'}), initial='')
    matrice_valutazione = forms.JSONField(required=False, widget=JSONEditorWidget(height="200px",options={"mode":"form", "mainMenuBar": False, "navigationBar": False, "statusBar": False}))
    tags = TagField(required=False, widget=LabelWidget)

    def __init__(self, *args, **kwargs):
        categoria_param = None
        if 'initial' in kwargs:
            filter_param = kwargs['initial'].get("_changelist_filters") #.get("_changelist_filters")
            categoria_param = None
            if filter_param:
                for filter in filter_param.split("&"):
                    if filter.split("=")[0]=="categoriaIstanza":
                        categoria_param = filter.split("=")[-1]
            print ("categoria_param", categoria_param)
        super(istanzeForm, self).__init__(*args, **kwargs)
        if categoria_param:
            categoria_istanza = catIstanze.objects.get(pk = int(categoria_param.split("=")[-1]))
            self.fields['categoriaIstanza'].initial = categoria_istanza
        self.backupCatasto = self.instance.coordCatastali
        self.backupGeom = self.instance.geom
        if self.instance.geom:
            wkt_geom_txt = self.instance.geom.wkt #kwargs['instance'].geom.wkt
            self.fields['wkt_geom'].widget.attrs.update({'placeholder': wkt_geom_txt})
        try:
            self.backupCategoriaIstanza = self.instance.categoriaIstanza
        except repertorioIstanze.categoriaIstanza.RelatedObjectDoesNotExist:
            self.backupCategoriaIstanza = None
        

    def clean_wkt_geom(self):
        wkt_geom = self.cleaned_data['wkt_geom']
        if wkt_geom != '':
            try:
                self.cleaned_data['geom'] = GEOSGeometry(wkt_geom, srid=3003)
            except Exception as e:
                print ("GEOM NON VALIDA", e)
                raise forms.ValidationError("il formato della geometria non è valido")
        return wkt_geom

    class Meta(object):
        model = repertorioIstanze
        exclude = ('', )
        readonly_fields = ('idIstanza', 'destVigente', 'destRichiesta', 'destAssegnata', 'note', 'istanzeCorrelate', 'stato', 'valutazione', 'variazioneCartografica',)

    def save(self, force_insert=False, force_update=False, commit=True):

        def set_correlate(geom):
            correlate = repertorioIstanze.objects.filter(geom__intersects=geom)
            self.instance.istanzeCorrelate.clear()
            for correlata in correlate:
                if correlata != self.instance:
                    self.instance.istanzeCorrelate.add(correlata)
            print (self.instance,self.instance.istanzeCorrelate.all(), file=sys.stderr)

        print ("begin SAVE", file=sys.stderr)

        #procedura per compilare il campo per il reverse link delle comunicazioni correlate all'istanza (DUPLICATA)
        #for comunicazione in self.cleaned_data['comunicazioni']:
        #    print ("COM",comunicazione, file=sys.stderr)
        #    comunicazione.istanza = self.instance
        #    comunicazione.istanza.save()

        if not self.instance.pk or self.instance.categoriaIstanza != self.backupCategoriaIstanza:
            cat = self.cleaned_data['categoriaIstanza']
            self.instance.idIstanza = prendi_primo_id_utile(cat)

        m = super(istanzeForm, self).save(commit=False)
        #if commit:
        m.save()

        #m.comunicazioni.set(self.cleaned_data['comunicazioni'])
        m.save()
        #print ("M", m.comunicazioni, file=sys.stderr)


        if self.cleaned_data.get('coordCatastali', None) != self.backupCatasto:
            if self.cleaned_data.get('coordCatastali', None):
                query,feedback = QueryFromCoordinateCatastali(self.cleaned_data['coordCatastali'])
                print ("QUERY", feedback, query, file=sys.stderr)
                cursor = connections["visio"].cursor()
                cursor.execute(query)
                row = cursor.fetchone()
                self.instance.coordCatastali = feedback
                if row[1]:
                    self.instance.geom = GEOSGeometry(row[1],3003)
                    self.instance.destVigente = destinazioniPRG(self.instance.geom.wkt)[0][:250]
                    set_correlate(self.instance.geom)
                self.instance.save()
        
        elif self.cleaned_data.get('wkt_geom', None):
            try:
                print ('SET GEOM TO WKT')
                new_geom = GEOSGeometry(self.cleaned_data.get('wkt_geom', None), srid=3003)
                if isinstance(new_geom, Polygon):
                    new_geom = MultiPolygon(new_geom)
                print ("obj.the_geom = new_geom - 930", file=sys.stderr)
                self.instance.geom = new_geom
                self.instance.save()
            except Exception as e:
                print(e, file=sys.stderr)
                pass

        elif self.cleaned_data['geom'] != self.backupGeom:
            #se geometria cambiata aggiorna destVigente e Istanze Correlate
            if self.cleaned_data['geom']:
                print ("GEOM:",self.cleaned_data['geom'].wkt, file=sys.stderr)
                #nuove destinazioni di PRG che intersecano la geomtria di istanza
                #self.instance.coordCatastali = ''
                self.instance.destVigente = destinazioniPRG(self.cleaned_data['geom'].wkt)[0][:250]
                #altre istanze correlate che si intersecano fra loro
                set_correlate(self.cleaned_data['geom'])
            else:
                self.instance.destVigente = "Istanza di carattere generale"
                self.instance.istanzeCorrelate.clear()
            self.instance.save()

        #if self.cleaned_data['destVigente']:
        #    self.instance.destVigente = self.cleaned_data['destVigente']
        #    self.instance.save()

        #procedura per compilare il campo per il reverse link delle comunicazioni correlate all'istanza (DUPLICATA)
        #if self.instance and self.instance.comunicazioni:
        #    for comunicazione in self.instance.comunicazioni.all():
        #        print ("COM",comunicazione, file=sys.stderr)
        #        comunicazione.istanza = self.instance
        #        comunicazione.istanza.save()
        print ("end SAVE", file=sys.stderr)
        return m

def istanze_accolte(modeladmin, request, queryset):
    queryset.update(stato='si')
istanze_accolte.short_description = "Filtra solo istanze accolte"

@admin.register(repertorioIstanze)
class padovaGeoAdmin(MassEditMixin, nested_admin.NestedModelAdmin,admin.OSMGeoAdmin,AjaxSelectAdmin):
    list_per_page = 16
    default_lon = 1725155
    default_lat = 5032083
    default_zoom = 13
    max_resolution = 350000
    num_zoom = 28
    max_zoom = 28
    min_zoom = 10
    map_width = 700
    map_height = 500
    map_srid = 3003
    wms_url = "https://rapper.comune.padova.it/qgisserver?MAP=/usr/lib/cgi-bin/servizi/default.qgs"
    wms_layer = 'PI,PI_CS'
    wms_name = 'PI'
    wms_options = {'transparent':"true",'format': 'image/png'}
    #map_template = '/home/urb/Dropbox/dev/istanze/repertorio/templates/admin/repertorio/openlayers_extralayers.html'
    map_template = 'admin/repertorio/openlayers_extralayers.html'
    readonly_fields = (
        'idIstanza', 'destVigente', 'destRichiesta',
        'destAssegnata', 'note', 'istanzeCorrelate',
        'valutazione', 'variazioneCartografica', #'stato', 'stato_valutazione', 
        'link_riferimenti_incrociati', 'link_stampa', 'link_copertina',
        'link_analisi','mappa_sola_lettura', 'trigger_nascondi_geom',
        'geom_edit_button', 'geom_disable_edit_button', 'link_streetview',
        'fake_categoria','ubicazione','coordCatastali_readonly','descrizione_sintetica',
        'analisi','link_gestione_etichette','sintesi_piu_recente','controdeduzione_piu_recente'
    )
    form = istanzeForm
    inlines = [corrispondenzaInline,sottoscrittoriInline,istruttorieInline]
    search_fields = ['idIstanza','ditta','note', 'valutazione', 'sottoscrittori__cognomeDenominazione','analisi','corrispondenza__mittente','corrispondenza__protComunicazione','coordCatastali'] #,'comunicazioni__mittente'
    list_display = ('pk', 'categoriaIstanza', 'idIstanza','ditta','stato','stato_valutazione', 'tag_list')
    list_filter = (TutteCategorie,'stato','stato_valutazione','tags',ControlIstanze)
    ordering = ['-id']
    actions = ['mappa_selezione' , 'export_shp', 'export_shp_comunicazioni', 'export_csv', 'istanze_in_sospeso','istanze_non_valutate',tag_wizard,'export_corrispondenza'] #'istanze_accolte',
    change_form_template = 'admin/repertorio/change_form.html'
    change_list_template = 'admin/repertorio/change_list.html'
    massadmin_exclude = ['tags','matrice_valutazione','istanzeCorrelate','geom','rif']

    class Media:
        js = (
            #"repertorio/js/jquery.js",
            "repertorio/js/my_admin.js",
            "certificati/js/salva_stato_layers.js",
            #'repertorio/mappeDiBase.js',
            #'pua/pua_finestramappa.js',
        )
        css = {
             'all': ('repertorio/css/my_admin.css','pua/pua_finestramappa.css') 
        }

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())

    def get_queryset(self, request):
            qs = super(padovaGeoAdmin, self).get_queryset(request).prefetch_related('tags')
            return qs.exclude(idIstanza=9999999)
    
    def __get_formsets_with_inlines(self, request, obj=None):
        for inline in self.get_inline_instances(request, obj):
            # hide MyInline in the add view
            if isinstance(inline, istruttorieInline):
                fs = inline.get_formset(request, obj)
                #formset.form.base_fields["matrice_valutazione"].widget, formset.form.base_fields["matrice_valutazione"].initial = default_matrice(obj)
            yield inline.get_formset(request, obj), inline
        
    def get_actions(self,request):
        actions = super(padovaGeoAdmin, self).get_actions(request)
        multiple_templates = modelli_doc.objects.filter(multiplo=True).filter(abilitato=True).order_by('-pk')
        for t in multiple_templates:
            action = self.create_action(t.titolo)
            actions["template_action_" + t.titolo] = (
                action,
                action.__name__,
                action.short_description
            )
        return actions

    def get_fieldsets(self, request, obj=None):
        #if obj: # editing an existing object

        if not obj or obj.categoriaIstanza.attiva:
            campo_categoria = 'categoriaIstanza'
        else:
            campo_categoria = 'fake_categoria'

        if request.GET.get('force_edit', '') or not obj:
            loc_mod_fields1 = ("posizione", {
                'classes': ('grp-collapse grp-open',),
                'fields': ('geom','coordCatastali','wkt_geom','geom_disable_edit_button') #'link_analisi'
            })
            loc_mod_fields2 = ("posizione_mod", {
                'classes': ('grp-collapse grp-closed',),
                'fields': ('trigger_nascondi_geom',)
            })

        else:
            loc_mod_fields1 = ("posizione", {
                'classes': ('grp-collapse grp-open',),
                'fields': (('coordCatastali_readonly','link_streetview','link_analisi'),'mappa_sola_lettura','geom_edit_button') #'link_analisi'
            })
            loc_mod_fields2 = ("posizione_mod", {
                'classes': ('posizione-nascosta grp-collapse grp-open',),
                'fields': ('geom','trigger_nascondi_geom')
            })

        admin_form = [
            ("intestazione", {
                'classes': ('grp-collapse grp-open',),
                'fields': (('idIstanza','stato','stato_valutazione', campo_categoria),('tags','link_gestione_etichette'),('sintesi_piu_recente','controdeduzione_piu_recente' ))#,'ditta',
            }),
            ("corrispondenza", {
                'classes': ("grp-collapse grp-open placeholder corrispondenza_set-group",),
                'fields': ('comunicazioni', )
            }),
            ("inlines", {
                'classes': ("grp-collapse grp-open placeholder sottoscrittori_set-group",),
                'fields': ()
            }),
            loc_mod_fields1,
            loc_mod_fields2,
            ('Altri campi', {
                'classes': ('grp-collapse grp-open',),
                'fields': (
                    #'rif',
                    #'destVigente',
                    #'destRichiesta',
                    #'destAssegnata',
                    'ubicazione',
                    'note',
                    #'accordo',
                    'link_riferimenti_incrociati',
                    #'stato',
                    #'valutazione',
                    #'variazioneCartografica',
                    #'link_stampa',
                    'link_copertina')
            }),
        ]

        if obj and valutazione.objects.filter(catIstanza__pk=obj.categoriaIstanza.pk).exists():
            admin_form.append(('Matrice di valutazione', {
                'classes': ('grp-collapse grp-open',),
                'fields': ('matrice_valutazione','descrizione_sintetica')
            }))
        return admin_form

    def mappa_selezione(modeladmin, request, queryset):
        return mapIstanze(request, qs=queryset)
    mappa_selezione.short_description = "Riproduci in mappa le istanze selezionate"

    def export_shp(modeladmin, request, queryset):
        return FileResponse(shapefileFromQueryset(queryset),content_type='application/zip')
    export_shp.short_description = "Esporta come shapefile"

    def export_shp_comunicazioni(modeladmin, request, queryset):
        return FileResponse(shapefileFromQueryset(queryset,exportComunicazioni=True),content_type='application/zip')
    export_shp_comunicazioni.short_description = "Esporta come shapefile con comunicazioni"

    def export_corrispondenza(modeladmin, request, queryset):
        target_path = os.path.join(settings.MEDIA_ROOT,"corrispondenza","export")
        if not os.path.exists(target_path):
            os.makedirs(target_path)
        for f in os.listdir(target_path):
            os.remove(os.path.join(target_path,f))
        for istanza in queryset:
            for comunicazione in istanza.corrispondenza_set.all():
                try:
                    name, ext = os.path.splitext(comunicazione.documento.path)
                except:
                    print ("DOC INESISTENTE:",istanza.idIstanza,comunicazione.protComunicazione)
                    continue
                export_name = "prot#%s_oss#%s%s" % (comunicazione.protComunicazione, str(istanza.idIstanza).zfill(3),ext)
                print (export_name)
                shutil.copyfile(comunicazione.documento.path,os.path.join(target_path,export_name))

        #zipMemory = BytesIO()
        zipname = os.path.join(settings.MEDIA_ROOT,"tmp","export_osservazioni_pervenute.zip")
        zip = ZipFile(zipname, 'w')

        for filename in os.listdir(target_path):
            zip.write(os.path.join(target_path,filename),filename)
            os.remove(os.path.join(target_path,filename))

        #os.rmdir(temp_dir)

        # fix for Linux zip files read in Windows
        for filename in zip.filelist:
            filename.create_system = 0

        zip.close()
        return FileResponse(open(zipname, 'rb'))
        #return HttpResponseRedirect(request.META['HTTP_REFERER'])
    export_corrispondenza.short_description = "Esporta documenti in corrispondenza"

    def export_csv(modeladmin, request, queryset):
        campi = []
        for istanza in get_summary(request, repertorioIstanze.objects.all() ):
            for key,value in istanza.items():
                if not key in campi:
                    campi.append(key)
        response = HttpResponse(content_type='text/csv')
        writer = csv.DictWriter(response, delimiter='\t', fieldnames=campi)
        writer.writeheader()
        writer.writerows( get_summary(request, queryset.order_by("categoriaIstanza", "idIstanza")) )
        return response
    export_csv.short_description = "Esporta come tabella csv"

    def link_analisi(self, obj):
        if obj.coordCatastali:
            param = "?coordinateCatastali=%s" % obj.coordCatastali.replace("[","").replace("]","")
        elif obj.geom:
            param = "?geometria=%s" % obj.geom.wkt
        else:
            param = None
        if param:
            return format_html('<a target="_blank" href="/certificati/catasto/%s">Analizza destinazioni urbanistiche vigenti</a>' % param)
    link_analisi.short_description = ''
    link_analisi.allow_tags = True

    def fake_categoria(self,obj):
        return str(obj.categoriaIstanza)
    fake_categoria.short_description = 'Categoria istanza'

    def coordCatastali_readonly(self,obj):
        return str(obj.coordCatastali)
    coordCatastali_readonly.short_description = 'Coordinate catastali'

    def link_riferimenti_incrociati(self,obj):
        if obj.geom:
            return format_html(riferimenti_incrociati(obj.geom.wkt,exclude_self={"istanze":obj.pk}))
    link_riferimenti_incrociati.short_description = 'Riferimenti ad altre informazioni su rapper'
    link_riferimenti_incrociati.allow_tags = True

    def link_gestione_etichette(self,obj):
        return format_html('<a target="_blank" href="/admin/taggit/tag">Collegamento a gestione delle etichette</a>')
    link_gestione_etichette.short_description = ''
    link_gestione_etichette.allow_tags = True

    def link_streetview(self, obj):
        links = '''<a href="/repertorio/sv/%s/" class="button" target="_blank">Streetview</a>'''
        return format_html(links % obj.pk)
    link_streetview.short_description = ''

    def link_stampa(self, obj):
        if obj.geom:
            WKT = obj.geom.wkt
        else:
            WKT = ''
        link = '<a target="_blank" href="/certificati/stampa_ricerca_mappali/?istanza=%s&categoria=%s&stato=%s&note=%s&valutazione=%s&WKT=%s&particelle=%s">STAMPA</a>' % (
            str(obj),
            obj.categoriaIstanza.tipoIstanza,
            dict(accoglimento_choiches)[obj.stato],
            quote_plus(obj.note),
            quote_plus(obj.valutazione),
            WKT,
            obj.coordCatastali
        )
        try:
            return format_html(link)
        except:
            return ""
    link_stampa.short_description = 'STAMPA DETTAGLIO'
    link_stampa.allow_tags = True

    def link_copertina(self, obj): #https://rapper.comune.padova.it
        service_link = """https://rapper.comune.padova.it/qgisserver
?MAP=/usr/lib/cgi-bin/servizi/PI/STAMPE_atlas.qgs
&SERVICE=WMS
&REQUEST=GetPrint
&TEMPLATE=COPERTINA_{10}
&version=1.3.0
&FORMAT=pdf
&DPI=100
&CRS=EPSG:3003
&map1:EXTENT={0}
&map1:SCALE={1}
&map0:EXTENT={9}
&map0:SCALE=160000
&gen={8}
{7}
        """

        #&id={2}
        #&ditta={3}
        #&note={4}
        #&cat={5}
        #&dest={6}

        if obj.geom and obj.geom.valid and not obj.geom.empty:
            max_extent_x = obj.geom.extent[2]-obj.geom.extent[0]
            max_extent_y = obj.geom.extent[3]-obj.geom.extent[1]
            if max_extent_x < 100 and max_extent_y<100:
                scale_base = 360/2
                scale = '1000'
            if max_extent_x < 330 and max_extent_y<330:
                scale_base = 360/2
                scale = '2000'
            elif max_extent_x < 800 and max_extent_y<800:
                scale_base = 900/2
                scale = '5000'
            elif max_extent_x < 1700 and max_extent_y<1700:
                scale_base = 1800/2
                scale = '10000'
            else:
                scale_base = 3600/2
                scale = '20000'
            extent = ','.join(list(map(lambda x: str(x), [obj.geom.envelope.centroid[0]-scale_base,obj.geom.envelope.centroid[1]-scale_base,obj.geom.envelope.centroid[0]+scale_base,obj.geom.envelope.centroid[1]+scale_base])))
            gen = ""
            apk = "&ATLAS_PK="+str(obj.pk)
            keymap_ext="1718744.622,5024586.231,1733765.093,5038342.145"
            #try:
            #    cdu = getDestinazioniHTMLFromGeometry(obj.geom.wkt,approssimazione=3)
            #except:
            #    cdu = "Errore geometrico: impossibile determinare le intersezioni con la pianificazione vigente"
            
        else:
            extent = "1719517.073,5024482.750,1733017.073,5037982.750"
            keymap_ext="1733765.093,5024586.231,1800000,5040000"
            scale = '75000'
            apk = "&ATLAS_PK="+str(obj.pk)
            #cdu = ""
            gen = "ISTANZA DI CARATTERE GENERALE"
            
        note = obj.note or ''
        if obj.destRichiesta:
            destRichiesta = "Destinazione richiesta: " + obj.destRichiesta
        else:
            destRichiesta = ""

        link = service_link.replace("\n","").format(
            extent,
            scale,
            str(obj.idIstanza or ''),
            obj.ditta or '',
            "",
            obj.categoriaIstanza or ' ',
            destRichiesta,
            apk,
            gen,
            keymap_ext,
            str(scale)
        )

        #cdu_param = '&cdu='+cdu.replace("\n", "").replace('"',"'")
        #if len(link+cdu_param) > 7100:
        #    link = link + cdu_param[:7100-len(link)]+"</td></tr></table></span>"
        #else:
        #    link = link + cdu_param

        getprint_service = '<a target="_blank" href="%s">STAMPA</a>' % link

        return format_html(getprint_service)
    link_copertina.short_description = 'STAMPA COPERTINA'
    link_copertina.allow_tags = True

    def mappa_sola_lettura(self, obj):
        if obj.pk:
            if obj.geom:
                html= '''<div id="map"></div><script>window.onload = loadFinestraMappa('%s')</script>''' % obj.geom.wkt
                return format_html(html)
            else:
                return format_html("ISTANZA DI CARATTERE GENERALE")
        else:
            return ''
    mappa_sola_lettura.short_description = ''

    def trigger_nascondi_geom(self, obj):
        return format_html('''<script>window.onload = hideGeom()</script>''')
    trigger_nascondi_geom.short_description = ''

    def geom_edit_button(self, obj):
        if obj.pk:
            link= '<a href="/admin/repertorio/repertorioistanze/%d/change/?force_edit=true">Abilita la modifica della geometria</a>' % obj.pk
            return format_html(link)
        else:
            return ''
    geom_edit_button.short_description = ''

    def geom_disable_edit_button(self, obj):
        if obj.pk:
            link= '<a href="/admin/repertorio/repertorioistanze/%d/change/?">Disabilita la modifica della geometria</a>' % obj.pk
            return format_html(link)
        else:
            return ''
    geom_disable_edit_button.short_description = ''

    def istanze_accolte(modeladmin, request, queryset):
        print ("ciao nina",request, file=sys.stderr)
        HttpResponseRedirect("/admin/repertorio/repertorioistanze/?stato__startswith=s")
    istanze_accolte.short_description = "Filtra solo istanze accolte e parzialmente accolte"

    def istanze_in_sospeso(modeladmin, request, queryset):
        HttpResponseRedirect("/admin/repertorio/repertorioistanze/?stato__endswith=%3F")
    istanze_in_sospeso.short_description = "Filtra solo istanze in sospeso"

    def istanze_non_valutate(modeladmin, request, queryset):
        HttpResponseRedirect("/admin/repertorio/repertorioistanze/?stato__exact=nv")
    istanze_non_valutate.short_description = "Filtra solo istanze non valutate"
    
    def create_action(self,template_name):
        def report_action(modeladmin, request, queryset):
            return report(modeladmin, request, queryset.order_by('categoriaIstanza', 'idIstanza'), template_name)
        report_action.__name__= "template_action_" + template_name
        report_action.short_description= "creazione documento " + template_name
        return report_action

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('stato','stato_valutazione','valutazione','istanzeCorrelate','destAssegnata','variazioneCartografica', 'accordo')
        return self.readonly_fields

    def save_formset(self, request, form, formset, change):

        def update_istanza(istanza_id):
            riassunto_note = ""
            ultima_valutazione = ""
            ultima_destAssegnata = ""
            ultima_destRichiesta = ""
            ultimo_stato_accoglimento = ""
            ultimo_stato_valutazione = ""
            istanza_obj = repertorioIstanze.objects.get(pk=istanza_id)
            for istruttoria in istruttorie.objects.filter(istanza_id=istanza_id).order_by('pk'):
                if istruttoria.rapporto_istruttoria:
                    riassunto_note += istruttoria.rapporto_istruttoria
                    riassunto_note += '\n'
                ultima_valutazione = istruttoria.dettaglio
                ultima_destAssegnata = istruttoria.destAssegnata
                ultima_destRichiesta = istruttoria.destRichiesta
                ultimo_stato_accoglimento = istruttoria.accoglimento if istruttoria.accoglimento != 'nv' else 'in'
                ultimo_stato_valutazione = istruttoria.stato_valutazione # if istruttoria.stato_valutazione != 'nv' else 'nv'
            istanza_obj.note = riassunto_note
            if ultima_valutazione:
                istanza_obj.valutazione = ultima_valutazione
            if ultima_destAssegnata:
                istanza_obj.destAssegnata = ultima_destAssegnata
            if ultima_destRichiesta:
                istanza_obj.destRichiesta = ultima_destRichiesta
            if ultimo_stato_accoglimento:
                istanza_obj.stato = ultimo_stato_accoglimento
            else:
                istanza_obj.stato = "nv"
            if ultimo_stato_valutazione:
                istanza_obj.stato_valutazione = ultimo_stato_valutazione
            else:
                istanza_obj.stato_valutazione = "nv"
            istanza_obj.save()

        print ("SAVE_FORMSET:",self, change, file=sys.stderr)
        instances = formset.save(commit=False)

        for obj in formset.deleted_objects:
            if 'istanza_id' in obj.__dict__:
                istanza = obj.istanza_id
                obj.delete()
                update_istanza(istanza)
            else:
                obj.delete()

        if instances:
            print ("ISTANCES:", file=sys.stderr)

            for instance in instances:
                print (instance.__dict__, file=sys.stderr)
                if 'mittente' in instance.__dict__:
                    instance.save()
                    instance.istanza.comunicazioni.add(instance)
                    if not instance.istanza.ditta:
                        instance.istanza.ditta = instance.mittente
                    instance.istanza.save()

                if 'cognomeDenominazione' in instance.__dict__:
                    instance.save()
                    instance.istanza.sottoscrizioni.add(instance)
                    instance.istanza.ditta = nomina_istanza(instance.istanza)
                    instance.istanza.save()

                if 'catIstruttoria_id' in instance.__dict__:

                    print ("ISTRUTTORIA", instance.id, instance.__dict__, file=sys.stderr)
                    if instance.id:
                        changed = []
                        dbInstance = istruttorie.objects.get(pk=instance.id)
                        for field in instance.__dict__:

                            try:
                                print (field,getattr(instance, field),getattr(dbInstance, field), file=sys.stderr)
                                if getattr(instance, field) != getattr(dbInstance, field):
                                    changed.append (field)
                            except:
                                pass
                        print ("CAMBIATI",changed, file=sys.stderr)
                    else:
                        changed = ['valutazione','accoglimento', 'stato_valutazione','destAssegnata', 'destRichiesta','dettaglio','variazioneCartografica'] #,"matrice_valutazione"

                    if 'tipoIstruttoria' in changed:
                        if instance.tipoIstruttoria == 3:
                            instance.accoglimento = "in" # se preistruttoria accoglimento è in corso di elaborazione
                        #elif instance.tipoIstruttoria == 4:
                        #    instance.accoglimento = "rp" # se recepimento accoglimento è recepito

                    if 'accoglimento' in changed:
                        instance.istanza.stato = instance.accoglimento
                        #if not instance.accoglimento in ('si','sp','s?'):
                        #    instance.istanza.destAssegnata = ''

                    if 'stato_valutazione' in changed:
                        instance.istanza.stato_valutazione = instance.stato_valutazione
                        if not instance.accoglimento in ('si','sp','s?'):
                            instance.istanza.destAssegnata = ''

                    instance.save()
                    update_istanza(instance.istanza_id)

                instance.save()

        formset.save_m2m()


def get_summary(request, queryset):
    istanze = []
    for item in queryset:
        schema = request.scheme+'://'
        host_complete_url = schema+request.META['HTTP_HOST']
        print (item,item.vas_piu_recente)
        istanza = OrderedDict([
            ("pk", item.pk ),
            ("idIstanza", item.idIstanza ),
            ("catIstanza", str(item.categoriaIstanza) ),
            ("link", host_complete_url + '/admin/repertorio/repertorioistanze/%d/' % item.pk ),
            ("ditta", item.ditta ),
            ("tags", ", ".join([tag for tag in item.tags.names()]) ),
            ("coordCatastali", decodificaCatasto(item.coordCatastali).capitalize() if item.coordCatastali else 'ISTANZA DI CARATTERE GENERALE' ),
            ("destVigente", item.destVigente ),
            ("note", re.sub('\<.*?\>\n','',item.note, flags=re.DOTALL) ),
            ("valutazione", item.valutazione),
            ("vas", item.vas_piu_recente),
            ("superficie", "{:10.2f}".format(item.geom.area) if item.geom else "" ),
            ("stato", item.get_stato_display() ),
            ("stato_valutazione", item.get_stato_valutazione_display() ),
            ("sottoscrittori", (",").join(["%s %s (%s)" % (s.cognomeDenominazione, s.nome, s.titolo) for s in item.sottoscrizioni.all()]) ),
            ("protocollo", (", ").join(["%s del %s" %(str(c.protComunicazione), c.dataComunicazione.strftime("%d/%m/%Y")) for c in item.corrispondenza_set.all()]) ), #corrispondenza.objects.filter(istanza_id=item.pk)
            ("corrispondenza",[]),
            ("iter",[]),
        ])

        for c in item.corrispondenza_set.all():
            istanza["corrispondenza"].append(c.as_dict)

        for i in item.istruttorie_set.all():
            istanza["iter"].append(i.as_dict)

        if item.geom:
            istanza["geom"] = item.geom.wkt
            istanza["xmin"] = item.geom.extent[0]
            istanza["ymin"] = item.geom.extent[1]
            istanza["xmax"] = item.geom.extent[2]
            istanza["ymax"] = item.geom.extent[3]
            istanza["ubicazione"] = item.ubicazione
        else:
            istanza["geom"] = False

        valutazione = item.get_valutazione()
        if valutazione:
            istanza["matrice"] = True
            istanza["istruttore"] = valutazione.istruttore
            #istanza["sintesi"] = valutazione.descrizione_sintetica
            istanza["valutazione"] = valutazione.dettaglio
            istanza["sintesi"] = valutazione.sintesi
            for campo in valutazione.get_matrice_ordinata():
                istanza["ext_"+campo[0]] = campo[1]
        else:
            istanza["matrice"] = False
            
        istanze.append(istanza)
        print (item.pk)
    return istanze


def report(modeladmin, request, queryset, modello_report):
    istanze = get_summary(request, queryset)
    numero_in_lettere = [None, "una", "due", "tre", "quattro", "cinque", "sei", "sette", "otto", "nove", "dieci", "undici", "dodici", "tredici", "quattrodici", "quindici", "sedici", "diciassette", "diciotto", "diciannove", "venti",]
    engine = Renderer()

    @engine.media_loader
    def wms_images_loader(value,mapservice,layers,width,height,dpi,force_extent=None,grow_factor=1.1,mimetype="image/png",filter=None,**kwargs):
        # load from images collection the image with `value` id.
        #mapservice,width,height,dpi,force_extent=None,grow_factor=1.5,mimetype="image/png",filter=None,
        #print ("IMAGE VALUE", value)
        #print ("ARGS",args)
        #print ("KWARGS",kwargs)

        if force_extent:
            xmin,ymin,xmax,ymax = force_extent
        else:
            if value:
                xmin,ymin,xmax,ymax = [value.xmin,value.ymin,value.xmax,value.ymax]
            else:
                return

        if not xmin:
            return

        if filter:
            #filterparam = requests.utils.requote_uri('&FILTER=istanze: "pk" in (%d)' % filter)
            filterparam = '&FILTER=istanze:'+str(filter)
        else:
            filterparam = ""

        
        centerx = int((xmin+xmax)/2)
        centery = int((ymin+ymax)/2)
        max_dim = (xmax-xmin) if (xmax-xmin) > (ymax-ymin) else (ymax-ymin)
        grow_amount = max_dim*grow_factor/2

        if mapservice:
            wms_params = """
MAP=%s
&LAYERS=%s
&SERVICE=WMS
&VERSION=1.3.0
&REQUEST=GetMap
&BBOX=%d,%d,%d,%d
&CRS=EPSG:3003
&WIDTH=%d
&HEIGHT=%d
&FORMAT=%s
&DPI=%d
&MAP_RESOLUTION=100
&FORMAT_OPTIONS=dpi:100
%s
                """ % (mapservice,layers,centerx-grow_amount,centery-grow_amount,centerx+grow_amount,centery+grow_amount,width,height,mimetype,dpi,filterparam)

            image_temppath = tempfile.NamedTemporaryFile().name
            wms_params = wms_params.replace("\n","")
            r = requests.get("https://rapper.comune.padova.it/qgisserver/",params=wms_params, stream=True, verify=False)
            if r.status_code == 200:
                #print ("LOADING",image_temppath,wms_params)
                with open(image_temppath, 'wb') as f:
                    r.raw.decode_content = True
                    shutil.copyfileobj(r.raw, f)  
                return (open(image_temppath, 'rb'), mimetype)
            else:
                print ("HTTP ERROR:",r.status_code,r.text,"https://rapper.comune.padova.it/qgisserver/?"+wms_params)
        

    modello = modelli_doc.objects.get(titolo=modello_report)
    if len(queryset) <= len(numero_in_lettere):
        istanze_tot=numero_in_lettere[len(queryset)]
        istanze_si=numero_in_lettere[len(queryset.filter(stato="si"))]
        istanze_no=numero_in_lettere[len(queryset.filter(stato="no"))]
    else:
        istanze_tot = ""
        istanze_si = ""
        istanze_no = ""

    result = engine.render(modello.modello_odt.path, parametri=modello.parametri,istanze=istanze, istanze_tot=istanze_tot, istanze_si=istanze_si, istanze_no=istanze_no)
    response = HttpResponse(content_type='application/vnd.oasis.opendocument.text')
    nome_file = modello_report.replace(" ", "_")
    response['Content-Disposition'] = 'inline; filename=%s.odt' % nome_file

    with tempfile.NamedTemporaryFile() as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response_content = output.read()
        response.write(response_content)

    return response

    #return ReportGenerator().create_report(modello, istanze, "istruttoria")

    #return render(request, 'pua_mappa.html', {'features':json.dumps(JsonFromQueryset(queryset))})

class sottoscrittoriForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(sottoscrittoriForm, self).__init__(*args, **kwargs)

    class Meta(object):
        model = sottoscrittori
        exclude = ('', )

    def save(self, force_insert=False, force_update=False, commit=True):
        m = super(sottoscrittoriForm, self).save(commit=False)
        self.instance.cognomeDenominazione = self.cleaned_data['cognomeDenominazione'].upper()
        self.instance.nome = self.cleaned_data['nome'].title()
        if commit:
            m.save()
        return m

class sottoscrittoriAdmin(admin.ModelAdmin):
    #inlines = [sottoscrittoriInline,]
    form = sottoscrittoriForm

class modelliForm(forms.ModelForm):
    class Meta:
        model = modelli_doc
        fields = '__all__'
        widgets = {
            'parametri': JSONEditorWidget(height="300px",options={"mode":"tree", "mainMenuBar": False, "navigationBar": False, "statusBar": False}),
        }

@admin.register(modelli_doc)
class modelliAdmin(admin.ModelAdmin):
    form = modelliForm

class matriciForm(forms.ModelForm):
    class Meta:
        model = valutazione
        fields = '__all__'
        widgets = {
            #"matrice": FlatJsonWidget()
            'matrice': JSONEditorWidget(height="600px",options={"mode":"form", "mainMenuBar": False, "navigationBar": False, "statusBar": False}),
        }

@admin.register(valutazione)
class matriciAdmin(admin.ModelAdmin):
    form = matriciForm

admin.site.site_header = '<a class="navbar-brand" href="/">SETTORE URBANISTICA E SERVIZI CATASTALI</a>'

#admin.site.register(modelli_doc)
#admin.site.unregister(User)#
#admin.site.unregister(Group)
#admin.site.register(User, UserAdmin)#
#admin.site.register(Group, GroupAdmin)
#admin.site.register(repertorioIstanze)
#admin.site.register(corrispondenza,corrispondenzaAdmin)
#admin.site.register(sottoscrittori,sottoscrittoriAdmin)
admin.site.register(catIstanze)
#admin.site.register(settori)
#admin.site.register(istruttorie, istruttorieAdmin)
#admin.site.register(catIstruttoria)

@admin.register(corrispondenza)
class corrispondenzAdmin(MassEditMixin, admin.OSMGeoAdmin):
    modifiable = False
