#from django.contrib.gis import forms
import floppyforms as forms
from .models import repertorioIstanze


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

class OSMMultiPolygonWidget(forms.gis.MultiPolygonWidget, forms.gis.BaseMetacartaWidget):
    pass

class MyMultiPolygonWidget(forms.gis.BaseGeometryWidget):
    map_width = 1000
    map_height = 700
    map_srid = 3003
    template_name = 'urb_map.html'
    
    class Media:
        js = (
            'https://openlayers.org/dev/OpenLayers.js',
            'floppyforms/js/MapWidget.js',
        )

#class MyBaseWidget(forms.OpenLayersWidget):
#    map_width = 1000
#    map_height = 700
#    map_srid = 3003


class mapForm(forms.Form):
    geom = forms.gis.MultiPolygonField(widget=MyMultiPolygonWidget)

#class mapForm(forms.Form):
#    geom = forms.MultiPolygonField(widget=MyBaseWidget)
