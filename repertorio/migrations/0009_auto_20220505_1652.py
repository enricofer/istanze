# Generated by Django 3.1.3 on 2022-05-05 14:52

import datetime
from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('repertorio', '0008_auto_20220215_1028'),
    ]

    operations = [
        migrations.AddField(
            model_name='repertorioistanze',
            name='analisi',
            field=models.TextField(blank=True, default='', verbose_name='Nota confidenziali - istruttoria più recente'),
        ),
        migrations.AddField(
            model_name='repertorioistanze',
            name='matrice_valutazione',
            field=jsonfield.fields.JSONField(blank=True, default={}, null=True),
        ),
        migrations.AlterField(
            model_name='repertorioistanze',
            name='categoriaIstanza',
            field=models.ForeignKey(limit_choices_to=models.Q(('termine', None), ('termine__gte', datetime.date(2022, 5, 5)), _connector='OR'), on_delete=django.db.models.deletion.PROTECT, to='repertorio.catistanze', verbose_name='Categoria istanza'),
        ),
        migrations.AlterField(
            model_name='valutazione',
            name='catIstanza',
            field=models.ForeignKey(limit_choices_to=models.Q(('termine', None), ('termine__gte', datetime.date(2022, 5, 5)), _connector='OR'), on_delete=django.db.models.deletion.PROTECT, to='repertorio.catistanze', verbose_name='Categoria istanza'),
        ),
    ]
