# Generated by Django 3.2.11 on 2022-07-01 11:48

import datetime
from django.db import migrations, models
import django.db.models.deletion
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0005_auto_20220424_2025'),
        ('repertorio', '0009_auto_20220505_1652'),
    ]

    operations = [
        migrations.CreateModel(
            name='tagging',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='repertorioistanze',
            name='tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
        migrations.AlterField(
            model_name='istruttorie',
            name='accoglimento',
            field=models.CharField(choices=[('nv', 'non valutata'), ('in', 'in corso di elaborazione'), ('rp', 'chiusa'), ('', ''), ('?', 'in sospeso'), ('?v', 'integrata, da verificare'), ('??', 'informazioni non sufficienti'), ('ri', 'ritirata'), ('', ''), ('no', 'non accoglibile'), ('np', 'non pertinente'), ('su', 'superata da altri provvedimenti'), ('ok', 'valutazione non necessaria'), ('re', 'replicata'), ('', ''), ('si', 'accoglibile'), ('s?', 'accoglibile, con prescrizioni'), ('sp', 'parzialmente accoglibile'), ('s-', 'accoglibile, non recepito')], default='in', help_text="Se tipo = 'verifica/indagine/analisi' lo stato diventa automaticamente 'in corso di elaborazione'", max_length=10, verbose_name='Variazione di stato'),
        ),
        migrations.AlterField(
            model_name='repertorioistanze',
            name='categoriaIstanza',
            field=models.ForeignKey(limit_choices_to=models.Q(('termine', None), ('termine__gte', datetime.date(2022, 7, 1)), _connector='OR'), on_delete=django.db.models.deletion.PROTECT, to='repertorio.catistanze', verbose_name='Categoria istanza'),
        ),
        migrations.AlterField(
            model_name='repertorioistanze',
            name='stato',
            field=models.CharField(blank=True, choices=[('nv', 'non valutata'), ('in', 'in corso di elaborazione'), ('rp', 'chiusa'), ('', ''), ('?', 'in sospeso'), ('?v', 'integrata, da verificare'), ('??', 'informazioni non sufficienti'), ('ri', 'ritirata'), ('', ''), ('no', 'non accoglibile'), ('np', 'non pertinente'), ('su', 'superata da altri provvedimenti'), ('ok', 'valutazione non necessaria'), ('re', 'replicata'), ('', ''), ('si', 'accoglibile'), ('s?', 'accoglibile, con prescrizioni'), ('sp', 'parzialmente accoglibile'), ('s-', 'accoglibile, non recepito')], default='nv', max_length=10),
        ),
        migrations.AlterField(
            model_name='valutazione',
            name='catIstanza',
            field=models.ForeignKey(limit_choices_to=models.Q(('termine', None), ('termine__gte', datetime.date(2022, 7, 1)), _connector='OR'), on_delete=django.db.models.deletion.PROTECT, to='repertorio.catistanze', verbose_name='Categoria istanza'),
        ),
    ]
