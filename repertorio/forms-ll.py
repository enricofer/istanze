from django import forms
from .models import repertorioIstanze
from leaflet.forms.widgets import LeafletWidget


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


class mapForm(forms.Form):
	
    
    class Meta:
        model = repertorioIstanze
        fields = ('geom',)
        widgets = {'geom': LeafletWidget()}

