from email.policy import default
from tkinter.font import families
from tkinter.tix import NoteBook
from django.contrib.gis.db import models
from django.core.validators import RegexValidator
from django.db.models.signals import post_delete
from django.db.models import Q
from django.dispatch import receiver
from jsonfield import JSONField

from django.conf import settings
#from django.utils.encoding import python_2_unicode_compatible
import os
import uuid
import json
import datetime
from time import strftime

from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase
from taggit.models import Tag

from certificati.coordinateCatastali import queryUbicazione

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

ex_accoglimento = [
    ("nv", "non valutata"),
    ("in", "in corso di elaborazione"),
    ("rp", "recepita"),
    ("", ""),
    ("?",  "in sospeso"),
    ("??", "informazioni non sufficienti"),
    ("ri", "ritirata"),
    ("", ""),
    ("no", "non accoglibile"),
    ("np", "non pertinente"),
    ("su", "superata da altri provvedimenti"),
    ("ok", "valutazione non necessaria"),
    ("re", "replicata"),
    ("", ""),
    ("si", "accoglibile"),
    ("s?", "accoglibile, con riserva"),
    ("s?", "accoglibile, con prescrizioni"),
    ("sp", "parzialmente accoglibile"),
]

ex_accoglimento2 = [
    ("nv", "non valutata"),
    ("in", "in corso di elaborazione"),
    ("rp", "chiusa"),
    ("", ""),
    ("?",  "in sospeso"),
    ("?v",  "integrata, da verificare"),
    ("??", "informazioni non sufficienti"),
    ("ri", "ritirata"),
    ("", ""),
    ("no", "non accoglibile"),
    ("np", "non pertinente"),
    ("su", "superata da altri provvedimenti"),
    ("ok", "valutazione non necessaria"),
    ("re", "replicata"),
    ("", ""),
    ("si", "accoglibile"),
    ("s?", "accoglibile, con prescrizioni"),
    ("sp", "parzialmente accoglibile"),
    ("s-", "accoglibile, non recepito"),
]

accoglimento_valutazione_choice = [
    ("?",  "in sospeso"),
    ("?v",  "integrata, da verificare"),
    ("??", "informazioni non sufficienti"),
    ("ri", "ritirata"),
    ("", ""),
    ("no", "non accoglibile"),
    ("np", "non pertinente"),
    ("su", "superata da altri provvedimenti"),
    ("ok", "valutazione non necessaria"),
    ("re", "replicata"),
    ("", ""),
    ("si", "accoglibile"),
    ("s?", "accoglibile, con prescrizioni"),
    ("sp", "parzialmente accoglibile"),
    ("s-", "accoglibile, non recepito"),
]

accoglimento_avanzamento_choice = [
    ("nv", "non valutata"),
    ("in", "in corso di elaborazione"),
    ("rp", "elaborata"),
]

accoglimento = accoglimento_valutazione_choice + accoglimento_avanzamento_choice

tipoIstruttoria_ridotta = [
    (None,'---------------'),
    (7,'istruttoria automatica - non usare'),
    (3,'verifica/indagine/analisi'),
    (8,'valutazione e controllo'),
    (4,'risoluzione istanza'),
]

tipoIstruttoria_estesa = [
    (None,'---------------'),
    (3,'preistruttoria'),
    (1,'parere'),
    (2,'controdeduzione'),
    (4,'recepimento'),
    (5,'sportello'),
    (6,'segnalazione'),
    (7,'avviso pubblico'),
    (8,'valutazione'),
]

# nota di migrazione valori tipoIstruttoria 10/02/2022
# da 1 a 8
# da 2 a 4
# da 5 a 8
# da 6 a 3

def griglia_valutazione ():
    return [
        {"label": "param1", 'default':'False', 'val':'True'},
        {"label": "param2", 'default':'False', 'val':'True'},
        {"label": "param3", 'default':'False', 'val':'True'},
        {"label": "param4", 'default':'False', 'val':'True'},
    ]

#@python_2_unicode_compatible
class corrispondenza(models.Model):

    def update_filename(instance, filename):
        path = "corrispondenza/%s/%s/" % (str(instance.dataComunicazione.year),str(instance.dataComunicazione.month))
        name,extension = os.path.splitext (filename)
        format = instance.protComunicazione +'_' + instance.mittente + extension
        return os.path.join(path, format)

    entrata_uscita_choice = (
        ('e',"ENTRATA"),
        ('u',"USCITA")
    )

    class Meta:
        verbose_name_plural = "Corrispondenze"
        verbose_name = "Comunicazione"

    recipiente = models.CharField(max_length=1,choices=entrata_uscita_choice)
    mittente = models.CharField(verbose_name="Destinatario per risposte",max_length=250,blank=True,null=True,)
    protComunicazione = models.CharField(validators=[RegexValidator(regex='^\d{7}$|^\d{11}$', message="Il protocollo si compone di 7 cifre (o 11 con l'anno)", code='nomatch')],max_length=11)
    dataComunicazione = models.DateField()
    note = models.TextField(blank=True)
    documento = models.FileField(upload_to=update_filename,blank=True)
    istanza = models.ForeignKey('repertorioIstanze',blank=True,null=True, on_delete=models.CASCADE)
    indirizzo = models.CharField(verbose_name="Indirizzo postale per risposte",max_length=250,blank=True,null=True,)
    email = models.CharField(verbose_name="Email per risposte",max_length=50,blank=True,null=True,)

    @property
    def as_dict(self):
        #res = {}
        #for f in self._meta.get_fields():
            #print (dir(f))
            #if hasattr(f,'default'):
            #print (f.name, f.default, f.__str__)
        return {f.name: (getattr(self, f.name) if hasattr(f,'default') else '') or '' for f in self._meta.get_fields()}
            
    def publish(self):
        if self.recipiente == 'u':
            self.mittente = "Settore urbanistica"
        self.save()

    def __str__(self):
        return "%s_%s_%s" % (self.protComunicazione,self.recipiente,self.mittente)

#@python_2_unicode_compatible
class sottoscrittori(models.Model):

    titolo_choice = (
        ('unico proprietario',"unico proprietario"),
        ('comproprietario',"comproprietario"),
        ('proprietari',"proprietari"),
        ('titolare di altro diritto reale',"titolare di altro diritto reale"),
        ('delegato','delegato'),
        ('liquidatore','liquidatore'),
        ('legale rappresentante',"legale rappresentante"),
        ('professionista incaricato',"professionista incaricato"),
        ('portavoce',"portavoce"),
        ('non specificato',"non specificato")
    )

    class Meta:
        verbose_name_plural = "Proponenti"
        verbose_name = "Proponente"

    cognomeDenominazione = models.CharField(max_length=100)
    nome = models.CharField(max_length=100, default='')
    titolo = models.CharField(max_length=40, choices=titolo_choice, default='non specificato')
    codicefiscale = models.CharField(max_length=16,blank=True)
    indirizzo = models.CharField(max_length=100,blank=True)
    email = models.EmailField(blank=True)
    note  = models.CharField(max_length=100,blank=True)
    istanza = models.ForeignKey('repertorioIstanze',blank=True,null=True, on_delete=models.CASCADE)

    def publish(self):
        self.save()


    def __str__(self):
        return self.get_titolo_display() + ': ' + self.cognomeDenominazione+" "+self.nome

#@python_2_unicode_compatible
class catIstanze(models.Model):
    class Meta:
        verbose_name_plural = "Categorie istanze"
        verbose_name = "Categoria istanze"
        ordering = ['-id']

    tipoIstanza = models.CharField(max_length=100)
    descrizione = models.TextField(blank=True)
    termine = models.DateField(blank=True,null=True)

    def publish(self):
        self.save()

    @property
    def attiva(self):
        if self.termine:
            if self.termine >= datetime.date.today():
                return True
            else:
                return False
        else:
            return True

    def __str__(self):
        return str(self.pk)+'_'+self.tipoIstanza

#@python_2_unicode_compatible
class repertorioIstanze(models.Model):
    class Meta:
        verbose_name_plural = "Istanze"

    #accoglimento_avanzamento_choice = accoglimento
    #accoglimento_valutazione_choice = accoglimento

    idIstanza = models.IntegerField()
    categoriaIstanza  = models.ForeignKey('catIstanze', verbose_name=u"Categoria istanza", on_delete=models.PROTECT, limit_choices_to=Q(termine=None)|Q(termine__gte=datetime.date.today()))
    ditta = models.CharField(max_length=100,blank=True)
    riferimento  = models.CharField(max_length=100,blank=True)
    sottoscrizioni = models.ManyToManyField('sottoscrittori',blank=True)
    comunicazioni = models.ManyToManyField('corrispondenza',blank=True)
    rif  = models.ForeignKey('repertorioIstanze',blank=True,null=True, on_delete=models.SET_NULL)
    istruttoria  = models.ForeignKey('istruttorie',blank=True,null=True, on_delete=models.DO_NOTHING)
    riassunto = models.CharField(max_length=250,blank=True)
    coordCatastali = models.CharField(max_length=250,blank=True)
    geom = models.MultiPolygonField(srid=3003,null=True,blank=True)
    note = models.TextField(verbose_name=u"Sintesi Istruttorie", blank=True)
    destVigente = models.CharField(max_length=250,blank=True)
    destRichiesta = models.CharField(max_length=250,blank=True)
    destAssegnata = models.CharField(max_length=250,blank=True,null=True)
    variazioneCartografica = models.IntegerField(blank=True, null=True)
    istanzeCorrelate = models.ManyToManyField('self', symmetrical=True, blank=True,)
    stato = models.CharField(verbose_name=u"Stato di avanzamento",max_length=10,blank=True,choices=accoglimento_avanzamento_choice, default='nv')
    stato_valutazione = models.CharField(verbose_name=u"Stato di valutazione",max_length=10,blank=True,choices=accoglimento_valutazione_choice, default='nv')
    valutazione = models.TextField(verbose_name=u"Istruttoria più recente",blank=True, default='')
    analisi = models.TextField(verbose_name=u"Motivazione - valutazione più recente (campo non più supportato)",blank=True, default='')
    sub_page = models.IntegerField(blank=True, null=True)
    accordo = models.CharField(max_length=4,blank=True, null=True)
    matrice_valutazione = JSONField(default={}, blank=True, null=True)
    tags = TaggableManager()

    @property
    def sintesi_piu_recente(self):
        for istruttoria in self.istruttorie_set.order_by("-pk"):
            if istruttoria.sintesi:
                return istruttoria.sintesi
                break
        return ""

    @property
    def controdeduzione_piu_recente(self):
        for istruttoria in self.istruttorie_set.order_by("-pk"):
            if istruttoria.dettaglio:
                return istruttoria.dettaglio
                break
        return ""

    @property
    def vas_piu_recente(self):
        for istruttoria in self.istruttorie_set.order_by("-pk"):
            if istruttoria.vas:
                return istruttoria.vas
                break
        return ""

    @property
    def contiene_vas(self):
        for istruttoria in self.istruttorie_set.order_by("-pk"):
            if istruttoria.vas:
                return True
                break
        return False

    @property
    def ubicazione(self):
        return queryUbicazione(self.geom.wkt)

    def publish(self):
        print ("PUBLISH: ",self)
        if self.coordCatastali:
            query = self.GeomFromCoordinateCatastali(self.coordCatastali)
            geom = repertorioIstanze.objects.raw(query)
            self.riassunto = geom.wkt
        self.save()
    
    def save(self, *args, **kwargs):
        default = valutazione.objects.filter(catIstanza__pk=self.categoriaIstanza.pk)
        if default:
            reset = False
            if self.matrice_valutazione:
                impronta_default = { regole["label"] for regole in default.first().matrice }
                impronta_corrente = { key for key,value in self.matrice_valutazione.items() }
            
                if impronta_corrente != impronta_default:
                    reset = True
            else:
                reset = True

            if reset:
                self.matrice_valutazione = default.first().definizione

        else:
            self.matrice_valutazione = {}

        super(repertorioIstanze,self).save(*args, **kwargs)
    
    def get_valutazione(self):
        return self.istruttorie_set.exclude(matrice_valutazione__isnull=True).last()

    def stato_decodifica(self):
        return dict(repertorioIstanze.accoglimento_choice)[self.stato]

    def next(self):
        prox = repertorioIstanze.objects.filter(Q(categoriaIstanza=self.categoriaIstanza)&Q(idIstanza__gt=self.idIstanza)).order_by("idIstanza")
        return prox.first() if prox else None

    def previous(self):
        prox = repertorioIstanze.objects.filter(Q(categoriaIstanza=self.categoriaIstanza)&Q(idIstanza__lt=self.idIstanza)).order_by("-idIstanza")
        return prox.first() if prox else None

    def __str__(self):
        if self.ditta:
            nome = self.ditta
        else:
            nome = "senza denominazione - inserire almeno un sottoscrittore"
        return str(self.pk)+'_'+self.categoriaIstanza.tipoIstanza+'-'+str(self.idIstanza) +'_'+ nome

    @property
    def valutazione_ok(self):
        if self.matrice_valutazione:
            return self.estrazione_valutazione(solo_validazione=True)
        else:
            return True

    @property
    def descrizione_sintetica(self):
        return self.estrazione_valutazione(solo_validazione=False)

    def estrazione_valutazione(self,solo_validazione=False):

        def exec_and_return(expression,glob={}):
            prep = f"""globals()["xxxx"] = {expression}"""
            exec(prep,glob)
            return globals()["xxxx"]

        desc = ''
        valida = True

        if self.matrice_valutazione:
            mv = valutazione.objects.filter(catIstanza__pk=self.categoriaIstanza.pk).first()
            if mv:
                gap = []
                for item in mv.matrice:
                    if not item["val"] in ("rank", "data","opt") and item["label"] in self.matrice_valutazione:
                        ex_locals = {"cond":self.matrice_valutazione[item["label"]]}
                        #cond = self.matrice_valutazione[item["label"]]
                        exec("res = cond" + item["val"], None,ex_locals)
                        if not ex_locals["res"]:
                            valida = False
                            gap.append(item["label"])
                if solo_validazione:
                    return valida
                if not valida:
                    desc += "Il controllo dei requisiti dell'istanza ha dato esito negativo poichè non risponde ai seguenti requisiti: \n"
                    if len(gap)<6:
                        desc += ", ".join(gap)
                    else:
                        desc += ", ".join(gap[:2]) + " ... ed altre %d lacune." % (len(gap)-2)
                    desc += "\n"
                else:
                    desc += "Il controllo dei requisiti dell'istanza ha dato esito positivo\n"
                    
                dati_estratti = ""
                for item in mv.matrice:
                    if item["val"] == "data" and item["label"] in self.matrice_valutazione:
                        if isinstance(item["default"],str) or self.matrice_valutazione[item["label"]] > 0:
                            dati_estratti += "%s: %s\n" % (item["label"], str(self.matrice_valutazione[item["label"]]))
                if dati_estratti:
                    desc += "La proposta presenta i seguenti parametri quantitativi:\n" 
                    desc += dati_estratti

                rank = 0
                for item in mv.matrice:
                    if item["val"] == "rank" and item["label"] in self.matrice_valutazione:
                        rank += self.matrice_valutazione[item["label"]]
                if rank > 0:
                    desc += "I requisiti qualitativi dell'istanza portano ad un punteggio complessivo di %d\n" % rank

                opt = []
                for item in mv.matrice:
                    if item["val"] == "opt" and item["label"] in self.matrice_valutazione:
                        if self.matrice_valutazione[item["label"]]:
                            opt.append(item["label"])
                if opt:
                    desc += "e si rileva quanto segue:\n"
                    desc += "\n".join(opt)
                    desc += "\n"
        return desc

#@python_2_unicode_compatible
class settori(models.Model):
    class Meta:
        verbose_name_plural = "Settori"
        verbose_name = "Settore"

    settore = models.CharField(max_length=250)

    def publish(self):
        self.save()

    def __str__(self):
        return self.settore

#@python_2_unicode_compatible
class valutazione(models.Model):
    class Meta:
        verbose_name_plural = "Matrici di valutazione"
        verbose_name = "Matrice di valutazione"

    catIstanza = models.ForeignKey('catIstanze', verbose_name=u"Categoria istanza", on_delete=models.PROTECT, limit_choices_to=Q(termine=None)|Q(termine__gte=datetime.date.today()))
    matrice = models.JSONField(default = griglia_valutazione)

    @property
    def definizione(self):
        return { regole["label"]: regole["default"] for regole in self.matrice }

    def publish(self):
        self.save()

    def __str__(self):
        return 'matrice_' + str(self.catIstanza)


#@python_2_unicode_compatible
class istruttorie(models.Model):
    class Meta:
        verbose_name_plural = "Istruttorie"
        verbose_name = "Istruttoria"


    def update_filename(instance, filename):
        path = "istruttorie/%s/" % str(instance.pk)
        if not os.path.exists(path):
            os.makedirs(path)
        return os.path.join(path, filename)

    #accoglimento_avanzamento_choice = accoglimento
    #accoglimento_valutazione_choice = accoglimento

    tipoIstruttoria_choice = tipoIstruttoria_ridotta

    catIstruttoria = models.ForeignKey('catIstruttoria',blank=True,null=True, help_text="Non più utilizzato", on_delete=models.SET_NULL)
    tipoIstruttoria = models.IntegerField(verbose_name='Tipo di Istruttoria', default=8, choices=tipoIstruttoria_choice)
    istanza = models.ForeignKey('repertorioIstanze',blank=True,null=True, on_delete=models.CASCADE)
    istruttore = models.CharField(help_text="se vuoto viene inserito automaticamente il nome dell'utente attualmente connesso", max_length=100,blank=True)
    accoglimento = models.CharField(
        verbose_name=u"Variazione di stato di avanzamento",
        max_length=10,choices=accoglimento_avanzamento_choice,
        help_text="Se tipo = 'verifica/indagine/analisi' lo stato diventa automaticamente 'in corso di elaborazione'",
        default='in')
    stato_valutazione = models.CharField(
        verbose_name=u"Variazione di stato di valutazione",
        max_length=10,choices=accoglimento_valutazione_choice,
        #help_text="Se tipo = 'verifica/indagine/analisi' lo stato diventa automaticamente 'in corso di elaborazione'",
        default='nv')
    data = models.DateField(help_text="se vuoto viene inserita automaticamente la data odierna", blank=True)
    dettaglio = models.TextField(help_text="Testo di risposta per le schede di controdeduzione - Parte contenente la determinazione della risposta all'istanza", verbose_name=u"Risposta: parte dispositiva", blank=True)
    destAssegnata = models.CharField(verbose_name=u"Destinazioni assegnate", max_length=250, blank=True)
    destRichiesta = models.CharField(verbose_name=u"Destinazioni richieste", max_length=250, blank=True)
    variazioneCartografica = models.IntegerField(verbose_name=u"Numero variazione", help_text="Numero di riferimento variazione cartografica per varianti complesse", blank=True, null=True)
    cartografia_variazione = models.CharField(verbose_name=u"'SIT.variante_prg_w'", max_length=30, blank=True, null=True)
    cartografia_versione = models.CharField(verbose_name=u"Versione cartografica", max_length=30, blank=True, null=True)
    normativa_versione = models.CharField(verbose_name=u"Versione normativa", max_length=30, blank=True, null=True)
    note = models.TextField(help_text="Annotazioni confidenziali da conservare come memoria del procedimento", verbose_name=u"Note confidenziali", blank=True)
    sintesi = models.TextField(help_text="Testo di risposta per le schede di controdeduzione - Parte contenente la sintesi descrittiva della controdeduzione. ", verbose_name=u"Risposta: parte sintesi", blank=True, null=True)
    allegati = models.FileField(upload_to=update_filename, blank=True, null=True)
    matrice_valutazione = JSONField(default={}, blank=True, null=True)
    vas = models.TextField(help_text="Testo di risposta per le schede di controdeduzione - Parte contenente la valutazione ambientale della controdeduzione proposta. ", verbose_name=u"Controdeduzione: parte valutazione ambientale", blank=True, null=True)

    @property
    def as_dict(self):
        return {f.name: (getattr(self, f.name) if hasattr(f,'default') else '') or '' for f in self._meta.get_fields()}

    @property
    def descrizione_sintetica(self):

        def exec_and_return(expression,glob={}):
            prep = f"""globals()["xxxx"] = {expression}"""
            exec(prep,glob)
            return globals()["xxxx"]

        desc = ''

        if self.matrice_valutazione:
            mv = valutazione.objects.filter(catIstanza__pk=self.istanza.categoriaIstanza.pk).first()
            if mv:
                valida = True
                gap = []
                for item in mv.matrice:
                    if not item["val"] in ("rank", "data","opt") and item["label"] in self.matrice_valutazione:
                        ex_locals = {"cond":self.matrice_valutazione[item["label"]]}
                        #cond = self.matrice_valutazione[item["label"]]
                        exec("res = cond" + item["val"], None,ex_locals)
                        if not ex_locals["res"]:
                            valida = False
                            gap.append(item["label"])
                if not valida:
                    desc += "Il controllo dei requisiti dell'istanza ha dato esito negativo rilevando le seguenti le seguenti lacune: \n"
                    if len(gap)<4:
                        desc += ", ".join(gap)
                    else:
                        desc += ", ".join(gap[:2]) + " ... ed altre %d lacune." % (len(gap)-3)
                    desc += "\n"
                else:
                    desc += "Il controllo dei requisiti dell'istanza ha dato esito positivo\n"
                    
                dati_estratti = ""
                for item in mv.matrice:
                    if item["val"] == "data" and item["label"] in self.matrice_valutazione:
                        if isinstance(item["default"],str) or self.matrice_valutazione[item["label"]] > 0:
                            dati_estratti += "%s: %s\n" % (item["label"], str(self.matrice_valutazione[item["label"]]))
                if dati_estratti:
                    desc += "La proposta presenta i seguenti parametri quantitativi:\n" 
                    desc += dati_estratti

                rank = 0
                for item in mv.matrice:
                    if item["val"] == "rank" and item["label"] in self.matrice_valutazione:
                        rank += self.matrice_valutazione[item["label"]]
                if rank > 0:
                    desc += "I requisiti qualitativi dell'istanza portano ad un punteggio complessivo di %d\n" % rank

                opt = []
                for item in mv.matrice:
                    if item["val"] == "opt" and item["label"] in self.matrice_valutazione:
                        if self.matrice_valutazione[item["label"]]:
                            opt.append(item["label"])
                if opt:
                    desc += "e si rileva quanto segue:\n"
                    desc += "\n".join(opt)
                    desc += "\n"
        return desc

    @property
    def rapporto_istruttoria(self):
        ds = self.descrizione_sintetica
        if (not ds and not self.dettaglio and not self.note):
            return ""
        

        if self.tipoIstruttoria == 7:
            try:
                i = "<inserimento da portale Istanze on line>\n"
                dati_inseriti = json.loads(self.note)
                for key in ["destinazione_pi_richiesta", "destinazioni_vigenti_rilevate", "motivazioni_richiesta", "esigenze_familiari", "destinazione_residenziale_proposta"]:
                    if dati_inseriti.get(key):
                        i += "%s: %s; " % (key.replace("_"," "),dati_inseriti.get(key))
                i += "\n"
            except Exception as e:
                print ("errore di decodifica json", e)
                i = ""
        else:
            i = "<Istruttoria n. %s di %s del %s>\n" % (self.pk, self.istruttore, self.data.strftime('%d %b %Y'))

        if self.tipoIstruttoria != 7 and self.sintesi:
            i += "Si osserva che:\n"
            i += self.sintesi.replace("\r\n\r\n", "; ")
            i += "\n"

        if ds:
            i += ds
            i += "\n"
        
        if self.dettaglio:
            i += "Valutazione:\n"
            i += self.dettaglio.replace("\r\n\r\n", "; ")
            i += "\n"
        return i

    def get_matrice_ordinata(self):
        rows = []
        if self.matrice_valutazione:
            mv = valutazione.objects.filter(catIstanza__pk=self.istanza.categoriaIstanza.pk).first()
            #print ("MATRICE ASSOCIATA",mv)
            if mv:
                for item in mv.matrice:
                    #print (item["label"],self.matrice_valutazione.get(item["label"]))
                    rows.append( [item["label"], self.matrice_valutazione.get(item["label"])] )
        return rows
    
    def save(self, *args, **kwargs):
        self.update_analisi_valutazione()
        super(istruttorie,self).save(*args, **kwargs)
    
    def delete(self, *args, **kwargs):
        super(istruttorie,self).delete(*args, **kwargs)
        self.update_analisi_valutazione()
    
    def update_analisi_valutazione(self):
        istruttorie_related = istruttorie.objects.filter(istanza__pk=self.istanza.pk).order_by("-pk")
        mod = False
        for istruttoria in istruttorie_related:
            if istruttoria.note:
                self.istanza.analisi = istruttoria.note
                self.istanza.save()
                mod = True
                break
        if not mod:
            self.istanza.analisi = ""
            self.istanza.save()
            mod = False

        mod = False
        last_sintesi = ""
        last_dettaglio = ""
        for istruttoria in istruttorie_related:
            if istruttoria.sintesi:
                last_sintesi = istruttoria.sintesi
            if istruttoria.dettaglio:
                last_dettaglio = istruttoria.dettaglio
        
        if last_sintesi or last_dettaglio:
            self.istanza.valutazione = \
                (last_sintesi or "") + \
                ('\n\n' if istruttoria.sintesi and last_dettaglio else "") + \
                (last_dettaglio or "")
            self.istanza.save()
            mod = True
        if not mod:
            self.istanza.valutazione = ""
            self.istanza.save()

    def accoglimento_decodifica(self):
        return dict(istruttorie.accoglimento_choice)[self.accoglimento]

    def __str__(self):
        return str(self.pk) +'_'+self.data.strftime('%Y-%m-%d')+ '_' + self.istruttore


#@python_2_unicode_compatible
class catIstruttoria(models.Model):
    class Meta:
        verbose_name_plural = "Categorie istruttorie"
        verbose_name = "Categoria istruttorie"

    catIstruttoria = models.CharField(max_length=250)

    def publish(self):
        self.save()

    def __str__(self):
        return str(self.pk)+'_'+self.catIstruttoria


class modelli(models.Model):

    def update_filename(self, filename):
        path = os.path.join('modelli/', uuid.uuid4().hex)
        if not os.path.exists(os.path.join(settings.MEDIA_ROOT, path)):
            os.makedirs(os.path.join(settings.MEDIA_ROOT, path))
        return os.path.join(path,filename)

    verbose_name_plural = "Modelli"
    verbose_name = "Modello"
    titolo = models.CharField(max_length=25)
    descrizione = models.TextField(verbose_name="Oggetto della risposta")
    abilitato = models.BooleanField(verbose_name="Modello abilitato",default=True)
    multiplo = models.BooleanField(verbose_name="Modello di documento per istanze multiple",default=False)
    parametri = JSONField(default={}, blank=True, null=True)
    modello_odt = models.FileField(upload_to=update_filename,)

    def __str__(self):
        return "%d_%s%s" % (self.pk,self.titolo,"_non_abilitato" if not self.abilitato else "")

    class Meta:
        verbose_name_plural = "Modelli precompilati di documento"
        verbose_name = "Modello precompilato"

@receiver(post_delete, sender=modelli)
def submission_delete(sender, instance, **kwargs):
    instance.modello_odt.delete(False)