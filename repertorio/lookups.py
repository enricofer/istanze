from ajax_select import register, LookupChannel
from selectable.base import LookupBase,ModelLookup
from selectable.registry import registry
from .models import settori,corrispondenza


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3" 


@register('corrispondenza')
class corrispondenzaLookup(LookupChannel):

    model = corrispondenza

    def get_query(self, q, request):
        return self.model.objects.filter(name=q)

    def format_item_display(self, item):
        return u"<span class='corrispondenza'>%s</span>" % item.mittente

class oldsettoreLookup(LookupBase):
    def get_query(self, request, term):
        data = [
            "Settore urbanistica e servizi catastali",
            "Settore ambiente", "Settore edilizia privata",
            "Settore gabinetto del Sindaco e relazioni esterne",
            "Settore urbanistica e servizi catastali",
            "Settore urbanistica e servizi catastali",
            "Settore patrimonio e partecipazioni",
        ]
        return [x for x in data if x.startswith(term)]

class settoreLookup(ModelLookup):
    model = settori
    search_fields = ('settore', )

class tipoIstanzaLookupOld(LookupBase):
    def get_query(self, request, term):
        data = [
        "Richiesta di modifica del Piano degli interventi",
        "Osservazione ai sensi LR 12/2004",
        "Riclassificazione di area edificabile",
        ]
        return [x for x in data if x.startswith(term)]

class tipoIstanzaLookup(LookupBase):
    def get_item_value(self, item):
        return [x for x in data if x.startswith(term)]

registry.register(settoreLookup)
registry.register(tipoIstanzaLookup)
