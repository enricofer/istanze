
from django.core.exceptions import PermissionDenied
import socket

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

def localhost_only(function):

    def wrap(request, *args, **kwargs):
        print("META",request.META)
        try:
            if request.META['REMOTE_ADDR']:
                remote = socket.gethostbyaddr(request.META['REMOTE_ADDR'])
            else:
                remote = socket.gethostbyaddr(request.META['HTTP_X_REAL_IP']) #forwarded ip
        except:
            remote = ('error','error','error')
        localhost = socket.gethostbyaddr("127.0.0.1")
        intranet = socket.gethostbyaddr("10.10.21.50")
        print (remote,localhost,intranet)
        if remote[0] == localhost[0] or remote[0] == intranet[0]:
            return function(request, *args, **kwargs)
        else:
            print ("Only localohost connections: %s, %s, %s" % (remote[0], localhost[0], intranet[0]))
            raise PermissionDenied

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap