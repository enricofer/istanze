from django.apps import AppConfig


class WarpConfig(AppConfig):
    name = 'django_warp'
