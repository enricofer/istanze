
from .models import CDU
from django.template.loader import render_to_string
from .coordinateCatastali import (
    verificaInCentroStorico,
    GeomFromCoordinateCatastali, 
    queryPRG,queryZONOM, queryCS, queryPI2030,
    queryPAT, queryPRA, queryPGRA, 
    destinazioniPRG, queryPUA, queryBonifiche

)
from vincoli.models import vincoli, stato_choice, dettvinc_choice, riflegis_choice

import html
import sys

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

def decodificaCatasto(coordinate_catastali):
    FMs = coordinate_catastali.split(' ',)
    out = ''
    for FM in FMs:
        decodeFM = FM.split('/')
        if decodeFM[0] != FM:
            if len(decodeFM[1].split('-')) == 1:
                mappaleDesc = 'MAPPALE'
            else:
                mappaleDesc = 'MAPPALI'
            out += ' FOGLIO '+ decodeFM[0]
            out += ' %s %s'% (mappaleDesc,decodeFM[1])
    return out

def destinazioni_articoli (tab, strict=False):
    if tab:
        dest = ''
        art = ''
        for riga in tab:
            if riga[0]:
                if strict and not riga[3]:
                    continue
                dest += riga[0].replace("ZTO","Zona Territoriale Omogenea") + ', '
                if riga[3]:
                    art += riga[3]+', '
        if dest:
            dest = dest[:-2]#+";"
        if art:
            art = art[:-2]
        dest = " ".join(dest.split())# elimina i doppi spazi
        intro = "gli articoli " if "," in art else "ll'articolo "
        return (dest, intro + art)
    else:
        return ("","")

def generaTestoCertificato(CDUId):

    editCDU = CDU.objects.get(pk=CDUId)
    poligono_richiesto = editCDU.poligono_richiesto
    coordinate_catastali = editCDU.coordinate_catastali

    #estrazione testo certificato

    if verificaInCentroStorico(poligono_richiesto.wkt):
        tCS = queryCS(poligono_richiesto.wkt, pre=None)
        prg_cs_dest, prg_cs_nta = destinazioni_articoli(tCS )
    else:
        prg_cs_dest = None
        prg_cs_nta =  None

    tPRG = queryPRG(poligono_richiesto.wkt, pre=None, certificato=True)
    prg_vigente_dest,prg_vigente_nta = destinazioni_articoli(tPRG)

    ZONOM = queryZONOM(poligono_richiesto.wkt, pre=None)
    if ZONOM[0]:
        zona_omogenea_txt = "\nche, ai fini della fruizione delle detrazioni d’imposta per gli interventi finalizzati al recupero o restauro della facciata esterna degli edifici esistenti, l'area è classificata come zona omogenea %s;" % ZONOM[0]
    else:
        zona_omogenea_txt = ""

    variante_in_salvaguardia = None

    tPI2030 = queryPI2030(poligono_richiesto.wkt, pre=None)
    prg_salvaguardia_dest, prg_salvaguardia_nta = destinazioni_articoli(tPI2030,strict=True)

    tPAT = queryPAT(poligono_richiesto.wkt, pre=None)
    pat_dest,pat_nta = destinazioni_articoli(tPAT)

    tPGRA = queryPGRA(poligono_richiesto.wkt, pre=None)
    dest = ''
    if tPGRA:
        for riga in tPGRA:
            dest += riga[0]+', '
        pgra = dest[:-2] + ";"
    else:
        pgra = None

    tPRA = queryPRA(poligono_richiesto.wkt, pre=None)
    dest = ''
    if tPRA:
        for riga in tPRA:
            dest += riga[0] 
        pra = "che " + dest + ";"
    else:
        pra = None

    tPUA = queryPUA(poligono_richiesto.wkt, pre=None)
    dest = []
    articoli = ''
    for riga in tPUA:
        dest.append({'nome':riga[0],'provvedimento':riga[1]})
        articoli += riga[3]+', '
    if articoli:
        articoli = articoli[:-2]
    if dest != []:
        pua = dest
    else:
        pua = None

    tBonifiche = queryBonifiche(poligono_richiesto.wkt, pre=None)
    bonifiche = []
    for bonifica in tBonifiche:
        testo = "che l'area è interessata da procedimento n. %s di bonifica ambientale ai sensi del D.M. n 471/99 e D.Lgs 152/2006 così classificato: %s" % (bonifica[3] ,bonifica[0]) 
        bonifiche.append(testo)

    vincoli_correlati = vincoli.objects.filter(geom__intersects=poligono_richiesto)
    vincoli_desc = ""
    if vincoli_correlati:
        for vincolo in vincoli_correlati:
            if vincolo.stato in [1,2,7]:
                dettvinc = dict(dettvinc_choice)[vincolo.dettvinc][3:]
                riflegis = dict(riflegis_choice)[vincolo.riflegis][3:]
                vincoli_desc += "che l'area è interessata da %s ex %s con denominazione %s; " % (dettvinc, riflegis, vincolo.denominazione)

    contenuto_certificato = render_to_string( "contenuto.tpl",{
        'prg_vigente_dest': prg_vigente_dest,
        'prg_vigente_nta': prg_vigente_nta,
        'inCentroStorico': verificaInCentroStorico(poligono_richiesto.wkt),
        'prg_cs_dest': prg_cs_dest,
        'prg_cs_nta': prg_cs_nta,
        'variante_in_salvaguardia': variante_in_salvaguardia,
        'prg_salvaguardia_dest': prg_salvaguardia_dest,
        'prg_salvaguardia_nta': prg_salvaguardia_nta,
        'pat_dest': pat_dest,
        'pat_nta': pat_nta,
        'vincoli': vincoli_desc,
        'zonom': zona_omogenea_txt,
        'pgra': pgra,
        'pra': pra,
        'piani_guida': None,
        'pua': pua,
        'bonifiche': bonifiche,
        'descrizione_catastale': decodificaCatasto(coordinate_catastali)
    })


    return html.unescape(contenuto_certificato)
