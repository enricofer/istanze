from django.apps import AppConfig


class CertificatiConfig(AppConfig):
    name = 'certificati'
