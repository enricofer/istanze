from django import template

register = template.Library()

@register.filter(name='breakline')
def breakline(value):
    try:
        return value.replace(' - ','\n')
    except:
        return value