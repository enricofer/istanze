import urllib
import requests
import json
import sys
import os
import uuid
import esrijson
from shapely.wkt import loads

from django.contrib.gis.geos import Polygon, GEOSGeometry
from django.template.loader import render_to_string
from django.conf import settings

#PDF handling
from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter

from .importExport import cartawebExport

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

CTR1996_DEF = {
        "id":"Ctr 1996",
        "title":"Ctr 1996",
        "opacity":0.69921875,
        #"visibleLayers":[0,2,3,4,5,6,7,8,10,11,13],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/CTR_PRG/MapServer/" #CTR_PRG
        #"url":  "https://oscar.comune.padova.it/server/rest/services/ctr_1996/MapServer/"
    }

CTR_PRG_DEF = {
        "id":"Ctr 1996",
        "title":"Ctr 1996",
        "opacity":0.69921875,
        "visibleLayers":[0,2,3,4,5,6,7,8,10,11,13],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/CTR_PRG/MapServer/" #CTR_PRG
    }

CATASTO_DEF = {
        "id":"Catasto",
        "title":"Catasto",
        "opacity":0.69921875,
        "visibleLayers":[4,6,7],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/catasto/MapServer/"
    }

PI_DEF = {
        "id":"Ctr 1996",
        "title":"Ctr 1996",
        "opacity":0.69921875,
        "visibleLayers":[2,3,4,5,6],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/prg/MapServer"
    }

DECA_DEF = {
        "id":"PI Decaduto",
        "title":"PI Decaduto",
        "opacity":0.69921875,
        "visibleLayers":[0],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/nessuno_sfondo/MapServer" #PI_decaduto
    }

TOPO_DEF = {
        "id":"Topo",
        "title":"Topo",
        "opacity":0.69921875,
        "visibleLayers":[0],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/base/MapServer"
    }

PI_CS_DEF = {
        "id":"PI Centro Storico",
        "title":"PI Centro Storico",
        "opacity":0.69921875,
        "visibleLayers":[3,4,6,7,9,10,11,12,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/pr1000/MapServer"
    }

PAT_TRASFORMABILITA_DEF = {
        "id":"Pat trasformabilita",
        "title":"Pat trasformabilita",
        "opacity":0.69921875,
        "visibleLayers":[36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/pat/MapServer"
    }

PAT_VINCOLI_DEF = {
        "id":"Pat vincoli",
        "title":"Pat vincoli",
        "opacity":0.69921875,
        "visibleLayers":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/pat/MapServer"
    }

PAT_FRAGILITA_DEF = {
        "id":"Pat invarianti",
        "title":"Pat invarianti",
        "opacity":0.69921875,
        "visibleLayers":[30,31,32,33,34],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/pat/MapServer"
    }

PAT_INVARIANTI_DEF = {
        "id":"Pat fragilita",
        "title":"Pat fragilita",
        "opacity":0.69921875,
        "visibleLayers":[21,22,23,24,25,26,27,28],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/pat/MapServer"
    }

PAT_AUC_DEF = {
        "id":"Pat fragilita",
        "title":"Pat fragilita",
        "opacity":0.69921875,
        "visibleLayers":[70],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/pat/MapServer"
    }

ZONOM_DEF = {
        "id":"Zone omogenee",
        "title":"Zone omogenee dm 1444/68",
        "opacity":0.69921875,
        "visibleLayers":[0],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/zonom/MapServer"
    }

PAI_DEF = {
        "id":"PAI",
        "title":"PAI",
        "opacity":0.69921875,
        "visibleLayers":[0,1],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/pai/MapServer"
    }

BONIFICA_DEF = {
        "id":"Bonifica",
        "title":"Bonifica",
        "opacity":0.69921875,
        "visibleLayers":[23],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/varie/MapServer"
    }

ORTOFOTO = {
        "id":"Ortofoto",
        "title":"Ortofoto",
        "opacity":1,
        "visibleLayers":[0],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/foto/MapServer"
    }

AGEA2015 = {
        "id":"Ortofoto",
        "title":"Ortofoto",
        "opacity":1,
        "visibleLayers":[0],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/agea_2015/MapServer"
    }

DBT2007 = {
        "id":"Db topografico",
        "title":"Db topografico",
        "opacity":1,
        "visibleLayers":[0],
        "minScale": 0,
        "maxScale": 0,
        "url":  "https://oscar.comune.padova.it/server/rest/services/dbt/MapServer"
    }

mapExtent_template = '{"xmin":%s,"ymin":%s,"xmax":%s,"ymax":%s,"spatialReference":{"wkid":3003}}'

printOutput_templates = {
    'a4':'A4 Portrait', #'No TB A4 Portrait',
    'a3':'A3 Portrait', #'No TB A3 Portrait',
    'a2':'MAP_ONLY', #'No TB A2 Portrait',
    'a1':'',
    'a0':'',
    'a4l':'A4 Landscape', #'No TB A4 Landscape',
    'a3l':'A3 Landscape', #'No TB A3 Landscape',
    'a2l':'MAP_ONLY', #'No TB A2 Landscape',
    'a1l':'',
    'a0l':'',


}

layoutElements_template = '[{"xOffset":0,"yOffset":0,"symbol":{"text":"%s","textSymbol":{"text":null,"color":{"red":0,"blue":0,"green":0,"alpha":255},"yoffset":0,"font":null,"xoffset":0,"type":"agsJsonSymbol","borderLineColor":null,"angle":0},"type":"pchTextElement"},"visible":true,"width":null,"geometry":{"x":0.6,"y":2.0},"name":"myScaleBar","height":null,"anchor":"topleft"},{"xOffset":0,"yOffset":0,"symbol":{"mapUnitLabel":"","numberFormat":{"roundingValue":3,"roundingOption":0},"pageUnitLabel":"","pageUnits":9,"separator":":","type":"pchScaleText","mapUnits":8,"backgroundColor":{"red":255,"blue":255,"green":255,"alpha":255},"style":0},"visible":true,"width":"","geometry":{"x":0.6,"y":1.5},"name":"myScaleBar","height":"","anchor":"topleft"}]'

proto_ESRI_style = {
        "color": {
            "red": 255,
            "green": 255,
            "blue": 255,
            "alpha": 0
        },
        "style": "esriSFSSolid",
        "type": "esriSFS",
        "outline": {
            "color": {
                "blue": 0,
                "red": 255,
                "green": 0,
                "alpha": 255
            },
            "width": 1.5,
            "style": "esriSLSSolid",
            "type": "esriSLS"
        }
    }

if settings.BASE_DEV == '/home/enrico/':
    base_url = 'http://cartografia.comune.padova.it/'
else:
    base_url = 'https://oscar.comune.padova.it/'

def get_best_fit_scale(polygon,scales,paper):

    paper_sizes = {
        'a4': (210,297),
        'a3': (297,420),
        #'a2': (420,594),
        #'a1': (594,840),
        #'a0': (840,1188),
        'a4l': (297,210),
        'a3l': (420,297),
        #'a2l': (594,420),
        #'a1l': (840,594),
        #'a0l': (1188,840),
    }
    center = polygon.centroid
    sheet = paper_sizes[paper]
    margin = 10
    #mapUnitsPerMillimeter = [0.5,1,2,5,10]
    cx = center.coords[0]
    cy = center.coords[1]
    for scale in scales:
        scaleFactor = scale / 1000
        wb = sheet[0] * scaleFactor / 2
        hb = sheet[1] * scaleFactor / 2
        wf = (sheet[0] - margin*2) * scaleFactor / 2
        hf = (sheet[1] - margin*2) * scaleFactor / 2
        bound = Polygon(((cx-wb,cy-hb), (cx+wb,cy-hb), (cx+wb,cy+hb), (cx-wb,cy+hb), (cx-wb,cy-hb)))
        frame = Polygon(((cx-wf,cy-hf), (cx+wf,cy-hf), (cx+wf,cy+hf), (cx-wf,cy+hf), (cx-wf,cy-hf)))
        if frame.contains(polygon):
            return bound, frame, scale
    return bound, frame, scaleFactor

def get_esri_ring(extent):
    xmin, ymin, xmax, ymax = extent
    ring = [
        [xmin, ymin],
        [xmax, ymin],
        [xmax, ymax],
        [xmin, ymax],
        [xmin, ymin],
    ]
    return ring

def get_pataplan_print(CDU, mapServices, title, dpi, outline, fill, width, scales, paper, overwrite=True, ESRIJSON=None, WKT=None, output_dir=None):
    '''
    se CDU è None allora è una richiesta di stampa per un poligono specificato in ESRI/WKT
    '''
    newmss = []
    for mp in mapServices:
        lyrdef = []
        if "visibleLayers" in mp:
            for l_id in mp["visibleLayers"]:
                lyrdef.append ({
                    "id": l_id,
                    "layerDefinition":{
                        "source":{
                            "type":"mapLayer",
                            "mapLayerId":l_id
                        }
                    }
                })
            mp["layers"] = lyrdef
            del mp['visibleLayers']
            newmss.append(mp)
    mapServices = newmss

    label = title
    if CDU:
        prot = str(CDU.protocollo_data.year)+CDU.protocollo_numero
        OUT_dir = os.path.join(settings.MEDIA_ROOT,'certificati',CDU.tipo,prot)
        #multipolygon_esri = json.loads(CDU.poligono_richiesto_esri)
        multipolygon_wkt = CDU.poligono_richiesto
        WKT = multipolygon_wkt.wkt
        #get_esrijson_geometry(json.loads(multipolygon_wkt.geojson))

    else:
        prot = str(uuid.uuid1())
        OUT_dir = os.path.join(settings.MEDIA_ROOT,'certificati','temp',prot)
        multipolygon_wkt = GEOSGeometry(WKT)
        #get_esrijson_geometry(json.loads(multipolygon_wkt.geojson))
        title = ''

    if ESRIJSON:
        multipolygon_esri = json.loads(ESRIJSON)
    else:
        
        if WKT.startswith("SRID=3003;"):
            WKT = WKT[10:]
        geom = loads(WKT)
        multipolygon_esri = esrijson.from_shape(geom)

    if output_dir:
        pdf_tmp = os.path.join(output_dir,"temp.pdf")
        pdf_path = os.path.join(output_dir,"map_%s.pdf" % str(uuid.uuid1()))
    else:
        pdf_tmp = os.path.join(OUT_dir,"temp.pdf")
        pdf_path = os.path.join(OUT_dir,prot+"-"+title.replace(" ","_")+".pdf")

    printOutput_template = printOutput_templates[paper]
    if paper != 'a4':
        #scales = (500, 1000, 2000, 5000,10000,)
        width = 2

    if not os.path.isfile(pdf_path) or overwrite:
        bound, frame, scale = get_best_fit_scale(multipolygon_wkt, scales, paper)
        extents = frame.extent

        printpar ={
            "extent": frame.extent,
            "scale": scale,
            "srid": 3003,
            "esri_poly": json.dumps(multipolygon_esri['rings']),
            "esri_style": json.dumps(proto_ESRI_style), #non implementato nel template json
            "esri_bound": get_esri_ring(bound.extent),
            "esri_frame": get_esri_ring(frame.extent),
            "title": title,
            "dpi": dpi,
            "auth": "Settore urbanistica e Servizi catastali",
            "copyright": "Comune di Padova"
        }

        web_map_as_json = render_to_string('cdu_print.json', {"printpar": printpar})
        web_map_as_dict = json.loads(web_map_as_json)


        web_map_as_dict["operationalLayers"] = mapServices + web_map_as_dict["operationalLayers"]
        web_map_as_json = json.dumps(web_map_as_dict)
        print (web_map_as_json)
        post_parameters = {
            "f": "json",
            "Web_Map_as_JSON": web_map_as_json,
            "Format": "PDF",
            "Layout_Template": printOutput_templates[paper]
        }

        req = requests.post("https://oscar.comune.padova.it/server/rest/services/ExportWebMapPadova/GPServer/Esporta%20mappa%20Web/execute", data = post_parameters)
        print(req.status_code, req.reason,"\nHEADERS:\n",req.headers,"\nCONTENT:\n",req.content)
        if req.status_code == 200:
            print ("JSON REQ",req.json(), file=sys.stderr)
            remoteFile = req.json()['results'][0]["value"]["url"]
            print (remoteFile, file=sys.stderr)
            try:
                os.makedirs(OUT_dir)
            except FileExistsError:
                pass
            r = requests.get(remoteFile)
            with open(pdf_tmp, "wb") as code:
                code.write(r.content)
            add_title("%s  SCALA 1:%d" % (label,scale),pdf_tmp,pdf_path)
            return pdf_path
        else:
            return None
    else:
        return pdf_path

def add_title(title, pfdfile_origin, pfdfile_destination):
    packet = io.BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=letter)
    can.drawString(27, 16, title)
    can.save()
    #move to the beginning of the StringIO buffer
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    # read your existing PDF
    existing_pdf = PdfFileReader(open(pfdfile_origin, "rb"))
    output = PdfFileWriter()
    # add the "watermark" (which is the new pdf) on the existing page
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    # finally, write "output" to a real file
    outputStream = open(pfdfile_destination, "wb")
    output.write(outputStream)
    outputStream.close()

def stampa_PI_CONTROLLO(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 1, scales=(500, 1000, 2000, 5000),paper = 'a4'):
    mapServices = [CTR1996_DEF, DECA_DEF, CATASTO_DEF, PI_DEF, TOPO_DEF]
    return get_pataplan_print(CDU, mapServices, "PI vigente con catasto", dpi, outline, fill, width, scales, paper)

def stampa_PI_CONTROLLO_CS(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 1, scales=(500, 1000, 2000, 5000,10000),paper = 'a4'):
    mapServices = [CATASTO_DEF, DECA_DEF, PI_CS_DEF, TOPO_DEF]
    return get_pataplan_print(CDU, mapServices, "PI del centro storico vigente con catasto", dpi, outline, fill, width, scales, paper)

def stampa_PI_VIGENTE(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(5000,10000),paper = 'a4'):
    mapServices = [CTR1996_DEF, DECA_DEF, PI_DEF]
    return get_pataplan_print(CDU, mapServices, "PI vigente", dpi, outline, fill, width, scales, paper)

def stampa_PI_CS_VIGENTE(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 2, scales=(500, 1000, 2000,),paper = 'a4'):
    mapServices = [DECA_DEF, PI_CS_DEF]
    return get_pataplan_print(CDU, mapServices, "PI del centro storico vigente", dpi, outline, fill, width, scales, paper)

def stampa_PAT_TRASFORMABILITA(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(5000,10000,),paper = 'a4'):
    mapServices = [CTR1996_DEF, PAT_TRASFORMABILITA_DEF]
    return get_pataplan_print(CDU, mapServices, "PAT trasformabilita", dpi, outline, fill, width, scales, paper)

def stampa_PAT_VINCOLI(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(5000,10000,),paper = 'a4'):
    mapServices = [CTR1996_DEF, PAT_VINCOLI_DEF]
    return get_pataplan_print(CDU, mapServices, "PAT vincoli", dpi, outline, fill, width, scales, paper)

def stampa_PAT_FRAGILITA(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(5000,10000,),paper = 'a4'):
    mapServices = [CTR1996_DEF, PAT_FRAGILITA_DEF]
    return get_pataplan_print(CDU, mapServices, "PAT fragilita", dpi, outline, fill, width, scales, paper)

def stampa_PAT_INVARIANTI(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(5000,10000,),paper = 'a4'):
    mapServices = [CTR1996_DEF, PAT_INVARIANTI_DEF]
    return get_pataplan_print(CDU, mapServices, "PAT invarianti", dpi, outline, fill, width, scales, paper)

def stampa_PAT_AUC(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(5000,10000,),paper = 'a4'):
    mapServices = [CTR1996_DEF, PAT_AUC_DEF]
    return get_pataplan_print(CDU, mapServices, "PAT Ambiti di Urbanizzazione Consolidata", dpi, outline, fill, width, scales, paper)

def stampa_PAI(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(5000,10000,),paper = 'a4'):
    mapServices = [CTR1996_DEF, PAI_DEF]
    return get_pataplan_print(CDU, mapServices, "PAI", dpi, outline, fill, width, scales, paper)

def stampa_Bonifica(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(5000,10000,),paper = 'a4'):
    mapServices = [CTR1996_DEF, BONIFICA_DEF]
    return get_pataplan_print(CDU, mapServices, "Aree sottoposte a bonifica", dpi, outline, fill, width, scales, paper)

def stampa_zonom(CDU, dpi = '200', outline = (255,0,0,255), fill = (255,255,255,0), width = 10, scales=(2000,5000,),paper = 'a4'):
    mapServices = [CTR_PRG_DEF, ZONOM_DEF]
    return get_pataplan_print(CDU, mapServices, "Zone omogenee ex DM 1444-68", dpi, outline, fill, width, scales, paper)
