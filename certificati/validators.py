from .coordinateCatastali import GeomFromCoordinateCatastali
from django.core.exceptions import ValidationError

import os

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

def validate_dxf(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.dxf']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'File non supportato.')
        
def validate_image(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg','.png','.tif','.bmp']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Immagine non supportata.')
        
def validate_geom_catasto(value):
        if value != '0/0':
                try:
                        geom = GeomFromCoordinateCatastali(value)
                except:
                        raise ValidationError(u'Coordinate catastali inesistenti')
                if not geom:
                        raise ValidationError(u'Coordinate catastali inesistenti')