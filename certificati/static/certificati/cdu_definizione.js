
{% localize off %}
var targetExtent = [1716014, 5023919, 1737137, 5038662];
{% endlocalize %}

var shifted = false;
var rotating = false

var map_catasto;
var polyTargetFeatures, polyTargetOverlay;
var geojsonObject;

var service_overlay, control_sorgente_overlay, control_destinazione_overlay;
var indiceSorgente, indiceDestinazione;

var targetProjection = new ol.proj.Projection({
  code: 'EPSG:3003',
  // The extent is used to determine zoom level 0. Recommended values for a
  // projection's validity extent can be found at http://epsg.io/.
  extent: targetExtent,
  units: 'm'
});
ol.proj.addProjection(targetProjection);

var geoJson_convertitore = new ol.format.GeoJSON();

var WKT_convertitore = new ol.format.WKT();

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function get_best_fit_scale(extent_scope){
    var center = ol.extent.getCenter(extent_scope);
    var sheet = [210,297];
    var margin = 5;
    var scales=[500,1000, 2000, 5000,10000,20000];
    cx = center[0];
    cy = center[1];
    for (var s = 0; scales.length; s++){
        var scale = scales[s];
        var scaleFactor = scale / 1000;
        var w = (sheet[0] - margin*2) * scaleFactor / 2;
        var h = (sheet[1] - margin*2) * scaleFactor / 2;
        var bound =  [cx-w , cy-h, cx+w, cy+h];
        if (ol.extent.containsExtent(bound, extent_scope)){
            return [bound, scaleFactor];
        }
    }
    return [bound, scaleFactor];
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        console.log(csrftoken);
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(document).ready(function(){
    // funzione per intecettare invio della form ed aggiungere alcuni campi prima del submit
    $('#ricalcola').click(function(eventObj) {

        var features = polyTargetOverlay.getSource().getFeatures();
        var feature = features[0];
        var geometry = feature.getGeometry();
        console.log(features);
        console.log(feature);
        console.log(geometry);

        var geom = WKT_convertitore.writeGeometry(geometry);

        var jsonData = {
            csrfmiddlewaretoken: '{{ csrf_token }}',
            poligono: JSON.stringify(geom)
        }

        $.ajax({
            url: '/certificati/ricalcola/',
            type: 'POST',
            data: JSON.stringify(jsonData),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            success: function(payload) {
                console.log(payload.cartaweb);
                table = payload.table;
                cartaweb = JSON.parse(payload.cartaweb);
                $('#destinazioni').html(table);
                }
        });
    });

    $('#exportCartaweb' ).click( function(event) {
        console.log('estrazione_'+$('#coordinateCatastali').val().replace('/','#').replace(' ','+')+'.txt')
        $("<a />", {
            "download": 'estrazione_'+$('#coordinateCatastali').val().replace('/','#').replace(' ','+')+'.txt',
            "href" : "data:application/json," + encodeURIComponent(JSON.stringify(cartaweb, null, '\t'))
            }).appendTo("body")
            .click(function() {
             $(this).remove()
            })[0].click()
        });

    $(document).on('keyup keydown', function(e){
        shifted = e.shiftKey;
        if (rotating && ! shifted){rotating = false}
    });

    $('#stampa').click( function(event) {
      var b = get_best_fit_scale(polyTargetOverlay.getSource().getExtent());
      $('#progress').removeClass("hidden");
      var bestBound = b[0];
      var bestScale = b[1];
      var esri_features = [];
      var converter = new ol.format.GeoJSON();
      var feats = JSON.parse(converter.writeFeatures(polyTargetOverlay.getSource().getFeatures()));
      for (var f = 0; f < feats.features.length; f++){
            var geojson_feat = feats.features[f];
            var new_esri_feat = {
                symbol: {
                    color: {
                        red: 255,
                        green: 0,
                        blue: 0,
                        alpha: 255
                    },
                    style: "esriSLSSolid",
                    type: "esriSLS",
                    width: bestScale * 1000 / 700/*,
                    outline: {
                        color: {
                            blue: 0,
                            red: 255,
                            green: 0,
                            alpha: 255
                        },
                        width: bestScale * 1000 / 500,
                        style: "esriSFSSolid",
                        type: "esriSFS"
                    }*/
                },
                geometry: Terraformer.ArcGIS.convert(geojson_feat.geometry)
            };
            esri_features.push(new_esri_feat);
      }
      esri_features_JSON = JSON.stringify(esri_features);

      var post_parameters = {
          'f': 'json',
          'mapExtent': '{"xmin":'+bestBound[0].toString()+',"ymin":'+bestBound[1].toString()+',"xmax":'+bestBound[2].toString()+',"ymax":'+bestBound[3].toString()+',"spatialReference":{"wkid":3003}}',
          'printOutput': '{"exportSettings":null,"borderWidth":[0.5,0.5,0.5,0.5],"width":21,"height":29.7,"pageUnits":8,"mapRotation":0,"format":"pdf","resolution":150,"toRemoveLayoutElements":["*"]}',
          'mapServices': '[{"visibleIds":"0,1,2,3,4,5,7,8,9,11","url":"https://oscar.comune.padova.it/server/rest/services/ctr_1996/MapServer/","alpha":0.69921875,"name":"Ctr 1996","type":"AGS"},{"type":"AGS","visibleIds":"0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18","name":"PI","alpha":0.69921875,"url":"https://oscar.comune.padova.it/server/rest/services/prg/MapServer"}]',
          'layoutElements': '[{"xOffset":0,"yOffset":0,"symbol":{"text":"DXF","textSymbol":{"text":null,"color":{"red":0,"blue":0,"green":0,"alpha":255},"yoffset":0,"font":null,"xoffset":0,"type":"agsJsonSymbol","borderLineColor":null,"angle":0},"type":"pchTextElement"},"visible":true,"width":null,"geometry":{"x":10.5,"y":29.2},"name":"myScaleBar","height":null,"anchor":"topmid"},{"xOffset":0,"yOffset":0,"symbol":{"mapUnitLabel":"","numberFormat":{"roundingValue":3,"roundingOption":0},"pageUnitLabel":"","pageUnits":9,"separator":":","type":"pchScaleText","mapUnits":8,"backgroundColor":{"red":255,"blue":255,"green":255,"alpha":255},"style":0},"visible":true,"width":"","geometry":{"x":0.5,"y":29.2},"name":"myScaleBar","height":"","anchor":"topleft"}]',
          'mapElements': esri_features_JSON
      }
      $.ajax({
          url: '/certificati/defprint/',
          type: 'POST',
          data: JSON.stringify(post_parameters),
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: false,
          crossOrigin: false,
          success: function(payload) {
              console.log(payload.print);
              $('#progress').addClass("hidden");
              $("<a />", {
                      "download": "stampaDXF.pdf",
                      "target": "_blank",
                      "href" : payload.print
                  }).appendTo("body").click(function() {
                        $(this).remove()
                  })[0].click()
          }
        });
    });

    $('#sorgente').click( function(event) {
        if (! $("#sorgente").hasClass( "active" )){
            $("#destinazione").removeClass( "active" );
            resetInteractions();
            map_catasto.addInteraction(sorgenteInteraction);
            controlliSorgente.clear();
            indiceSorgente = 0;
        } else {
            resetInteractions();
        }
    });

    $('#destinazione').click( function(event) {
        if (! $("#destinazione").hasClass( "active" )){
            $("#sorgente").removeClass( "active" );
            resetInteractions();
            map_catasto.addInteraction(destinazioneInteraction);
            controlliDestinazione.clear();
            indiceDestinazione = 0;

        } else {
            resetInteractions();
        }
    });

    $('#allinea').click( function(event) {
        if ((indiceDestinazione > 1)&&(indiceSorgente > 1)) {
            var sorgenti = new Array(indiceSorgente);
            var destinazioni = new Array(indiceDestinazione);
            var feats = controlliSorgente.getFeatures();
            for (var f = 0; f < feats.length; f++){
                feat = feats[f];
                sorgenti[feat.get('indice')] = feat.getGeometry().getCoordinates();
            }
            var feats = controlliDestinazione.getFeatures();
            for (var f = 0; f < feats.length; f++){
                feat = feats[f];
                destinazioni[feat.get('indice')] = feat.getGeometry().getCoordinates();
            }
            var deltaX = destinazioni[1][0] - sorgenti[1][0];
            var deltaY = destinazioni[1][1] - sorgenti[1][1];

            var S2_traslato = [sorgenti[2][0]+deltaX, sorgenti[2][1] + deltaY];
            var deltaX_t = S2_traslato[0] - destinazioni[1][0];
            var deltaY_t = S2_traslato[1] - destinazioni[1][1];
            var alpha_t = Math.atan2(deltaY_t,deltaX_t);

            var deltaX_d = destinazioni[2][0] - destinazioni[1][0];
            var deltaY_d = destinazioni[2][1] - destinazioni[1][1];
            var alpha_d = Math.atan2(deltaY_d,deltaX_d);

            var rotazione = alpha_d - alpha_t;

            alert(rotazione*180/Math.PI)

            var feats = polyTargetOverlay.getSource().getFeatures();
            for (var f=0; f < feats.length; f++){
                var feat = feats[f];
                var geom = feat.getGeometry().clone();
                geom.translate(deltaX, deltaY);
                geom.rotate(rotazione, destinazioni[1]);
                feat.setGeometry(geom);

            }
            $("#sorgente").removeClass( "active" );
            $("#destinazione").removeClass( "active" );
            resetInteractions();
            indiceSorgente = 0;
            indiceDestinazione = 0;
        }
    });

    $('#centra').click( function(event) {
        var dxf_center = ol.extent.getCenter(polyTargetOverlay.getSource().getExtent());
        var view_center = map_catasto.getView().getCenter();
        var deltaX = view_center[0]-dxf_center[0];
        var deltaY = view_center[1]-dxf_center[1];
        var feats = polyTargetOverlay.getSource().getFeatures();
        for (var f=0; f < feats.length; f++){
            var feat = feats[f];
            var geom = feat.getGeometry().clone();
            geom.translate(deltaX, deltaY);
            feat.setGeometry(geom);
        }
    });

    console.log('doc ready');
});

function resetInteractions () {
    map_catasto.removeInteraction(sorgenteInteraction);
    map_catasto.removeInteraction(destinazioneInteraction);
};

window.app = {};
var app = window.app;

/**
 * @constructor
 * @extends {ol.interaction.Pointer}
 */
app.Drag = function() {

  ol.interaction.Pointer.call(this, {
    handleDownEvent: app.Drag.prototype.handleDownEvent,
    handleDragEvent: app.Drag.prototype.handleDragEvent,
    handleMoveEvent: app.Drag.prototype.handleMoveEvent,
    handleUpEvent: app.Drag.prototype.handleUpEvent
  });

  /**
   * @type {ol.Pixel}
   * @private
   */
  this.coordinate_ = null;
  this.basepoint_ = null;

  /**
   * @type {string|undefined}
   * @private
   */
  this.cursor_ = 'pointer';

  /**
   * @type {ol.Feature}
   * @private
   */
  this.feature_ = null;

  /**
   * @type {string|undefined}
   * @private
   */
  this.previousCursor_ = undefined;

  this.dragging_ = false;

};
ol.inherits(app.Drag, ol.interaction.Pointer);

/**
 * @param {ol.MapBrowserEvent} evt Map browser event.
 * @return {boolean} `true` to start the drag sequence.
 */
app.Drag.prototype.handleDownEvent = function(evt) {
  var map = evt.map;

  var feature = map.forEachFeatureAtPixel(evt.pixel,
      function(feature, layer) {
        return feature;
      },
      null,
      function(layer) {
        return layer;// === polyTargetOverlay;
      });

  if ((feature) && (! this.dragging_)) {
    this.coordinate_ = evt.coordinate;
    this.basepoint_ = this.coordinate_;
    console.log(this.coordinate_)
    this.dragging_ = true
    this.feature_ = feature;
  } else {
    this.coordinate_ = false;
    this.basepoint_ = false;
    this.dragging_ = false;
  }

  return !!feature;
};

/**
 * @param {ol.MapBrowserEvent} evt Map browser event.
 */
app.Drag.prototype.handleDragEvent = function(evt) {

  var map = evt.map;

  var feature = map.forEachFeatureAtPixel(evt.pixel,
      function(feature, layer) {
        return feature;
      },
      null,
      function(layer) {
        return layer === polyTargetOverlay;
      });


  var geometry = /** @type {ol.geom.SimpleGeometry} */
      (this.feature_.getGeometry());
  /*
  if (shifted){

    var deltaX = evt.coordinate[0] - this.basepoint_[0];
    var deltaY = evt.coordinate[1] - this.basepoint_[1];
    geometry.rotate(-this.last_alpha, this.basepoint_);
    alpha2 = Math.atan2(deltaY,deltaX);
    this.last_alpha = alpha2;
    geometry.rotate(alpha2, this.basepoint_);

  } else {
    var deltaX = evt.coordinate[0] - this.coordinate_[0];
    var deltaY = evt.coordinate[1] - this.coordinate_[1];
    geometry.translate(deltaX, deltaY);
  }*/
  if (shifted){
    line_coord = [[this.basepoint_[0],this.basepoint_[1]],[evt.coordinate[0],evt.coordinate[1]]]
    console.log("this.basepoint_")
    console.log(this.basepoint_)
    console.log("evt.coordinate")
    console.log(evt.coordinate)
    new_line = new ol.Feature({

        geometry: new ol.geom.LineString(line_coord),
        name: "service"
    });
    service_overlay.getSource().clear();
    service_overlay.getSource().addFeature(new_line)
  }
  //this.coordinate_[0] = evt.coordinate[0];
  //this.coordinate_[1] = evt.coordinate[1];
};

/**
 * @param {ol.MapBrowserEvent} evt Event.
 */
app.Drag.prototype.handleMoveEvent = function(evt) {
  if (this.cursor_) {
    var map = evt.map;
    var feature = map.forEachFeatureAtPixel(evt.pixel,
        function(feature, layer) {
          return feature;
      },
      null,
      function(layer) {
        return layer === polyTargetOverlay;
        });
    var element = evt.map.getTargetElement();
    if (feature) {
      if (element.style.cursor != this.cursor_) {
        this.previousCursor_ = element.style.cursor;
        element.style.cursor = this.cursor_;
      }
    } else if (this.previousCursor_ !== undefined) {
      element.style.cursor = this.previousCursor_;
      this.previousCursor_ = undefined;
    }
  }
};

/**
 * @param {ol.MapBrowserEvent} evt Map browser event.
 * @return {boolean} `false` to stop the drag sequence.
 */
app.Drag.prototype.handleUpEvent = function(evt) {
  if (shifted){
        var feats = polyTargetOverlay.getSource().getFeatures();
        var deltaX = evt.coordinate[0] - this.basepoint_[0];
        var deltaY = evt.coordinate[1] - this.basepoint_[1];
        console.log("DELTA");
        console.log(deltaX);
        console.log(deltaY);
        for (f=0; f < feats.length; f++){
            var feat = feats[f];
            var geom = feat.getGeometry().clone();
            geom.translate(deltaX, deltaY);
            feat.setGeometry(geom);
        }
  }
  service_overlay.getSource().clear();
  this.coordinate_ = null;
  this.basepoint_ = null;
  this.feature_ = null;
  this.dragging_ = false;
  //$('#ricalcola').trigger( "click" );
  return false;
};

var controlInteractions = ol.interaction.defaults().extend([new app.Drag()]);

var sorgenteInteraction, destinazioneInteraction;
var controlliSorgente = new ol.source.Vector();
var controlliDestinazione = new ol.source.Vector();

function initInteractions(){
        sorgenteInteraction = new ol.interaction.Draw({
            source: controlliSorgente,
            type: /** @type {ol.geom.GeometryType} */ ("Point"),
            style: new ol.style.Style ({
                    image: new ol.style.Icon( ({
                      anchor: [0.5, 1],
                      anchorXUnits: 'fraction',
                      anchorYUnits: 'fraction',
                      opacity: 0.8,
                      src: '/static/certificati/marker_s_0.png'
                    }))
                })
        })
        sorgenteInteraction.on('drawend', function(e) {
            indiceSorgente += 1;
            e.feature.setProperties({
            'indice': indiceSorgente,
            })
        })

        destinazioneInteraction = new ol.interaction.Draw({
            source: controlliDestinazione,
            type: /** @type {ol.geom.GeometryType} */ ("Point"),
            style: new ol.style.Style ({
                    image: new ol.style.Icon( ({
                      anchor: [0.5, 1],
                      anchorXUnits: 'fraction',
                      anchorYUnits: 'fraction',
                      opacity: 0.8,
                      src: '/static/certificati/marker_d_0.png'
                    }))
                })
        })
        destinazioneInteraction.on('drawend', function(e) {
            indiceDestinazione += 1;
            e.feature.setProperties({
            'indice': indiceDestinazione,
            })
        })
}