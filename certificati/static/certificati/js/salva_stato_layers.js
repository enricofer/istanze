    function encode(c){var x='charCodeAt',b,e={},f=c.split(""),d=[],a=f[0],g=256;for(b=1;b<f.length;b++)c=f[b],null!=e[a+c]?a+=c:(d.push(1<a.length?e[a]:a[x](0)),e[a+c]=g,g++,a=c);d.push(1<a.length?e[a]:a[x](0));for(b=0;b<d.length;b++)d[b]=String.fromCharCode(d[b]);return d.join("")}

    function decode(b){var a,e={},d=b.split(""),c=f=d[0],g=[c],h=o=256;for(b=1;b<d.length;b++)a=d[b].charCodeAt(0),a=h>a?d[b]:e[a]?e[a]:f+c,g.push(a),c=a.charAt(0),e[o]=f+c,o++,f=a;return g.join("")}


    function write_cookie(name, value) {
        var cookie = [name, '=', encode(value), '; path=/; samesite=LAX;'].join('');
        document.cookie = cookie;
    }
    function read_cookie(name) {
        var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
        result && (result = result[1]);
        if ( result ) {
            return decode(result);
        }
        
    }
    
    function eraseCookie(name) {
        //document.cookie = name + '=; Max-Age=0'
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT'
    }

    function foreach_activelayer(obj,callback,then_func) {

        function traverse_layer_tree(obj){
            if (('getLayers' in obj)){
                obj.getLayers().forEach(traverse_layer_tree)
            } else {
                if ( obj.getVisible() ) {
                    callback(obj)
                }
            }
        }

        obj.getLayers().forEach(traverse_layer_tree)
        then_func()
    }

    function layer_tree(obj){
        function traverse_layer_tree(obj){
            if (('getLayers' in obj)){
                const group = {}
                obj.getLayers().forEach(traverse_layer_tree,group)
                this[obj.get("title")] = group
            } else {
                this[obj.get("title")] = {
                    visible:obj.get("visible")
                }
            }
        }
        const tree = {}
        obj.getLayers().forEach(traverse_layer_tree, tree)
        return tree
    }

    function layer_list(obj,as_obj){
        function traverse_layer_tree(obj){
            if (('getLayers' in obj)){
                obj.getLayers().forEach(traverse_layer_tree,this)
            } else {
                this[obj.get("title")] = as_obj ? obj :{visible:obj.getVisible()}
            }
        }
        as_obj = as_obj ? as_obj : false
        const tree = {}
        obj.getLayers().forEach(traverse_layer_tree, tree)
        return tree
    }

    function save_layers_state(map){
        console.log("saving layer state")
        const current_layers_state = layer_list(map)
        write_cookie("layers_state_"+map.getTarget(),JSON.stringify(current_layers_state))
    }
    
    function restore_layers_state(map, exclude){
        if (!exclude) {
            this.exclude = []
        } else {
            this.exclude = exclude
        }
        function traverse_layer_tree(obj){
            if (('getLayers' in obj)){
                obj.getLayers().forEach(traverse_layer_tree)
            } else {
                if (this.exclude.includes(obj.get("title"))) {
                    console.log("layer_excluded:", obj.get("title"))
                } else if ( obj.get("title") in this.layers_state){
                    obj.setVisible(this.layers_state[obj.get("title")].visible)
                } else {
                    console.log("not restored")
                }
            }
        }
        try{
            this.layers_state = JSON.parse( read_cookie("layers_state_"+map.getTarget()) )
            map.getLayers().forEach(traverse_layer_tree)
        } catch(err) {
            console.log(err,err.message)
            erase_cookie("layers_state_"+map.getTarget())
        }
        
    }

    function enable_layer_state_tracking(map, exclude){

        eraseCookie("layers_state_finestramappa")
        eraseCookie("layers_state_map_1_win")
        eraseCookie("layers_state_map_2_win")

        if (read_cookie("layers_state_"+map.getTarget())) {
            console.log("layer state restored")
            restore_layers_state(map, exclude)
        } else {
            console.log("layer state default")
        }

        for (const [lyrname, lyr] of Object.entries(layer_list(map,true))){
            lyr.on('change:visible', function(evt) {
                save_layers_state(map)
            })
        }
    }