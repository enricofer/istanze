from logging import exception
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
from django.urls import reverse
from django.template.loader import render_to_string
from django.contrib.gis.geos import GEOSGeometry, MultiPolygon, MultiLineString
from django.conf import settings
from django.contrib.gis.gdal import DataSource
from django.core.serializers import serialize
from django.core.validators import RegexValidator
from datetime import datetime
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.db.models import Q
from PIL import Image


import qrcode
from PyPDF2 import PdfFileMerger, PdfFileReader
from secretary import Renderer

from django.views.generic import ListView

from .generaCertificato import generaTestoCertificato
from .coordinateCatastali import (
    is_wkt,
    GeomFromCoordinateCatastali, 
    queryGSV, queryUbicazione, 
    queryPRG, queryPAT, queryPGRA, queryCS, queryBonifiche,
    destinazioniPRG, queryPUA, SV, queryZONOM, queryISOVALORE, queryUNVOL,queryPI2030,queryPRA,
    verificaInCentroStorico,queryCatasto, queryParticelle,
    queryViario, queryVia, geocode, queryCivici,contestoCatasto,
    inserimento_correlazione, modifica_correlazione, disabilita_correlazione,
    checkAreaTroppoEstesa
)

from .importExport import cartawebExport,geojsonToCartawebExport,geometryConversion
from .getPataplanPrints import (
    get_pataplan_print, stampa_PI_CONTROLLO,stampa_PI_CONTROLLO_CS,
    stampa_PI_VIGENTE,stampa_PI_CS_VIGENTE,stampa_PAT_TRASFORMABILITA,
    stampa_PAT_VINCOLI, stampa_Bonifica, stampa_zonom, stampa_PAT_FRAGILITA, stampa_PAT_INVARIANTI, stampa_PAT_AUC #,stampa_PAI
)
from .getPataplanPrints import (
    AGEA2015, CTR1996_DEF, CATASTO_DEF, 
    PI_DEF, TOPO_DEF, PI_CS_DEF, PAT_TRASFORMABILITA_DEF, 
    PAT_VINCOLI_DEF, ORTOFOTO, DBT2007, 
    PAT_FRAGILITA_DEF, PAT_INVARIANTI_DEF, PAT_AUC_DEF
)
from .forms import CDUForm, DGForm,UploadDXFForm
from .models import CDU, individuazioni, log_analisi
from .fix_utf8 import fixit

from pua.models import pua,tipologia_choice as tipologia_choice_map
from vincoli.models import vincoli
from repertorio.models import repertorioIstanze,modelli

import sys
import json
import os
import urllib
import requests
import traceback
import urllib.request
import shutil
import html
import random
import uuid
import re
import locale
import tempfile
import io

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


coordinate_catastali_validator = "^((((\d+)\:)?(\d+)([a-zA-Z]{1})?\/((\d+)\-)*?(\d+|[A-Z]))\s)*(((\d+)\:)?(\d+)([a-zA-Z]{1})?\/((\d+)\-)*?(\d+|[A-Z]))$"

avanzamento = {'cd':{
    'inserimento':[['Inserimento','inserimento/cd',True],['Individuazione','individuazione',None],['Verifica','verifica',None],['Approvazione','approvazione',None],['Emissione','emissione',None]],
    'individuazione':[['Inserimento','inserimento/cd',True],['Individuazione','individuazione',True],['Verifica','verifica',None],['Approvazione','approvazione',None],['Emissione','emissione',None]],
    'verifica':[['Inserimento','inserimento/cd',True],['Individuazione','individuazione',True],['Verifica','verifica',True],['Approvazione','approvazione',None],['Emissione','emissione',None]],
    'approvazione':[['Inserimento','inserimento/cd',True],['Individuazione','individuazione',True],['Verifica','verifica',True],['Approvazione','approvazione',True],['Emissione','emissione',None]],
    'emissione':[['Inserimento','inserimento/cd',True],['Individuazione','individuazione',True],['Verifica','verifica',True],['Approvazione','approvazione',True],['Emissione','emissione',True]]
},'dg':{
    'inserimento':[['Inserimento','inserimento/dg',True],['Allineamento','definizione',None],['Emissione','stampa',None]],
    'definizione':[['Inserimento','inserimento/dg',True],['Allineamento','definizione',True],['Emissione','stampa',None]],
    'stampa':[['Inserimento','inserimento/dg',True],['Allineamento','definizione',True],['Emissione','stampa',True]]
}}

print_params_template = """
{
   "mapOptions":{
      "showAttribution":true,
      "extent":{
         "xmin":1722604.4114950257,
         "ymin":5031686.192969228,
         "xmax":1723650.2768001785,
         "ymax":5032012.481155575,
         "spatialReference":{
            "wkid":102091,
            "latestWkid":3003
         }
      },
      "spatialReference":{
         "wkid":102091,
         "latestWkid":3003
      },
      "scale":500
   },
   "operationalLayers":[
      {
         "id":"Disegna_Linee_0",
         "opacity":1,
         "minScale":0,
         "maxScale":0,
         "featureCollection":{
            "layers":[
               {
                  "layerDefinition":{
                     "name":"Definizione Grafica",
                     "geometryType":"esriGeometryPolyline"
                  },
                  "featureSet":{
                     "geometryType":"esriGeometryPolyline",
                     "features":[]
                  }
               }
            ]
         }
      }, 
        {
            "id": "printGraphics",
            "opacity": 0,
            "minScale": 0,
            "maxScale": 0,
            "featureCollection": {
                "layers": [{
                    "layerDefinition": {
                        "name": "polygonLayer",
                        "geometryType": "esriGeometryPolygon"
                    },
                    "featureSet": {
                        "geometryType": "esriGeometryPolygon",
                        "features": [{
                            "geometry": {
                                "rings": [],
                                "spatialReference": {
                                    "wkid": 0
                                }
                            },
                            "symbol": {
                                "color": [255, 0, 0, 128],
                                "outline": {
                                    "color": [33, 33, 33, 255],
                                    "width": 1,
                                    "type": "esriSLS",
                                    "style": "esriSLSSolid"
                                },
                                "type": "esriSFS",
                                "style": "esriSFSSolid"
                            }
                        }]
                    }
                }]
            }
        }
   ],
   "exportOptions":{
      "outputSize":[
         670,
         500
      ],
      "dpi":96
   },
   "layoutOptions":{
      "titleText":"Definizione Grafica",
      "authorText":"rapper.comune.padova.it",
      "copyrightText":"",
      "customTextElements":[
         {
            "NOTE1":"NOTE1",
            "NOTE2":"NOTE2"
         }
      ],
      "scaleBarOptions":{
         "metricUnit":"esriMeters",
         "metricLabel":"m"
      },
      "legendOptions":{
      }
   }
}
"""


def utente_settore_check(user):
    print ('utente_settore_check: ',user.groups.filter(name='settore').exists() or user.groups.filter(name='cdu').exists(), file=sys.stderr)
    return user.groups.filter(name='settore').exists() or user.groups.filter(name='cdu').exists()

def utente_cdu_check(user):
    print ('utente_cdu_check: ',user.groups.filter(name='cdu').exists(), file=sys.stderr)
    return user.groups.filter(name='cdu').exists()

# Create your views here.

def JsonFromQueryset(features):
    #features_geojson = serialize('geojson', features,geometry_field='geom',srid=3003, fields=('idIstanza','ditta',))
    features_geojson = {
        "type": "FeatureCollection",
        "crs": {"type": "name", "properties": {"name": "EPSG:3003"}},
        "features": []
    }
    for feature in features:
        try:
            if feature.poligono_richiesto:
                geometry = json.loads(feature.poligono_richiesto.json)
                properties = {
                    "pk": feature.pk,
                    "richiedente": feature.richiedente_1,
                }
                feature = {
                    "type": "Feature",
                    "geometry": geometry,
                    "properties": properties,
                }
                features_geojson["features"].append(feature)
        except:
            pass
    return features_geojson

def as_geojson(request):
    json = JsonFromQueryset(CDU.objects.all())
    return JsonResponse(json)

def execute (cmd):
    print ('EXECUTE OS COMMAND: ',cmd, file=sys.stderr)
    logCmdFilename = os.path.join(settings.MEDIA_ROOT,'certificati/cmd.log')
    os.system('%s >> "%s"  2>&1' % (cmd,logCmdFilename))
    os.system('echo "<br/>\n<br/>\n" >> "%s"  2>&1' % (logCmdFilename))

def getCDUPath(Item, abs=True):
    relativePath = os.path.join('certificati',Item.tipo,str(Item.protocollo_data.year)+Item.protocollo_numero)
    absolutePath = os.path.join(settings.MEDIA_ROOT,relativePath)
    try:
        os.makedirs(absolutePath)
    except FileExistsError:
        pass
    if abs:
        return absolutePath
    else:
        return relativePath

def handle_uploaded_file(f,Item):
    fileRelativePath = os.path.join(getCDUPath(Item, abs=False),f.name.replace(" ","_"))
    fileAbsolutePath = os.path.join(settings.MEDIA_ROOT,fileRelativePath)
    with open(fileAbsolutePath, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return fileRelativePath

def CDU_hp (request):
    return HttpResponseRedirect("/certificati/cd")


@login_required
@user_passes_test(utente_settore_check)
def CDU_lista (request,tipo):
    orderby = request.GET.get('ordine', '-pk')
    cdu_items = CDU.objects.filter(tipo=tipo,cancellato=False).order_by(orderby)
    cerca = request.GET.get('cerca', None)
    if cerca:
        prot_combinazione = re.compile(r'\d\d\d\d\d\d\d')
        if prot_combinazione.match(cerca):
            cdu_items = cdu_items.filter(protocollo_numero=cerca)
        else:
            cdu_items = cdu_items.filter(Q(richiedente_1__icontains=cerca)|Q(richiedente_2__icontains=cerca))

    paginator = Paginator(cdu_items, 20)
    page = request.GET.get('page')
    if page:
        cdu_page = paginator.page(page)
    else:
        cdu_page = paginator.page(1)    

    return render(request, 'cdu_lista.html', {'ordine': orderby, 'trash':None, 'tipo': tipo, 'items': cdu_page,'settings': settings, 'random':"?dum="+str(random.randint(10000000,99999999))})

@login_required
@user_passes_test(utente_settore_check)
def CDU_map (request,tipo):
    cdu_items = CDU.objects.filter(tipo=tipo,cancellato=False).order_by('-pk')
    features = JsonFromQueryset(cdu_items)
    return render(request, 'cdu_mappaCertificati.html', {'features':features, 'tipo': tipo})

@login_required
@user_passes_test(utente_cdu_check)
def CDU_lista_trash (request,tipo):
    cdu_items = CDU.objects.filter(tipo=tipo,cancellato=True).order_by('-pk')
    return render(request, 'cdu_lista.html', {'trash':True, 'tipo': tipo, 'items': cdu_items,'settings': settings, 'random':"?dum="+str(random.randint(10000000,99999999))})

@login_required
@user_passes_test(utente_cdu_check)
def CDU_edit_hub(request,CDUId = None):
    if CDUId:
        editCDU = CDU.objects.get(pk=CDUId)
        if editCDU.stato == "inserimento":
            stato = "inserimento/"+editCDU.tipo
        else:
            stato = editCDU.stato
        return HttpResponseRedirect('/certificati/%s/%s/' % ( stato ,str(CDUId)))

@login_required
@user_passes_test(utente_cdu_check)
def CDU_new(request, tipo):
    if tipo == 'cd':
        #return HttpResponseRedirect('/certificati/inserimento/cd/')
        return CDU_inserimento(request, tipo)
    if tipo == 'dg':
        return HttpResponseRedirect('/certificati/inserimento/dg/')
    else:
        # gestire errore
        pass


@login_required
@user_passes_test(utente_cdu_check)
def aggiornamento_correlazione(request, fid=None): #, fid):
    if fid:
        fid = int(fid)
    if request.method == 'DELETE':
        if fid < 0:
            fid = abs(fid)
            apply_value = False
        else:
            apply_value = True
        result = disabilita_correlazione(fid, apply_value, request.user.username)
        if result:
            return JsonResponse({"fid":fid,"action":"deleted"})
        else:
            return JsonResponse({"fid":fid,"action":"error"})
    feature = json.loads(request.body)
    geometry = GEOSGeometry(json.dumps(feature["geometry"]))
    print ("AGGIORNAMENTO GEOM", geometry.wkt)
    if request.method == 'POST':
        new_pk = inserimento_correlazione(geometry.wkt, request.user.username)
        return JsonResponse({"fid":new_pk,"action":"added"})
    if request.method == 'PUT':
        fid = fid or feature["id"]
        result = modifica_correlazione(fid, geometry.wkt, request.user.username)
        old_geom = GEOSGeometry(result).json
        return JsonResponse({"fid":fid,"action":"modified","new_geom":geometry.json,"old_geom":old_geom})


@login_required
@user_passes_test(utente_cdu_check)
def CDU_inserimento(request, tipo, CDUId = None):

    def DXF2Geom(Item): #function per importare dxf, tradurlo in geojson e trasformarlo in geometria geos
        Item.rilievo_raw = handle_uploaded_file(request.FILES['rilievo_raw'],Item)
        file_dxf = settings.MEDIA_ROOT + str(Item.rilievo_raw)
        file_poly_geojson = file_dxf + ".poly.geojson"
        execute ('''ogr2ogr -overwrite  -f "GeoJSON" -dim 2 %s %s''' % (file_poly_geojson,file_dxf))
        f_poly = open(file_poly_geojson, encoding='utf-8')
        poly_json = f_poly.read()
        poly_dict = json.loads(poly_json)
        multi_poly = []
        for feat in poly_dict['features']:
            multi_poly.append(GEOSGeometry(json.dumps(feat['geometry'])).convex_hull)
        Item.poligono_richiesto = MultiPolygon (multi_poly )
        Item.coordinate_catastali = "0/0"

    if request.method == 'POST':
        if tipo == 'cd':
            form = CDUForm(request.POST, request.FILES)
        elif tipo == 'dg':
            form = DGForm(request.POST, request.FILES)
        if form.is_valid():
            if CDUId:
                Item = CDU.objects.get(pk=CDUId)
                if tipo == 'cd' and 'rilievo_raw' in request.FILES:
                    DXF2Geom(Item)
                elif form.cleaned_data['wkt_geom']:
                    Item.poligono_richiesto = GEOSGeometry( form.cleaned_data['wkt_geom'], srid=3003 )
                elif Item.coordinate_catastali != form.data['coordinate_catastali']:
                    geometria,feedback = GeomFromCoordinateCatastali(form.data['coordinate_catastali'],check=True)
                    Item.coordinate_catastali = feedback
                    Item.poligono_richiesto = GEOSGeometry( geometria )
                    #set to dirty
                    Item.contenuto_variato = True
            else:
                Item = CDU()
                Item.protocollo_numero = form.data['protocollo_numero']
                Item.protocollo_data = form.cleaned_data['protocollo_data']
                Item.coordinate_catastali = form.cleaned_data['coordinate_catastali'] or "0/0"
                if tipo == 'cd' and 'rilievo_raw' in request.FILES:
                    DXF2Geom(Item)
                elif form.cleaned_data['wkt_geom']:
                    Item.poligono_richiesto = GEOSGeometry( form.cleaned_data['wkt_geom'], srid=3003 )
                else:
                    geometria,feedback = GeomFromCoordinateCatastali(form.data['coordinate_catastali'],check=True)
                    if geometria:
                        Item.coordinate_catastali = feedback
                        Item.poligono_richiesto = GEOSGeometry( geometria, srid=3003 )
            Item.tipo = tipo
            Item.richiedente_1 = form.data['richiedente_1']
            #Item.richiedente_2 = form.data['richiedente_2']
            Item.richiedente_indirizzo = form.data['richiedente_indirizzo']
            Item.richiedente_email = form.data['richiedente_email']
            Item.richiedente_telefono = form.data['richiedente_telefono']
            Item.modificato_da = request.user.username
            Item.save()
            _dir = getCDUPath(Item)
            try:
                os.makedirs(_dir)
            except FileExistsError:
                pass
            if tipo == 'dg':
                if 'rilievo_raw' in request.FILES:
                    Item.rilievo_raw = handle_uploaded_file(request.FILES['rilievo_raw'],Item)
                if 'rilievo_raster' in request.FILES:
                    Item.rilievo_raster = handle_uploaded_file(request.FILES['rilievo_raster'],Item)
                Item.save()
                if Item.rilievo_raster:
                    raster = os.path.join(settings.MEDIA_ROOT,Item.rilievo_raster.name)
                    tmp = os.path.join(_dir,'tmp.tga') #there is an issue with png format https://github.com/autotrace/autotrace/issues/21
                    vector = os.path.join(_dir,os.path.basename(raster)+'.dxf')
                    execute('convert %s -negate -threshold 25%% -negate %s' % (raster,tmp))
                    execute('autotrace  -despeckle-level 2 -centerline -color-count 2 -output-file %s -output-format DXF %s' % (vector, tmp))
                    os.remove(tmp)
                    if Item.rilievo_ok:
                        os.remove(os.path.join(settings.MEDIA_ROOT,Item.rilievo_ok.name))
                        Item.rilievo_ok = None
                    Item.rilievo_raw = vector.replace(settings.MEDIA_ROOT,'')
                Item.stato = 'definizione'
            elif tipo == 'cd':
                Item.stato = 'individuazione'
            Item.save()
            return HttpResponseRedirect('/certificati/%s/%s/' % (Item.stato, str(Item.pk)))
        else:
            print ("Form is invalid:", file=sys.stderr)
            return render(request, 'cdu_inserimento.html', {'idx': CDUId, 'tipo':tipo, 'form': form, 'avanzamento': avanzamento[tipo]['inserimento']})

    if CDUId:
        editCDU = CDU.objects.get(pk=CDUId)
        av = avanzamento[tipo][editCDU.stato]
        initial_data = {"wkt_geom": editCDU.poligono_richiesto.wkt}
        if tipo == 'cd':
            form = CDUForm(instance = editCDU, initial=initial_data)
        elif tipo == 'dg':
            form = DGForm(instance = editCDU)
    else:
        editCDU = None
        av = avanzamento[tipo]['inserimento']
        initial_data = {
            "wkt_geom": request.GET.get('wkt_geom', ""),
            "coordinate_catastali": request.GET.get('coordinate_catastali', "")
        }
        if tipo == 'cd':
            form = CDUForm(initial=initial_data)
        elif tipo == 'dg':
            form = DGForm()
    return render(request, 'cdu_inserimento.html', {'idx': CDUId, 'item':editCDU,'tipo':tipo, 'form': form, 'avanzamento': av})

@login_required
@user_passes_test(utente_cdu_check)
def CDU_individuazione(request, CDUId):
    errore = None
    if request.method == 'POST':
        geometry = json.loads(request.POST.get('poligono', ''))
        geometry_ESRI = json.loads(request.POST.get('poligono_EsriJSON', ''))
        editCDU = CDU.objects.get(pk=CDUId)
        editCDU.poligono_richiesto = GEOSGeometry( geometry )
        editCDU.poligono_richiesto_esri = geometry_ESRI
        editCDU.contenuto_txt = generaTestoCertificato(CDUId)
        editCDU.stato = 'verifica'
        editCDU.contenuto_variato = True
        editCDU.modificato_da = request.user.username
        editCDU.save()
        return HttpResponseRedirect('/certificati/verifica/%s/' % str(CDUId))

    if CDUId:
        editCDU = CDU.objects.get(pk=CDUId)
        coordinate_catastali = editCDU.coordinate_catastali
        #features = GeomFromCoordinateCatastali(coordinate_catastali,"geojson")
        particelle = editCDU.poligono_richiesto.geojson
        av = avanzamento['cd'][editCDU.stato]

        table = queryPRG(editCDU.poligono_richiesto.wkt,approssimazione=1) + \
            queryZONOM(editCDU.poligono_richiesto.wkt) + \
            queryPUA(editCDU.poligono_richiesto.wkt) + \
            queryPAT(editCDU.poligono_richiesto.wkt)+ \
            queryPGRA(editCDU.poligono_richiesto.wkt) + \
            queryBonifiche(editCDU.poligono_richiesto.wkt) + \
            queryPRA(editCDU.poligono_richiesto.wkt)
        html = render_to_string ('CDU_destinazioni_table.html',{"table": table})
        html += '</br>' + riferimenti_incrociati(editCDU.poligono_richiesto.wkt)
        fabbricati = GeomFromCoordinateCatastali(coordinate_catastali,"geojson", tab='fabbricati_cat')

    else:
        coordinate_catastali = ""
        errore = "verificare le coordinate catastali"
        av = avanzamento['cd']['individuazione']
        html = None
        cartaweb = None

    return render(request, 'cdu_individuazione.html', {'idx': CDUId, 'item':editCDU, 'features': particelle, 'fabbricati': fabbricati, "coordinateCatastali": coordinate_catastali, "errore": errore, 'avanzamento': av, 'table':html})

def CDU_inquadramento(request,CDUId):
    if request.method == 'GET':
        stampaCDU = CDU.objects.get(pk=CDUId)
        wkt = stampaCDU.poligono_richiesto.wkt
        titolo = str(stampaCDU.protocollo_data.year) + stampaCDU.protocollo_numero + "\n" + stampaCDU.richiedente_1
        txt = stampaCDU.coordinate_catastali + "\n\n" + stampaCDU.contenuto_txt
        ind_url = '/certificati/individuazione/?wkt=%s&titolo=%s&desc=%s' % (
            wkt,
            urllib.parse.quote(titolo),
            urllib.parse.quote(txt)
        )
        #ind_url = '/certificati/individuazione/?wkt=%s' % wkt
        if len(ind_url) < 4000:
            return HttpResponseRedirect(ind_url)
        else:
            return stampa_individuazione(request,wkt,titolo,txt)

def stampa_individuazione(request, wkt=None, titolo=None, desc=None, callback=None):
    if request.method == 'GET':
        wkt = wkt or request.GET.get('wkt', '')
        sottocategoria = request.GET.get('sottocategoria', '')
        template = request.GET.get('modello', '')
        indi = individuazioni()
        indi.utente = request.user
        indi.the_geom = GEOSGeometry(wkt)
        if not template:
            if (indi.the_geom.extent[2] - indi.the_geom.extent[0] < 250) and (indi.the_geom.extent[3] - indi.the_geom.extent[1] < 250):
                template = 'IN1000'
            elif (indi.the_geom.extent[2] - indi.the_geom.extent[0] < 500) and (indi.the_geom.extent[3] - indi.the_geom.extent[1] < 500):
                template = 'IN2000'
            else:
                template = 'IN5000'
        titolo = titolo or request.GET.get('titolo', '')
        indi.titolo = titolo[:50]
        indi.desc = desc or request.GET.get('desc', '')
        indi.sottocategoria = request.GET.get('sottocategoria', 0)
        indi.save()
        callback = callback or request.GET.get('callback', '')
        getprint_url = """
http://localhost/qgisserver/?MAP=/dati/archiviazione/servizi/DEFAULT/INDIVIDUAZIONI.qgs
&SERVICE=WMS
&REQUEST=GetPrint
&TEMPLATE=%s
&version=1.3.0
&FORMAT=pdf
&DPI=150
&CRS=EPSG:3003
&map0:EXTENT=1718485,5024555,1733772,5038911
&map0:SCALE=150000
&ATLAS_PK=%d
        """ % (template, indi.pk)
        
        individuazioni_dir = os.path.join(settings.MEDIA_ROOT,"individuazioni")
        if not os.path.exists(individuazioni_dir):
            os.makedirs(individuazioni_dir)
        tmpname = datetime.now().strftime("%Y%m%d_%H%M%S") + "_" + request.user.username +".pdf"
        local_filename = os.path.join(individuazioni_dir, tmpname ) 

        print ("INDI FILE",local_filename)

        result = "ok"
        
        try:
            with urllib.request.urlopen(getprint_url.replace("\n","")) as response, open(local_filename, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)
            callback_url = settings.MEDIA_URL+"/individuazioni/"+tmpname
        except exception as e:
            callback_url = "ERROR"
            result = traceback.format_exc()

        if callback:
            return JsonResponse({"callback":callback_url, "individuazione": indi.pk, "result": result})
        else:
            return HttpResponseRedirect(callback_url.replace("\n", ""))


def CDU_rigenera_mappa(request, CDUId, paper):
    pdf_globale = rigenera_mappe(CDUId,overwrite=True,p=paper)
    editCDU = CDU.objects.get(pk=CDUId)
    editCDU.inquadramento_pdf = os.path.relpath(pdf_globale,settings.MEDIA_ROOT)
    editCDU.save()
    return JsonResponse({'pdf_globale':settings.MEDIA_URL+str(editCDU.inquadramento_pdf)})

def rigenera_mappe(CDUId, overwrite = None, p = 'a4'):
    editCDU = CDU.objects.get(pk=CDUId)
    prot = str(editCDU.protocollo_data.year)+editCDU.protocollo_numero
    CDU_prefix = getCDUPath(editCDU)
    pdf_globale = os.path.join(CDU_prefix,prot+"-inquadramento.pdf")
    idx_path = os.path.join(CDU_prefix,"idx")

    if not os.path.isfile(pdf_globale) or overwrite:
        #remove inquadramento
        if os.path.isfile(pdf_globale):
            os.remove(pdf_globale)

        #stampa pdf

        if verificaInCentroStorico(editCDU.poligono_richiesto.wkt):
            pdf_seq = [stampa_PI_CONTROLLO_CS,stampa_PI_CS_VIGENTE,stampa_PI_VIGENTE,]
        else:
            pdf_seq = [stampa_PI_CONTROLLO,stampa_PI_VIGENTE,stampa_zonom]
        pdf_seq += [stampa_PAT_VINCOLI,stampa_PAT_INVARIANTI,stampa_PAT_FRAGILITA,stampa_PAT_TRASFORMABILITA, stampa_PAT_AUC,stampa_Bonifica,] #,stampa_PAI

        #pdf_seq = [stampa_PI_CONTROLLO] #rimuovere_testing

        pdf_output = [];
        steps = len(pdf_seq);
        step = 1

        for pdf in pdf_seq:

            file_idx = open(idx_path, 'w')
            file_idx.write("%s/%s" % (str(step),str(steps)))
            file_idx.close()
            pdf_output.append(pdf(editCDU, paper = p))
            step += 1

        #merge pdf
        merger = PdfFileMerger()
        for file in pdf_output:
            f = open(file, 'rb')
            merger.append(PdfFileReader(f))
        merger.write(pdf_globale)
        #cancella files temporanei
        for pdffile in pdf_output:
            os.remove(pdffile)
        #cancella indice
        os.remove(os.path.join(CDU_prefix,"idx"))
    return pdf_globale

@login_required
@user_passes_test(utente_cdu_check)
def CDU_verifica(request, CDUId):
    errore = None
    pdf_output = {}
    if request.method == 'POST':
        editCDU = CDU.objects.get(pk=CDUId)
        editCDU.contenuto_txt = request.POST.get('contenuto', '')
        editCDU.stato = 'approvazione'
        editCDU.modificato_da = request.user.username
        editCDU.save()
        editCDU.certificato_pdf = genera_certificato_pdf(request,CDUId)
        editCDU.save()
        return HttpResponseRedirect('/certificati/approvazione/%s/' % str(CDUId))

    if CDUId:
        editCDU = CDU.objects.get(pk=CDUId)
        prot = str(editCDU.protocollo_data.year)+editCDU.protocollo_numero
        pdf_globale = os.path.join(getCDUPath(editCDU),prot+"-inquadramento.pdf")
        inquadramento_generato = os.path.isfile(pdf_globale)
        contenuto_certificato = editCDU.contenuto_txt
        av = avanzamento['cd'][editCDU.stato]

    else:
        contenuto_certificato = ""
        errore = "verificare le coordinate catastali"
        av = avanzamento['cd']['verifica']

    return render(request, 'cdu_verifica.html', {'idx': CDUId, 'item':editCDU, 'inquadramento':inquadramento_generato, 'pdf_output':pdf_globale.replace(settings.MEDIA_ROOT,settings.MEDIA_URL)+"?dum="+str(random.randint(10000000,99999999)), 'contenuto_certificato': contenuto_certificato, "errore": errore, 'avanzamento': av})

#@login_required
#@user_passes_test(utente_cdu_check)
def CDU_certificato_bozza(request,CDUId):
    editCDU = CDU.objects.get(pk=CDUId)
    modello = modelli.objects.get(pk=27) #NB generalizzare il template!!
    ditta = editCDU.richiedente_1#+' '+editCDU.richiedente_2
    content = editCDU.contenuto_txt.replace('–','-').split('\n')
    schema = 'https://' if request.is_secure else 'http://'
    html = render_to_string('certificato.html',{
        'host': schema+request.META['HTTP_HOST'],
        'protocollo_uscita':editCDU.protocollo_emissione,
        'protocollo_entrata':editCDU.protocollo_numero,
        'ditta': ditta,
        'data_presentazione':editCDU.protocollo_data,
        'contenuto':content,
        'data_emissione':editCDU.data_emissione,
        'parametri': modello.parametri,
    })
    #html_fixed = fixit(html)
    return HttpResponse(html)

#@login_required
#@user_passes_test(utente_cdu_check)
def CDU_certificato_odt(request,CDUId,modelloId):
    modello = modelli.objects.get(pk=modelloId)
    editCDU = CDU.objects.get(pk=CDUId)
    ditta = editCDU.richiedente_1#+' '+editCDU.richiedente_2
    contenuto_txt = editCDU.contenuto_txt
    content = editCDU.contenuto_txt.replace('–','-').split('\r\n')
    schema = 'https://' if request.is_secure else 'http://'
    locale.setlocale(locale.LC_ALL, "it_IT.utf8")
    data_presentazione = editCDU.protocollo_data.strftime("%d %B %Y")
    data = {
        'host': schema+request.META['HTTP_HOST'],
        'protocollo':editCDU.protocollo_numero,
        'ditta': ditta,
        'data_presentazione':data_presentazione,
        'contenuto':content,
        'data_emissione':editCDU.data_emissione,
        'parametri': modello.parametri,
    }
    fm = editCDU.coordinate_catastali.split(" ")[0].split("/")
    doc_name = "%s %s %s" % (editCDU.protocollo_numero, editCDU.richiedente_1, "f "+fm[0]+" m "+fm[1])
    engine = Renderer()
    template = modello.modello_odt.path
    result = engine.render(template, **data)
    response = HttpResponse(content_type='application/vnd.oasis.opendocument.text')
    response['Content-Disposition'] = 'inline; filename=%s.odt' % doc_name

    with tempfile.NamedTemporaryFile() as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response_content = output.read()
        response.write(response_content)

    return response

def genera_certificato_pdf(request,CDUId):
    editCDU = CDU.objects.get(pk=CDUId)
    prot = str(editCDU.protocollo_data.year)+editCDU.protocollo_numero
    schema = 'https://' if request.is_secure else 'http://'
    CDU_dir = getCDUPath(editCDU)
    try:
        os.makedirs(CDU_dir)
    except FileExistsError:
        pass

    #certificato_pdf_REL = os.path.join('certificati',prot,prot+"-certificato.pdf")
    #certificato_pdf_ROOT = os.path.join(settings.MEDIA_ROOT,certificato_pdf_REL)

    certificato_pdf_ROOT = os.path.join(CDU_dir,prot+"-certificato.pdf")
    certificato_pdf_REL = certificato_pdf_ROOT.replace(settings.MEDIA_ROOT,'')

    url_bozza = schema+request.META['HTTP_HOST'] + "/certificati/bozza/"+str(CDUId)+"/" #la bozza é richiamata con apposita url con id interno non con il protocollo
    execute ('wget  --no-proxy --no-check-certificate %s -O %s' % (url_bozza, certificato_pdf_ROOT+'.html'))
    execute ('iconv -f UTF-8 -t ISO-8859-1 -o %s  %s' % (certificato_pdf_ROOT+'.iso.html',certificato_pdf_ROOT+'.html'))
    execute ('htmldoc --hfimage1 /home/rapper/prod/rapper/certificati/static/certificati/logo001.png --path /home/rapper/prod/rapper/certificati/static/certificati  --size A4 --right 26mm --left 26mm --top 20mm --bottom 20mm --fontsize 13 --fontspacing 1.4 --webpage -f %s %s' % ( certificato_pdf_ROOT, certificato_pdf_ROOT+'.iso.html'))
    #execute ('rm %s  %s' % (certificato_pdf_ROOT+'.iso.html',certificato_pdf_ROOT+'.html'))
    return certificato_pdf_REL

def test_genera_certificato_odt(request,CDUId):
    editCDU = CDU.objects.get(pk=CDUId)
    prot = str(editCDU.protocollo_data.year)+editCDU.protocollo_numero
    CDU_dir = getCDUPath(editCDU)
    try:
        os.makedirs(CDU_dir)
    except FileExistsError:
        pass

    certificato_odt_ROOT = os.path.join(CDU_dir,prot+"-certificato.pdf")
    certificato_pdf_REL = certificato_pdf_ROOT.replace(settings.MEDIA_ROOT,'')
    url_bozza_odt = "http://"+request.META['HTTP_HOST'] + "/certificati/odt/"+str(CDUId)+"/27/" #la bozza é richiamata con apposita url con id interno non con il protocollo
    execute ('wget  --no-proxy --no-check-certificate %s -O %s' % (url_bozza, certificato_pdf_ROOT+'.odt'))
    execute ('iconv -f UTF-8 -t ISO-8859-1 -o %s  %s' % (certificato_pdf_ROOT+'.iso.html',certificato_pdf_ROOT+'.html'))
    execute ('htmldoc --size A4 --right 26mm --left 26mm --top 20mm --bottom 20mm --fontsize 13 --fontspacing 1.4 --webpage -f %s %s' % ( certificato_pdf_ROOT, certificato_pdf_ROOT+'.iso.html'))
    execute ('rm %s  %s' % (certificato_pdf_ROOT+'.iso.html',certificato_pdf_ROOT+'.html'))
    return certificato_pdf_REL

@login_required
@user_passes_test(utente_cdu_check)
def CDU_approvazione(request, CDUId):
    if request.method == 'POST':
        editCDU = CDU.objects.get(pk=CDUId)
        editCDU.protocollo_emissione = request.POST.get('protocollo_emissione', '')
        editCDU.data_emissione = datetime.strptime(request.POST.get('data_emissione', ''),'%d/%m/%Y')
        editCDU.stato = 'emissione'
        editCDU.modificato_da = request.user.username
        editCDU.save()
        editCDU.certificato_pdf = genera_certificato_pdf(request,CDUId)
        editCDU.save()
        return HttpResponseRedirect('/certificati/emissione/%s/' % str(CDUId))

    editCDU = CDU.objects.get(pk=CDUId)
    av = avanzamento['cd'][editCDU.stato]
    return render(request, 'cdu_approvazione.html', {'idx':CDUId,'item':editCDU, 'protocollo_emissione':editCDU.protocollo_emissione,'data_emissione':editCDU.data_emissione,'certificato_pdf':editCDU.certificato_pdf.url+"?dum="+str(random.randint(10000000,99999999)), 'avanzamento': av})

@login_required
@user_passes_test(utente_cdu_check)
def CDU_emissione(request, CDUId):
    editCDU = CDU.objects.get(pk=CDUId)
    table = queryPRG(editCDU.poligono_richiesto.wkt, approssimazione=1)
    cartaweb = cartawebExport(editCDU.poligono_richiesto.wkt)
    html = render_to_string ('CDU_destinazioni_table.html',{"table": table})
    features = editCDU.poligono_richiesto.geojson
    return render(request, 'cdu_emissione.html',{'features':features,'table':html,'random':"?dum="+str(random.randint(10000000,99999999)), 'certificato':editCDU, 'settings':settings})

@login_required
@user_passes_test(utente_cdu_check)
def CDU_remove(request,tipo, CDUId):
    editCDU = CDU.objects.get(pk=CDUId)
    editCDU.cancellato = True
    editCDU.modificato_da = request.user.username
    editCDU.save()
    return HttpResponseRedirect('/certificati/%s/' % tipo)

@login_required
@user_passes_test(utente_cdu_check)
def CDU_recover(request,tipo, CDUId):
    editCDU = CDU.objects.get(pk=CDUId)
    editCDU.cancellato = False
    editCDU.modificato_da = request.user.username
    editCDU.save()
    return HttpResponseRedirect('/certificati/trash/%s/' % tipo)

@login_required
@user_passes_test(utente_cdu_check)
def CDU_empty_trash_ex(request,tipo):
    da_cancellare = CDU.objects.filter(tipo=tipo,cancellato=True)
    for item in da_cancellare:
        certificato_pdf_ROOT = os.path.join(settings.MEDIA_ROOT,str(item.certificato_pdf))
        inquadramento_pdf_ROOT = os.path.join(settings.MEDIA_ROOT,str(item.inquadramento_pdf))
        try:
            os.remove(certificato_pdf_ROOT)
        except:
            pass
        try:
            os.remove(inquadramento_pdf_ROOT)
        except:
            pass
        item.delete()
    return HttpResponseRedirect('/certificati/trash/%s/' % tipo)

@login_required
@user_passes_test(utente_cdu_check)
def CDU_empty_trash(request,tipo):
    da_cancellare = CDU.objects.filter(tipo=tipo,cancellato=True)
    for item in da_cancellare:
        base_dir = getCDUPath(item)
        if os.path.isdir(base_dir):
            for file_object in os.listdir(base_dir):
                try:
                    os.remove(os.path.join(base_dir,file_object))
                except:
                    pass
            try:
                os.rmdir(base_dir)
            except:
                pass
        item.delete()
    return HttpResponseRedirect('/certificati/trash/%s/' % tipo)

@login_required
@csrf_exempt
def cerca_catasto(request):
    return cerca_catasto_func(request)

@csrf_exempt
def cat_bp(request):
    return cerca_catasto_func(request)

def cerca_catasto_func(request):

    errore = None
    particelle = None
    fabbricati = None
    coordCatastaliGet = None
    geometriaGet = None
    html = None
    cartaweb = None
    geom = None
    geojson = None
    area_estesa = False
    wkt = None
    catasto_sintassi_validator = re.compile(coordinate_catastali_validator)
    feedback = ""
    correlazione = False
    table = []

    starttime = datetime.now()

    if request.method == 'GET':
        formato = request.GET.get('format', '')
        geojson = formato == 'json'
        wkt  = formato == 'wkt'
        coordCatastaliGet = request.GET.get('coordinateCatastali', '')
        geometriaGet = request.GET.get('geometria', '')
        if coordCatastaliGet and not catasto_sintassi_validator.match(coordCatastaliGet):
            return JsonResponse({"geom": "", "feedback": "verificare il formato di inserimento delle coordinate catastali"})
        if formato and formato in ("geojson","WKT") and coordCatastaliGet:
            particelle,feedback = GeomFromCoordinateCatastali(coordCatastaliGet,formato,check=True)
            return JsonResponse({"geom": particelle, "feedback": feedback})

    if request.method == 'POST' or coordCatastaliGet or geometriaGet:
        if request.content_type == "application/json":
            body = request.body.decode('utf-8')
            postData = json.loads(body)
            formato = postData["format"]
            coordCatastaliPost = postData["coordinateCatastali"]
        else:
            formato = request.POST.get('format', '')
            coordCatastaliPost = request.POST.get('coordinateCatastali', '')
            correlazione = request.POST.get('correlazione', '') == 'enabled'
        geojson = formato == 'json'
        wkt  = formato == 'wkt'
        if request.POST.get('geometria', '') or geometriaGet:
            geom = geometriaGet or request.POST.get('geometria', '')
            particelle = GEOSGeometry(geom).geojson
            if checkAreaTroppoEstesa(geom):
                table = [["ERRORE NELLA GENERAZIONE DELLE DESTINAZIONI: L'AREA DI ANALISI E' TROPPO ESTESA","","",""]]
                rif = ''
                area_estesa = True
            else:
                table = queryPRG(geom) + \
                    queryISOVALORE(geom) + \
                    queryUNVOL(geom) + \
                    queryZONOM(geom) + \
                    queryPUA(geom) + \
                    queryPAT(geom) + \
                    queryPGRA(geom) + \
                    queryBonifiche(geom) + \
                    queryPRA(geom)
                rif = '</br>' + riferimenti_incrociati(geom)
        elif coordCatastaliGet or coordCatastaliPost:
            #catasto_sintassi_validator = re.compile("^((((\d+)\:)?(\d+)\/((\d+)\-)*?(\d+))\s)*(((\d+)\:)?(\d+)\/((\d+)\-)*?(\d+))$")
            errore = None
            coordCatastali = coordCatastaliGet or coordCatastaliPost.strip().replace('[','').replace(']','')

            if is_wkt(coordCatastali) or catasto_sintassi_validator.match(coordCatastali):
                particelle,feedback = GeomFromCoordinateCatastali(coordCatastali,"geojson",check=True,correction=correlazione)
                if not particelle:
                    errore = "verificare le coordinate catastali"
                    geom = None
                else:
                    geom = GeomFromCoordinateCatastali(coordCatastali,"WKT",correction=correlazione)
                    fabbricati = GeomFromCoordinateCatastali(coordCatastali,"geojson",tab='fabbricati_cat',correction=correlazione)
            else:
                errore = "il formato delle coordinate catastali e' scorretto"
            if geom:
                if checkAreaTroppoEstesa(geom):
                    table = [["ERRORE NELLA GENERAZIONE DELLE DESTINAZIONI: L'AREA DI ANALISI E' TROPPO ESTESA","","",""]]
                    rif = ''
                    area_estesa = True
                else:
                    table = queryPRG(geom) + \
                        queryISOVALORE(geom) + \
                        queryUNVOL(geom) + \
                        queryZONOM(geom) + \
                        queryPUA(geom) + \
                        queryPAT(geom) + \
                        queryPGRA(geom) + \
                        queryBonifiche(geom) + \
                        queryPRA(geom)
                    rif = '</br>'  +  riferimenti_incrociati(geom)
            else:
                table = [errore]
    if not errore and table:
        if wkt:
            return JsonResponse({"geom": geom, "coordinateCatastali": feedback, "table": table, "cross_ref": riferimenti_incrociati(geom), "errore": errore})
        else:
            html = render_to_string ('CDU_destinazioni_table.html',{"table": table})
            html += rif


    if table and feedback:
        delta = datetime.now()-starttime
        log_analisi.objects.create(
            richiesta=feedback[:50],
            elaborazione=delta.total_seconds() * 1000,
            momento=datetime.now(),
            utente=request.user
        )

    if geojson:
        if errore:
            return JsonResponse({'errore':errore})
        else:
            return JsonResponse({"type": "FeatureCollection", "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3003" } }, "features": [particelle]})
    else:
        if geom and not area_estesa:
            contesto = contestoCatasto(GEOSGeometry(geom).buffer(100).extent,correction=correlazione)
        else:
            contesto = '{"type": "FeatureCollection","features": []}'
        cdu_enabled = request.user.groups.filter(name='cdu').exists()
        return render(request, 'cdu_cercaCatasto.html', {
            'particelle': particelle,
            'fabbricati': fabbricati,
            "coordinateCatastali": feedback,
            "errore": errore,
            "area_estesa": area_estesa,
            "table": html,
            "contesto": contesto,
            "is_cdu": cdu_enabled,
            "correlazione": "enabled" if correlazione else "disabled"
        })

@csrf_exempt
def ubicazione(request):
    if request.method == 'POST':
        geom_urlsafe = request.POST.get('geom', '')
        dist = request.POST.get('dist', '')
        return HttpResponse(queryUbicazione(geom_urlsafe, dist),content_type="text/plain; charset=utf-8")

@csrf_exempt
def gsv(request):
    if request.method == 'GET':
        wktgeom = GEOSGeometry(request.GET.get('wkt', ''))
        return SV(wktgeom.centroid)
    if request.method == 'POST':
        geom_urlsafe = request.POST.get('geom', '')
        dist = request.POST.get('dist', '')
        noTransform = request.POST.get('noreproject', '')
        return HttpResponse(queryGSV(geom_urlsafe, dist, noTransform),content_type="text/plain; charset=utf-8")

@csrf_exempt
def destinazioni_txt(request):
    if request.method == 'POST':
        geom_urlsafe = request.POST.get('geom', '')
        semplice = request.POST.get('semplice', '')
        temi = request.POST.get('temi', '')
        as_txt = request.POST.get('txt', '')
        as_json = request.POST.get('json', '')
    elif request.method == 'GET':
        geom_urlsafe = urllib.parse.unquote(request.GET.get('geom', ''))
        semplice = request.GET.get('semplice', '')
        temi = request.GET.get('temi', '')
        as_txt = request.GET.get('txt', '')
        as_json = request.GET.get('json', '')
    else:
        geom_urlsafe = None
    if geom_urlsafe:
        if temi:
            temi_list = [tema for tema in temi.split(',')]
        else:
            temi_list = ['PI','PAT']
        if as_json:
            content = json.dumps(queryPRG(geom_urlsafe))
        elif as_txt:
            output = ''
            table = ''
            if semplice:
                table = queryPRG(geom_urlsafe, verboso=False)
            else:
                table = queryPRG(geom_urlsafe, verboso=True)
            for item in table:
                if not item[0] in ('Piano degli interventi vigente:', 'Area non pianificata', 'Totale', 'Piano degli Interventi del Centro Storico:',''):
                    output += item[0]+', '
            return HttpResponse(output[:-2], content_type="text/plain; charset=utf-8")
        else:
            table = []
            for tema in temi_list:
                print ("TEMA:",tema)
                if tema in ["PI", "CS", "PI2030", "PAT", "ISOVALORE", "UNVOL", "ZONOM", "PGRA", "Bonifiche" "PRA"]:
                    if tema == "PI":
                        table += queryPRG(geom_urlsafe,approssimazione=5)
                    else:
                        table += globals()["query"+tema](geom_urlsafe)

            if semplice:
                content = render_to_string ('CDU_destinazioni_table_s.html',{"table": table})
            else:
                content = render_to_string ('CDU_destinazioni_table.html',{"table": table})
        return HttpResponse(content, content_type='text/plain; charset=utf-8')
    else:
        raise Exception('Errore: Geometria Nulla')

def getDestinazioniHTMLFromGeometry(geom_urlsafe, approssimazione=None):
    table = queryPRG(geom_urlsafe, approssimazione=5)+queryISOVALORE(geom_urlsafe)+queryUNVOL(geom_urlsafe)+queryZONOM(geom_urlsafe)+queryPAT(geom_urlsafe)+queryPUA(geom_urlsafe)
    return render_to_string ('CDU_destinazioni_table_c.html',{"table": table})

@csrf_exempt
def export2vector(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        postData = json.loads(body_unicode)
        wkt = json.loads(postData['poligono'])
        formato = postData['formato']
        newfile = geometryConversion(wkt, format=formato)
        return JsonResponse({'download_url':newfile.replace(settings.MEDIA_ROOT,settings.MEDIA_URL)})

@login_required
@csrf_exempt
def cerca_mappali(request):
    return cerca_mappali_func(request)

@csrf_exempt
def mapp_bp(request):
    return cerca_mappali_func(request)

def cerca_mappali_func(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        postData = json.loads(body_unicode)
        filtro_wkt = postData['filtro_wkt']
        correlazione = postData['correlazione']
        return JsonResponse({'mappali':queryCatasto(filtro_wkt,correlazione=correlazione)})

def getContestoGeojson(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        postData = json.loads(body_unicode)
        coordinateCatastali = postData['coordinateCatastali']
        correlazione = postData['correlazione']
        geom = GeomFromCoordinateCatastali(coordinateCatastali,correction=correlazione)
        if geom:
            contesto = contestoCatasto(GEOSGeometry(geom).buffer(100).extent,correction=correlazione)
        else:
            contesto = '{"type": "FeatureCollection","features": []}'
        return JsonResponse(json.loads(contesto))

def ricerca_mappali(request):
    if request.method == 'POST':
        correlazione = request.POST.get('correlazione', '') == 'enabled'
        coordinateCatastali = request.POST.get('coordinateCatastali', '')
    elif request.method == 'GET':
        correlazione = request.GET.get('correlazione', '') == 'enabled'
        coordinateCatastali = request.GET.get('coordinateCatastali', '')
    else:
        coordinateCatastali = None

    cdu_enabled = request.user.groups.filter(name='cdu').exists()

    if coordinateCatastali:
        geom = GeomFromCoordinateCatastali(coordinateCatastali,correction=correlazione)
        if geom:
            contesto = contestoCatasto(GEOSGeometry(geom).buffer(100).extent,correction=correlazione)
        else:
            contesto =  '{"type": "FeatureCollection","features": []}'
        print ("CORRELAZIONE MAPPALI", request.POST.get('correlazione', ''), correlazione)
        preset = {
            'preset':json.dumps(queryParticelle(coordinateCatastali,correction=correlazione)),
            'correlazione': correlazione,
            'contesto': contesto,
            'is_cdu': cdu_enabled
        }
    else:
        preset = {'preset': '','correlazione': False, 'contesto':'', 'is_cdu': cdu_enabled}

    response =  render (request,'cdu_cercaMappali.html', preset)
    response['Access-Control-Allow-Origin'] = "*"
    return response

@login_required
@user_passes_test(utente_cdu_check)
def definizione(request, DGId):
    errore = None
    features = None
    html = None
    cartaweb = None
    file_dxf = ""
    centro = None
    table = []
    editDG = CDU.objects.get(pk=DGId)
    file_dxf = settings.MEDIA_ROOT + str(editDG.rilievo_raw)
    #serve per centrare la mappa a partire dalle coordinate catastali
    cc = editDG.coordinate_catastali
    if cc and cc != '0/0':
        lotto_json = GeomFromCoordinateCatastali(cc,"geojson")
        centro = GEOSGeometry(lotto_json).centroid
    else:
        lotto_json = None
        centro = GEOSGeometry('POINT (1725000 5032500)', srid=3003)
    if request.method == 'POST':
        features = request.POST.get('features', '')
        editDG = CDU.objects.get(pk=DGId)
        relative_path = os.path.join(getCDUPath(editDG, abs = False),"rilievo_ok.geojson")
        absolute_path = os.path.join(settings.MEDIA_ROOT,relative_path)
        f = open(absolute_path, 'w')
        f.write(features)
        editDG.stato = "stampa"

        poligono_feat = request.POST.get('poligono', '')
        if poligono_feat != "GEOMETRYCOLLECTION EMPTY":
            try:
                editDG.poligono_richiesto = MultiPolygon(GEOSGeometry(poligono_feat))
            except TypeError:
                editDG.poligono_richiesto = GEOSGeometry(poligono_feat)
        
        editDG.rilievo_ok = relative_path
        editDG.save()
        return HttpResponseRedirect('/certificati/stampa/%s/' % str(DGId))

    if editDG.rilievo_ok:
        features_json, poly_json, html = dg_semilavorato(editDG)

    else:
        file_geojson = file_dxf + ".geojson"
        file_poly_geojson = file_dxf + ".poly.geojson"
        #os.remove(file_geojson)
        if editDG.rilievo_raster:
            simplify_factor = 1
        else:
            simplify_factor = 0.1
        execute ('ogr2ogr -simplify %s -overwrite -f "GeoJSON" -dim 2 %s %s' % (simplify_factor,file_geojson,file_dxf))
        execute ('''ogr2ogr -overwrite -sql "select * from entities where Layer = 'poligono'" -f "GeoJSON" -dim 2 %s %s''' % (file_poly_geojson,file_dxf))
        f = open(file_geojson, encoding='utf-8')
        features_json = f.read()

        #featurejson2multilinestring
        multilinegeom = None
        for feat in json.loads(features_json)["features"]:
            geom = GEOSGeometry(json.dumps(feat["geometry"]))
            if feat["geometry"]["type"] == "LineString":
                importGeom = MultiLineString([geom])
            elif feat["geometry"]["type"] == "MultiLineString":
                importGeom = geom
            else:
                importGeom = None
            if importGeom:
                if not multilinegeom:
                    multilinegeom = importGeom
                else:
                    multilinegeom = multilinegeom.union(importGeom)
        print (multilinegeom.wkt)


        f_poly = open(file_poly_geojson, encoding='utf-8')
        poly_json = f_poly.read()
        poly_dict = json.loads(poly_json)
        if not poly_dict['features']:
            errore = "DEFINIRE IL CONTESTO DELLA DEFINIZIONE"
        else:
            #centro = editDG.poligono_richiesto.centroid
            poligono_geom = GEOSGeometry( json.dumps(poly_dict['features'][0]['geometry']) )
            table = queryPRG(poligono_geom.wkt)
            html = render_to_string ('CDU_destinazioni_table.html',{"table": table})
            editDG.poligono_richiesto = MultiPolygon(poligono_geom,)
            editDG.save()
            #cartaweb = geojsonToCartawebExport(json.loads(features))

    return render(request, 'cdu_definizione.html', {
        'centra': centro,
        'item': editDG,
        'idx':DGId,
        'lotto':lotto_json,
        'poligono':poly_json,
        'features': features_json,
        "file_dxf": file_dxf,
        "errore": errore,
        'cartaweb':cartaweb,
        'table': html,
        'avanzamento':avanzamento['dg'][editDG.stato]
    })

@login_required
@user_passes_test(utente_cdu_check)
def stampa(request, DGId):
    editDG = CDU.objects.get(pk=DGId)
    features_json, poly_json, html = dg_semilavorato(editDG)
    etichetta = "Definizione grafica n. %s, prot. %s del %s" % (DGId, editDG.protocollo_numero, editDG.protocollo_data)
    if editDG.certificato_pdf:
        definizione_url = settings.MEDIA_URL+str(editDG.certificato_pdf)
        note = editDG.note
        errore = ''
    else:
        definizione_url = None
        errore = 'definizione non ancora generata'
        note = None
    return render(request, 'cdu_stampa.html', {
        'etichetta':etichetta,
        'errore':errore,
        'idx':DGId,
        'item': editDG,
        'poligono':poly_json,
        'features': features_json,
        "file_dxf": editDG.rilievo_ok,
        'settings': settings,
        'table': html,
        'avanzamento':avanzamento['dg'][editDG.stato],
        'definizione_url':definizione_url,
        'note': note
    })

@csrf_exempt
def stampa_ricerca_mappali(request):
    MAP_LAYER_MAPPING = {
        'agea 2015': AGEA2015,
        'ortofoto 2007': ORTOFOTO,
        'vie': TOPO_DEF,
        'catasto (servizio comune PD)': CATASTO_DEF,
        'base DBT 2007': DBT2007,
        'base CRT 1996': CTR1996_DEF,
        'Piano degli interventi vigente': PI_DEF,
        'PI centro storico vigente': PI_CS_DEF,
        'PAT trasformabilità': PAT_TRASFORMABILITA_DEF,
        'PAT fragilità': PAT_FRAGILITA_DEF,
        'PAT invarianti': PAT_INVARIANTI_DEF,
        'PAT vincoli': PAT_VINCOLI_DEF,
        'poligono delle ditte catastali richieste': None
    }
    if request.method == 'GET': #chiamata diretta per stampa repertorioIstanze
        ISTANZA = {
            'istanza': request.GET.get("istanza",''),
            'categoria': request.GET.get("categoria",''),
            'stato': request.GET.get("stato",''),
            'note': request.GET.get("note",''),
            'valutazione': request.GET.get("valutazione",''),
        }
        PRINTLAYERS = [
            PI_DEF, PI_CS_DEF, CTR1996_DEF, TOPO_DEF
        ]
        WKT = request.GET.get("WKT",'')
        if WKT:
            table = queryPRG(WKT)+queryPUA(WKT)+queryPAT(WKT)+queryPGRA(WKT)+queryBonifiche(WKT)+queryPRA(WKT)
            DESTINAZIONI = render_to_string ('CDU_destinazioni_table.html',{"table": table})
            PARTICELLE = request.GET.get("particelle",'')
        else:
            DESTINAZIONI=""
            PARTICELLE=""
        ESRIJSON=""
        SCALES = (1000, 2000, 5000,10000,20000, 50000, 100000)

    elif request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        postData = json.loads(body_unicode)
        PRINTLAYERS = [] #[MAP_LAYER_MAPPING[layer_name] for layer_name in postData['visible_layers']]
        for layer_name in postData['visible_layers']:
            if layer_name in MAP_LAYER_MAPPING:
                PRINTLAYERS.append(MAP_LAYER_MAPPING[layer_name])

        DESTINAZIONI = postData['destinazioni']
        ESRIJSON=postData['ESRIJSON']
        PARTICELLE= postData['particelle']
        WKT=postData['WKT']
        ISTANZA = None
        SCALES = (1000, 2000, 5000, 10000)

        if None in PRINTLAYERS:
            i = PRINTLAYERS.index(None)
            del PRINTLAYERS[i]
    else:
        WKT=None
        ISTANZA = None

    if WKT or ISTANZA:

        temp_dir = os.path.join(settings.MEDIA_ROOT,'certificati','temp',str(uuid.uuid1()))
        tabella_pdf = genera_pdf_da_html( temp_dir, render_to_string('cdu_destinazioni_page.html',{
            "table": DESTINAZIONI,
            'particelle':PARTICELLE,
            'no_creds':True,
            'istanza': ISTANZA,
        }))

        if ISTANZA and not WKT:
            with open(tabella_pdf, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/pdf")
                response['Content-Disposition'] = 'inline; filename=istanza_%s.pdf' % ISTANZA["istanza"].replace(" ","_")
                return response
        else:
            map_pdf = get_pataplan_print( None, PRINTLAYERS, ISTANZA["istanza"] if ISTANZA else PARTICELLE, '200', (255,0,0,255), (255,255,255,0), 1, SCALES, 'a4', WKT=WKT ) #, ESRIJSON=ESRIJSON

            pages = [tabella_pdf, map_pdf]
            output_pdf = os.path.join(os.path.dirname(map_pdf),'output.pdf')
            merger = PdfFileMerger()
            for page in pages:
                f = open(page, 'rb')
                merger.append(PdfFileReader(f))
            merger.write(output_pdf)
            #cancella files temporanei
            for pdffile in pages:
                os.remove(pdffile)
            if ISTANZA:
                with open(output_pdf, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/pdf")
                    response['Content-Disposition'] = 'inline; filename=istanza_%s.pdf' % ISTANZA["istanza"].replace(" ","_")
                    return response
            else:
                return JsonResponse({'download_url': output_pdf.replace(settings.MEDIA_ROOT,settings.MEDIA_URL)})

    else:
        return JsonResponse({})

def genera_pdf_da_html(folder,html):
    try:
        os.makedirs(folder)
    except FileExistsError:
        pass

    iso_html_ROOT = os.path.join(folder,"destinazioni.iso.html")
    page_html_ROOT = os.path.join(folder,"destinazioni.html")
    page_pdf_ROOT = os.path.join(folder,"destinazioni.pdf")
    page_pdf_REL = page_pdf_ROOT.replace(settings.MEDIA_ROOT,'')

    with open(page_html_ROOT, 'wb') as html_file:
        html_file.write(bytes(html, 'UTF-8'))

    execute ('iconv -f UTF-8 -t ISO-8859-1 -o %s  %s' % (iso_html_ROOT,page_html_ROOT))
    execute ('htmldoc --bodyfont HELVETICA --size A4 --right 26mm --left 26mm --top 20mm --bottom 20mm --fontsize 13 --fontspacing 1.4 --webpage -f %s %s' % ( page_pdf_ROOT, iso_html_ROOT))
    execute ('rm %s  %s' % (iso_html_ROOT, page_html_ROOT))
    return page_pdf_ROOT

def dg_semilavorato(editDG):
    f = open(settings.MEDIA_ROOT + str(editDG.rilievo_ok), encoding='utf-8')
    features_json = f.read()
    poly_geom = editDG.poligono_richiesto
    table = queryPRG(poly_geom.wkt)
    html = render_to_string ('CDU_destinazioni_table.html',{"table": table})
    poly_geom_json = poly_geom.json
    poly_feat_dict = {
        "type": "FeatureCollection",
        "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3003" } },
        "features": [{
              'type': 'Feature',
              'geometry': json.loads(poly_geom_json)
        }]
    }
    poly_feat_json = json.dumps(poly_feat_dict)
    return (features_json, poly_feat_json, html)

@csrf_exempt
def defprint(request):
    base_url = 'https://oscar.comune.padova.it/'
    serie = [
        {
            "id":"PI1.pdf",
            "title":"PI VIGENTE",
            "opacity":0.69921875,
            "minScale": 0,
            "maxScale": 0,
            "url":  "https://oscar.comune.padova.it/server/rest/services/prg/MapServer"
        },
        {
            "id":"PI2.pdf",
            "title":"PI ADOTTATO con D.C.C. 49 del 12/4/2022",
            "opacity":0.69921875,
            "minScale": 0,
            "maxScale": 0,
            "url":  "https://oscar.comune.padova.it/server/rest/services/PI/MapServer"
        }
    ]
    pdf_output = []

    

    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        postData = json.loads(body_unicode)

        itemDG = CDU.objects.get(pk=postData["id"])

        for dgmap in serie:

                postData["printpar"]["esri_poly"] = json.dumps(postData["feats"])

                web_map_as_dict = json.loads(print_params_template)

                print (web_map_as_dict["layoutOptions"])

                web_map_as_dict["mapOptions"]["scale"] = postData["printpar"]["scale"]
                web_map_as_dict["mapOptions"]["extent"]["xmin"] = postData["printpar"]["extent"][0]
                web_map_as_dict["mapOptions"]["extent"]["ymin"] = postData["printpar"]["extent"][1]
                web_map_as_dict["mapOptions"]["extent"]["xmax"] = postData["printpar"]["extent"][2]
                web_map_as_dict["mapOptions"]["extent"]["ymax"] = postData["printpar"]["extent"][3]
                web_map_as_dict["mapOptions"]["extent"]["spatialReference"]["wkid"] = postData["printpar"]["srid"]
                web_map_as_dict["mapOptions"]["spatialReference"]["wkid"] = postData["printpar"]["srid"]
                web_map_as_dict["operationalLayers"][0]["featureCollection"]["layers"][0]["layerDefinition"]["geometryType"] = "esriGeometryPolyline"
                web_map_as_dict["operationalLayers"][0]["featureCollection"]["layers"][0]["featureSet"]["geometryType"] = "esriGeometryPolyline"
                web_map_as_dict["operationalLayers"][0]["featureCollection"]["layers"][0]["featureSet"]["features"] = postData["feats"]
                web_map_as_dict["operationalLayers"][1]["featureCollection"]["layers"][0]["featureSet"]["features"][0]["geometry"]["rings"] = [ postData["printpar"]["esri_bound"], postData["printpar"]["esri_frame"]]
                web_map_as_dict["operationalLayers"][1]["featureCollection"]["layers"][0]["featureSet"]["features"][0]["geometry"]["spatialReference"]["wkid"] = postData["printpar"]["srid"]
                web_map_as_dict["layoutOptions"]["titleText"] = postData["printpar"]["title"] + " prot. %d%s" % (itemDG.protocollo_data.year, itemDG.protocollo_numero) 
                web_map_as_dict["layoutOptions"]["authorText"] = postData["printpar"]["copyright"]
                web_map_as_dict["layoutOptions"]["copyrightText"] = postData["printpar"]["auth"]
                web_map_as_dict["layoutOptions"]["customTextElements"] = [{"Date": dgmap['title']+" "+itemDG.note}]
                web_map_as_dict["operationalLayers"] = [dgmap] + web_map_as_dict["operationalLayers"]

                print (web_map_as_dict["layoutOptions"])

                post_parameters = {
                    "f": "json",
                    "Web_Map_as_JSON": json.dumps(web_map_as_dict),
                    "Format": "PDF",
                    "Layout_Template": postData["Layout_Template"]
                }
                req = requests.post(base_url + "/server/rest/services/ExportWebMapPadova/GPServer/Esporta%20mappa%20Web/execute", data = post_parameters)
                if req.status_code == 200:
                    remoteFile = req.json()['results'][0]["value"]["url"]
                else:
                    remoteFile = ''
                editDG = CDU.objects.get(pk=postData['id'])
                base_path = getCDUPath(editDG, abs=False)
                pdf_relative_path = os.path.join(base_path,dgmap["id"])
                pdf_absolute_path = os.path.join(settings.MEDIA_ROOT, pdf_relative_path)
                try:
                    r = requests.get(remoteFile)
                    if r.status_code == 200:
                        with open(pdf_absolute_path, "wb") as code:
                            code.write(r.content)

                        editDG.certificato_pdf = pdf_relative_path
                        editDG.contenuto_txt = postData["printpar"]['note']
                        editDG.save()
                        errore = None
                        pdf_output.append(pdf_absolute_path)
                    else:
                        errore = "Immagine non valida"
                except Exception as e:
                    errore = "Immagine non caricata: " + str(e)
        
        #merge pdf
        target_relative_path = os.path.join(base_path,"definizione_%d.pdf" % itemDG.pk)
        target_absolute_path = os.path.join(settings.MEDIA_ROOT, target_relative_path)
        target_url_path = os.path.join(settings.MEDIA_URL, target_relative_path)
        merger = PdfFileMerger()
        for file in pdf_output:
            f = open(file, 'rb')
            merger.append(PdfFileReader(f))
        merger.write(target_absolute_path)
        #cancella files temporanei
        for pdffile in pdf_output:
            os.remove(pdffile)

        return JsonResponse({'definizione_url':target_url_path, 'errore':errore})


@csrf_exempt
def get_fase(request, CDUId):
    editCDU = CDU.objects.get(pk=CDUId)
    idx_absolute_path = os.path.join(getCDUPath(editCDU),"idx")
    print (idx_absolute_path)
    if os.path.isfile(idx_absolute_path):
        idx_file = open(idx_absolute_path,'r')
        progress = idx_file.read()
        print ("PROGRESS:",progress)
        idx_file.close()
        return JsonResponse({'fase':progress})
    else:
        print ("PROGRESS:","OK")
        return JsonResponse({'fase':None})


@csrf_exempt
def calcola_destinazioni(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        postData = json.loads(body_unicode)
        if 'poligono' in postData:
            geometry = json.loads(postData['poligono'])
            poligono_geom = GEOSGeometry( geometry )
            if checkAreaTroppoEstesa(poligono_geom.wkt):
                table = [["ERRORE NELLA GENERAZIONE DELLE DESTINAZIONI: L'AREA DI ANALISI E' TROPPO ESTESA","","",""]]
                rif = ''
            else:
                table = []
                if "temi" in postData:
                    temi = postData['temi']
                else:
                    temi = "PRG,ISOVALORE,UNVOL,ZONOM,PUA,PAT,PGRA,Bonifiche,PRA"
                for tema in temi.split(","):
                    if tema:
                        print ("TEMA",globals()["query"+tema])
                        table += globals()["query"+tema](poligono_geom.wkt)
                rif = '</br>' + riferimenti_incrociati(poligono_geom.wkt)
            html = render_to_string ('CDU_destinazioni_table.html',{"table": table})
            html += rif
            return JsonResponse({'table':html})
        else:
            return JsonResponse({'table':''})

def rapporto(request, featureWKT):
    print (urllib.parse.unquote(featureWKT))
    return HttpResponse( riferimenti_incrociati (urllib.parse.unquote(featureWKT)) )

def riferimenti_incrociati(featureWKT, output='html', exclude_self={}):

    target_raw = GEOSGeometry(featureWKT)
    target = target_raw.buffer(0)

    oggetti = {
        'istanze': {
            "queryset": repertorioIstanze.objects.filter(geom__intersects=target)[:20],
            "label": "ISTANZE AL SETTORE:",
            "link": "/admin/repertorio/repertorioistanze/%d/change/"
        },
        'piani': {
            "queryset": pua.objects.filter(the_geom__intersects=target).exclude( tipologia='Altro').exclude(pubblicato=False)[:20],
            "label": "PIANI CORRELATI:",
            "link": "/admin/pua/pua/%d/change/"
        },
        'certificati': {
            "queryset": CDU.objects.filter(poligono_richiesto__intersects=target).exclude(cancellato=True)[:20],
            "label": "CERTIFICATI DI DESTINAZIONE URBANISTICA:",
            "link": "/certificati/inserimento/cd/%d/"
        },
        'vincoli': {
            "queryset": vincoli.objects.filter(geom__intersects=target)[:20],
            "label": "VINCOLI:",
            "link": "/admin/vincoli/vincoli/%d/change/"
        },
        'promemoria': {
            "queryset": pua.objects.filter( Q(tipologia='Altro') & Q(sottocategoria='PPP') ).filter(the_geom__intersects=target)[:20],
            "label": "PROMEMORIA:",
            "link": "/admin/pua/pua/%d/change/"
        }
    }
    
    rapporto_lista = []

    for oggetto_key,oggetto_def in oggetti.items():
        if not oggetto_key in exclude_self:
            exclude_self[oggetto_key] = None
        
        rapporto_tmp = []
        for item in oggetto_def["queryset"]:
            if not ((exclude_self.get(oggetto_key) and exclude_self.get(oggetto_key) == item.pk)):
                rapporto_tmp.append([str(item),oggetto_def["link"] % item.pk,"",""])
        if rapporto_tmp:
            rapporto_lista.append([oggetto_def["label"],"","",""])
            for item in rapporto_tmp:
                rapporto_lista.append(item)
    
    if output == 'lista':
        rapporto_out = rapporto_lista
    elif output == 'html':
        trs = ""
        for riga in rapporto_lista:
            if riga[1]:
                td = '<a href="%s" target="blank">%s</a>' % (riga[1], riga[0])
            else:
                td = riga[0]
            trs += '<tr><td class="contenuto">%s</td></tr>' % td
        rapporto_out = '<table id="riferimenti_incrociati">%s</table>' % trs
    
    return rapporto_out

@csrf_exempt
def getViario(request):
    return JsonResponse(queryViario(request.GET.get('q',None)),safe=False)

@csrf_exempt
def getVia(request):
    return JsonResponse(queryVia(request.GET.get('q',None)))

@csrf_exempt
def getGeocode( request ):
    return JsonResponse(geocode(request.GET.get('q',None)))

@csrf_exempt
def getCivici(request):
    return JsonResponse(queryCivici(request.GET.get('q',None)),safe=False)

@csrf_exempt
def CDU_copertina(request,id):
    service_link = """http://10.10.21.50/qgisserver
?MAP=/usr/lib/cgi-bin/servizi/PI/STAMPE_cdu.qgs
&SERVICE=WMS
&REQUEST=GetPrint
&TEMPLATE=SCHEDA
&version=1.3.0
&FORMAT=pdf
&DPI=150
&CRS=EPSG:3003
&ATLAS_PK=%s
&map0:EXTENT=1717595,5022322,1733891,5038635
&map0:SCALE=300000
    """ % id
    return HttpResponseRedirect(service_link.replace("\n", ""))

@csrf_exempt
def getqr(request):
    if request.method == 'GET':
        text = request.GET.get('text', '')

    img = qrcode.make(text)
    bfile = io.BytesIO()
    img.save(bfile, "PNG")
    bfile.seek(0)
    
    rea_response = HttpResponse(bfile, content_type='image/png')
    #rea_response['Content-Disposition'] = 'attachment; filename={}'.format('qr.png')
    return rea_response

    
