# Generated by Django 3.1.3 on 2022-04-20 10:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('certificati', '0006_auto_20220415_1200'),
    ]

    operations = [
        migrations.AddField(
            model_name='individuazioni',
            name='utente_txt',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
