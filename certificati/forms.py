from django import forms
from django.forms import ModelForm
from django.core.validators import RegexValidator
from .models import CDU
from bootstrap_datepicker_plus.widgets import DatePickerInput
from .validators import validate_geom_catasto



__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

class UploadDXFForm(forms.Form):
    file_dxf = forms.FileField()

class CDUForm(ModelForm):

    wkt_geom = forms.CharField(required=False, widget=forms.Textarea( attrs={'class': 'form-control','rows':'3'}), initial='')
    coordinate_catastali = forms.CharField(required=False, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': "Coordinate Catastali nel formato: FG1/MAPP1[-MAPP2-MAPP3...] [FG2/MAPP1[-MAPP2-MAPP3...]...] - inserire 0/0 se indefinite"
    }))

    class Meta:
        model = CDU
        widgets = {
            'richiedente_1': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_2': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_indirizzo': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_email': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'protocollo_data': DatePickerInput( options={
                    "format": "DD/MM/YYYY",
                    "locale": "it",
                } ), 
            'protocollo_numero': forms.TextInput(attrs={'class': 'form-control'}),
            'note': forms.Textarea(attrs={'class': 'form-control','rows':'3'}),
        }
        fields = ['richiedente_1','richiedente_2', 'richiedente_indirizzo', 'richiedente_email', 'richiedente_telefono', 'protocollo_numero', 'protocollo_data', 'coordinate_catastali', 'wkt_geom', 'rilievo_raw', 'note']

        

    
class DGForm(ModelForm):

    wkt_geom = forms.CharField(required=False, widget=forms.Textarea( attrs={'class': 'form-control','rows':'3'}), initial='')
    coordinate_catastali = forms.CharField(required=False, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': "Coordinate Catastali nel formato: FG1/MAPP1[-MAPP2-MAPP3...] [FG2/MAPP1[-MAPP2-MAPP3...]...] - inserire 0/0 se indefinite"
    }))

    class Meta:
        model = CDU
        widgets = {
            'richiedente_1': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_2': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_indirizzo': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_email': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'protocollo_data': DatePickerInput( options={
                "format": "DD/MM/YYYY",
                "locale": "it",
            } ), 
            'protocollo_numero': forms.TextInput(attrs={'class': 'form-control'}),
            'note': forms.Textarea(attrs={'class': 'form-control'}),
        }
        fields = ['richiedente_1', 'richiedente_2', 'richiedente_indirizzo', 'richiedente_email', 'richiedente_telefono', 'protocollo_numero', 'protocollo_data', 'coordinate_catastali', 'wkt_geom','rilievo_raster','rilievo_raw', 'note']
