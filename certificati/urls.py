from django.conf.urls import url,include
from django.urls import path
#from django.contrib.auth.views import login, logout, password_change, password_change_done
import django.contrib.auth.views

from . import views

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

urlpatterns = [
    url(r'^catasto/$', views.cerca_catasto, name='cerca_cat'),
    url(r'^export/$', views.export2vector, name='export'),
    url(r'^geojson/$', views.as_geojson, name='as_geojson'),
    url(r'^mappali/$', views.ricerca_mappali, name='ricerca_map'),
    url(r'^contesto/$', views.getContestoGeojson, name='contesto'),
    url(r'^aggiornamento_correlazione/(?P<fid>-?\d+|)/?$', views.aggiornamento_correlazione, name='aggiornamento_correlazione'),
    url(r'^cerca_mappali/$', views.cerca_mappali, name='cerca_map'),
    url(r'^mapp_bp/$', views.mapp_bp, name='mapp_bp'),
    url(r'^cat_bp/$', views.cat_bp, name='cat_bp'),
    url(r'^definizione/(\d+)/$', views.definizione, name='definizione'),
    url(r'^destinazioni/$', views.destinazioni_txt, name='destinazioni'),
    url(r'^rapporto/(.*)/$', views.rapporto, name='rapporto'),
    url(r'^ubicazione/$', views.ubicazione, name='ubicazione'),
    url(r'^gsv/$', views.gsv, name='gsv'),
    url(r'^civici/$', views.getCivici, name='viario'),
    url(r'^viario/$', views.getViario, name='viario'),
    url(r'^via/$', views.getVia, name='viario'),
    url(r'^geocoder/$', views.getGeocode, name='viario'),
    url(r'^stampa/(\d+)/$', views.stampa, name='stampa'),
    url(r'^odt/(\d+)/(\d+)/$', views.CDU_certificato_odt, name='stampa'),
    url(r'^stampa_ricerca_mappali/$', views.stampa_ricerca_mappali, name='stampa_ricerca_mappali'),
    url(r'^defprint/$', views.defprint, name='defprint'),
    url(r'^qrcode/$', views.getqr, name='getqr'),
    url(r'^ricalcola/$', views.calcola_destinazioni, name='calcola_dest'),
    url(r'^rigenera/(\d+)/(\w+)/$', views.CDU_rigenera_mappa, name='rigenera_mappa'),
    url(r'^new/(cd|dg)/$', views.CDU_new, name='cdu_new'),
    url(r'^cancella/(cd|dg)/(\d+)/$', views.CDU_remove, name='cdu_remove'),
    url(r'^recupera/(cd|dg)/(\d+)/$', views.CDU_recover, name='cdu_recover'),
    url(r'^inserimento/(?P<tipo>cd|dg)/(?P<CDUId>\d+)/$', views.CDU_inserimento, name='cdu_inserimento_id'),
    url(r'^inserimento/(cd|dg)/$', views.CDU_inserimento, name='cdu_inserimento'),
    url(r'^trash/(cd|dg)/$', views.CDU_lista_trash, name='cdu_lista_trash'),
    url(r'^emptytrash/(cd|dg)/$', views.CDU_empty_trash, name='cdu_empty_trash'),
    url(r'^individuazione/(\d+)/$', views.CDU_individuazione, name='cdu_individuazione'),
    url(r'^inquadramento/(\d+)/$', views.CDU_inquadramento, name='cdu_individuazione'),
    url(r'^individuazione/$', views.stampa_individuazione, name='stampa_individuazione'),
    url(r'^verifica/(\d+)/$', views.CDU_verifica, name='cdu_verifica'),
    url(r'^copertina/(\d+)/$', views.CDU_copertina, name='cdu_copertina'),
    url(r'^bozza/(\d+)/$', views.CDU_certificato_bozza, name='cdu_certificato_bozza'),
    url(r'^approvazione/(\d+)/$', views.CDU_approvazione, name='cdu_approvazione'),
    url(r'^emissione/(\d+)/$', views.CDU_emissione, name='cdu_approvazione'),
    url(r'^editcdu/(?P<CDUId>\d+)/$', views.CDU_edit_hub, name='cdu_edit_hub'),
    url(r'^progress/(\d+)/$', views.get_fase, name='get_fase'),
    url(r'^(cd|dg)/$', views.CDU_lista, name='cdu_lista'),
    url(r'^map/(cd|dg)/$', views.CDU_map, name='cdu_map'),
    url(r'^$', views.CDU_hp, name='cdu_hp'),
    url(r'^login/', django.contrib.auth.views.LoginView, name='auth_login'),
    url(r'^logout/', django.contrib.auth.views.LogoutView, name='auth_logout'),
    #url(r'^change-password/$', password_change,{'template_name': 'registration/password_change_form.html', 'post_change_redirect': 'cdu_password_change_done'},name='cdu_change-password'),
    #url(r'^password-changed/$', password_change_done,{'template_name': 'registration/password_change_done.html'},name='cdu_password_change_done'),
]
