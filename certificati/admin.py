from django.contrib import admin
from django.contrib.gis import admin
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from simple_history.admin import SimpleHistoryAdmin
from massadmin.massadmin import MassEditMixin

from .models import CDU, individuazioni, log_analisi, pratiche

# Register your models here.
#admin.site.unregister(User)
#admin.site.unregister(Group)
#admin.site.register(User, UserAdmin)
#admin.site.register(Group, GroupAdmin)
admin.site.register(CDU, SimpleHistoryAdmin)
admin.site.register(individuazioni)
#admin.site.register(pratiche)
admin.site.register(log_analisi)

@admin.register(pratiche)
class praticheAdmin(MassEditMixin, admin.OSMGeoAdmin):
    modifiable = False
