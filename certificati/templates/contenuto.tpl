che l'area così descritta in Catasto Terreni:{{descrizione_catastale}}
sulla base degli elaborati del Piano degli Interventi (P.I.) vigente{% if inCentroStorico %} risulta inserita entro il perimetro della Zona del Centro Storico;
che gli interventi urbanistico-edilizi nella detta area devono rispettare le prescrizioni delle Norme di Attuazione del Piano degli Interventi ed, in particolare, dell'articolo 9 e della parte III, dettanti la principale disciplina di zona;
che l'area è classificata come {{ prg_cs_dest|upper }} e disciplinata da{{ prg_cs_nta|lower }} delle Norme Tecniche di Attuazione (N.T.A.) del Piano degli Interventi;
{% if prg_vigente_dest %}che l'area è inoltre classificata come: {{ prg_vigente_dest|upper }} con riferimento a{{ prg_vigente_nta }} delle Norme Tecniche di Attuazione (N.T.A.) del Piano degli Interventi;{% endif %}{% else %}, destinata a: {{ prg_vigente_dest|upper }};
che gli interventi urbanistico-edilizi nella detta area sono disciplinati in via principale da{{ prg_vigente_nta }} delle Norme Tecniche di Attuazione (N.T.A.) del Piano degli Interventi;{% endif %}{{ zonom }}
che sulla base degli elaborati del Piano degli interventi(P.I.) Adottato con Deliberazione di Consiglio Comunale n. 49 in data 12/04/2022 l'area è destinata a: {{ prg_salvaguardia_dest|upper }} di cui a{{ prg_salvaguardia_nta }} delle Norme Tecniche Operative (N.T.O.) del Piano Adottato;{% if pua %}
{% for piano in pua %}che {{ piano.nome }} {% if piano.provvedimento %} di cui a {{ piano.provvedimento }}{% endif %}; {% endfor %}{% endif %}
che con delibera di Giunta Provinciale n. 142 del 4 settembre 2014 è stato approvato il Piano di Assetto del Territorio - P.A.T. del Comune di Padova, che richiama in forma esplicita le previsioni e la normativa del P.A.T.I. e che non modifica l'assetto previsionale del P.I. Vigente; l'area è interessata da {{ pat_dest|upper }}. di cui {{ pat_nta }} delle N.T.A. del P.A.T;{% if pgra %}
che il Piano di Gestione del Rischio di Alluvioni (P.G.R.A.), adottato in Conferenza Istituzionale Permanente dell’Autorità di bacino distrettuale delle Alpi Orientali con delibera n. 3 del 21 dicembre 2021, classifica l'area come: {{ pgra|upper }} {% endif %}{% if bonifiche %}
{% for bonifica in bonifiche %}{{ bonifica }};
{% endfor %}{% endif %}{% if pra %}
{{ pra }}
{% endif %}
{{ vincoli }}