import sys
import json
import collections
import math
import os
from pathlib import Path
from contextlib import contextmanager
from datetime import datetime
from telnetlib import theNULL
import uuid

from django.db import connection, connections
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.db.models.functions import MakeValid
from django.core.serializers import serialize
from django.http import JsonResponse, HttpResponseRedirect
from django.conf import settings

from pua.models import tipologia_choice as tipologia_choice_map

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

@contextmanager
def __temporary_filename(suffix=".txt"):
    try:
        tempfilename = os.path.join(settings.MEDIA_ROOT,"tmp", str(uuid.uuid4()).split("-")[0]+suffix)
        #Path(tempfilename).touch()
        yield tempfilename
    finally:
        os.unlink(tempfilename)

def temporary_filename(suffix=".txt"):
    #tempfilename = os.path.join(settings.MEDIA_ROOT,"tmp", str(uuid.uuid4()).split("-")[0]+suffix)
    tempfilename = os.path.join("/","tmp", str(uuid.uuid4()).split("-")[0]+suffix)
    #Path(tempfilename).touch()
    return tempfilename

estrazioneUbicazioneTemplate = '''
SELECT 
    toponomastica_osm.name AS ubicazione,
    st_length (ST_ShortestLine (toponomastica_osm.the_geom, ST_GeomFromText('%s',3003))) as dist
FROM altro.toponomastica_osm
WHERE NOT ST_Disjoint(toponomastica_osm.the_geom,ST_buffer(ST_GeomFromText('%s',3003),%s))
ORDER BY dist
LIMIT 3
'''

estrazioneViario = '''
SELECT nome_via as via
FROM toponomastica.vie_lin
ORDER BY nome_via
'''

estrazioneViarioQ = '''
SELECT 
    nome_via as via,
    st_astext(the_geom) as wkt
FROM toponomastica.vie_lin
WHERE nome_via ILIKE '%%%s%%'
ORDER BY "nome_via"
'''

estrazioneViaLine = '''
SELECT st_astext(the_geom) as wkt
FROM toponomastica.vie_lin
WHERE nome_via = '%s'
'''

estrazioneViaPoli = '''
SELECT st_astext(the_geom) as wkt
FROM toponomastica.vie_poli
WHERE nome_via = '%s'
'''

estrazioneCivici = '''
SELECT st_astext(the_geom) as wkt,numero,numcivn,numcivl
FROM toponomastica.civici
WHERE nome_via = '%s'
ORDER BY numcivn
'''

estrazioneGeocode = '''
select 
	min(rowid) as id,
	min(nome_via) as via,
	st_union(the_geom) as wkt
from
	toponomastica.vie_poli
where
	nome_via ILIKE '%%%s%%'
group by
    codice
'''

estrazioneGSVectorTemplate = '''
SELECT
	min(ST_AsText(ST_Transform(ST_ShortestLine (grafo.the_geom, st_centroid(ST_GeomFromText('%s',3003))),4326))) as wkt_geom,
	min(st_length (ST_ShortestLine (grafo.the_geom, ST_GeomFromText('%s',3003)))) as dist,
    min(grafo.name),
	min(grafo.id)
FROM ( SELECT toponomastica_osm.id,toponomastica_osm.name, toponomastica_osm.the_geom AS the_geom
FROM altro.toponomastica_osm
WHERE NOT ST_Disjoint(toponomastica_osm.the_geom,ST_buffer(ST_GeomFromText('%s',3003),%s))
) as grafo
GROUP BY grafo.id
ORDER BY dist
LIMIT 1
'''

estrazioneGSVectorTemplateNOTRANSTFORM = '''
SELECT
	min(ST_AsText(ST_ShortestLine (grafo.the_geom, st_centroid(ST_GeomFromText('%s',3003))))) as wkt_geom,
	min(st_length (ST_ShortestLine (grafo.the_geom, ST_GeomFromText('%s',3003)))) as dist,
    min(grafo.name),
	min(grafo.id)
FROM ( SELECT toponomastica_osm.id,toponomastica_osm.name, toponomastica_osm.the_geom AS the_geom
FROM altro.toponomastica_osm
WHERE NOT ST_Disjoint(toponomastica_osm.the_geom,ST_buffer(ST_GeomFromText('%s',3003),%s))
) as grafo
GROUP BY grafo.id
ORDER BY dist
LIMIT 1
'''

estrazionePaiTemplate = '''
SELECT DISTINCT pai_decodifica.definizione
FROM altro.pai_pericolosita_idraulica
INNER JOIN altro.pai_decodifica ON pai_pericolosita_idraulica."pericolosi" = pai_decodifica.chiave
WHERE NOT ST_Disjoint(pai_pericolosita_idraulica.the_geom,ST_GeomFromText('%s',3003))
'''

estrazionePGRAPericoloTemplate = '''
WITH const (wkt_geometry) as (
   values ('%s')
)
SELECT 
    MIN(pericolo),
    MIN(classe),
    ST_AREA(ST_UNION(ST_intersection(pgra_pericolosita_idraulica.the_geom,ST_GeomFromText(wkt_geometry,3003)))) as area
FROM const, altro.pgra_pericolosita_idraulica
WHERE NOT ST_Disjoint(pgra_pericolosita_idraulica.the_geom,ST_GeomFromText(wkt_geometry,3003))
GROUP BY pericolo
'''

estrazionePGRARischioTemplate = '''
WITH const (wkt_geometry) as (
   values ('%s')
)
SELECT 
    MIN(rischio),
    MIN(classe),
    ST_AREA(ST_UNION(ST_intersection(pgra_rischio_idraulico.the_geom,ST_GeomFromText(wkt_geometry,3003)))) as area
FROM const, altro.pgra_rischio_idraulico
WHERE NOT ST_Disjoint(pgra_rischio_idraulico.the_geom,ST_GeomFromText(wkt_geometry,3003))
GROUP BY rischio
'''

estrazioneBSLTemplate = '''
SELECT pai_bacino_scolante_laguna."gid"
FROM altro.pai_bacino_scolante_laguna
WHERE NOT ST_Disjoint(pai_bacino_scolante_laguna.the_geom,ST_GeomFromText('%s',3003))
'''

estrazioneBonificheTemplate = '''
SELECT CAST(aree_sottoposte_a_bonifica."id" AS integer) AS id,aree_sottoposte_a_bonifica."statoprati"
FROM altro.aree_sottoposte_a_bonifica
WHERE NOT ST_Disjoint(aree_sottoposte_a_bonifica.the_geom,ST_GeomFromText('%s',3003))
'''

estrazioneCheckCSTemplate = '''
SELECT centro_storico.the_geom
FROM
    ( SELECT the_geom FROM pi_vigente.pi_limiti WHERE layer = 55 ) AS centro_storico
WHERE NOT ST_Disjoint(centro_storico.the_geom,ST_GeomFromText('%s',3003))
'''

estrazioneCSUPTemplate = '''
WITH const (wkt_geometry) as (
   values ('%s')
)
SELECT
    ST_asText(the_geom) as wkt_geom,
    ST_area(the_geom) as sup,
    ST_area(ST_Intersection(the_geom,ST_GeomFromText(wkt_geometry,3003))) as int_sup
FROM pi_cs.cs_up,const
WHERE NOT ST_Disjoint("cs_up".the_geom,ST_GeomFromText(wkt_geometry,3003))
ORDER BY int_sup DESC
LIMIT 10
'''

estrazioneCSTemplate = '''
SELECT
    --il calcolo dell'area va fatto sulle destinazioni d'uso uniti per prevenire errori derivanti da geometrie duplicate
    ST_area(ST_Union(estrazione.the_geom)) AS super, 
    MIN(estrazione.layer) as layer,
    MIN(estrazione.norma1) as norma1,
    MIN(estrazione.despro1) as despro1,
    MIN(estrazione.despro) as despro,
    MIN(estrazione.piano3) as piano3,
    MIN(estrazione.piano4) as piano4,
    MIN(estrazione.urba01) as urba01,
    MIN(estrazione.urba02) as urba02,
    MIN(estrazione.duso01) as duso01,
    MIN(estrazione.duso02) as duso02,
    MIN(estrazione.livello) as livello,
    MIN(estrazione.code_nam) as code_nam,
    MIN(estrazione.code_tipo) as code_tipo,
    row_number() OVER () AS ogc_fid,
    ST_AREA(ST_Union(estrazione.the_geom)) AS area,
    MIN("nto_norma1") as nto_norma1,
    MIN("nto_despro1") as nto_despro1,
    MIN("nto_despro") as nto_despro

FROM (
    SELECT
        cs_blob."ogc_fid",
        cs_blob."layer",
        cs_blob."norma1",
        cs_blob."despro1",
        cs_blob."despro",
        cs_blob.piano1,
        cs_blob.piano2,
        cs_blob.piano3,
        cs_blob.piano4,
        cs_blob.urba01,
        cs_blob.urba02,
        cs_blob.urba03,
        cs_blob.urba04,
        cs_blob.urba05,
        cs_blob.duso01,
        cs_blob.duso02,
        cs_blob.duso03,
        cs_blob.duso04,
        cs_blob.livello,
        cs_blob.code_nam,
        cs_blob.code_tipo,
        deco1.nto as nto_norma1,
        deco2.nto as nto_despro1,
        deco3.nto as nto_despro,
        ST_Intersection(ST_MAKEVALID("cs_blob".the_geom),ST_MAKEVALID(ST_GeomFromText('%s',3003))) AS the_geom
    FROM pi_cs.cs_blob 
    LEFT JOIN pi_variante_generale.pi2030_decodifica as deco1 ON cs_blob."norma1" = deco1.descrizione 
    LEFT JOIN pi_variante_generale.pi2030_decodifica as deco2 ON cs_blob."despro1" = deco2.descrizione 
    LEFT JOIN pi_variante_generale.pi2030_decodifica as deco3 ON cs_blob."despro" = deco3.descrizione 
    WHERE cs_blob."layer" <> 'PR_ED_tutte' AND NOT ST_Disjoint(ST_PointOnSurface("cs_blob".the_geom),ST_GeomFromText('%s',3003))
    LIMIT 100
) as estrazione
GROUP BY layer,norma1,despro1,despro,livello
ORDER BY layer
'''

estrazionePUATemplate = '''
    SELECT
        pua.id_pua,
        pua.ditta,
        pua.tipologia,
        pua.stato,
        pua.delib_amb,
        pua.delib_ado,
        pua.delib_appr,
        pua.delib_var,
        pua.cdu,
        pua.pubblicato,
        pua.denominazione
    FROM public.pua
    WHERE (
            (pua.tipologia IN ('PUA') and not pua.stato IN ('sospeso','decaduto','revocato','test') AND pua.pubblicato) OR
            (not pua.tipologia IN ('PUA') and pua.pubblicato and pua.cdu != '')
          )
		  AND NOT ST_Disjoint(ST_MAKEVALID(pua.the_geom),ST_MAKEVALID(ST_GeomFromText('%s',3003)))
    LIMIT 100
'''

estrazioneISOVALORETemplate = '''
    SELECT
        zone_isovalore."ZONA_ISO" as zona_isovalore,
        zone_isovalore."DENOM" as denominazione,
        zone_isovalore."Q_AB_NUOVO" as areeurb_valoreconvenzionale,
        zone_isovalore."U_INAREAVr" as areeurb_valorearea,
        zone_isovalore."DU_INAREAVr" as areenonurb_valorearea
    FROM pi_variante_generale.zone_isovalore
    WHERE NOT ST_Disjoint(ST_MAKEVALID(zone_isovalore.the_geom),ST_POINTONSURFACE(ST_GeomFromText('%s',3003)))
'''

estrazioneUNVOLTemplate = '''
    SELECT
        (SUM (un_vol_av * ST_AREA(the_geom)) OVER()) as volume,
        (SUM (ST_AREA(the_geom)) OVER()) as superficie_coperta
    FROM dbt."UN_VOL"
    WHERE ST_Within(ST_POINTONSURFACE(the_geom),ST_MAKEVALID(ST_GeomFromText('%s',3003)))
    LIMIT 200
'''

estrazioneConsulteTemplate = '''
    SELECT
        consulte_2,
        zona
    FROM altro.consulte
    WHERE ST_contains(the_geom,st_centroid(ST_MAKEVALID(ST_GeomFromText('%s',3003))))
'''

estrazionePatTemplate = '''
SELECT "pat_blob"."ogc_fid",
       "pat_blob"."layer",
       "pat_blob"."riflegis",
       "pat_blob"."articolo",
       "pat_blob"."tipovinc",
       "pat_blob"."tipologia",
       "pat_blob"."denom",
       "pat_blob"."pericolo",
       "pat_blob"."areeurbc",
       "pat_blob"."tipoint",
       "pat_blob"."nota",
       "pat_blob"."tipocomp",
       "pat_blob"."qualita",
       "pat_blob"."n_areacomp",
       "pat_blob"."tipotut",
       "pat_blob"."tippaes",
       "pat_blob"."tipoelem",
       "pat_blob"."tiporisp",
       "pat_blob"."tipamb",
       "pat_blob"."tipstmon",
       ST_Intersection(ST_MAKEVALID("pat_blob".the_geom),ST_MAKEVALID(ST_GeomFromText('%s',3003))) AS the_geom
FROM pat.pat_blob
WHERE NOT ST_Disjoint("pat_blob".the_geom,ST_MAKEVALID(ST_GeomFromText('%s',3003)))
ORDER BY layer
LIMIT 100
'''

estrazionePI2030Template = '''
WITH const (wkt_geometry) as (
   values ('%s')
)
SELECT 
       MIN("pi2030"."descrizione") as descrizione,
       MIN("pi2030"."nto") as nto,
       CASE 
            WHEN MIN("pi2030"."sigla") is not NULL THEN MIN("pi2030"."filename") || '-' || coalesce(MIN("pi2030"."zto"),MIN("pi2030"."tipo"),MIN("pi2030"."tipologia")) || '/' || MIN("pi2030"."sigla")
            WHEN MIN("pi2030"."zto") is not NULL THEN MIN("pi2030"."filename") || '-' || MIN("pi2030"."zto") 
            WHEN MIN("pi2030"."tipo") is not NULL THEN MIN("pi2030"."filename") || '-' || MIN("pi2030"."tipo")
            ELSE MIN("pi2030"."filename") END as zto,
       '#' || string_agg(CAST("pi2030"."id" as text),'-') as id,
       MIN("pi2030"."tipo") as tipo,
       MIN("pi2030"."tipologia") as tipologia,
       MIN("pi2030"."sigla") as sigla,
       MIN("pi2030"."note") as note,
       CASE 
            WHEN MIN("pi2030"."zto") = 'D4' THEN MIN("pi2030"."sigla")
            ELSE CAST(MIN("pi2030"."n_scheda") as text)
       END as n_scheda,
       MIN("pi2030"."filename") as filename,
       MIN(decodifica.force_order) as force_order,
       ST_AREA(ST_UNION(ST_Intersection(ST_MAKEVALID("pi2030".the_geom),ST_MAKEVALID(ST_GeomFromText(wkt_geometry,3003))))) AS area
FROM const, pi_variante_generale.pi2030, (
	SELECT * FROM (
    VALUES 
        (1, 'b0501011_ZTO'),
        (2, 'b0501051_ViabProg'),
        (3, 'b0501071_EdificiIncongrui'),
        (4, 'b0501063_CaseColoniche'),
        (5, 'b0501021_AmbPUA'),
        (6, 'b0501031_Schede'),
        (7, 'b0501061_Architetture900'),
        (8, 'b0501041_Perimetri'),
        (9, 'b0501051_PerimetriCS'),
        (10,'b0105051_CentriAbitati')
	) AS decodifica (force_order,filename)) as decodifica
WHERE pi2030.filename = decodifica.filename and not pi2030.filename in ('QU','a0102011_ConfiniComunali','b0501081_CentroUrbano','b0105051_CentriAbitati') and NOT ST_Disjoint("pi2030".the_geom,ST_MAKEVALID(ST_GeomFromText(wkt_geometry,3003))) 
GROUP BY decodifica.force_order,zto,tipo,sigla
ORDER BY decodifica.force_order, area DESC
LIMIT 100
'''

estrazionePRATemplate = '''
WITH const (wkt_geometry) as (
   values ('%s')
)
SELECT
       MIN(pi2030.sigla) as sigla,
       ST_AREA(ST_UNION(ST_Intersection(ST_MAKEVALID("pi2030".the_geom),ST_MAKEVALID(ST_GeomFromText(wkt_geometry,3003))))) AS area
FROM const,pi_variante_generale.pi2030
WHERE pi2030.tipo='zone di tutela aeroportuale' AND NOT ST_Disjoint("pi2030".the_geom,ST_MAKEVALID(ST_GeomFromText(wkt_geometry,3003)))
GROUP BY pi2030.sigla
'''

estrazioneZONOMTemplate = '''
SELECT "zoneomogenee_export"."tipo_omo", ST_NumGeometries(ST_MAKEVALID(ST_GeomFromText('%s',3003)))
FROM pi_vigente_export.zoneomogenee_export
WHERE ST_Intersects(zoneomogenee_export.the_geom,ST_PointOnSurface(ST_MAKEVALID(ST_GeomFromText('%s',3003))))
LIMIT 100
'''

estrazioneTemplate = '''
SELECT
    MIN(estrazione.nome) as nome,
    --il calcolo dell'area va fatto sulle destinazioni d'uso uniti per prevenire errori derivanti da geometrie duplicate
    ST_area(ST_Union(estrazione.the_geom)) AS super,
    MIN(estrazione.definizione) as definizione,
    MIN(estrazione.articolo) as articolo,
    MIN(estrazione.layer) as layer,
    MIN(estrazione.note) as note,
    MIN(estrazione.interno_pe) as interno_pe,
    row_number() OVER () AS ogc_fid,
    ST_AsText(ST_Union(estrazione.the_geom)) AS wkt_geom
    --,ST_area(ST_Union(estrazione.the_geom)) AS area

FROM (
    SELECT "pi_blob"."ogc_fid",
           "pi_blob"."gid",
           "pi_blob"."layer",
           "pi_blob"."interno_pe",
           "pi_blob"."note",
           "pi_blob"."super",
           "pi_blob"."nome",
           "pi_blob"."gruppo",
           "pi_blob"."url",
           "pi_blob"."articolo",
           "pi_blob"."definizione",
           ST_Intersection(ST_MAKEVALID("pi_blob".the_geom),ST_MAKEVALID(ST_GeomFromText('%s',3003))) AS the_geom
    FROM "pi_%s"."pi_blob"
    WHERE NOT ST_Disjoint("pi_blob".the_geom,ST_MAKEVALID(ST_GeomFromText('%s',3003)))
    LIMIT 100
) AS estrazione

GROUP BY estrazione.layer,estrazione.note
ORDER BY super DESC
'''

estrazioneCatastoTemplate = '''
    SELECT gid,REGEXP_REPLACE(REGEXP_REPLACE(foglio, '^0+', ''), '0$', '') as foglio,mappale,the_geom FROM catasto.particelle_cat
    WHERE NOT ST_Disjoint(particelle_cat.the_geom,ST_GeomFromText('%s',3003))
    LIMIT 1000
'''

destinazioniTemplate = '''
SELECT
    string_agg(estrazione.nome, ', ') as dest,
  string_agg(estrazione.articolo, ', ') as art

FROM (

    SELECT
            SUM (St_Area(ST_Intersection(ST_MAKEVALID("pi_blob".the_geom),ST_GeomFromText('%s',3003))))AS area,
            "pi_blob"."nome",
            "pi_blob"."articolo",
            ST_Union(ST_Intersection(ST_MAKEVALID("pi_blob".the_geom),ST_GeomFromText('%s',3003))) AS the_geom
        FROM "pi_%s"."pi_blob"
        WHERE NOT ST_Disjoint("pi_blob".the_geom,ST_GeomFromText('%s',3003))
    GROUP BY nome, articolo
    ORDER BY area
    LIMIT 100

) AS estrazione
WHERE estrazione.nome <> 'Area comunale'
'''

checkAreaTroppoEstesaTemplate = '''
SELECT bool_or(res)
FROM
(
	(SELECT 'pi_blob' as tab, COUNT(*)>100 as res
	FROM "pi_vigente"."pi_blob"
	WHERE NOT ST_Disjoint(the_geom,ST_MAKEVALID(ST_GeomFromText('{wktgeom}',3003)))
	)
	UNION
	(SELECT 'pi_cs' as tab, COUNT(*)>50 as res
	FROM pi_cs.cs_up
	WHERE NOT ST_Disjoint(the_geom,ST_MAKEVALID(ST_GeomFromText('{wktgeom}',3003)))
	)
	UNION
	(SELECT 'pi_2030' as tab, COUNT(*)>100 as res
	FROM pi_variante_generale.pi2030
	WHERE NOT ST_Disjoint(the_geom,ST_MAKEVALID(ST_GeomFromText('{wktgeom}',3003)))
	)
) as cc
'''

checkVincoloCimiterialeTemplate = '''
SELECT ST_intersects(ST_MAKEVALID(ST_GeomFromText('%s',3003)),ST_MAKEVALID(altro.fasce_rispetto_cimiteriale.the_geom))
FROM altro.fasce_rispetto_cimiteriale
'''

checkDestinazioniPerimetri = '''
SELECT ST_intersects(ST_MAKEVALID(ST_GeomFromText('%s',3003)),ST_MAKEVALID(altro.fasce_rispetto_cimiteriale.the_geom))
FROM altro.fasce_rispetto_cimiteriale
'''

checkDecadenza = '''
SELECT ST_ASText(ST_Union(ST_intersection(ambito.the_geom,pi_decaduto.the_geom))) as wkt_decadenza
FROM pi_variante_generale.pi_decaduto,(
	SELECT ST_MAKEVALID(ST_GeomFromText('%s',3003)) AS the_geom
) AS ambito
WHERE NOT ST_DISJOINT(ambito.the_geom,pi_decaduto.the_geom)
LIMIT 100
'''

checkViab_ex = '''
SELECT ST_ASText(ST_MAKEVALID(ST_Union(viabilita.the_geom))) as wkt_viab, ST_Area(ST_Union(viabilita.the_geom)) as area
FROM (
	SELECT ST_difference(ambito.the_geom,ST_UNION(pi_zoning.the_geom) OVER()) as the_geom
	FROM pi_vigente.pi_zoning,(
			SELECT ST_MAKEVALID(ST_GeomFromText('%s',3003)) AS the_geom
	) AS ambito
    LIMIT 100
) as viabilita
'''

checkViab = '''
SELECT ST_ASText(ST_MAKEVALID(ST_Union(viabilita.the_geom))) as wkt_viab, ST_Area(ST_Union(viabilita.the_geom)) as area
FROM (
        SELECT ST_difference(ST_MAKEVALID(ST_GeomFromText('%s',3003)),ST_UNION(zoning.the_geom) OVER()) as the_geom
        FROM (
			select the_geom
			from pi_vigente.pi_zoning
			where not st_disjoint(the_geom, ST_MAKEVALID(ST_GeomFromText('%s',3003)))
			limit 50
		) AS zoning
) as viabilita
'''

checkTemplateAUC = '''
SELECT ST_area(ST_intersection(ST_MAKEVALID(ST_GeomFromText('%s',3003)),st_union(ST_MAKEVALID(pat_blob.the_geom)))) as area
FROM pat.pat_blob
WHERE layer = 'c1106011_AmbitiUrbC' AND ST_intersects(ST_MAKEVALID(ST_GeomFromText('%s',3003)),ST_MAKEVALID(pat_blob.the_geom))
'''

shortest_line_to_OSMTemplate = '''
SELECT ST_AsEWKT(min_line), distance FROM (
SELECT ST_SetSRID(ST_ShortestLine(the_geom,ST_GeomFromEWKT('SRID=3003;POINT({x} {y})')),3003) as min_line, ST_Length(ST_ShortestLine(ST_GeomFromEWKT('SRID=3003;POINT({x} {y})'),the_geom)) as distance FROM (
	SELECT the_geom FROM altro.toponomastica_osm WHERE not st_disjoint(the_geom,ST_BUFFER(ST_GeomFromEWKT('SRID=3003;POINT({x} {y})'),{context}))
) AS context ) AS context_ordered ORDER BY distance
'''

listaDestinazioni = '''
SELECT 
	pi_blob_zone.nome, 
	pi_blob_zone.layer, 
	case when pi_blob_zone.layer = dest_decadute.layer then true else false end as check_decaduto 
FROM pi_%s.pi_blob_zone
LEFT JOIN
(
	select distinct layer from pi_variante_generale.pi_decaduto
) as dest_decadute on dest_decadute.layer = pi_blob_zone.layer
'''

contestoCatastoGeojson = '''
SELECT jsonb_build_object(
    'type',     'FeatureCollection',
    'features', jsonb_agg(features.feature)
)
FROM (
  SELECT jsonb_build_object(
    'type',       'Feature',
    'id',         gid,
    'geometry',   ST_AsGeoJSON(the_geom)::jsonb,
    'properties', to_jsonb(inputs) 
  ) AS feature
  FROM (
	  	select * from catasto.particelle_cat
		where st_intersects(the_geom,st_multi(ST_MakeEnvelope(%s,3003)))
  ) inputs) features;
'''

particelleGeojson = '''
SELECT jsonb_build_object(
    'type',     'FeatureCollection',
    'features', jsonb_agg(features.feature)
)
FROM (
  SELECT jsonb_build_object(
    'type',       'Feature',
    'id',         gid,
    'geometry',   ST_AsGeoJSON(the_geom)::jsonb,
    'properties', to_jsonb(inputs) - 'gid' - 'the_geom'
  ) AS feature
  FROM ({query}) inputs) features;
'''

selezionePuntiCorrelazione = '''
SELECT 
    ST_x(ST_StartPoint(the_geom)) as source_x,
    ST_y(ST_StartPoint(the_geom)) as source_y,
    ST_x(ST_EndPoint(the_geom)) as target_x,
    ST_y(ST_EndPoint(the_geom)) as target_y
FROM altro.origine_destinazione_correlazione_catasto
where not disabilitato is true and st_intersects(the_geom,st_buffer(st_multi(ST_MakeEnvelope(%s,3003)),50))
'''

comandoCorrelationeCatasto = '''
ogr2ogr -f "geojson" -overwrite {opts} {geojson_output} PG:"dbname='{NAME}' host={HOST} port={PORT} user='{USER}' password='{PASSWORD}' sslmode=disable" -sql "(SELECT row_number() over () AS _uid_,* FROM ({query}) AS _subq_1_)"
'''

def fix_geometry(wkt_geom):
    query = """
SELECT ST_AsText(ST_MAKEVALID(ST_UnaryUnion(ST_GeomFromText('%s',3003)))) as fixed_geom
    """ % wkt_geom
    cursor = connections['visio'].cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    return GEOSGeometry(res[0], 3003)

def is_wkt(geom):
    try:
        g = GEOSGeometry(geom)
    except:
        return False
    if g.geom_type in ('Polygon','MultiPolygon'):
        return g
    else:
        return False

def SV(target):

    def qgsheading(fromP, toP):
        result = math.atan2((toP.x() - fromP.x()),(toP.y() - fromP.y()))
        result = math.degrees(result)
        return (result + 360) % 360

    def heading(line):
        result = math.atan2((line[1][0] - line[0][0]),(line[1][1] - line[0][1]))
        result = math.degrees(result)
        if result < 0:
            result = 360 + result
        return (result + 360) % 360

    def exheading(line):

        pointA = line[1]
        pointB = line[0]

        if (type(pointA) != tuple) or (type(pointB) != tuple):
            raise TypeError("Only tuples are supported as arguments")

        lat1 = math.radians(pointA[0])
        lat2 = math.radians(pointB[0])

        diffLong = math.radians(pointB[1] - pointA[1])

        x = math.sin(diffLong) * math.cos(lat2)
        y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
                * math.cos(lat2) * math.cos(diffLong))

        initial_bearing = math.atan2(x, y)

        initial_bearing = math.degrees(initial_bearing)-90
        compass_bearing = (initial_bearing + 360) % 360

        return compass_bearing

    pov_wkt = pov_from_OSM(target)

    pov_line = GEOSGeometry(pov_wkt)
    head = heading(pov_line)
    pov_line.transform('EPSG:4326')
    url = "https://maps.google.com/maps?q=&layer=c&cbll={lat},{lon}&cbp=12,{head},0,0,15&z=18".format(lat=pov_line[0][1],lon=pov_line[0][0],head=head)
    
    return HttpResponseRedirect(url)

def pov_from_OSM (punto, context = 200):
    query = shortest_line_to_OSMTemplate.format(x=punto[0],y=punto[1],context=context)
    cursor = connections['visio'].cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    if res:
        return res[0]
    else:
        return ''

def coordinateCatastaliToWHERE(coordinateCatastali, tabellaAnno = 'particelle_cat'):
    #tabellaAnno =  ('particelle_cat_' + str(tabellaAnno)) if tabellaAnno else 'particelle_cat'
    checkSql = "SELECT mappale FROM catasto.%s WHERE foglio='%s' and mappale='%s'"
    cursor = connections['visio'].cursor()
    feedback = ''
    sqlWhere = ""
    fogliMappali = coordinateCatastali.split(" ")
    for foglioMappali in fogliMappali:
        listaFoglio = foglioMappali.split("/")
        foglio = listaFoglio[0]
        feedback += ' %s/' % foglio
        if foglio[-1:].isalpha():
            foglio = "000"[:3-len(foglio[:-1])]+foglio[:-1]+foglio[-1:].upper()
        else:
            foglio = "000"[:3-len(foglio)]+foglio+"0"
        listaMappali = listaFoglio[1]
        mappali = listaMappali.split("-")
        for mappale in mappali:
            sqlWhere += "(foglio = '%s' AND mappale ='%s') OR " % (foglio,mappale)
            cursor.execute(checkSql % (tabellaAnno,foglio,mappale))
            row = cursor.fetchone()
            if row:
                feedback += mappale + '-'
            else:
                feedback += '[%s]-' % mappale
        feedback = feedback[:-1]
        #sqlWhere += " OR "
    return "("+sqlWhere[:-4]+")", feedback

def QueryFromCoordinateCatastali(coordinateCatastali, table = 'particelle_cat'):
    annorif = coordinateCatastali.replace('[','').replace(']','').split(":")
    if len(annorif) > 1:
        tabellaAnno = table + "_"+annorif[0]
        coordinateCatastali = annorif[1]
    else:
        tabellaAnno = table
    sqlWhere, feedback = coordinateCatastaliToWHERE(coordinateCatastali, tabellaAnno = tabellaAnno)
    psql=   'SELECT row_number() OVER () AS ogc_fid, '+\
            'ST_SetSRID(ST_Multi(ST_Union(catasto.%s.the_geom)),3003) AS the_geom, ' % (tabellaAnno) +\
            'ST_AsText(ST_SetSRID(ST_Multi(ST_Union(catasto.%s.the_geom)),3003)) AS wkt ' % (tabellaAnno) +\
            'FROM catasto.%s WHERE %s' % (tabellaAnno,sqlWhere)
    return (psql,feedback[1:])

def queryUbicazione (featureWKT, dist=300):
    cursor = connections['visio'].cursor()
    queryDef = estrazioneConsulteTemplate % featureWKT
    cursor.execute(queryDef) 
    row = cursor.fetchone()  
    consultatxt = "Consulta %s - %s" % (row[0], row[1])
    queryDef = estrazioneUbicazioneTemplate % (featureWKT,featureWKT,dist)
    cursor.execute(queryDef)
    result = ""
    for row in cursor.fetchall():
        if row[0] and not row[0] in result:
            result += row[0]+', '
    if result:
        return consultatxt + "; " + result[:-2]
    else:
        return consultatxt

def queryGSV (featureWKT, dist, noTransform=False):
	cursor = connections['visio'].cursor()
	if noTransform:
		tmpl = estrazioneGSVectorTemplateNOTRANSTFORM
	else:
		tmpl = estrazioneGSVectorTemplate
	queryDef = tmpl % (featureWKT,featureWKT,featureWKT,dist)
	cursor.execute(queryDef)
	result = cursor.fetchall()[0]
	return result[0]

def queryCatasto (featureWKT, correlazione):
    cursor = connections['visio'].cursor()
    queryDef = estrazioneCatastoTemplate % featureWKT
    return geojsonCatastoFromQuery(queryDef, correlazione)

def contestoCatasto(ext,correction=False):
    if correction:
        return contestoCatastoCorretto(ext)
    geojsonQuery = contestoCatastoGeojson % (','.join([str(i) for i in ext]))
    cursor = connections['visio'].cursor()
    cursor.execute(geojsonQuery)
    row = cursor.fetchone()
    if row:
        return row[0]
    else:
        return ''

def getGcpParams(extent,mode="-tps"):
    ext_str = (','.join([str(i) for i in extent]))
    puntiDiCorrezioneQuery = selezionePuntiCorrelazione % (ext_str)
    cursor = connections['visio'].cursor()
    cursor.execute(puntiDiCorrezioneQuery)
    ogr_params = ""
    for row in cursor.fetchall():
        ogr_params += "-gcp {:.2f} {:.2f} {:.2f} {:.2f} ".format(*row)
    return ogr_params + mode

def wktExtentFromQuery(query, geom="the_geom"):
    envelope_query = "select st_astext(ST_Envelope(ST_Union(%s))) as tb_extent from (%s) as subq" % (geom, query)
    cursor = connections['visio'].cursor()
    cursor.execute(envelope_query)
    row = cursor.fetchone()
    return row[0]

def contestoCatastoCorretto(ext):
    ext_str = (','.join([str(i) for i in ext]))
    query = "select * from catasto.particelle_cat where st_intersects(the_geom,st_buffer(ST_MakeEnvelope({extent_string},3003),50))".format(extent_string=ext_str)
    return correzioneFromQuery(query)

def geometriaCorretta(geom):
    query = "select ST_GeomFromText('%s') as the_geom" % geom.wkt
    collection = json.loads(correzioneFromQuery(query))
    geojson_geom = collection["features"][0]["geometry"]
    return GEOSGeometry(json.dumps(geojson_geom))

def bufferedExtFromWkt(wkt,buffer=500):
    return GEOSGeometry(wkt).buffer(buffer).extent

def correzioneFromQuery(query, ext=None):
    geojson_filename = temporary_filename(".geojson") 
    if not ext:
        ext = bufferedExtFromWkt(wktExtentFromQuery(query))
    ogrcmd = comandoCorrelationeCatasto.format(query=query, opts=getGcpParams(ext), geojson_output=geojson_filename, **settings.DATABASES['visio'])  
    os.system(ogrcmd)
    with open(geojson_filename,"r") as output:
        geojson_output = output.read()
    return geojson_output

def inserimento_correlazione(wkt, user):
    insert_fields = {
        "geom": wkt,
        "sviluppo": "creato da %s %s" % (user, str(datetime.now()))
    }
    query = """
INSERT INTO altro.origine_destinazione_correlazione_catasto(ogc_fid,the_geom, sviluppo)
VALUES (default,ST_GeomFromText('{geom}',3003), '{sviluppo}')
RETURNING ogc_fid;""".format(**insert_fields)
    cursor = connections['visio'].cursor()
    cursor.execute(query)
    row = cursor.fetchone()
    return row[0]

def modifica_correlazione(fid, wkt, user):
    update_fields = {
        "fid": fid,
        "geom": wkt,
        "sviluppo": "modificato da %s %s" % (user, str(datetime.now()))
    }
    query = """
UPDATE altro.origine_destinazione_correlazione_catasto
SET 
    the_geom = ST_GeomFromText('{geom}',3003), 
    sviluppo = '{sviluppo}'
WHERE ogc_fid = {fid}
RETURNING (SELECT ST_AsText(the_geom) as old_wkt FROM altro.origine_destinazione_correlazione_catasto WHERE ogc_fid = {fid});""".format(**update_fields)
    cursor = connections['visio'].cursor()
    cursor.execute(query)
    row = cursor.fetchone()
    return row[0]

def disabilita_correlazione(fid, apply_value, user):
    azione = "disabilitato" if apply_value else "abilitato"
    disable_fields = {
        "fid": fid,
        "sviluppo": "%s da %s %s" % (azione, user, str(datetime.now())),
        "val": "true" if apply_value else "false"
    }
    query = """
UPDATE altro.origine_destinazione_correlazione_catasto
SET 
    disabilitato = {val}, 
    sviluppo = '{sviluppo}'
WHERE ogc_fid = {fid}
RETURNING *;""".format(**disable_fields)
    cursor = connections['visio'].cursor()
    cursor.execute(query)
    if cursor.fetchone():
        return True
    else:
        return None
    



def GeomFromCoordinateCatastali(coordinateCatastali, formato='WKT', srid_origine = None, srid_destinazione = None, check=None, tab = 'particelle_cat',correction=False):
    
    if is_wkt(coordinateCatastali):
        geom = is_wkt(coordinateCatastali)
        feedback = coordinateCatastali
    else:
        geom = None
        query,feedback = QueryFromCoordinateCatastali(coordinateCatastali, table = tab)
        if query:
            cursor = connections['visio'].cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            if row and row[1]:
                geom = GEOSGeometry( row[1] )
            else: #le coordinate catastali sono inesistenti
                if check:
                    return (None,None)
                else:
                    return None
    if geom:
        if correction:
            geom = geometriaCorretta(geom)
        if srid_destinazione:
            geom.transform(srid_destinazione)
        if formato == 'WKT':
            geomRes = geom.wkt
        elif formato == 'geojson':
            geomRes = geom.geojson
        elif formato == 'KML':
            geomRes = geom.kml
        elif formato == 'ogr':
            geomRes = geom.ogr
        if check:
            return (geomRes,feedback)
        else:
            return geomRes

    return None

def queryParticelle_ (coordinateCatastali):
    if not coordinateCatastali:
        return []
    sqlWhere, feedback = coordinateCatastaliToWHERE(coordinateCatastali)
    psql=   'SELECT foglio, mappale, ST_AsText(ST_SetSRID(catasto.particelle_cat.the_geom,3003)) AS wkt ' +\
            'FROM catasto.particelle_cat WHERE %s' % sqlWhere

    cursor = connections['visio'].cursor()
    cursor.execute(psql)
    resultArray = []
    for row in cursor.fetchall():
        if row[0][-1:] != '0':
            foglio = row[0].lstrip('0')
        else:
            foglio = row[0][:-1].lstrip('0')
        resultArray.append([foglio,row[1],row[2]])
    return resultArray

def queryParticelle (coordinateCatastali, correction=False):
    if not coordinateCatastali:
        return []
    sqlWhere, feedback = coordinateCatastaliToWHERE(coordinateCatastali)
    psql="""
SELECT gid, 
    TRIM(BOTH '0' FROM foglio) as foglio, 
    mappale, 
    catasto.particelle_cat.the_geom AS the_geom
FROM catasto.particelle_cat WHERE %s""" % sqlWhere
    return geojsonCatastoFromQuery(psql, correction=correction)

def geojsonCatastoFromQuery(query, correction=False):
    jsonExtractionQuery = particelleGeojson.format(query=query)
    cursor = connections['visio'].cursor()
    cursor.execute(jsonExtractionQuery)
    result = cursor.fetchone()
    if correction:
        return correzioneGeojson(result[0])
    return result[0]

def correzioneGeojson(geojson_string):
    geojson_obj = json.loads(geojson_string)
    if not geojson_obj["features"]:
        geojson_obj["features"] = []
    corrected_feats = []
    for feat in geojson_obj["features"]:
        geom = GEOSGeometry(json.dumps(feat["geometry"]))
        feat["geometry"] = json.loads(geometriaCorretta(geom).geojson)
        corrected_feats.append(feat)
    geojson_obj["features"] = corrected_feats
    return json.dumps(geojson_obj)

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def queryPRG(featureWKT, piano="vigente",pre = True, verboso=True, approssimazione=0, certificato=None):
    
    #clear dock widget
    resultArray = []
    if settings.BASE_DEV == '/home/enrico/':
        return resultArray #TEST!!

    cursor_check = connections['visio'].cursor()
    cursor_decaduto = connections['visio'].cursor()

    queryVincoloCimiteriale = checkVincoloCimiterialeTemplate % featureWKT
    cursor_check.execute(queryVincoloCimiteriale)
    checkVincoloCimiteriale = cursor_check.fetchone()[0]

    queryAUC = checkTemplateAUC % (featureWKT,featureWKT)
    cursor_check.execute(queryAUC)
    checkAUC = cursor_check.fetchone()[0]

    cursor = connections['visio'].cursor()

    cursor.execute(listaDestinazioni % piano)
    zoning = []
    check_decaduto = []
    for row in cursor.fetchall():
        zoning.append(row[0])
        if row[2]:
            check_decaduto.append(row[1])

    queryDef = estrazioneTemplate % (featureWKT, piano, featureWKT)
    cursor.execute(queryDef)

    cursor_decaduto = connections['visio'].cursor()
    queryDef = checkDecadenza % (featureWKT)
    cursor_decaduto.execute(queryDef)

    row = cursor_decaduto.fetchone()
    decadenza_geom =  fix_geometry(row[0]) if row[0] else None

    cursor_viab = connections['visio'].cursor()
    queryDef = checkViab % (featureWKT,featureWKT)

    cursor_viab.execute(queryDef)
    row = cursor_viab.fetchone()
    viabilita_geom = fix_geometry(row[0])  if row[1] else None

    areaNonPianificata = round (row[1] if row[1] else 0, 0)


    #self.dockwidget.tableWidget.setRowCount(query.size())
    areaPianificata = 0
    areaComunale = 0
    inCentroStorico = None
    area_di_rispetto_geom = None
    db_rows = dictfetchall(cursor)

    for rc,db_row in enumerate(db_rows):
        if db_row["nome"] == 'Area comunale':
            areaComunale = db_row["super"]
        elif db_row["nome"] in zoning:
            areaPianificata += db_row["super"]

        if db_row["layer"] == 55:
            inCentroStorico = True

        if db_row["layer"] == 27:
            area_di_rispetto_geom = GEOSGeometry(db_row["wkt_geom"])

    #cursor.execute(queryDef)
    if pre:
        table = [["Piano degli interventi vigente:","","",""]]
    else:
        table = []
    ultima_riga = None
    in_zoning = 0
    for row in db_rows:
        row_layer = row["layer"]

        if not row["definizione"]:
            continue
        if  row["layer"] == 55  : #non inserire Centro storico nella lista
            continue
        if row["nome"] == 'Area comunale':
            dest = 'Totale'
            super_filtered = row["super"]
            art = ""
            if not pre:
                continue
        elif row["layer"] == 27:
            #sostituito da dicitura parte per destinazione singola
            #ultima_riga = ['con sovrapposta area di rispetto',round(row[1],0),round(row[1]/areaComunale*100,1),"33"]
            continue
        else:
            if row["super"]/areaComunale*100 < approssimazione:
                #strano il continue non funziona quando ultimo ciclo
                continue
            #filtraggio necessario per evitare doppi perimetri
            super_filtered = row["super"] if row["super"] <= areaComunale else areaComunale
            if row["definizione"] and "%" in row["definizione"]:
                if verboso:
                    dest = row["definizione"].replace('%', " "+str(row["note"] or ''))
                else:
                    dest = row["nome"] + " "+str(row["note"] or '')
            else:
                if verboso:
                    dest = row["definizione"]
                else:
                    dest = row["nome"]
            
            if area_di_rispetto_geom and row["nome"] in zoning: #and certificato
                
                cg = GEOSGeometry(row["wkt_geom"], 3003).buffer(0)
                
                ar = area_di_rispetto_geom.buffer(0)
                if ar.intersects(cg) and ar.intersection(cg).area > 5:
                    dest += " con sovrapposta area di rispetto"
            
            if row["layer"] in check_decaduto:
                cg = fix_geometry( row["wkt_geom"]).buffer(-0.5)
                if decadenza_geom and decadenza_geom.intersects(cg):
                    check_parziale = decadenza_geom.intersection(cg).area
                    if cg.area - check_parziale < 1:
                        dest += " (Destinazione decaduta *)"
                    else:
                        dest += " (Destinazione parzialmente decaduta *)"

            #if area_di_rispetto_geom and certificato and row["nome"] in zoning and GEOSGeometry(row["wkt_geom"]).intersects(area_di_rispetto_geom):
            #    dest += " con sovrapposta area di rispetto"
            if row["nome"] in zoning: #controlla se in destinazioni di zoning per clausola 'PARTE A'
                in_zoning += 1
                dest = "§" + str(dest) # necessaria la conversione per evitare errori su destinazioni nulle

            art = row["articolo"]

        if pre:
            table.append([dest,round(super_filtered,0),round(super_filtered/areaComunale*100,1),art])
        else:
            table.append([dest,"","",art])

    #areaNonPianificata = round(areaComunale-areaPianificata,0)
    try:
        areaNonPianificataPercent = round((areaComunale-areaPianificata)/areaComunale*100,1)
    except ZeroDivisionError:
        areaNonPianificataPercent = None

    if areaNonPianificata:
        viab_def = "Sedi stradali"
        if decadenza_geom and decadenza_geom.intersects(viabilita_geom):
            
            print ("viabilita_geom", viabilita_geom.ewkt)
            print ("decadenza_geom", decadenza_geom.ewkt)
            check_parziale = decadenza_geom.buffer(0).intersection( viabilita_geom.buffer(0) ).area
            
            if viabilita_geom.area - check_parziale < 1:
                viab_def += " (Destinazione decaduta *)"
            else:
                viab_def += " (Destinazione parzialmente decaduta *)"
        #apponi la riga per le parti non pianificate che diventa sede stradale per la produzione del certificato
        if pre:
            table.append([viab_def,areaNonPianificata,areaNonPianificataPercent,""])
        else:
            if not inCentroStorico and areaNonPianificataPercent > 1.0: # se l'area non pianificata è minima tralasciare l'indicazione nel certificato
                table.append([viab_def,"","",""])

    #scansione della tabella per l'apposizione della clausola parte
    if in_zoning > 1:
        clausola = 'Parte '
    else:
        clausola = ''
    for r in table:
        if r[0]:
            r[0] = r[0].replace('§',clausola)

    #apposizione dell'ultima riga con sovrapposta area di rispetto
    if checkVincoloCimiteriale:
        table.append(["compresa nella fascia di rispetto cimiteriale (R.D.27.07.1934 n.1265 e s.m.i.)","","",""])
    elif ultima_riga:
        table.append(ultima_riga)

    if checkAUC:
        if checkAUC < areaComunale:
            preAUC = "parzialmente compresa nell'"
        else:
            preAUC = "compresa nell'"
    else:
        preAUC = "esclusa dall'"
    
    table.append([preAUC+"ambito di urbanizzazione consolidata ex L.R. 14/2017 di cui all D.C.C 53 del 28/07/2020 ","","",""])

    #se in centro storico appendi destinazioni proprie del centro storico. non vale per la generazione del certificato
    if pre and inCentroStorico:
        table = table + queryCS(featureWKT, pre=pre)

    #se generazione certificato in centro storico non genera descrizioni in mancanza di destinazioni
    if not pre and rc<2 and inCentroStorico:
        table = None

    return table

def verificaInCentroStorico(featureWKT):

    if settings.BASE_DEV == '/home/enrico/':
        return None #TEST!!

    cursor = connections['visio'].cursor()
    queryDef = estrazioneCheckCSTemplate % featureWKT
    cursor.execute(queryDef)
    if cursor.fetchone():
        return True
    else:
        return None

def destinazioniPRG(featureWKT, piano = "vigente"):
    cursor = connections['visio'].cursor()
    queryDef = destinazioniTemplate % (featureWKT,featureWKT,piano,featureWKT)
    cursor.execute(queryDef)
    row = cursor.fetchone()
    return (row[0],row[1])

def checkAreaTroppoEstesa(featureWKT):
    cursor = connections['visio'].cursor()
    queryDef = checkAreaTroppoEstesaTemplate.format(wktgeom=featureWKT)
    cursor.execute(queryDef)
    row = cursor.fetchone()
    return row[0]

def queryPAT(featureWKT, pre = True):

    if settings.BASE_DEV == '/home/enrico/':
        return [["","","",""]] #TEST!!

    cursor1 = connections['visio'].cursor()
    cursor2 = connections['visio'].cursor()
    cursor3 = connections['visio'].cursor()
    queryDef = estrazionePatTemplate % (featureWKT, featureWKT)
    cursor1.execute(queryDef)
    if pre:
        table = [["","","",""],["Piano di Assetto del Territorio:","","",""]]
    else:
        table = []
    decodificato = []
    for row in dictfetchall(cursor1):
        sql = """SELECT descrizione,nta,campo,id FROM pat.decodifica_pat WHERE layer='%s' ORDER BY layer""" % row['layer']
        cursor2.execute(sql)
        dettaglio = cursor2.fetchone()
        if not dettaglio:
            #layer non compreso nella mappa di decodifica: per esempio i confini comunali
            continue
        cursor2.execute(sql)
        if dettaglio[2]:
            #layer compreso nella mappa di decodifica ma con campo di decodifica
            for dettaglio in cursor2.fetchall():
                valore_target = row[dettaglio[2]] or ""
                sql = """SELECT descrizione,nta,campo,id FROM pat.decodifica_pat WHERE layer='%s' AND campo = '%s' AND valore = '%s'""" % (row['layer'],dettaglio[2],valore_target.replace("'","''").strip())
                cursor3.execute(sql)
                dettaglio2 = cursor3.fetchone()
                if dettaglio2:
                    if not dettaglio2[3] in decodificato: # controlla se il valore è già stato decodificato
                        table.append([dettaglio2[0],"","", dettaglio2[1]])
                        decodificato.append(dettaglio2[3]) # segna valore come decodificato
        else:
            #layer compreso nella mappa di decodifica senza campo di decodifica
            if not dettaglio[3] in decodificato: # controlla se il valore è già stato decodificato
                table.append([dettaglio[0],"","",  dettaglio[1]])
                decodificato.append(dettaglio[3]) # segna valore come decodificato

    return table

def queryZONOM(featureWKT, pre = True):

    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazioneZONOMTemplate % (featureWKT,featureWKT)
    cursor.execute(queryDef)
    table = []
    row = cursor.fetchone()
    if not row or row[1] != 1:
        return ["","","", ""]
    if pre:
        return [["","","",""],["Zona Omogenea ai sensi del DM 1444/1968: "+row[0],"","",""]]
    else:
        return [row[0],"","", ""]


def queryPI2030(featureWKT, pre = True, tolleranza=0.1):
    area_tot = GEOSGeometry(featureWKT).area
    cursor = connections['visio'].cursor()
    queryDef = estrazionePI2030Template % featureWKT
    cursor.execute(queryDef)
    table = []
    if pre:
        table = [["","","",""],["Nuovo PI 2030:","","",""]]
    rows = dictfetchall(cursor)
    inzoning = []
    for row in rows:
        if 100*row["area"]/area_tot > tolleranza:
            if row["filename"] == "b0501011_ZTO":
                inzoning.append(row["area"])

    if len(inzoning) == 1 and 100*(1 -inzoning[0]/area_tot) <= tolleranza:
        partezoning = False
    else:
        partezoning = True

    for row in rows:
        if 100*row["area"]/area_tot > tolleranza:
            pre = ""
            if row["filename"] == "b0501011_ZTO" and partezoning:
                pre = 'Parte '
            if row["filename"] == 'b0501021_AmbPUA' and not row["tipo"] in ('PUA approvato','PUA convenzionato'):
                continue
            if row["filename"] in (
                    'b0501041_Perimetri',
                    'b0501051_ViabProg',
                    'b0501031_Schede',
                    'b0501021_AmbPUA',
                    'b0501061_Architetture900',
                    'b0501051_PerimetriCS',
                ):
                pre = "Sovrapposta a "
                if area_tot - row["area"] > 0.1:
                    pre = "Con parte sovrapposta a "
                if row["tipo"] in ('centro storico',):
                    pre = "Ricadente in "

            if row["descrizione"]:
                label = pre + row["descrizione"].format(**row)
            else:
                if not row["filename"] == 'b0501021_AmbPUA':
                    label = row["zto"] + " id:"+ (row["id"] or '')+" tipo:"+(row["tipo"] or "")+" sigla:"+(row["sigla"]or "")

            table.append([
                label,
                round (row["area"],1),
                round (100*row["area"]/area_tot,1),
                row["nto"]
            ])
    return table

def queryISOVALORE(featureWKT, pre = True):
    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazioneISOVALORETemplate % (featureWKT,)
    cursor.execute(queryDef)
    table = []
    row = cursor.fetchone()
    if not row:
        return ["","","", ""]
    if pre:
        return [["","nuovo","aree urb.","aree non urb."],["Zona Isovalore: %s/%s" % (row[0],row[1]),"%d €/mq" % row[2],"%d €/mc" % row[3],"%d €/mc" % row[4]]]
    else:
        return [row[0],row[2],row[3],row[4]]

def queryUNVOL(featureWKT, pre = True):
    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazioneUNVOLTemplate % (featureWKT,)
    cursor.execute(queryDef)
    table = []
    row = cursor.fetchone()
    if not row:
        return ["","","", ""]
    if pre:
        return [["","","",""],["Unità volumetrica 2007: ","", "%d mq" % row[1],"%d mc" % row[0]]]
    else:
        return ["unita volumetrica","",row[1],row[1]]


def queryPAI(featureWKT, pre = True):

    if settings.BASE_DEV == '/home/enrico/':
        return [["","","",""]] #TEST!!

    #clear dock widget
    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazionePaiTemplate % (featureWKT,)
    cursor.execute(queryDef)
    table = []
    for row in cursor.fetchall():
        table.append([row[0],"","", ""])
    queryDef = estrazioneBSLTemplate % (featureWKT,)
    cursor.execute(queryDef)
    data = cursor.fetchone()
    if data:
        table.append(["Ambito del Bacino scolante in laguna di Venezia con classificazione di tipo 1","","", ""])
    if pre and table != []:
        pre = [["","","",""],["Piano di Assetto Idrogeologico (PAI):","","",""]]
        table = pre+table
    return table

def queryPGRA(featureWKT, pre = True):
    cursor = connections['visio'].cursor()
    
    table = []
    for template in [estrazionePGRAPericoloTemplate, estrazionePGRARischioTemplate]:
        queryDef = template % featureWKT
        cursor.execute(queryDef)
        rows_affected = cursor.rowcount
        area_totale = GEOSGeometry(featureWKT).area

        for row in cursor.fetchall():
            if abs(area_totale-row[2]) > 1:
                dest = "Parte area di " + row[1].lower().replace("rischio", "rischio idraulico")
            else:
                dest = "Area di " +row[1].lower().replace("rischio", "rischio idraulico")
            table.append([dest,"","", ""])

    if pre and table != []:
        pre = [["","","",""],["Piano di Gestione del Rischio di Alluvioni (PGRA):","","",""]]
        table = pre+table
    
    return table

def queryPRA(featureWKT, pre = True):
    cursor = connections['visio'].cursor()
    
    table = []
    queryDef = estrazionePRATemplate % featureWKT
    cursor.execute(queryDef)
    rows_affected = cursor.rowcount
    area_totale = GEOSGeometry(featureWKT).area

    areePRA = ''
    for row in cursor.fetchall():
        if abs(area_totale-row[1]) > 1:
            areePRA += "in parte %s, " % row[0]
        else:
            areePRA = "%s, " % row[0]

    if areePRA:
        dicitura = "l'area è situata all'interno della Zona di tutela %s, di cui al Piano di Rischio Aeroportuale, in fase di adozione, redatto ai sensi dell'art. 707 comma 5  del D.Lgs. 09.05.2005, n. 96 \"Revisione della parte aeronautica del Codice della navigazione\""

        table.append([dicitura % areePRA[:-2],"","", ""])

    if pre and table != []:
        pre = [["","","",""],["Piano di Rischio Aeroportuale (art. 707 comma 5  del D.Lgs. 09.05.2005, n. 96), in fase di adozione:","","",""]]
        table = pre+table
    
    return table


def queryBonifiche(featureWKT, pre = True):

    if settings.BASE_DEV == '/home/enrico/':
        return [["","","",""]] #TEST!!

    #clear dock widget
    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazioneBonificheTemplate % (featureWKT)
    cursor.execute(queryDef)
    print (queryDef)
    if pre:
        table = [["","","",""],["Bonifiche ambientali:","","",""]]
    else:
        table = []
    for row in cursor.fetchall():
        table.append([row[1],"","", "n." + str(row[0])])
    return table

def queryCS(featureWKT, pre = True):

    if settings.BASE_DEV == '/home/enrico/':
        return [["","","",""]] #TEST!!

    areatot = GEOSGeometry(featureWKT).area

    #fase1 individuazione Unità di piano
    upArray = []
    cursor1 = connections['visio'].cursor()
    cursor2 = connections['visio'].cursor()
    queryUP = estrazioneCSUPTemplate % (featureWKT)
    print (queryUP)
    cursor1.execute(queryUP)
    if pre:
        table = [["","","",""],["Piano degli Interventi del Centro Storico:","","","9"]]
    else:
        table = []

    for up in cursor1.fetchall():
        if 100*up[2]/areatot < 0.1:
            continue
        queryDest = estrazioneCSTemplate % (featureWKT,up[0])
        print ("queryCS_queryDest",queryDest)
        cursor2.execute(queryDest)
        defUp = ""
        control = []
        nta = ''
        nto = ''
        destinazioni_cs = dictfetchall(cursor2)

        #determinazione classe dell'unità di piano
        control_up = []
        for dest in destinazioni_cs:
            if dest['layer'] == 'PR_02_01_01_fl':
                control_up.append( dest['norma1'].replace('-','').replace('     ',' ') )
        
        if control_up:
            control_up.sort()
            defUp += control_up[0] 
            prefisso = " con aree scoperte"
        else:
            defUp += "aree scoperte"
            prefisso = ""
            
        #individuazione di altre informazioni
        def_dest = []
        for dest in destinazioni_cs:
            if dest["area"] < 0.1:
                continue
            d = None
            print ("CONTROL", dest)
            #if dest['layer'] != 'PR_avanzi_sy' and dest['super']/up[1] < 0.02: #controllo per eliminare falsi positivi all'intersezione
            #    continue
            if dest['layer'] == 'PR_10_00': # aree scoperte
                if dest['despro'] or dest['norma1']:
                    d = prefisso + " destinate a %s %s" % ((dest['despro'] or "").replace('-',''), (dest['norma1'] or "").replace('-',''))
            elif dest['layer'] == 'PR_02_01_01_fl': # aree coperte
                if dest['despro1']:
                    d = " con aree coperte destinate a "+dest['despro1'].replace('-','')
            elif dest['layer'] == 'PR_avanzi_sy': # punti fake#
                if dest['livello'] == 50:
                    defUp += " con  modifica n. %s ai sensi dell'art 5.1.1 del P.A.T." % dest['code_nam']
            if d and not d in def_dest:
                def_dest.append(d)
            
            for n in ('piano3','piano4','urba01','urba02','duso01','duso02'):
                if dest[n] and not dest[n] in nta:
                    nta += dest[n]+", "
            for n in ('nto_norma1','nto_despro1','nto_despro'):
                if dest[n] and not dest[n] in nto:
                    nto += dest[n]+", "

        defUp += ",".join(def_dest)
        nta = nta[:-2]
        nto = nto[:-2]
        rif_norma = nta.lower().replace('art','') + " (N.T.O Nuovo Piano Interventi: %s)" % nto
        newUPtab = [defUp.lower(),str(round(up[2],1)) ,str(round(100*up[2]/areatot,1)), rif_norma]
        print("DEST",newUPtab)
        table.append(newUPtab)
            #if not newUPtab in table:
            #    table.append(newUPtab)

    return table


def exqueryCS(featureWKT, pre = True):

    if settings.BASE_DEV == '/home/enrico/':
        return [["","","",""]] #TEST!!

    #clear dock widget
    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazioneCSTemplate % (featureWKT,featureWKT)
    cursor.execute(queryDef)
    if pre:
        table = [["","","",""],["Piano degli Interventi del Centro Storico:","","","9"]]
    else:
        table = []

    control = []

    for row in cursor.fetchall():
        if row[1] == 'PR_10_00': # aree scoperte
            despro = "area scoperta destinata a " + (row[4] or "")
            norma = row[2] or ""
            if row[4] or row[2]:
                table.append([despro.lower() +" " + norma.lower(),"","", ""])
        elif row[1] == 'PR_02_01_01_fl': # aree coperte
            despro = " area coperta destinata a: " + (row[3].replace('-','') or "")
            norma = " con destinazione d'uso: "+(row[2].replace('-','') or "")
            nta = ""
            if norma:
                for i in range(5,10):
                    if row[i]:
                        nta += row[i]+", "
                nta = nta[:-2]
            if row[3] or row[2]:
                table.append([despro.lower() + norma.lower(),"","", nta.lower().replace('art','')])
        elif row[1] == 'PR_avanzi_sy#######': # punti fake#
            if row[11] == 50:
                table.append([row[12].lower(),"","", ""])
    '''
    for row in cursor.fetchall():
        if row[1] == 'PR_10_00': # aree scoperte
            despro = "Area a " + (row[4] or "")
            norma = row[2] or ""
            if row[4] or row[2]:
                table.append([despro.lower() +" " + norma.lower(),"","", ""])
        elif row[1] == 'PR_02_01_01_fl': # aree coperte
            despro = " con destinazione " + (row[3].replace('-','') or "") #despro = " - Destinazione: " + (row[3].replace('-','') or "")
            norma = (row[2].replace('-','') or "") #norma = "Norma: " + (row[2].replace('-','') or "")
            if norma:

            nta = ""
            for i in range(5,10):
                if row[i]:
                    nta += row[i]+" "
            if row[3] or row[2]:
                table.append([norma.lower() +despro.lower() ,"","", nta.lower().replace('art','')])
        elif row[1] == 'PR_avanzi_sy#######': # punti fake#
            if row[11] == 50:
                table.append([row[12].lower(),"","", ""])
    '''

    return table

def queryPUA(featureWKT, pre = True):

    if settings.BASE_DEV == '/home/enrico/':
        return [["","","",""]] #TEST!!

    tipologia_choice = collections.OrderedDict(tipologia_choice_map)

    ex_tipologia_choice = {
        "PG":"Piano guida",
        "DEL":"Delimitazione d'ambito",
        "PdL":"Piano di lottizzazione",
        "PdR":"Piano di recupero",
        "PP":"Piano particolareggiato",
        'PS':"Piano S?",
        'PdL/AdP':"Accordo di programma",
        'PdL/Erp':"PEEP",
        'SC':"Servizio civico",
        'PIRU':"PIRU",
        'PIRUEA':"PIRUEA",
        'PUA':"Piano urbanistico attuativo",
        'SUAP':"Piano SUAP"
    }

    #clear dock widget
    resultArray = []
    cursor = connections['default'].cursor()
    queryDef = estrazionePUATemplate % (featureWKT)
    cursor.execute(queryDef)
    if pre:
        table = [["","","",""],["Piani Urbanistici Attuativi (PUA):","","",""]]
        spec = ''
    else:
        table = []
        spec = ' per la predisposizione di un piano urbanistico attuativo'

    trovato_PUA = False
    for row in dictfetchall(cursor):
        if row['denominazione']:
            pua_nome = row['denominazione']
        else:
            pua_nome = "%s %s" % (tipologia_choice[row['tipologia']],row['ditta'])
        if row['tipologia'].lower() == 'pg':
            if row['delib_appr']:
                provvedimento = "Delibera "+ row['delib_appr']
            else:
                provvedimento = ''
            table.append(['''l'area è interessata da piano guida%s; rif "%s"''' % (spec,row['ditta'].upper()),provvedimento,"",""])
        elif row['tipologia'].lower() == 'def':
            if row['delib_amb']:
                provvedimento = "Delibera "+ row['delib_amb']
            else:
                provvedimento = ''
            table.append(['''l'area è interessata da delimitazione dell'ambito di intervento; rif "%s"''' % (spec,row['ditta'].upper()),provvedimento,"",""])
        elif row['tipologia'].lower() == 'altro':
            table.append(["","","",""])
        else:
            provvedimenti= 'provvedimenti: '
            if row['delib_amb']:
                provvedimenti += "Delimitazione %s, " % row['delib_amb']
            if row['delib_ado']:
                provvedimenti += "Adozione %s, " % row['delib_ado']
            if row['delib_appr']:
                provvedimenti += "Approvazione %s, " % row['delib_appr']
            if row['delib_var']:
                provvedimenti += "Variante %s, " % row['delib_var']
            if provvedimenti == 'provvedimenti: ':
                provvedimenti = ''
            else:
                provvedimenti = provvedimenti[:-2]

            if row['stato'] in ('decaduto', 'sospeso', 'revocato'):
                stato = ' %s' % row['stato']
            else:
                stato = ''

            table.append(['''l'area è compresa nel piano urbanistico attuativo%s: "%s"''' % (stato, pua_nome.upper()),provvedimenti,"", ""])
        if row['cdu']:
            table[-1][0] = '%s%s%s' % ( table[-1][0], '; ' if table[-1][0] else '',  row['cdu'].replace('\n', ' '))
        trovato_PUA = True
    if trovato_PUA:
        return table
    else:
        return []

def queryViario(q=None):
    resultArray = []
    cursor = connections['visio'].cursor()
    if q:
        queryDef = estrazioneViarioQ % q
    else:
        queryDef = estrazioneViario
    cursor.execute(queryDef)
    for row in cursor.fetchall():
        resultArray.append(row[0])
    return resultArray

def queryVia(nome):
    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazioneViaLine % nome.replace("'","''")
    cursor.execute(queryDef)
    res = cursor.fetchone()
    if res:
        return {
            "via": nome,
            "wkt": res[0]
        }

def queryCivici(nome_via):
    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazioneCivici % nome_via.replace("'","''")
    cursor.execute(queryDef)
    for row in cursor.fetchall():
        resultArray.append({
            "via": nome_via,
            "num_completo": row[1],
            "num": row[2],
            "let": row[3] or "",
            "wkt": row[0]
        })
    return resultArray

def geocode(nome):
    resultArray = []
    cursor = connections['visio'].cursor()
    queryDef = estrazioneGeocode % nome.replace("'","''")
    cursor.execute(queryDef)
    
    features_geojson = {
        "type": "FeatureCollection",
        "features": []
    }

    for row in cursor.fetchall():
        geometry = GEOSGeometry(row[2]).transform(4326, clone=True)
        geometry_json = json.loads(geometry.point_on_surface.json)
        properties = {
            "osm_id": str(row[0]),
            "bbox": geometry.extent,
            "country": "Italia",
            "osk_key": "highway",
            "city": "Padova",
            "osm_value": "tertiary",
            "postcode": "35020",
            "name": row[1],
            "state": "Veneto",
            "wkt": GEOSGeometry(row[2]).wkt
        }
        feature = {
            "type": "Feature",
            "geometry": geometry_json,
            "properties": properties,
        }
        features_geojson["features"].append(feature)
    return features_geojson