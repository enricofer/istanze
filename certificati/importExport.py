# -*- coding: utf-8 -*-
"""
/***************************************************************************
 cartawebImportExport

 PROCEDURA per importare ed esportare dati da cartaweb
                              -------------------
        begin                : 2015-06-18
        git sha              : $Format:%H$
        copyright            : (C) 2015 by Enrico Ferreguti
        email                : ferregutie@comune.padova.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from django.contrib.gis.geos import GEOSGeometry
from django.conf import settings

import sys
import uuid
import json
import os
import time
import shutil

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


def execute (cmd):
    print ('EXECUTE OS COMMAND: ',cmd, file=sys.stderr)
    logCmdFilename = os.path.join(settings.MEDIA_ROOT,'certificati/cmd.log')
    os.system('%s >> "%s"  2>&1' % (cmd,logCmdFilename))
    os.system('echo "<br/>\n<br/>\n" >> "%s"  2>&1' % (logCmdFilename))

punti = 1
linee = 2
aree = 3
boh = 99

template_txt = '''
{
    "symcolor3": -1,
    "gid": "%(gid)s",
    "symcolor": 0,
    "symangle": 0,
    "symtypename": "TextSymbol",
    "attrib": {
        "labeled": false,
        "wkid": 3003
    },
    "geomtype": "MAPPOINT",
    "symtext": "%(symtext)s",
    "cmenu": {
        "link": null,
        "builtInItems": {
            "quality": false,
            "play": false,
            "loop": false,
            "rewind": false,
            "forwardAndBack": false,
            "save": false,
            "print": false,
            "zoom": false
        },
        "clipboardMenu": false,
        "customItems": [
            {
                "enabled": true,
                "caption": "Cancella",
                "separatorBefore": false,
                "visible": true
            }
        ],
        "clipboardItems": {
            "cut": false,
            "paste": false,
            "selectAll": true,
            "copy": false,
            "clear": false
        }
    },
    "symltype": "",
    "gname": "%(gname)s",
    "symtype": "Arial",
    "geomarray": [
        {
            "x": %(x)s,
            "y": %(y)s,
            "spatialReference": {
                "wkid": %(geomsr)s
            }
        }
    ],
    "symfonte": "",
    "symalpha2": 1,
    "geomsr": %(geomsr)s,
    "symfontp": "middle",
    "symwidth": 1,
    "symsize": 15,
    "symcolor2": -1,
    "symalpha1": 1
}
'''

template_poligon = '''
{
    "geomarray": %(geomarray)s,
    "symltype": "solid",
    "symwidth": %(symwidth)s,
    "geomtype": "%(geomtype)s",
    "symtypename": "%(symtypename)s",
    "symcolor2": %(symcolor2)s,
    "symalpha1": %(symalpha1)s,
    "symfontp": "middle",
    "symsize": %(symsize)s,
    "symfonte": "",
    "attrib": {
        "wkid": %(geomsr)s,
        "labeled": true
    },
    "symcolor": %(symcolor)s,
    "symangle": 0,
    "geomsr": %(geomsr)s,
    "symtype": "%(symtype)s",
    "gid": "%(gid)s",
    "gname": "%(gname)s",
    "symtext": "%(gname)s",
    "symcolor3": %(symcolor3)s,
    "symalpha2": %(symalpha2)s
}
'''

def cartawebExport(wkt):
    parametri = {}
    parametri["symwidth"] = 1.5
    parametri["symcolor"] = 16777215
    parametri["symcolor2"] = 16711680
    parametri["symcolor3"] = 0
    parametri["symalpha1"] = 0
    parametri["symalpha2"] = 1
    parametri["symsize"] = 1.5

    fa = "["


    parametri["geomtype"] = 'POLYGON'
    parametri["symtypename"] ="SimpleFillSymbol"
    parametri["symtype"] = "solid"
    parametri["geomsr"] = "3003"

    multigeom = GEOSGeometry(wkt)

    #print pointCollection
    for geom in multigeom:
        for ring in geom:
            ga = "["
            for point in ring:
                #print ("VERTICE:",point, file=sys.stderr)
                vString = '{"x":%s,"y":%s,"spatialReference":{"wkid":%s}},' % (point[0],point[1],parametri["geomsr"])
                ga += vString
            ga = ga[:-1] + "]"
            
            #print ("GA:",ga, file=sys.stderr)
        
            parametri["gid"] = "export wkt"
            parametri["gname"] = "export wkt"
            parametri["geomarray"] = ga
        
            #print parametri
            featDef = template_poligon % parametri
            fa += featDef+',\n'
    fa = fa[:-2]+"]"

    #print ("FA:",fa, file=sys.stderr)
    
    return fa.replace('\n','').replace('    ','')
    
    
def geojsonToCartawebExport(geojson):
    esriJSON = ''
    for feat in geojson['features']:
        geometry = GEOSGeometry(feat)
        esriJSON += cartawebExport(geometry.wkt)
    return esriJSON
    
def geometryConversion(wkt_geom,format='geojson'):
    temp_path = os.path.join(settings.MEDIA_ROOT, 'certificati', 'temp')
    print ("BASE_DIR",settings.BASE_DIR)
    print ("BASE_DEV",settings.BASE_DEV)
    print ("MEDIA_ROOT",settings.MEDIA_ROOT)
    print ("temp_path",temp_path)
    if not os.path.exists(temp_path):
        os.makedirs(temp_path)
    #cancella i vecchi file nella directory temporanea
    now = time.time()
    for f in os.listdir(temp_path):
        file_path = os.path.join(temp_path,f)
        print ("FILE:",f, os.stat(file_path).st_mtime, now - 60, file=sys.stderr)
        if os.stat(file_path).st_mtime < now - 600: #1 * 86400:
            if os.path.isdir(file_path):
                shutil.rmtree(file_path)
            else:
                os.remove(file_path)
    
    uuid_path = os.path.join(temp_path, str(uuid.uuid4()) )
    try:
        os.makedirs(temp_path)
    except FileExistsError: 
        pass
    
    print ("FORMATO:",format, file=sys.stderr)
    print ("WKT: ",wkt_geom, file=sys.stderr)
    
    if format == 'cartaweb':
        payload = cartawebExport(wkt_geom)
        file_path = os.path.join(uuid_path+'.cartaweb')
    elif format in ('geojson', 'dxf', 'zip'):
        geom = GEOSGeometry(wkt_geom)
        geojsonObject = {
            "type": "FeatureCollection",
            "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3003" } },
            "features": [{
                'type': 'Feature',
                'geometry': json.loads(geom.json)
            }]
        }
        payload =json.dumps(geojsonObject)
        file_path = os.path.join(uuid_path+'.geojson')
    
    file_txt = open(file_path, 'w')
    file_txt.write(payload)
    file_txt.close()
    
    print ("file_path:",file_path, file=sys.stderr)
    
    if format == 'dxf':
        # converti file geojson a dxf tramite
        dxf_path = os.path.join(uuid_path+'.dxf')
        toDxf = 'ogr2ogr -nlt MULTILINESTRING -f "DXF" %s %s' % (dxf_path,file_path)
        execute (toDxf)
        return dxf_path
    elif format == 'zip':
        # converti file geojson a shp tramite
        shp_path = os.path.join(uuid_path+'.shp')
        dbf_path = os.path.join(uuid_path+'.dbf')
        shx_path = os.path.join(uuid_path+'.shx')
        prj_path = os.path.join(uuid_path+'.prj')
        zip_path = os.path.join(uuid_path+'.zip')
        print ("shp", os.path.exists(file_path), file_path)
        toShp = 'ogr2ogr -f "ESRI Shapefile" %s %s ' % (shp_path,file_path)
        toShp += '&& zip -j %s %s %s %s %s ' % (zip_path, shp_path, dbf_path, shx_path, prj_path)
        #toShp += '&& rm %s %s %s %s' % (shp_path, dbf_path, shx_path, prj_path)
        execute (toShp)
        return zip_path
    else:
        return file_path