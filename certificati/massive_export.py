# exec with ./manage.py shell < certificati/massive_export.py

from certificati.getPataplanPrints import get_pataplan_print
from certificati.coordinateCatastali import GeomFromCoordinateCatastali, queryGSV, queryUbicazione, queryPRG, queryPAT, queryPAI, destinazioniPRG, queryPUA, verificaInCentroStorico,queryCatasto, queryParticelle
from certificati.getPataplanPrints import (
    AGEA2015, CTR1996_DEF, CATASTO_DEF, 
    PI_DEF, TOPO_DEF, PI_CS_DEF, PAT_TRASFORMABILITA_DEF, 
    PAT_VINCOLI_DEF, ORTOFOTO, DBT2007, 
    PAT_FRAGILITA_DEF, PAT_INVARIANTI_DEF,CTR_PRG_DEF
)
from certificati.views import genera_pdf_da_html

from django.conf import settings
from django.template.loader import render_to_string

from PyPDF2 import PdfFileMerger, PdfFileReader

import os
import uuid
import csv
from datetime import datetime

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

out_dir = "/home/urb/pub/__ROCCOHD/export"

part_list = [
    "141/670",
    "141/669",
    "141/664",
    "141/663",
    "141/666",
    "141/665",
]

with open(os.path.join(out_dir,'particellare.csv'), newline='') as csvfile:
    csvrows = csv.reader(csvfile, delimiter=';')
    part_list = []
    for rowid,row in enumerate(csvrows):
        print (row)
        if not rowid ==0:
            part_list.append("%s/%s" % (row[0],row[1]))

print (part_list)

for PARTICELLE in part_list:
    WKT = GeomFromCoordinateCatastali(PARTICELLE,"WKT")
    print (PARTICELLE, WKT)
    table = queryPRG(WKT)+queryPUA(WKT)+queryPAT(WKT)+queryPAI(WKT)
    DESTINAZIONI = render_to_string ('CDU_destinazioni_table.html',{"table": table})
    SCALES = (1000, 2000, 5000,1000,20000, 50000, 100000)
    temp_dir = os.path.join(out_dir,'temp',str(uuid.uuid1()))
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)
    tabella_pdf = genera_pdf_da_html( temp_dir, render_to_string('cdu_destinazioni_page.html',{
        "table": DESTINAZIONI,
        'particelle':PARTICELLE,
        'no_creds':True,
        'istanza': None,
    }))

    PRINTLAYERS = [PI_DEF,PI_CS_DEF,CTR_PRG_DEF]
    SCALES = (1000, 2000, 5000,1000,20000, 50000, 100000)
    map_PI_pdf = get_pataplan_print( None, PRINTLAYERS, PARTICELLE, '200', (255,0,0,255), (255,255,255,0), 1, SCALES, 'a4', WKT=WKT, output_dir=temp_dir ) #, ESRIJSON=ESRIJSON

    PRINTLAYERS = [PAT_VINCOLI_DEF,CTR_PRG_DEF]
    SCALES = (5000,10000)
    map_PAT1_pdf = get_pataplan_print( None, PRINTLAYERS, PARTICELLE, '200', (255,0,0,255), (255,255,255,0), 1, SCALES, 'a4', WKT=WKT, output_dir=temp_dir ) #, ESRIJSON=ESRIJSON

    PRINTLAYERS = [PAT_TRASFORMABILITA_DEF,CTR_PRG_DEF]
    SCALES = (5000,10000)
    map_PAT4_pdf = get_pataplan_print( None, PRINTLAYERS, PARTICELLE, '200', (255,0,0,255), (255,255,255,0), 1, SCALES, 'a4', WKT=WKT, output_dir=temp_dir ) #, ESRIJSON=ESRIJSON
    
    pages = [tabella_pdf, map_PI_pdf, map_PAT1_pdf, map_PAT4_pdf]
    output_pdf = os.path.join(out_dir,'%s.pdf' % PARTICELLE.replace("/","_"))

    merger = PdfFileMerger()
    for page in pages:
        f = open(page, 'rb')
        merger.append(PdfFileReader(f))
    merger.write(output_pdf)
    #cancella files temporanei
    for pdffile in pages:
        os.remove(pdffile)