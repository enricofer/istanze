from django.contrib.gis.db import models
from django.core.validators import RegexValidator
from django.conf import settings
from .validators import validate_dxf, validate_image,validate_geom_catasto
from simple_history.models import HistoricalRecords

import os
from datetime import datetime


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


# Create your models here.
class CDU(models.Model):

    def upload_dxf(instance, filename):
        path = "certificati/%s/%s/" % (instance.tipo,str(instance.protocollo_data.year)+instance.protocollo_numero)
        name,extension = os.path.splitext (filename)
        return os.path.join(path, filename)

    class Meta:
        verbose_name_plural = "Richieste di CDU"
        verbose_name = "Richiesta di CDU"

    stato_choice = (
        ('inserimento',"inserimento"),
        ('individuazione',"individuazione"),
        ('verifica',"verifica"),
        ('approvazione',"approvazione"),
        ('emesso',"emesso"),
        ('definizione',"definizione"),
    )

    tipo_choice = (
        ("cd",'cdu'),
        ("dg",'definizione grafica'),
    )

    catasto_sintassi_validator = "^((((\d+)\:)?(\d+)([a-zA-Z]{1})?\/((\d+)\-)*?(\d+))\s)*(((\d+)\:)?(\d+)([a-zA-Z]{1})?\/((\d+)\-)*?(\d+))$"

    stato = models.CharField(max_length=15,choices=stato_choice,blank=True)
    tipo = models.CharField(max_length=2,choices=tipo_choice,blank=True)
    richiedente_1 = models.CharField(verbose_name="richiedente 1 (Cognome/Societa)",max_length=250)
    richiedente_2 = models.CharField(verbose_name="richiedente 2 (Nome/legale rappresentante)",max_length=250, blank=True)
    richiedente_indirizzo = models.CharField(max_length=250,blank=True)
    richiedente_email = models.CharField(max_length=250,blank=True)
    richiedente_telefono = models.CharField(max_length=250,blank=True)
    protocollo_numero = models.CharField(validators=[RegexValidator(regex='^\d{7}$|^\d{11}$', message="Il protocollo si compone di 7 cifre (o 11 con l'anno)", code='nomatch')],max_length=11)
    protocollo_data = models.DateField()
    coordinate_catastali = models.CharField(validators=[RegexValidator(regex=catasto_sintassi_validator, message="il formato delle coordinate catastali è scorretto", code='nomatch'), validate_geom_catasto],max_length=500)
    #coordinate_catastali = models.CharField(max_length=500)
    rilievo_raster = models.FileField(verbose_name="rilievo come immagine da rasterizzare",upload_to=upload_dxf, null=True,blank=True)#, validators=[validate_image]
    rilievo_raw = models.FileField(verbose_name="file vettoriale DXF",upload_to=upload_dxf, null=True,blank=True)#, validators=[validate_dxf]
    rilievo_ok = models.FileField(null=True,blank=True)
    poligono_richiesto = models.MultiPolygonField(srid=3003,null=True,blank=True)
    poligono_richiesto_esri = models.TextField(blank=True)
    note = models.TextField(blank=True)
    contenuto_txt = models.TextField(null=True,blank=True)
    certificato_pdf = models.FileField(null=True,blank=True)
    protocollo_emissione = models.CharField(validators=[validate_geom_catasto, RegexValidator(regex='^\d{7}$|^\d{11}$', message="Il protocollo si compone di 7 cifre (o 11 con l'anno)", code='nomatch')],max_length=11,null=True)
    data_emissione = models.DateField(null=True,blank=True)
    inquadramento_pdf = models.FileField(null=True,blank=True)
    modificato_da = models.CharField(max_length=50,blank=True)
    cancellato = models.BooleanField(default=False)
    contenuto_variato = models.BooleanField(default=False)
    history = HistoricalRecords()

    def publish(self):
        self.save()

    def __str__(self):
        return "%s%s_%s-%s" % (str(self.protocollo_data.year or 'YYYY'),self.protocollo_numero,self.tipo,self.richiedente_1)

class individuazioni(models.Model):
    the_geom = models.MultiPolygonField(srid=3003, blank=True)
    titolo = models.CharField(max_length=50, blank=True)
    desc  = models.TextField(null=True, blank=True)
    sottocategoria = models.IntegerField(null=True, blank=True)
    momento = models.DateTimeField( default=datetime.now,blank=True,null=True)
    utente = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True,null=True, on_delete=models.SET_NULL,)
    utente_txt = models.CharField(max_length=30, null=True, blank=True)
    
    def save(self, *args, **kwargs):
        self.utente_txt = self.utente.username
        return super(individuazioni, self).save(*args, **kwargs)

    def __str__(self):
        return "%d_%s" % (self.pk, self.utente_txt)

class log_analisi(models.Model):
    richiesta = models.CharField(max_length=50, blank=True)
    elaborazione = models.IntegerField(verbose_name="tempo di elaborazione in millisecondi",null=True, blank=True)
    momento = models.DateTimeField( default=datetime.now)
    utente = models.ForeignKey(settings.AUTH_USER_MODEL,blank=True,null=True, on_delete=models.SET_NULL,)

    def __str__(self):
        return "%d_%s" % (self.pk, self.utente.username)

class pratiche(models.Model):
    class Meta:
        managed = False
        db_table = 'pratiche'
        verbose_name_plural = "Pratiche"
        verbose_name = "Pratica"

    geom = models.PointField(srid=3003, blank=True, db_column='the_geom')
    num = models.IntegerField(null=True, blank=True, db_column='N°')
    fid = models.IntegerField(primary_key=True, db_column='id')
    istanza = models.TextField(null=True,blank=True, db_column='Istanza')
    intestatario = models.TextField(null=True,blank=True, db_column='Intestatario predefinito')
    incaricato = models.TextField(null=True,blank=True, db_column='Incaricato predefinito')
    stato = models.TextField(null=True,blank=True, db_column='Stato istanza')
    tipologia = models.TextField(null=True,blank=True, db_column='Tipologia istanza')
    oggetto = models.TextField(null=True,blank=True, db_column='Oggetto')
    data_domanda = models.TextField(null=True,blank=True, db_column='Data della domanda')
    prot_domanda = models.TextField(null=True,blank=True, db_column='Protocollo della domanda')
    id_fascicolo = models.TextField(null=True,blank=True, db_column='Identificativo del fascicolo')
    ubicazione = models.TextField(null=True,blank=True, db_column='Ubicazione')
    responsabile = models.TextField(null=True,blank=True, db_column='Responsabile')
    data_rilascio = models.TextField(null=True,blank=True, db_column='Data di rilascio')
    num_rilascio = models.TextField(null=True,blank=True, db_column='Numero di rilascio')
    #codvia = models.IntegerField(null=True, blank=True, db_column='CODICE')
