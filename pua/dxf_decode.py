import sys
import os
import json
import itertools
import copy
import math
import collections

try:
    from osgeo import ogr, osr
except:
    import ogr, osr

from django.conf import settings

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

geom_types = {
    "LINESTRING": ogr.wkbMultiLineString,
    "MULTILINESTRING": ogr.wkbMultiLineString,
    "POINT": ogr.wkbMultiPoint,
    "POLYGON": ogr.wkbPolygon,
    "MULTIPOLYGON": ogr.wkbMultiPolygon,
}


class geojson_layer:
    def __init__(self,srid):
        self.feature_collection = {"type": "FeatureCollection",
                  "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::"+str(srid)}},
                  "features": []
                  }
        self.srid = srid

    def clearCollection(self):
        self.__init__(self.srid)

    def addFeature(self,feat):
        #feat['geometry']['type'] = self.geomType
        self.feature_collection["features"].append(feat)

    def getGeojson_asTxt(self):
        return json.dumps(self.feature_collection)

    def getGeojson_asDict(self):
        return self.feature_collection


class dxf_decode:

    def __init__(self,regole_def,repository,srid = 3003):
        self.regoleLayers = json.loads(regole_def['LIVELLI'])
        self.GeomConfine = ogr.CreateGeometryFromWkt(regole_def['CONFINE'])
        self.layersObbigatori=[]
        self.layersFacoltativi=[]
        self.layersPoligonali=[]
        self.layersDiscontinui=[]
        for layer,layerProp in self.regoleLayers.items():
            if layerProp["obbligatorio"]:
                self.layersObbigatori.append(layer)
            else:
                self.layersFacoltativi.append(layer)
            if layerProp["geometria"] == 'POLYGON':
                self.layersPoligonali.append(layer)
            if layerProp["discontinuo"]:
                self.layersDiscontinui.append(layer)
        self.layerAmmessi=self.layersObbigatori+self.layersFacoltativi
        self.base_dir = os.path.abspath(repository)
        if not os.path.exists(self.base_dir):
            os.makedirs(self.base_dir)
        self.srs = osr.SpatialReference()
        self.srid = srid
        self.srs.ImportFromEPSG(srid)
        self.DXFdatasource = None


    def sequenza(self,list):
        for elem in itertools.cycle(list):
            yield elem

    def setDXFDatasource(self,DXFfile):
        DXFdriver = ogr.GetDriverByName('DXF')
        try:
            self.DXFdatasource = DXFdriver.Open(os.path.abspath(DXFfile), 0)
            return (True,'01: File DXF valido')
        except:
            self.DXFdatasource = None
            return(None,'01: File DXF non valido')

    def getDXFDatasource(self):
        return self.DXFdatasource



    def validate(self,DXFfile):

        try:
            result,msg = self.setDXFDatasource(DXFfile)
        except:
            {'esito': False,'mesg':['File DXF non valido']}
        #if not result:
        #    return None,msg #DXFSOURCE invalid

        self.layersObbigatoriCheck = copy.copy(self.layersObbigatori)

        DrvName = 'ESRI Shapefile'
        DrvName = 'MEMORY'
        self.Drv = ogr.GetDriverByName(DrvName)

        
        #self.base_dir = os.path.join(self.repository,os.path.basename(os.path.abspath(DXFfile[:-4])))


        self.processed_ds = self.Drv.CreateDataSource(self.base_dir)

        
        #cancella datasouces se esistono
        for ext in ['.shp','.geojson']:
            for outgeom in ['poligoni','linee','punti','vestizione']:
                pathfile = os.path.join(self.base_dir,outgeom+ext)
                if os.path.exists(pathfile):
                    self.Drv.DeleteDataSource(pathfile)


        self.processed_layers = {
            'POLYGON': {'ogr_layer':self.processed_ds.CreateLayer('poligoni',srs = self.srs,geom_type=geom_types['POLYGON'])},
            'LINESTRING': {'ogr_layer':self.processed_ds.CreateLayer('linee',srs = self.srs,geom_type=geom_types['LINESTRING'])},
            'POINT': {'ogr_layer':self.processed_ds.CreateLayer('punti',srs = self.srs,geom_type=geom_types['POINT'])},
            'ALTRO': {'ogr_layer':self.processed_ds.CreateLayer('vestizione',srs = self.srs,geom_type=geom_types['LINESTRING'])}
        }

        self.processed_dxf = []
        fielddef = [
            {
                'nomecampo': 'layer',
                'tipocampo': ogr.OFTString ,
                'char': 30,
            },
            {
                'nomecampo': 'dim',
                'tipocampo': ogr.OFTReal ,
                'char': 10,
            },
            {
                'nomecampo': 'info',
                'tipocampo': ogr.OFTString ,
                'char': 50,
            },
        ]
        for layername, layer in self.processed_layers.items():
            for campo in fielddef:
                layer['ogr_layer'].CreateField(ogr.FieldDefn(campo['nomecampo'],campo['tipocampo']))
        try:
            self.DXFextent = self.DXFdatasource.GetLayerByIndex(0).GetExtent()
        except:
            return {'esito': False,'mesg':['File DXF non valido']}
        #layer = self.DXFdatasourceDXFdatasource.GetLayerByIndex(0)
        DXFlayers = self.DXFdatasource.ExecuteSQL( "SELECT DISTINCT Layer FROM entities")
        self.esito_validazione = []
        self.processed_report = collections.OrderedDict()

        # identificazione polylines chiuse ed appartenzea ai layers ammessi
        for i in range(0, DXFlayers.GetFeatureCount()):
            DXFlayerName = DXFlayers.GetFeature(i).GetFieldAsString(0)
            DXFprocessLayer = self.DXFdatasource.ExecuteSQL( "SELECT * FROM entities WHERE Layer='%s' AND (OGR_GEOMETRY='LINESTRING' OR OGR_GEOMETRY='POINT')" % DXFlayerName)
            
            if DXFprocessLayer.GetFeatureCount()>0:
                if DXFlayerName in self.layerAmmessi or "%" in self.layerAmmessi:
                    if DXFlayerName == 'PUA_PERIMETRO':
                        perimetro = True
                    else:
                        perimetro = None
                    
                    if "%" in self.layerAmmessi:
                        DXFlayerName = '%'

                    report = self.splitLayer(DXFprocessLayer,DXFlayerName,self.regoleLayers[DXFlayerName]["geometria"],check = perimetro)

                    if report['valido']:
                        self.processed_dxf.append(DXFlayerName)
                    else:
                        self.esito_validazione.append(report['messaggio'])
                        continue
                        
                else:
                    self.esito_validazione.append( "Layer '%s' omesso: non compreso nella lista dei layers ammessi, viene caricato come vestizione grafica" % DXFlayerName)
                    report = self.splitLayer(DXFprocessLayer,DXFlayerName,'ALTRO',vestizione = True)

                self.processed_report[DXFlayerName] = report

        self.applica_info()

        self.calcola_dim()

        self.join_discontinui()

        self.esito_validazione.append('Layer processati: '+', '.join(self.processed_dxf))

        geojson = self.Export_to_GeoJSON_dict()
        with open(DXFfile+".geojson", "w") as f:
            f.write(json.dumps(geojson))
            f.close()
        
        #self.Export_to_GeoJSON()
        #self.Export_to_SHP()
        #processed_ds.Destroy()

        #print ('TEST1',list(set(self.layersObbigatori) & set(self.processed_dxf)), file=sys.stderr)
        #print ('TEST2',self.layersObbigatori, file=sys.stderr)
        #print ('TEST3',list(set(self.layersObbigatori) - set(self.processed_dxf)), file=sys.stderr)
        
        layerMancanti = list(set(self.layersObbigatori) - set(self.processed_dxf))

        print ("layersObbigatori",self.layersObbigatori)
        print ("processed_dxf",set(self.processed_dxf))
        print ("layerMancanti",layerMancanti)
        
        if layerMancanti == []:
            return {'esito':True,'mesg':self.esito_validazione,'report':self.processed_report,'standard':self.calcola_standard(),'geojson':json.dumps(geojson)}
        else:
            
            for layer in layerMancanti:
                self.processed_report[layer] = {
                    'presente': None,
                    'valido': None,
                    'obbligatorio':True,
                    'vestizione':None,
                    'geometria':self.regoleLayers[layer]["geometria"],
                    'entita_max':self.regoleLayers[layer]["max"],
                    'entita_processate': 0,
                    'dim': 0,
                    'messaggio': 'Layer %s obbligatorio non presente' % layer,
                    'db': 'mancanti'
                }
            self.esito_validazione.append('I seguenti layer obbligatori sono mancanti: '+', '.join(layerMancanti))
            return {'esito':None,'mesg':self.esito_validazione,'report':self.processed_report,'standard':self.calcola_standard(),'geojson':json.dumps(geojson)}


    def calcola_dim(self):
        for layername, layer in self.processed_layers.items():
            layer['ogr_layer'].ResetReading()
            while True:
                feat = layer['ogr_layer'].GetNextFeature()
                if feat:
                    layer_origine = feat.GetFieldAsString("layer")
                    if layer_origine != 'altro':
                        geom = feat.GetGeometryRef()
                        if layer['ogr_layer'].GetGeomType() in (ogr.wkbPolygon, ogr.wkbMultiPolygon):
                            feat.SetField('dim',geom.GetArea())
                            self.processed_report[layer_origine]['dim'] += geom.GetArea()
                        elif layer['ogr_layer'].GetGeomType() in (ogr.wkbLineString, ogr.wkbMultiLineString):
                            feat.SetField('dim',geom.Length())
                            self.processed_report[layer_origine]['dim'] += geom.Length()
                        info_txt = feat.GetFieldAsString("info")
                        if info_txt and self.regoleLayers[layer_origine]['info_tipo'] == 'numeric':
                            self.processed_report[layer_origine]['info'] += float(info_txt)
                        layer['ogr_layer'].SetFeature(feat)
                else:
                    break

    def calcola_standard(self):
        standard = collections.OrderedDict()
        for field in ('sup_perimetro','sup_ambito','sup_servizi','sup_verde','sup_park','sup_privata','sup_cessioni','vol_res','slp_no_res'):
            standard[field] = 0
        for layer,dati in self.processed_report.items(): #aggiorna campi
            if dati['db'] in ('sup_perimetro','sup_ambito'):
                standard[dati['db']] = round(dati['dim'],2)
            elif dati['db'] in ('sup_servizi','sup_verde','sup_park','sup_privata','sup_cessioni'):
                standard[dati['db']] += round(dati['dim'],2)
            if dati['db'] in ('vol_res','slp_no_res'):
                standard[dati['db']] = round(dati['info'],2)
        try:
            standard['indice_territoriale'] = round(standard['vol_res']/standard['sup_perimetro'],2)
        except:
            standard['indice_territoriale'] = None
        try:
            standard['indice_territoriale_non_residenziale'] = round(standard['slp_no_res']/standard['sup_perimetro'],2)
        except:
            standard['indice_territoriale_non_residenziale'] = None
        try:
            standard['abitanti_teorici'] = round(standard['vol_res']/192,2)
        except:
            standard['abitanti_teorici'] = None
        try:
            standard['standard_parcheggio'] = round(standard['sup_park']/standard['abitanti_teorici'],2)
        except:
            standard['standard_parcheggio'] = None
        try:
            standard['standard_verde'] = round(standard['sup_verde']/standard['abitanti_teorici'],2)
        except:
            standard['standard_verde'] = None
        return standard

    def join_discontinui(self):
        for layer in self.layersDiscontinui:
            layer_discontinuo = self.processed_ds.ExecuteSQL( "SELECT * FROM poligoni WHERE layer='%s'" % layer)#.encode('utf-8'))

            if layer_discontinuo.GetFeatureCount() > 1:
                bind_feat = layer_discontinuo.GetNextFeature()
                bind_geom = bind_feat.GetGeometryRef()
                infoTxt = bind_feat.GetFieldAsString("info")
                while True:
                    disc_feat = layer_discontinuo.GetNextFeature()
                    if disc_feat:
                        disc_geom = disc_feat.GetGeometryRef()
                        bind_geom = bind_geom.Union(disc_geom)
                        if disc_feat.GetFieldAsString("info") and disc_feat.GetFieldAsString("info") != '':
                            infoTxt += ' + ' + disc_feat.GetFieldAsString("info")
                        self.processed_layers['POLYGON']['ogr_layer'].DeleteFeature(disc_feat.GetFID())
                    else:
                        break
                bind_feat.SetField('layer',layer)
                bind_feat.SetField('dim',bind_geom.GetArea())
                bind_feat.SetField('info',infoTxt)
                bind_feat.SetGeometry(bind_geom)
                self.processed_layers['POLYGON']['ogr_layer'].SetFeature(bind_feat)
        self.processed_ds.ExecuteSQL( "SELECT * FROM poligoni")

    def applica_info(self):
        for layerP in self.layersPoligonali:
            point_layer = self.processed_ds.ExecuteSQL( "SELECT * FROM punti WHERE layer='%s'" % layerP)#.encode('utf-8'))
            poly_layer = self.processed_ds.ExecuteSQL( "SELECT * FROM poligoni WHERE layer='%s'" % layerP)#.encode('utf-8'))
            #poly_layer.ResetReading()
            if point_layer.GetFeatureCount() > 0: #cicla solo se il dxflayer ha dei corrispondenti puntiinfo
                while True: # cicla fra i poligoni
                    poly_feat = poly_layer.GetNextFeature()
                    if poly_feat:
                        poly_geom = poly_feat.GetGeometryRef()
                        point_layer.ResetReading()
                        while True: # cicla fra i punti
                            point_feat = point_layer.GetNextFeature()
                            if point_feat:
                                point_geom = point_feat.GetGeometryRef()
                                if poly_geom.Intersects(point_geom): #eredita il layer info dall'entita punto contenuta
                                    poly_feat.SetField('info',point_feat.GetFieldAsString("info"))
                                    self.processed_layers['POLYGON']['ogr_layer'].SetFeature(poly_feat)
                                    self.processed_layers['POINT']['ogr_layer'].DeleteFeature(point_feat.GetFID())
                            else:
                                break
                    else:
                        break
            #elif point_layer.GetFeatureCount() > 1:
            #    self.esito_validazione.append("Layer %s: informazione incoerente, rilevati %s elementi informativi" % (layerP,point_layer.GetFeatureCount()))
                
        self.processed_ds.ExecuteSQL( "SELECT * FROM punti")
        self.processed_ds.ExecuteSQL( "SELECT * FROM poligoni")


    def splitLayer (self,layer_in,childLayerName,layerName_out, vestizione = None,check = None):
        if childLayerName in self.layerAmmessi:
            vestizione = None
            obbligatorio = self.regoleLayers[childLayerName]["obbligatorio"]
            max = self.regoleLayers[childLayerName]["max"]
            geometria = self.regoleLayers[childLayerName]["geometria"]
            db = self.regoleLayers[childLayerName]["db"]
        else:
            vestizione = True
            obbligatorio = None
            geometria = 'LINESTRING'
            max = 0
            db = 'altro'

        report = {
            'presente': True,
            'valido': None,
            'obbligatorio': obbligatorio,
            'vestizione':vestizione,
            'entita_max': max,
            'geometria': geometria,
            'entita_processate': 0,
            'entita_non_valide':0,
            'dim': 0,
            'info': 0 if childLayerName in self.regoleLayers.keys() and self.regoleLayers[childLayerName]['info_tipo'] == 'numeric' else '',
            'messaggio': "LAYER %s: " % childLayerName,
            'db': db,
        }

        layer_out = self.processed_layers[layerName_out]['ogr_layer']

        layer_in.ResetReading()
        out_bounds = 0
        processed_entities = 0
        invalid_entities = 0
        spatial_reference = layer_out.GetSpatialRef()
        spatial_reference.AutoIdentifyEPSG()
        
        print (childLayerName,layer_in.GetFeatureCount(), file=sys.stderr)

        while True:
            feat = layer_in.GetNextFeature()

            if feat:
                if vestizione or processed_entities < self.regoleLayers[childLayerName]["max"]:
                    if self.GeomConfine.Intersects(feat.GetGeometryRef()):

                        if vestizione:
                            layer_out = self.processed_layers['ALTRO']['ogr_layer']
                            layer_name = 'altro'
                            info_txt = 'importato da '+ childLayerName
                        else:
                            if childLayerName in self.layersPoligonali and feat.GetGeometryRef().GetGeometryName() == 'LINESTRING':
                                layer_out = self.processed_layers['POLYGON']['ogr_layer'] #le linestring vengono convertite in poligoni
                            else:
                                layer_out = self.processed_layers[feat.GetGeometryRef().GetGeometryName()]['ogr_layer']
                            layer_name = childLayerName
                            info_txt = feat.GetFieldAsString('Text')

                        new_feat = ogr.Feature(layer_out.GetLayerDefn())
                        new_feat.SetField('layer',layer_name)
                        new_feat.SetField('info',info_txt)
                        
                        the_geom = feat.GetGeometryRef()
                        the_geom_points = the_geom.GetPointCount()

                        if not vestizione and childLayerName in self.layersPoligonali and feat.GetGeometryRef().GetGeometryName() == 'LINESTRING': #se il tipo richiesto per il layer e poligono vengono prese solo le geometrie chiuse

                            #procedura per testare se il poligono e' chiuso o meno con una approssimazione
                            approx = 0.01
                            check_start = ((math.floor(the_geom.GetPoint(0)[0]/approx)*approx),(math.floor(the_geom.GetPoint(0)[1]/approx)*approx))
                            check_end = ((math.floor(the_geom.GetPoint(the_geom_points-1)[0]/approx)*approx),(math.floor(the_geom.GetPoint(the_geom_points-1)[1]/approx)*approx))

                            #print (childLayerName,check_start,check_end, file=sys.stderr)
                            if (check_start == check_end):

                                rebuild_geom = ogr.Geometry(layer_out.GetGeomType())
                                ring = ogr.Geometry(ogr.wkbLinearRing)
                                last_vertex = ogr.Geometry(ogr.wkbPoint)
                                for idx in range(0,the_geom_points):
                                    vertex  = the_geom.GetPoint(idx)
                                    if vertex != last_vertex:
                                        ring.AddPoint_2D(vertex[0],vertex[1])
                                        last_vertex = vertex
                                ring.AddPoint_2D(the_geom.GetPoint(0)[0],the_geom.GetPoint(0)[1])
                                rebuild_geom.AddGeometry(ring)
                                new_geom = ogr.ForceToPolygon(rebuild_geom)


                                if new_geom.IsValid():
                                    new_feat.SetField('dim',new_geom.GetArea() or ring.GetArea())
                            else:
                                print ("ERRORE: entità non poligonale in layer poligonale", file=sys.stderr)
                                new_geom = None #expunge non polygonal features


                        else:
                            new_geom = the_geom
                            if layer_out.GetGeomType() in (ogr.wkbLineString, ogr.wkbMultiLineString):
                                new_feat.SetField('dim',new_geom.Length())
                        
                        if new_geom and new_geom.IsValid(): #expunge non polygonal features
                            new_feat.SetGeometry(new_geom)
                            err = layer_out.CreateFeature(new_feat)
                            if err == 0:
                                processed_entities += 1
                            else:
                                print ("ERRORE:"+str(err), file=sys.stderr)
                                invalid_entities += 1
                        else:
                            invalid_entities += 1
                    else:
                        out_bounds +=1
                else:
                    report['valido'] = None
                    report['messaggio'] += "LAYER %s NON IMPORTATO: CONTIENE PIU ENTITA' DEL CONSENTITO (%s)" % (childLayerName,self.regoleLayers[childLayerName]["min"])
                    report['entita_processate'] = processed_entities
                    return (report)
            else:
                break

        report['entita_processate'] = processed_entities
        report['entita_non_valide'] = invalid_entities

        if invalid_entities > 0:
            report['messaggio'] += "ATTENZIONE, CONTIENE %s ENTITA' NON VALIDE O NON COERENTI CON IL LAYER; " % invalid_entities

        if out_bounds > 0:
            report['valido'] = None
            report['messaggio'] += "NON IMPORTATO: CONTIENE ENTITA' FUORI DAL PERIMETRO CONSENTITO"
            return (report)

        if processed_entities == 0:
            report['valido'] = None
            report['messaggio'] += "NON IMPORTATO: NON CONTIENE ENTITA' VALIDE"
            return (report)

        report['valido'] = True
        report['messaggio'] += "IMPORTATO, CONTIENE %s ENTITA' VALIDE" % processed_entities
        return (report)


    def ex_Export_to_GeoJSON (self):

        for layername,layer in self.processed_layers.items():

            layer['ogr_layer'].ResetReading()
            export_layer = geojson_layer(self.srid)
            while True:
                feat = layer['ogr_layer'].GetNextFeature()
                if feat:
                    export_layer.addFeature(json.loads(feat.ExportToJson()))
                else:
                    break

            with open(os.path.join(self.base_dir,layer['ogr_layer'].GetName()+".geojson"), "w") as f:
                f.write(export_layer.getGeojson_asTxt())
                f.close()

        

    def Export_to_GeoJSON_dict (self):

        if not self.processed_layers:
            return {'errore': "Esportazione non possibile: file non ancora validato"}
        export_layer = geojson_layer(self.srid)
        id = 0
        for layername,layer in self.processed_layers.items():
            ogr_layer = layer['ogr_layer']
            ogr_layer.ResetReading()
            while True:
                feat = ogr_layer.GetNextFeature()
                if feat:
                    featStruct = json.loads(feat.ExportToJson())
                    featStruct['id'] = id
                    id += 1
                    export_layer.addFeature(featStruct)
                else:
                    break
        return export_layer.getGeojson_asDict()



