
previeni_rinvio = false;

function segnala (curatore) {
  
  const campo_segnalazione = document.getElementById("id_segnalazione");
  const nome_piano = document.getElementById("id_id_pua").value+"-"+document.getElementById("id_ditta").value+": "
  let segnalazione = campo_segnalazione.value
  console.log(curatore,segnalazione)
  if (segnalazione=='' || previeni_rinvio ) {
    $("#link_segnala").css('color', 'red')
    campo_segnalazione.placeholder = "Inserire la descrizione del problema"; 
  } else {
    var dataString = "applicazione=4&tipo_problema=badinfo&descrizione=" + nome_piano + segnalazione + "&link=" + window.location
    console.log(dataString)
    $.ajax({
        url: "/",
        data: dataString,
        cache: false,
        processData: false,
        contentType: false,
        type: 'GET',
        success: function (dataofconfirm) {
            $("#link_segnala").css('display', 'none')
            $(".link_segnalazione").parent().removeClass( "grp-open" ).addClass( "grp-closed" )
            previeni_rinvio = true;
        }
    });
  }
}
