# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desidered behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError
from django.contrib.gis.geos import Polygon
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.db.models import Q
from django.conf import settings
from urllib.parse import urlparse

import requests
import os
import time
import uuid
import re
import sys
import shutil
import datetime

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

tipologia_choice = (
    ('PUA',"PUA"),
    ("VPL","Vecchio Piano di lottizazione"),
    ("PI","Variante al Piano degli interventi"),
    ("MPI","Modifica al Piano degli interventi"),
    ('AdProg',"Accordo di programma"),
    ('AdPian',"Accordo di pianificazione"),
    ('Altro',"Altro"),
)

sottocategoria_choice = (
    ("PG","Piano guida"),
    ("DEL","Delimitazione d'ambito"),
    ("PdL","Piano di lottizzazione"),
    ("PdR","Piano di recupero"),
    ("PP","Piano particolareggiato"),
    ('PS',"Piano Speciale"),
    ('PeepErp',"PEEP"),
    ('SC',"Servizio civico"),
    ('PIRU',"PIRU-PIRUEA"),
    ('SUAP',"SUAP"),
    ('OP',"Opera pubblica"),
    ('AOU',"Atto unilaterale d'obbligo"),
    ('PPP',"Promemoria piani e progetti"),
)

stato_choice = (
    ("piano guida","01  piano guida"),
    ("ambito richiesto","02  ambito richiesto"),
    ("ambito delimitato","03  ambito delimitato"),
    ('istruttoria',"04  istruttoria"),
    ("adottato","05  adottato"),
    ('approvato',"06  approvato"),
    ('convenzionato',"07  convenzionato"),
    ('esecuzione',"08  in esecuzione"),
    ('collaudo parziale',"09 collaudo parziale"),
    ('collaudato',"10  collaudato"),
    ('sospeso',"10 sospeso"),
    ('revocato',"11 revocato"),
    ('decaduto',"12 decaduto"),
    ('ratificato',"13 ratificato"),
    ('autorizzato',"14 autorizzato"),
    ('archiviato',"15 archiviato"),
    ('contenzioso',"16 contenzioso"),
    ('attuato con PC',"17 Attuato in modalità diretta"),
    ('non rilevato',"99 non rilevato"),
)

tipo_allegato_choice = (
    ('DEL',"Delibera"),
    ('DET',"Determina"),
    ('PRE',"Documento preliminare"),
    ('COL',"Certificato di collaudo"),
    ('CON',"Convenzione"),
    ('CES',"Atto di cessione/asservimento aree"),
    ('AUO',"Atto unilaterale d'obbligo"),
    ('PVL',"Planimetria raster"),
    ('GEO',"Planimetria vettoriale"),
    ('PNO',"Documentazione di piano"),
    ('VAS',"Procedura V.A.S."),
    ('VNC',"Procedura V.Inc.A."),
    ('IDR',"Invarianza idraulica"),
    ('MON',"Monitoraggio"),
    ('PAR',"Parere"),
    ('SCP',"Scheda planivolumetrica"),
    ('PED',"Progetto edilizio"),
    ('ALT',"Altra documentazione"),
)

def check_url_ok(url):
    url = url or ""
    schema = url.split("://")[0]
    if schema in ("http", "https"):
        r = requests.head(url)
        if r.status_code == 200:
            return True
    return False

class pua(models.Model):

    def update_filename(instance, filename):
        path = "pua/planivol/%s" % instance.pk
        if not os.path.exists(path):
            os.makedirs(path)
        name,extension = os.path.splitext (filename)
        format = str(instance.data) +'_' + str(instance.cat) + '_'+ str(instance.titolo) + extension
        return os.path.join(path, format)

    gid = models.AutoField(primary_key=True)
    id_pua = models.CharField(max_length=5, blank=True, null=True)
    id_pguida = models.CharField(max_length=5, blank=True, null=True)
    objectid = models.IntegerField(blank=True, null=True)
    num_cad = models.CharField(max_length=20, blank=True, null=True)
    num_archiv = models.CharField(max_length=20, blank=True, null=True, verbose_name='Numero di archivio')
    tipologia = models.CharField(max_length=7,choices=tipologia_choice)
    sottocategoria = models.CharField(max_length=7,choices=sottocategoria_choice, blank=True, null=True)
    ditta = models.CharField(verbose_name=u"Denominazione interna", max_length=254, blank=True, null=True)
    prg_zona = models.CharField(max_length=100, blank=True, null=True)
    prg_norma = models.CharField(max_length=10, blank=True, null=True)
    delib_amb = models.CharField(max_length=50, verbose_name=u"Delibera di delim. d'ambito", blank=True, null=True)
    delib_ado = models.CharField(max_length=50, verbose_name=u"Delibera di adozione", blank=True, null=True)
    delib_appr = models.CharField(max_length=50, verbose_name=u"Delibera di approvazione", blank=True, null=True)
    delib_var = models.CharField(max_length=50, verbose_name=u"Delibera/e di variante", blank=True, null=True)
    stato = models.CharField(max_length=20 ,choices=stato_choice)
    convenzion = models.CharField(max_length=20, blank=True, null=True)
    shape_area = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    shape_leng = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    sup_perimetro = models.DecimalField(verbose_name=u"Superficie", max_digits=10, decimal_places=3, blank=True, null=True)
    vol_res = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    slp_no_res = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    collaudo = models.IntegerField(verbose_name=u"Anno di collaudo", blank=True, null=True)
    note = models.CharField(max_length=254, blank=True, null=True)
    determina = models.CharField(max_length=254, blank=True, null=True)
    cessione_a = models.CharField(max_length=254, blank=True, null=True)
    ubicazione = models.CharField(max_length=254, blank=True, null=True)
    quartiere = models.SmallIntegerField(blank=True, null=True)
    cod_via = models.IntegerField(blank=True, null=True)
    the_geom = models.MultiPolygonField(srid=3003, blank=True, null=True)
    sup_servizi = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    sup_verde = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    sup_park = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    planivol = models.ImageField(upload_to=update_filename, blank=True, null=True)
    infos  = models.ForeignKey('info',blank=True,null=True, related_name='infos', on_delete=models.SET_NULL) #da cancellare
    provvedimenti  = models.ForeignKey('provvedimento',blank=True,null=True, on_delete=models.SET_NULL) #da cancellare
    validazione = models.TextField(blank=True, null=True)
    zonizzazione = models.FileField(upload_to=update_filename,blank=True, null=True)
    sup_ambito = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    sup_privata = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    sup_cessioni = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    det_collaudo = models.CharField(max_length=254, verbose_name=u"Determina/e di collaudo", blank=True, null=True)
    vol_misto = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)
    denominazione = models.CharField(verbose_name=u"Denominazione ufficiale", max_length=254, blank=True, null=True)
    data_stato =  models.DateField( verbose_name=u"Data di cambiamento di stato", blank=True, null=True)
    anno_stato =  models.IntegerField( verbose_name=u"Anno di cambiamento di stato", blank=True, null=True)
    cdu = models.TextField(verbose_name=u"Testo personalizzato per CDU",blank=True, null=True)
    pubblicato = models.BooleanField(verbose_name=u"Pubblicato nell'applicazione CDU", default=True)
    coordinate_catastali = models.CharField(max_length=254, blank=True, null=True)
    pratiche_edilizie = models.CharField(max_length=100, blank=True, null=True)
    elaborati = models.CharField(verbose_name=u"Elaborati di piano", max_length=500, blank=True, null=True)
    convenzione = models.CharField(max_length=128, blank=True, null=True)
    planivol_raster = models.CharField(verbose_name=u"Planivolumetrico raster", max_length=128, blank=True, null=True)
    planivol_vector = models.CharField(verbose_name=u"Planivolumetrico vettoriale", max_length=128, blank=True, null=True)
    dds = models.DateField( verbose_name=u"Presentazione del documento del sindaco", blank=True, null=True)
    efficacia = models.DateField( verbose_name=u"Data di efficacia del piano", blank=True, null=True)
    version = models.CharField(verbose_name=u"Versione ESRI", max_length=30, blank=True, null=True)
    qc = models.CharField(verbose_name=u"Quadro Conoscitivo", max_length=128, blank=True, null=True)
    cat_osservazioni = models.ForeignKey('repertorio.catIstanze',verbose_name=u"Riferimento a osservazioni", blank=True,null=True, on_delete=models.SET_NULL, limit_choices_to=Q(termine=None)|Q(termine__gte=datetime.date.today()))
    curatore = models.ForeignKey(settings.AUTH_USER_MODEL,verbose_name=u"Referente interno",blank=True,null=True, on_delete=models.SET_NULL,limit_choices_to={'groups__name': 'pua'},)
    variante_rif = models.ForeignKey('pua',verbose_name=u"Variante di riferimento",blank=True,null=True, on_delete=models.SET_NULL,limit_choices_to={'tipologia': 'PI'},)
    decadenza = models.DateField( verbose_name=u"Data di decadenza del piano", blank=True, null=True)

    #link_elab = models.TextField(verbose_name=u"Link ad Elaborati di piano", blank=True, null=True)
    #referente = models.ForeignKey(verbose_name=u"Link ad Elaborati di piano", blank=True, null=True)

    @property
    def anno_estratto(self):
        campi_data = [self.delib_amb, self.delib_ado, self.delib_appr, self.delib_var, self.det_collaudo ]
        anno_max = 0

        for campo in campi_data:
            if campo:
                res = re.findall(r"\/(\d{4})(\s|$)", str(campo) )
                #print ("RE", res)
                for r in res:
                    anno_estratto = int(r[0])
                    if anno_estratto > anno_max:
                        anno_max = anno_estratto
        return anno_max if anno_max > 0 else ""

    @property
    def ha_planimetria_vector(self):
        return not self.planivol_vector in ("",None,False)

    @property
    def ha_planimetria_raster(self):
        return not self.planivol_raster in ("",None,False)

    @property
    def ha_elaborati(self):
        return not self.elaborati in ("",None,False)

    @property
    def ha_elaborati_corretti(self):
        return check_url_ok(self.elaborati)

    @property
    def ha_elaborati_in_iter_corretti(self):
        res = True
        for iter in self.info_set.all():
            if iter.ha_rif_elaborati and not iter.ha_rif_elaborati_corretti:
                res = False
        return res

    @property
    def ha_convenzione(self):
        for iter in self.info_set.all():
            file_related = iter.files_allegati_set.filter(tipo='CON').last()
            if file_related and os.path.exists(os.path.join(settings.BASE_DIR + file_related.documento.url)):
                return True
        return False

    @property
    def ha_collaudo(self):
        for iter in self.info_set.all():
            file_related = iter.files_allegati_set.filter(tipo='COL').last()
            if file_related and os.path.exists(os.path.join(settings.BASE_DIR + file_related.documento.url)):
                return True
        return False
            
    @property
    def ha_elaborati_in_iter(self):
        res = False
        for iter in self.info_set.all():
            if iter.ha_rif_elaborati:
                res = True
        return res
            
    @property
    def _ok(self):
        if self.tipologia != "PUA":
            return True
        res = True
        stati = ["adottato","approvato","convenzionato",'esecuzione','collaudo parziale','collaudato']
        #res = res and not self.curatore in ("",None,False)
        res = res and not self.denominazione in ("",None,False)
        if self.stato in stati[stati.index("adottato"):-1]:
            res = res and not self.delib_ado in ("",None,False)
            if self.anno_estratto >=2012:
                res = res and (self.ha_elaborati_corretti or self.ha_elaborati_in_iter_corretti)
            res = res and self.ha_planimetria_raster 
        if self.stato in stati[stati.index("approvato"):-1]:
            res = res and not self.delib_appr in ("",None,False)
        if self.stato in stati[stati.index("convenzionato"):-1]:
            res = res and self.ha_convenzione 
        if self.stato in stati[stati.index("esecuzione"):-1]:
            res = res and not self.pratiche_edilizie in ("",None,False)
        if self.stato in stati[stati.index("collaudo parziale"):-1]:
            res = res and self.ha_collaudo 
            res = res and not self.det_collaudo in ("",None,False)
        return res
            
    @property
    def ok(self):
        if self.lacune:
            return False
        else:
            return True
            
    @property
    def lacune(self):
        if self.tipologia != "PUA":
            return True
        res = []
        stati = ["adottato","approvato","convenzionato",'esecuzione','collaudo parziale','collaudato']
        #res = res and not self.curatore in ("",None,False)
        if self.denominazione in ("",None,False):
            res.append( pua._meta.get_field('denominazione').verbose_name.title() )
        if self.stato in stati[stati.index("adottato"):]:
            if self.delib_ado in ("",None,False):
                res.append( pua._meta.get_field('delib_ado').verbose_name.title() )
            if self.anno_estratto and int(self.anno_estratto) >=2012:
                if not self.ha_elaborati_corretti:
                    res.append( "riferimento elaborati non corretto" )
                if not self.ha_elaborati_in_iter_corretti:
                    res.append( "riferimenti web in iter non corretti" )
            if not self.ha_planimetria_raster:
                res.append( "Planimetria raster" )
        if self.stato in stati[stati.index("approvato"):]:
            if self.delib_appr in ("",None,False):
                res.append( pua._meta.get_field('delib_appr').verbose_name.title() )
            if not self.decadenza:
                res.append( pua._meta.get_field('decadenza').verbose_name.title() )
        if self.stato in stati[stati.index("convenzionato"):]:
            if not self.ha_convenzione:
                res.append( "Convenzione" )
        if self.stato in stati[stati.index("esecuzione"):]:
            if self.pratiche_edilizie in ("",None,False):
                res.append( pua._meta.get_field('pratiche_edilizie').verbose_name.title() )
        if self.stato in stati[stati.index("collaudo parziale"):]:
            if not self.ha_collaudo:
                res.append( "Collaudo" )
            if self.det_collaudo in ("",None,False):
                res.append( pua._meta.get_field('det_collaudo').verbose_name.title() )
        if res:
            return ", ".join(res).upper()
        else:
            return ""
            
    @property
    def ko(self):
        return not self.ok

    @property
    def num_iter(self):
        return len(self.info_set.all())

    @property
    def num_allegati(self):
        num_all = 0
        for iter in self.info_set.all():
            num_all += iter.num_allegati
        num_all += len(self.files_allegati_set.all())
        return num_all

    class Meta:
        managed = True
        db_table = 'pua'
        verbose_name_plural = "Piani urbanistici"
        verbose_name = "Piano urbanistico"

    def __str__(self):
        return '%s-%s' % (self.id_pua,self.ditta.replace(' ','_'))
    
    def save(self, *args, **kwargs):
        self.id_pua = self.id_pua.upper()
        return super(pua, self).save(*args, **kwargs)

    def clean_fields(self, exclude=None):
        if not self.the_geom:
            raise ValidationError({'the_geom':["Specificare una geometria"]})

class info(models.Model):

    def update_filename(instance, filename):
        path = "pua/informazioni/%s" % instance #settings.MEDIA_ROOT,
        #if not os.path.exists(os.path.join(settings.MEDIA_ROOT,path)):
        #    os.makedirs(os.path.join(settings.MEDIA_ROOT,path))
        name,extension = os.path.splitext (filename)
        format = str(instance.data) +'_' + str(instance.cat) + '_'+ str(instance.titolo) + extension
        return os.path.join(path, format)

    cat_choice = (
        ('IN',"corrispondenza in entrata"),
        ('OUT',"corrispondenza in uscita"),
        ('AMM',"atto amministrativo"),
        ('DOC',"documentazione"),
        ('MEM',"promemoria"),
    )

    tipo_atto_choice = (
        ('DCC',"Delibera di Consiglio Comunale"),
        ('DGC',"Delibera di Giunta Comunale"),
        ('DET',"Determina dirigenziale"),
        ('DOC',"altra documentazione"),
    )

    cat = models.CharField(max_length=5,choices=cat_choice)
    titolo = models.CharField(max_length=50)
    data = models.DateField()
    descrizione = models.TextField(blank=True)
    modulo = models.FileField(upload_to=update_filename,blank=True)
    geometria = models.FileField(upload_to=update_filename,blank=True)
    allegati = models.ForeignKey('files_allegati',blank=True,null=True, on_delete=models.SET_NULL) #da cancellare
    id_pua = models.CharField(max_length=5, blank=True, null=True)
    rif_pua  = models.ForeignKey('pua',blank=True,null=True, on_delete=models.CASCADE)
    #geometria_valida = models.BooleanField(default=None)
    validazione = models.TextField(blank=True, null=True)
    zonizzazione = models.FileField(upload_to=update_filename,blank=True)
    stato = models.CharField(max_length=20,verbose_name=u"Cambiamento di stato",choices=stato_choice,blank=True)
    rif_atto = models.CharField(verbose_name=u"Riferimento amministrativo",max_length=50,blank=True)
    rif_web = models.CharField(max_length=500,blank=True)
    tipo_atto = models.CharField(max_length=3,verbose_name=u"Tipo",choices=tipo_atto_choice,blank=True)
    utente = models.CharField(max_length=20,blank=True)

    @property
    def ha_rif_elaborati(self):
        return not self.rif_web in ("",None,False)

    @property
    def ha_rif_elaborati_corretti(self):
        return check_url_ok(self.rif_web)

    @property
    def num_allegati(self):
        return len(self.files_allegati_set.all())

    class Meta:
        managed = True
        db_table = 'pua_info'
        verbose_name_plural = "ITER"
        verbose_name = "cronologia"

    def _save(self, *args, **kwargs):
        if self.cat == 'DOC':
            if not self.tipo_atto:
                self.tipo_atto = 'DOC'
        super(Model, self).save(*args, **kwargs)

    def clean_fields(self, exclude=None):
        #if self.cat =='AMM':
        #    if not self.tipo_atto:
        #        raise ValidationError({'tipo_atto':["Specificare il tipo di atto"]})
        #    if not self.rif_atto or self.rif_atto == '':
        #        raise ValidationError({'rif_atto':["Se cat è atto amministrativo inserire gli estremi dell'atto"]})
        #    if not self.stato or self.stato == '':
        #        raise ValidationError({'stato':["Se cat è atto amministrativo specificare lo stato del piano"]})
        if self.cat in ('IN', 'OUT'): #, 'DOC'
            if not ( re.match("^\d{7}$|^\d{11}$", self.rif_atto) ):
                raise ValidationError({'rif_atto':["nel caso di corrispondenza esprimere un riferimento come protocollo (7 cifre 11 cifre con l'anno)"]})
        if self.cat in ('AMM',): #, 'DOC'
            if not ( 
                re.match("(?i)(?P<tipo>d.?c.?c.?|d.?g.?c.?|p.?e.?d.?)?(?P<sep>\s|[n.°#]|^)+(?P<num>\d{1,4})+.(?P<anno>\d{4})(\s|$)", self.rif_atto) or
                re.match("(?i)(?P<tipo>d.?e.?t.?|d.?i.?r.?)?(?P<sep>\s|[n.°#]|^)+(?P<anno>\d{4})+.(?P<sett>\d{2})+.(?P<num>\d{1,4})(\s|$)", self.rif_atto)
            ):
                raise ValidationError({'rif_atto':["nel caso di atto esprimere un riferimento come delibera (DCC o DGC NN/ANNO) o come determina (DET ANNO/SETTORE/NN)"]})
            #if self.stato or self.stato != '':
            #    raise ValidationError({'stato':["La variazione di stato è ammessa solo se cat è atto amministrativo"]})

    def __str__(self):
        return "%s %s %s" % (self.data,dict(self.cat_choice)[self.cat].upper(),self.titolo)


class provvedimento(models.Model):

    def update_filename(instance, filename):
        path = "pua/provvedimenti/%s" % time.time()
        if not os.path.exists(path):
            os.makedirs(path)
        name,extension = os.path.splitext (filename)
        format = instance.tipo +'_' + str(instance.numero) + '_'+ str(instance.data) + extension
        return os.path.join(path, format)

    tipo_choice = (
        ('DCC',"Delibera di Consiglio Comunale"),
        ('DGC',"Delibera di Giunta Comunale"),
        ('DET',"Determina dirigenziale"),
    )

    argomento_choice = (
        ("Piano guida","Piano guida"),
        ("Delimitazione d'ambito","Delimitazione d'ambito"),
        ("Adozione","Adozione"),
        ('Approvazione',"Approvazione"),
        ('Variante',"Variante"),
        ('Esproprio',"Esproprio"),
        ('Collaudo',"collaudo"),
    )

    tipo = models.CharField(max_length=3,choices=tipo_choice)
    numero = models.IntegerField()
    data = models.DateField()
    argomento = models.CharField(max_length=22,choices=argomento_choice)
    id_pua = models.CharField(max_length=5, blank=True, null=True)
    descrizione = models.TextField(blank=True)
    documento = models.FileField(upload_to=update_filename,blank=True)
    rif_pua  = models.ForeignKey('pua',blank=True,null=True, on_delete=models.SET_NULL)

    class Meta:
        managed = True
        db_table = 'pua_provvedimento'
        verbose_name_plural = "provvedimenti"
        verbose_name = "provvedimento"

    def __str__(self):
        return "%s %s %s" % (self.data,tipo_dict[self.tipo].upper(),self.argomento)


class files_allegati(models.Model):

    class Meta:
        verbose_name_plural = "Allegati"
        verbose_name = "Allegato"

    def update_filename(instance, filename):
        instance.uuid = uuid.uuid4().hex
        return 'pua/allegati/%s/%s' % (instance.uuid, filename)

    tipo = models.CharField(max_length=3,choices=tipo_allegato_choice)
    data = models.DateField(auto_now_add=True, blank=True)
    uuid = models.CharField(max_length=32, verbose_name=u"file key", default='f11a83a698a543ccb5bdba4ed17ed281') #uuid.uuid4().hex #risulta una migrazione nuova ogni volta!
    rif_provvedimento = models.CharField(max_length=20, verbose_name=u"Provvedimento", blank=True, null=True)
    documento = models.FileField(upload_to=update_filename,)
    rif_warp = models.ForeignKey('django_warp.rasterMaps',blank=True,null=True, on_delete=models.CASCADE) #models.IntegerField(blank=True,null=True,)
    rif_info = models.ForeignKey('info',blank=True,null=True, on_delete=models.CASCADE)
    rif_pua  = models.ForeignKey('pua',blank=True,null=True, on_delete=models.CASCADE)

    def __str__(self):
        tipo_dict = dict(tipo_allegato_choice)
        if self.rif_warp and not self.rif_warp.destinazione:
            alert = "DA CORRELARE"
        else:
            alert = ""
        if self.documento.name:
            path = os.path.basename(self.documento.name)
        else:
            path = "deleted"
        return "%s (%s) %s" % (tipo_dict[self.tipo].upper(), self.data or "", alert) #, path

@receiver(pre_delete, sender=files_allegati)
def files_allegati_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    #if instance.rif_warp:
        #url = 'http://172.25.193.167:8989/warp/trashimage/{id}/'.format(id=instance.rif_warp.pk)
        #r = requests.get(url)
        #print ("WARP TRASH",url,r.status_code,r.text, file=sys.stderr)
        
    subdir = os.path.dirname(os.path.join(settings.MEDIA_ROOT,instance.documento.name))
    instance.documento.delete(False)
    if os.path.exists(subdir):
        shutil.rmtree(subdir)
    #cancellazione immagine su warp da admin ---->
