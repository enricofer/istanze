from django.conf.urls import url,include
from . import views

from django.conf import settings
import django.contrib.auth.views

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

urlpatterns = [
    url(r'^validazione/$', views.valida_dxf, name='valida_dxf'),
    url(r'^promemoria/$', views.promemoria, name='promemoria'),
    url(r'^geojson/$', views.as_geojson, name='as_geojson'),
    url(r'^mappa/$', views.mappa_pua_tutto, name='as_map'),
    url(r'^sv/(\d+)/$', views.streetview_link, name='as_sv'),
    url(r'^mappa/(?P<PUAId>\d+)/$', views.mappa_pua_singolo, name='as_map'),
    url(r'^edit/(?P<PUAId>\d+)/$', views.modulo_pua, name='edit_pua'),
    url(r'^new/$', views.modulo_pua, name='new_pua'),
    url(r'^cat_osservazioni/$', views.new_cat_osservazioni, name='new_cat_osservazioni'),
    url(r'^login/', django.contrib.auth.views.LoginView),
    url(r'^logout/', django.contrib.auth.views.LogoutView),
]
