# -*- coding: utf-8 -*-

import sys
import os
import argparse
import json

from regole import regoleTutto
from dxf_decode import dxf_decode

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

parser = argparse.ArgumentParser(description='trasformazione di file DXF')
parser.add_argument('-i', '--includi', dest='INCLUDI', nargs='*', help='inserire un elenco di file DXF da processare')

args = parser.parse_args()

print (regoleTutto['LIVELLI'])
print (json.loads(regoleTutto['LIVELLI']))
print (args)

decoder = dxf_decode(regoleTutto,os.path.dirname(__file__),srid = 3003)


for DXF in args.INCLUDI:
    dxffile = os.path.abspath(DXF)
    report = decoder.validate(dxffile)
    print (report['report'])