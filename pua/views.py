from django.shortcuts import render
from .dxf_decode import dxf_decode
from django.conf import settings
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.gis.geos import GEOSGeometry, MultiPolygon, Polygon

from .models import pua,info,provvedimento,tipologia_choice
from .forms import PUAForm
from .regole import regoleDefinizione
from .admin import JsonFromQueryset

from certificati.coordinateCatastali import GeomFromCoordinateCatastali, SV, fix_geometry
from repertorio.models import catIstanze

import json
import sys
import os
import datetime
import uuid
from datetime import date
# Create your views here.

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


def handle_uploaded_file(f):
    fileAbsolutePath = os.path.join(settings.MEDIA_ROOT,'pua','temp.dxf')
    with open(fileAbsolutePath, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return fileAbsolutePath

def valida_dxf(request):
    dxf = ''
    report = None
    dxfFileName = None
    geom = None
    if request.method == 'POST':
        #print (request.FILES, file=sys.stderr)
        #print (request.POST, file=sys.stderr)
        dxf = handle_uploaded_file(request.FILES['DXFFile'])
        dxfFileName = request.FILES['DXFFile'].name
        decoder = dxf_decode(regoleDefinizione,os.path.join(settings.MEDIA_ROOT,'pua'),srid = 3003)
        report = decoder.validate(dxf)
        #geom = json.dumps(report['geojson'])
        print (report['report'], file=sys.stderr)
    return render(request, 'dxf_validazione.html', {'dxf': dxfFileName, 'report':report})

def as_geojson(request):
    json = JsonFromQueryset(pua.objects.all())
    return JsonResponse(json)

def modulo_pua(request,PUAId = None):

    if request.method == 'POST':
        form = PUAForm(request.POST, request.FILES)
        if form.is_valid():
            pass
    else:
        if PUAId:
            editPUA  = pua.objects.get(pk=PUAId)
            form = PUAForm(instance = editPUA)
        else:
            form = PUAForm()
    return render(request, 'pua_edit.html', {'form': form, 'PUAId': PUAId})

def mappa_pua_singolo(request,PUAId):
    queryset = pua.objects.filter(pk=PUAId)
    item = queryset.first()
    if item.zonizzazione:
        with open(os.path.join(settings.MEDIA_ROOT,item.zonizzazione.name),"r") as f:
            zoning = f.read()
    else:
        zoning = None
    
    print ("zoning", zoning, file=sys.stderr)
    return render(request, 'pua_mappa.html', {'features':json.dumps(JsonFromQueryset(queryset)), "zoning": zoning }) #os.path.join(settings.MEDIA_URL,item.zonizzazione.name or '')})

def mappa_pua_tutto(request):
    queryset = pua.objects.all()
    return render(request, 'pua_mappa.html', {'features':json.dumps(JsonFromQueryset(queryset)), "zoning": None})

def streetview_link(request,piano_id):
    piano  = pua.objects.get(pk=piano_id)
    return SV(piano.the_geom.centroid)

def new_cat_osservazioni(request):
    if request.method == 'GET':
        obj = pua.objects.get(id_pua=request.GET.get("pid",''))
        tipopiano = dict(tipologia_choice)
        new_istanza = catIstanze.objects.create(
            tipoIstanza="OSSERVAZIONI %s %s %s" % (("ALLA " if tipopiano == 'PI' else "AL "), tipopiano[obj.tipologia].upper(), obj.ditta.upper()),
            descrizione=(obj.denominazione if obj.denominazione else obj.ditta).upper(),
            termine = datetime.date.today()+datetime.timedelta(days=60)
        )
        obj.cat_osservazioni = new_istanza
        obj.save()
        return HttpResponseRedirect('/admin/pua/pua/%d/change/' % obj.pk)

def promemoria(request):
    if request.method == 'POST':
        nuovo_promemoria = pua()
        nuovo_promemoria.id_pua = "MM" + str(uuid.uuid4()).upper()[:3]
        nuovo_promemoria.ditta = request.POST.get("titolo","")
        nuovo_promemoria.tipologia = 'Altro'
        nuovo_promemoria.stato = 'non rilevato'
        nuovo_promemoria.curatore = request.user
        nuovo_promemoria.sottocategoria = 'PPP'
        if request.POST.get("poligono",''):
            istanza_geom = GEOSGeometry(fix_geometry(request.POST.get("poligono",'')))
            if isinstance(istanza_geom, Polygon): 
                nuovo_promemoria.the_geom = MultiPolygon(istanza_geom)
            else:
                nuovo_promemoria.the_geom = istanza_geom
        elif request.POST.get("coordinateCatastali",''):
            nuovo_promemoria.the_geom = GEOSGeometry (GeomFromCoordinateCatastali(request.POST.get("coordinateCatastali",'')))

        nuovo_promemoria.coordinate_catastali = request.POST.get("coordinateCatastali",'')
        nuovo_promemoria.save()
        nuovo_iter = info()
        nuovo_iter.cat = 'MEM'
        nuovo_iter.titolo = request.POST.get("descrizione","Promemoria piani e progetti")
        nuovo_iter.data = date.today()
        nuovo_iter.utente = request.user.username
        nuovo_iter.rif_pua = nuovo_promemoria
        if request.POST.get("link",'').upper().startswith("HTTP"):
            link = request.POST.get("link",'')
        else:
            schema = "file://"
            if request.POST.get("link",'').startswith("\\"):
                 schema = "file:///"
            link = schema +request.POST.get("link",'').replace("\\","/")
        nuovo_iter.rif_web = link
        nuovo_iter.save()
        return JsonResponse({"pk":nuovo_promemoria.pk})


