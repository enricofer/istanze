from django.test import TestCase
from pua.models import pua

# Create your tests here.
class modello_PUA(TestCase):

    def setUp(self):
        pua.objects.create(id_pua="TEST1", num_archiv="1.2.3", tipologia='Altro', stato='non rilevato', ditta='tmp1')
        pua.objects.create(id_pua="TEST2", num_archiv="3.3.3", tipologia='Altro', stato='non rilevato', ditta='tmp2')