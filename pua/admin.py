from django.contrib import admin
from django import forms
from django.contrib.gis import admin
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.admin import SimpleListFilter
from django.contrib.gis.geos import GEOSGeometry, MultiPolygon, Polygon, fromstr
from django.contrib.gis import geos
from django.core.exceptions import ValidationError
from django.conf import settings
from django.shortcuts import render
from django.utils.html import format_html
from django.http import FileResponse, HttpResponse, HttpResponseRedirect
from datetime import datetime, date
from urllib.parse import urlparse
from django.db.models import Q
from django.contrib.admin.models import LogEntry
from django.contrib.gis import forms as gis_forms

#from import_export.admin import ImportExportActionModelAdmin
#from import_export.formats.base_formats import Format
from shapely.geometry import mapping #,MultiPolygon 
from shapely import wkt
from fiona import collection
from fiona.crs import from_epsg
from io import BytesIO
from zipfile import ZipFile
from pdf2image import convert_from_path

import os
import sys
import json
import nested_admin
#from nested_inline.admin import NestedStackedInline, NestedModelAdmin
import urllib
import requests
import re
import traceback
import uuid
import csv

from .models import pua,info,files_allegati
from django_warp.models import build_vrt, datasets, rasterMaps
from django_warp.views import duplicate
from .dxf_decode import dxf_decode
from .regole import regoleDefinizione
from certificati.coordinateCatastali import checkAreaTroppoEstesa,GeomFromCoordinateCatastali, queryPRG
from certificati.views import ubicazione, riferimenti_incrociati
from certificati.confine_comunale import CONFINE_WKT
from django_warp.models import rasterMaps,datasets
from repertorio.models import istruttorie, repertorioIstanze

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

#from simple_history.admin import SimpleHistoryAdmin

DEV_HOST = "10.10.21.50"
PROD_HOST = "rapper.comune.padova.it"

admin.site.disable_action('delete_selected')

def get_schede_link(ids, geom = None):

    if geom:
        if geom.extent[2] - geom.extent[0] < 250:
            template = 'piani1000'
        elif geom.extent[2] - geom.extent[0] < 500:
            template = 'piani2000'
        elif geom.extent[2] - geom.extent[0] < 1250:
            template = 'piani5000'
        else:
            template = 'piani10000'
    else:
        template = 'piani2000'

    return """https://rapper.comune.padova.it/qgisserver
?MAP=/dati/archiviazione/servizi/PUA/SCHEDE_PUA.qgs
&SERVICE=WMS
&REQUEST=GetPrint
&TEMPLATE=%s
&version=1.3.0
&FORMAT=pdf
&DPI=150
&CRS=EPSG:3003
&ATLAS_PK=%s
&map2:EXTENT=1717850,5023865,1734997,5038305
&map2:SCALE=251654
    """ % (template, str(ids))

def PUA_schede(modeladmin, request, queryset):
    pks = ''
    for piano in queryset:
        pks += str(piano.pk) + ','
    pks = pks[:-1]
    service_link = get_schede_link(pks)
    return HttpResponseRedirect(service_link.replace("\n", ""))
    
#&map1:EXTENT=1717850,5023865,1734997,5038305
#&map1:SCALE=251654

def shapefileFromQueryset(dataset):
    """
    Returns format representation for given dataset.
    """
    attribs = {
        'id_pua': 'str',
        'tipologia': 'str',
        'sottocategoria': 'str',
        'ditta': 'str:200',
        'delib_amb': 'str',
        'delib_ado': 'str',
        'delib_appr': 'str',
        'delib_var': 'str:100',
        'det_coll': 'str',
        'stato_av': 'str',
        'stato_val': 'str',
        'anno_stato': 'int',
        'superficie': 'float:15.2',
        'vol_res': 'float:15.2',
        'slp_no_res': 'float:15.2',
        'vol_misto': 'float:15.2',
    }

    schema = {'geometry': 'MultiPolygon', 'properties': attribs}
    #TODO: it would be much better to save the artifacts to an in memory buffer
    temp_dir = os.path.join(settings.MEDIA_ROOT,'pua','tmp',str(uuid.uuid1()))
    os.makedirs(temp_dir)
    temp_shp = os.path.join(temp_dir,"pua.shp")
    with collection(temp_shp, "w", "ESRI Shapefile", schema, crs=from_epsg(3003), encoding='utf-8') as output:

        for thing in dataset:

                data = dict()
                data['id_pua'] = thing.id_pua
                data['tipologia'] = thing.tipologia
                data['sottocategoria'] = thing.sottocategoria
                data['ditta'] = thing.ditta
                data['delib_amb'] = thing.delib_amb
                data['delib_ado'] = thing.delib_ado
                data['delib_appr'] = thing.delib_appr
                data['delib_var'] = thing.delib_var
                data['det_coll'] = thing.det_collaudo
                data['stato_av'] = thing.stato
                data['stato_val'] = thing.stato_valutazione
                data['anno_stato'] = thing.anno_stato
                data['superficie'] = float(thing.sup_perimetro) if thing.sup_perimetro else None
                data['vol_res'] = float(thing.vol_res) if thing.vol_res else None
                data['slp_no_res'] = float(thing.slp_no_res) if thing.slp_no_res else None
                data['vol_misto'] = float(thing.vol_misto) if thing.vol_misto else None

                the_geom = wkt.loads(thing.the_geom.wkt)
                output.write({'properties': data, 'geometry': mapping(the_geom)})

        print ('COLLECTION', file=sys.stderr)
        print (dir(output), file=sys.stderr)
        print (output, file=sys.stderr)

    #zipFileName = os.path.join(temp_dir,'pua.zip')
    zipMemory = BytesIO()
    zip = ZipFile(zipMemory, 'w')

    for filename in os.listdir(temp_dir):
        zip.write(os.path.join(temp_dir,filename),filename)
        os.remove(os.path.join(temp_dir,filename))

    os.rmdir(temp_dir)

    # fix for Linux zip files read in Windows
    for filename in zip.filelist:
        filename.create_system = 0

    zip.close()
    zipMemory.seek(0)
    return zipMemory


def JsonFromQueryset(features):
    #features_geojson = serialize('geojson', features,geometry_field='geom',srid=3003, fields=('idIstanza','ditta',))
    features_geojson = {
        "type": "FeatureCollection",
        "crs": {"type": "name", "properties": {"name": "EPSG:3003"}},
        "features": []
    }
    for feature in features:
        geometry = json.loads(feature.the_geom.json)
        properties = {
            "pk": feature.pk,
            "id_pua": feature.id_pua,
            "ditta": feature.ditta,
        }
        feature = {
            "type": "Feature",
            "geometry": geometry,
            "properties": properties,
        }
        features_geojson["features"].append(feature)
    return features_geojson

def mappa_tutti_pua(modeladmin, request, queryset):
    return render(request, 'pua_mappa.html', {'features':json.dumps(JsonFromQueryset(pua.objects.all()))})

def mappa_pua(modeladmin, request, queryset):
    return render(request, 'pua_mappa.html', {'features':json.dumps(JsonFromQueryset(queryset))})

def export_csv(modeladmin, request, queryset):
    queryset = queryset.order_by('tipologia', 'stato', 'id_pua')
    response = HttpResponse(content_type='text/csv')
    writer = csv.writer(response,delimiter=';',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow([
        'link',
        'tipologia', 
        'anno_estratto',
        'ID', 
        'ditta', 
        'denominazione', 
        'curatore', 
        'stato',
        'ok',
        'lacune',
        'delib_amb', 
        'delib_ado', 
        'delib_appr', 
        'delib_var', 
        'decadenza',
        'det_collaudo', 
        'pratiche_edilizie', 
        'ha_elaborati',
        'ha_elaborati_corretti',
        'ha_elaborati_in_iter',
        'ha_elaborati_in_iter_corretti',
        'num_schede_iter',
        'num_allegati',
        'ha_convenzione',
        'ha_collaudo',
        'ha_planimetria_raster',
        'ha_planimetria_vector',
    ])
    for item in queryset:
        #schema = 'https://' if request.is_secure else 'http://'
        schema = request.scheme+'://'
        host_complete_url = schema+request.META['HTTP_HOST']
        link = host_complete_url + '/admin/pua/pua/%d/' % item.pk
        writer.writerow([
            link,
            item.get_tipologia_display(),
            item.anno_estratto,
            item.id_pua,
            item.ditta,
            item.denominazione,
            item.curatore,
            item.get_stato_display(),
            item.ok,
            item.lacune,
            item.delib_amb,
            item.delib_ado,
            item.delib_appr,
            item.delib_var,
            item.decadenza,
            item.det_collaudo,
            item.pratiche_edilizie,
            item.ha_elaborati,
            item.ha_elaborati_corretti,
            item.ha_elaborati_in_iter,
            item.ha_elaborati_in_iter_corretti,
            item.num_iter,
            item.num_allegati,
            item.ha_convenzione,
            item.ha_collaudo,
            item.ha_planimetria_raster,
            item.ha_planimetria_vector
        ])
    return response

def export_shp(modeladmin, request, queryset):
    return FileResponse(shapefileFromQueryset(queryset),content_type='application/zip')

export_csv.short_description = "Esporta come CSV i piani selezionati"
export_shp.short_description = "Esporta come shapefile i piani selezionati"
mappa_pua.short_description = "Riproduci in mappa i piani selezionati"
mappa_tutti_pua.short_description = "Riproduci in mappa tutti i piani"
mappa_tutti_pua.acts_on_all = True
PUA_schede.short_description = "Esporta le schede pdf dei piani selezionati"

def extract_date_from_text(txt,div="/"):
    if not txt:
        txt = ''
    pattern = '((\s*|^)(?P<giorno>\d{1,2}).(?P<mese>\d{1,2}).(?P<anno>\d{4})(\s+|$))'
    try:
        maxdate = None
        for i,sub in enumerate(re.finditer(pattern, txt)):
            capture = sub.groupdict()
            date = datetime.strptime('%s/%s/%s'%(capture["giorno"],capture["mese"],capture["anno"]), '%d/%m/%Y').date()
            if not maxdate or date > maxdate:
                maxdate = date
        if maxdate:
            return maxdate
        else:
            return ''
    except Exception as e:
        traceback.print_tb(e.__traceback__, file=sys.stderr)
        return ''


def get_determine_link(txt,div="/"):
    pattern = '(?i)(?P<tipo>d.?e.?t.?|d.?i.?r.?)?(?P<sep>\s|[n.°#]|^)+(?P<anno>\d{4})+.(?P<sett>\d{2})+.(?P<num>\d{1,4})(\s|$)'
    try:
        links = ''
        for i,sub in enumerate(re.finditer(pattern, txt)):
            capture = sub.groupdict()
            n_det = '0000'[len(capture["num"]):]+capture["num"]
            sett = capture["sett"]
            anno = capture["anno"]
            if i == 0:
                num = ''
                acapo = ''
            else:
                num = str(i)
                acapo = '<br/>'
            html = """
    <a target="_blank" href="http://domino/percorsi/delibereesecutivepadova.nsf/ElencoDeterminazioni?SearchView&Query=(%5BProtocolloSettore%5D={anno}/{sett}/{num})&SearchOrder=4">{acapo}Link {n} a Determine dirigenziali</a>
            """

            link = html.format(num=n_det, sett=sett, anno=anno, n=num, acapo=acapo)
            links += link
        return format_html(links)
    except Exception as e:
        traceback.print_tb(e.__traceback__, file=sys.stderr)
        return ''

def get_delibere_link(txt,target,div="/"):
    pattern = '(?i)(?P<tipo>d.?c.?c.?|d.?g.?c.?|p.?e.?d.?)?(?P<sep>\s|[n.°#]|^)+(?P<num>\d{1,4})+.(?P<anno>\d{4})(\s|$)'
    try:
        links = ''
        for i,sub in enumerate(re.finditer(pattern, txt)):
            capture = sub.groupdict()
            a_delib = capture["anno"]
            if i == 0:
                num = ''
                acapo = ''
            else:
                num = str(i)
                acapo = '<br/>'
            if capture["tipo"].upper().replace(".", "") == 'DCC' or target.upper() == 'CONSIGLIO':
                n_delib = '0000'[len(capture["num"]):]+capture["num"]
                html = """
    <a target="_blank" href="http://172.20.0.115/Percorsi/DelibereEsecutivePadova.nsf/DelibereConsiglioEsecutive?SearchView&Query=(%5BNumeroCronologicoC%5D={num}/{anno}+OR+%5BNumeroCronologicoC%5D={anno}/{num})">{acapo}Link {n} a Delibere di Consiglio</a>
                """
            elif capture["tipo"].upper().replace(".", "") == 'DGC' or target.upper() == 'GIUNTA':
                n_delib = '0000'[len(capture["num"]):]+capture["num"]
                html = """
    <a target="_blank" href="http://172.20.0.115/Percorsi/DelibereEsecutivePadova.nsf/DelibereGiuntaEsecutive?SearchView&Query=(%5BNumeroCronologico%5D={num}/{anno}+OR+%5BNumeroCronologico%5D={anno}/{num})">{acapo}Link {n} a Delibere di Giunta</a>
                """
            elif capture["tipo"].upper().replace(".", "") == 'PED' or target.upper() == 'PRATICA':
                n_delib = '00000'[len(capture["num"]):]+capture["num"]
                html = """
    <a target="_blank" href="https://percorsi.comune.padova.it/percorsi/edilizia/pratica.asp?codPratica={anno}{num}&nomeApp=pratiche%20edilizie&codiceFiscale=A442497586&magic=442497586">{acapo}Link {n} a Pratiche edilizie</a>
                """
                
            else:
                return ''

            link = html.format(num=n_delib, anno=a_delib, n=num, acapo=acapo)
            links += link
        return format_html(links)
    except Exception as e:
        traceback.print_tb(e.__traceback__, file=sys.stderr)
        return ''


class allegatiInlineFormset(forms.models.BaseInlineFormSet):

    def save_new(self, form, commit=True):
        obj = super(allegatiInlineFormset, self).save_new(form, commit=False)
        #setattr(obj, self.date_field, datetime.now())
        if commit:
            obj.save()
        return obj

class allegatiFormsetMixin(object):
    formset = allegatiInlineFormset

    def get_formset(self, request, obj=None, **kwargs):
        formset = super(allegatiFormsetMixin, self).get_formset(request, obj, **kwargs)
        formset.request = request
        return formset


class allegatiInInfo(allegatiFormsetMixin,nested_admin.NestedStackedInline):

    model = files_allegati
    extra = 0
    readonly_fields = ( 'link_correlazione',)
    fields = (('documento','tipo','link_correlazione'),)
    
    def link_correlazione(self, obj):
        if obj.rif_warp:
            html = """
    <a target="_blank" href="/warp/imgset/%d/?ext=%s">GEOREFERENZIA</a>
            """ #
            #ext = ",".join([str(a) for a in obj.the_geom.extent])
            rif_parent = obj.rif_pua or obj.rif_info.rif_pua
            link = html % (obj.rif_warp.pk, json.dumps(rif_parent.the_geom.extent))
            return format_html(link)
        else:
            return ""
    link_correlazione.short_description = ''
    link_correlazione.allow_tags = True

class allegatiInline(allegatiFormsetMixin,nested_admin.NestedStackedInline):

    model = files_allegati
    extra = 0
    readonly_fields = ( 'link_correlazione',)
    fields = (('documento','tipo','link_correlazione'),)
    
    def link_correlazione(self, obj):
        if obj.rif_warp:
            html = """
    <a target="_blank" href="/warp/imgset/%d/?ext=%s">GEOREFERENZIA</a>
            """ #
            #ext = ",".join([str(a) for a in obj.the_geom.extent])
            rif_parent = obj.rif_pua or obj.rif_info.rif_pua
            link = html % (obj.rif_warp.pk, json.dumps(rif_parent.the_geom.extent))
            return format_html(link)
        else:
            return ""
    link_correlazione.short_description = ''
    link_correlazione.allow_tags = True

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class informazioniInlineFormset(forms.models.BaseInlineFormSet):

    def save_new(self, form, commit=True):
        obj = super(informazioniInlineFormset, self).save_new(form, commit=False)
        obj.utente = self.request.user.username
        #setattr(obj, self.date_field, datetime.now()) #riferimento alla classe intermedia mixin
        if commit:
            obj.save()
        return obj

    def save_existing(self, form, instance, commit=True):
        obj = super(informazioniInlineFormset, self).save_existing(form, instance, commit=False)
        obj.utente = self.request.user.username
        if commit:
            obj.save()
        return obj

class informazioniFormsetMixin(object): #classe lasciata per aggiungere eventuali informazioni elaborate
    formset = informazioniInlineFormset

    user_field = "sconosciuto"

    def get_formset(self, request, obj=None, **kwargs):
        formset = super(informazioniFormsetMixin, self).get_formset(request, obj, **kwargs)
        formset.form.base_fields["rif_atto"].widget.attrs.update({'placeholder': 'dcc dgc NN/AAAA ... det AAAA/SS/NN ...'})
        formset.request = request
        formset.user_field = request.user.username
        return formset

class informazioniInline(informazioniFormsetMixin,nested_admin.NestedStackedInline): 

    model = info
    extra = 0
    fields = (('cat','stato',),('data','rif_atto', 'link_atto'),'titolo','descrizione',('rif_web','link_rif_web'),'utente')
    readonly_fields = ('utente', 'link_rif_web', 'link_atto')
    ordering = ['data']

    inlines = [allegatiInInfo]

    def link_atto(self, obj):
        if "DGC " in obj.rif_atto.upper().replace(".",""):
            return get_delibere_link(obj.rif_atto,'Giunta')
        elif "DCC " in obj.rif_atto.upper().replace(".",""):
            return get_delibere_link(obj.rif_atto,'Consiglio')
        elif "DET " in obj.rif_atto.upper().replace(".",""):
            return get_determine_link(obj.rif_atto)
        elif "PED " in obj.rif_atto.upper().replace(".",""):
            return get_delibere_link(obj.rif_atto, 'Pratica')
        else:
            return ''
    link_atto.short_description = ''

    def link_rif_web(self, obj):
        if obj.rif_web:
            link= '<a target="_blank" href="%s">Link a Elaborati</a>' % (obj.rif_web,)
            return format_html(link)
        else:
            return 'Elaborati non presenti'
    link_rif_web.short_description = ''


class puaForm(forms.ModelForm):

    #data_stato_ = forms.DateField(widget=forms.DateInput(format="%d/%m/%Y"))
    wkt_geom = forms.CharField(required=False, widget=forms.Textarea( attrs={'rows':'2', 'cols': '10'}), initial='')
    data_stato = forms.DateField(required=False,input_formats=['%Y-%m-%d','%d/%m/%Y'])
    data_stato_estratta = forms.DateField(required=False,input_formats=['%Y-%m-%d','%d/%m/%Y'])
    det_collaudo = forms.CharField(required=False,widget=forms.TextInput(attrs={'style':'width:300px', 'placeholder': 'det AAAA/SS/NN ...'}))
    pratiche_edilizie = forms.CharField(required=False,widget=forms.TextInput(attrs={'style':'width:300px', 'placeholder': 'ped NN/ANNO ...'}))
    delib_amb = forms.CharField(required=False, label='Delibera di delimitazione', widget=forms.TextInput(attrs={'style':'width:300px', 'placeholder': 'dcc NN/AAAA ...'}))
    delib_ado = forms.CharField(required=False, label='Delibera di adozione',widget=forms.TextInput(attrs={'style':'width:300px', 'placeholder': 'dgc NN/AAAA ...'}))
    delib_appr = forms.CharField(required=False, label='Delibera di approvazione',widget=forms.TextInput(attrs={'style':'width:300px', 'placeholder': 'dgc NN/AAAA ...'}))
    sup_perimetro = forms.FloatField(required=False, label='Superficie')
    ditta = forms.CharField(required=True, label='Denominazione interna', widget=forms.TextInput(attrs={'class': "vTextField", 'maxlength':'254'}))
    segnalazione = forms.CharField(required=False,widget=forms.TextInput(attrs={'style':'width:758px, max-width:758px'}))

    '''
    def _save(self, commit=True):
        wkt_geom = self.cleaned_data.get('wkt_geom', None)
        instance = super(puaForm, self).save(commit=False)
        instance.the_geom = GEOSGeometry(wkt_geom, srid=3003)
        if commit:
            instance.save()
        print (instance.the_geom)
        return instance
    '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs and kwargs['instance']:  
            #print ("FORM",args, self.fields,file=sys.stderr)
            if kwargs['instance'].the_geom:
                wkt_geom_txt = kwargs['instance'].the_geom.wkt
                self.fields['wkt_geom'].widget.attrs.update({'placeholder': wkt_geom_txt})
            #pass

    def clean_tipologia(self):
        if self.instance.tipologia in ('Altro', 'AdProg') and not (self.cleaned_data['tipologia'] in ('Altro', 'AdProg')):
            raise forms.ValidationError("Non è consentito il cambiamento di tipologia in categorie diverse da 'Altro' e 'Accordo di programma'")
        return self.cleaned_data['tipologia']

    def clean_wkt_geom(self):
        wkt_geom = self.cleaned_data['wkt_geom']
        if wkt_geom != '':
            try:
                the_geom = GEOSGeometry(wkt_geom, srid=3003)
                self.cleaned_data['the_geom'] = self.instance.the_geom if self.instance.the_geom else GEOSGeometry("MULTIPOLYGON (((1731000 5034187, 1731085 5034301, 1731123 5034195, 1731053 5034197, 1731000 5034187)))", srid=3003)
            except Exception as e:
                print ("GEOM NON VALIDA", e)
                raise forms.ValidationError("il formato della geometria non è valido")
        return wkt_geom

    class Meta(object):
        model = pua
        fields = '__all__'

class ControlCollaudati(SimpleListFilter):
    title = 'controllo' 
    parameter_name = 'controllo_pua'

    def lookups(self, request, model_admin):
        return (
            ('convenzionati_senza_convenzione', 'senza convenzione'),
            ('convenzionati_con_convenzione', 'con convenzione'),

            ('senza_collaudo', 'senza certificato di collaudo'),
            ('con_collaudo', 'con certificato di collaudo'),

            ('senza_riferimento_det_collaudo', 'senza riferimento a determina di collaudo'),
            ('con_riferimento_det_collaudo', 'con riferimento a determina di collaudo'),

            ('senza_riferimento_delimitazione', 'senza riferimento a delibera di delimitazione'),
            ('con_riferimento_delimitazione', 'con riferimento a delibera di delimitazione'),

            ('senza_riferimento_elaborati', 'senza riferimento a repository elaborati'),
            ('con_riferimento_elaborati', 'con riferimento a repository elaborati'),

            ('senza_raster', 'senza planimetria raster', ),
            ('con_raster_nogeoref', 'con planimetria raster non georeferenziata', ),
            ('con_raster_georef', 'con planimetria raster georeferenziata', ),

            ('senza_planivol_dxf', 'senza planimetria vettoriale'),
            ('con_planivol_dxf', 'con planimetria vettoriale'),
            #('collaudati_senza_planimetria_allegata', 'collaudati senza planimetria allegata',),
            #('collaudati_con_riferimento', 'collaudati con riferimento a determina', ),
            #('collaudati_senza_riferimento', 'collaudati senza riferimenti a determina', ),
            #('collaudati_con_determina_allegata', 'collaudati con determina allegata', ),
            #('collaudati_senza_determina_allegata', 'collaudati senza determina allegata',),
            #('collaudati_con_delibera_allegata', 'collaudati con delibera allegata', ),
            #('collaudati_senza_delibera_allegata', 'collaudati senza delibera allegata',),
            #('collaudati_con_convenzione_allegata', 'collaudati con convenzione allegata', ),
            #('collaudati_senza_convenzione_allegata', 'collaudati senza convenzione allegata',),
            #('collaudati_con_nta_allegate', 'collaudati con NTA allegate', ),
            #('collaudati_senza_nta_allegate', 'collaudati senza NTA allegate',),
            #('collaudati_con_planimetria_allegata', 'collaudati con planimetria allegata', ),
            #('collaudati_senza_planimetria_allegata', 'collaudati senza planimetria allegata',),
            #('collaudati_con_planimetria_allegata_georeferenziata', 'collaudati con planimetria allegata e georeferenziata', ),
            #('collaudati_con_planimetria_allegata_non_georeferenziata', 'collaudati con planimetria allegata ma non georeferenziata', ),
        )

    def queryset(self, request, qs):

        #collaudati = qs.filter(stato='collaudato')

        planimetrie = files_allegati.objects.filter(tipo="PVL")
        pua_con_planimetria_raster_pk = [p.rif_pua.pk if p.rif_pua else p.rif_info.rif_pua.pk for p in planimetrie]
        pua_con_planimetria_georeferenziata_pk = []
        for p in planimetrie:
            if p.rif_warp and p.rif_warp.destinazione:
                pua_con_planimetria_georeferenziata_pk.append(p.rif_pua.pk if p.rif_pua else p.rif_info.rif_pua.pk)
        pua_con_planimetria_raster_georef = qs.filter(Q(pk__in=pua_con_planimetria_raster_pk) & Q(pk__in=pua_con_planimetria_georeferenziata_pk))
        pua_con_planimetria_raster_no_georef = qs.filter(pk__in=pua_con_planimetria_raster_pk)
        pua_senza_planimetria_raster = qs.exclude(pk__in=pua_con_planimetria_raster_pk)

        planivol_dxf = files_allegati.objects.filter(tipo="GEO")
        con_planivol_dxf_pk = [p.rif_pua.pk if p.rif_pua else p.rif_info.rif_pua.pk for p in planivol_dxf]
        piani_senza_planivol_dxf = qs.exclude(pk__in=con_planivol_dxf_pk)
        piani_con_planivol_dxf = qs.filter(pk__in=con_planivol_dxf_pk)

        convenzioni = files_allegati.objects.filter(tipo="CON")
        piani_con_convenzione_pk = [p.rif_pua.pk if p.rif_pua else p.rif_info.rif_pua.pk for p in convenzioni]
        piani_senza_convenzione = qs.exclude(pk__in=piani_con_convenzione_pk)
        piani_con_convenzione = qs.filter(pk__in=piani_con_convenzione_pk)

        collaudi = files_allegati.objects.filter(tipo="COL")
        pua_con_collaudo_pk = [p.rif_pua.pk if p.rif_pua else p.rif_info.rif_pua.pk for p in collaudi]
        piani_senza_collaudo = qs.exclude(pk__in=pua_con_collaudo_pk)
        piani_con_collaudo = qs.filter(pk__in=pua_con_collaudo_pk)

        if self.value() == 'convenzionati_senza_convenzione':
            return piani_senza_convenzione
        elif self.value() == 'convenzionati_con_convenzione':
            return piani_con_convenzione

        elif self.value() == 'con_collaudo':
            return piani_con_collaudo
        elif self.value() == 'senza_collaudo':
            return piani_senza_collaudo

        elif self.value() == 'con_riferimento_det_collaudo':
            return qs.filter(Q(det_collaudo__isnull=False) | ~Q(det_collaudo__exact=""))
        elif self.value() == 'senza_riferimento_det_collaudo':
            return qs.exclude( Q(det_collaudo__isnull=False) | ~Q(det_collaudo__exact=""))

        elif self.value() == 'con_riferimento_delimitazione':
            return qs.filter(Q(delib_amb__isnull=False) | ~Q(delib_amb__exact=""))
        elif self.value() == 'senza_riferimento_delimitazione':
            return qs.exclude( Q(delib_amb__isnull=False) | ~Q(delib_amb__exact=""))

        elif self.value() == 'con_riferimento_elaborati':
            return qs.filter(Q(elaborati__isnull=False) | ~Q(elaborati__exact=""))
        elif self.value() == 'senza_riferimento_elaborati':
            return qs.exclude( Q(elaborati__isnull=False) | ~Q(elaborati__exact=""))

        elif self.value() == 'con_planivol_dxf':
            return piani_con_planivol_dxf
        elif self.value() == 'senza_planivol_dxf':
            return piani_senza_planivol_dxf
        
        elif self.value() == 'con_raster_georef':
            return pua_con_planimetria_raster_georef
        elif self.value() == 'con_raster_nogeoref':
            return pua_con_planimetria_raster_no_georef
        elif self.value() == 'senza_raster':
            return pua_senza_planimetria_raster

        else:
            return qs

def has_planivol_raster(obj):
    planivolumetrici_raster = files_allegati.objects.filter(tipo="PVL")
    pua_con_raster = [p.rif_pua.pk if p.rif_pua else p.rif_info.rif_pua.pk for p in planivolumetrici_raster]
    raster_related = [p.rif_warp.pk for p in planivolumetrici_raster if (p.rif_warp and (p.rif_pua and p.rif_pua == obj) or (p.rif_info and p.rif_info.rif_pua == obj))]

    print ("pua_con_raster", pua_con_raster, raster_related)
    if raster_related and obj.pk in pua_con_raster:
        return max(raster_related)
    else:
        return False

@admin.register(pua)
class puaAdmin(nested_admin.NestedModelAdmin, admin.OSMGeoAdmin):#admin.OSMGeoAdmin):
    change_form_template = 'admin/pua/change_form.html'
    change_list_template = 'admin/pua/change_list.html'
    list_per_page = 12
    default_lon = 1725155
    default_lat = 5032083
    default_zoom = 13
    max_resolution = 350000
    num_zoom = 28
    max_zoom = 28
    min_zoom = 10
    map_width = 750
    map_height = 500
    map_srid = 3003
    wms_url = "https://rapper.comune.padova.it/qgisserver?MAP=/usr/lib/cgi-bin/servizi/default.qgs"
    wms_layer = 'PI,PI_CS'
    wms_name = 'PI'
    wms_options = {'transparent':"true",'format': 'image/png'}
    map_template = 'admin/pua/openlayers_extralayers.html'
    modifiable = True
    readonly_fields = (
        'indice_territoriale_residenziale','indice_territoriale_non_residenziale','abitanti_teorici','standard_parcheggio','standard_verde',
        'sup_ambito','destinazioni_table','wkt_desc','link_map_esterna','anno_estratto',
        'link_delib_amb', 'link_delib_ado', 'link_delib_appr', 'link_delib_var' ,'link_deter_col', 'link_pratiche','link_clipboard_wkt',
        'link_convenzione','link_elaborati','link_planivol_vector','link_planivol_raster','mappa_sola_lettura', 'the_geom_edit_button',
        'trigger_nascondi_the_geom', 'the_geom_disable_edit_button', 'link_streetview', 'link_cert_collaudo', 'link_variante_rif', 'link_istanze', 'link_lista_rifweb',
        'link_osservazioni', 'link_segnalazione', 'link_scheda', 'link_nuova_osservazione', 'cat_osservazioni_RO', 'link_crea_cat_osservazioni','link_riferimenti_incrociati'
        #'convenzione', 'planivol_vector', 'planivol_raster',
    )
    form = puaForm
    #inlines = [informazioniInline, allegatiInline, ] #informazioniInline,
    list_display = ('id_pua','ditta','denominazione','tipologia', 'sottocategoria','stato','anno_estratto','sup_perimetro','vol_res','vol_misto','slp_no_res', 'sup_cessioni', 'sup_privata', 'sup_verde', 'sup_park')
    search_fields = ['id_pua','ditta','denominazione','note','stato','infos__titolo','infos__descrizione']
    list_filter = ('tipologia', 'sottocategoria','stato' ,ControlCollaudati)
    ordering = ['id_pua','ditta']
    actions = [mappa_pua,mappa_tutti_pua,PUA_schede,export_shp,export_csv]

    class Media:
        js = (
            #"https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.js",
            #"https://unpkg.com/ol-layerswitcher@3.2.0",
            "pua/extra_lib.js",
            "certificati/js/salva_stato_layers.js",
            #'repertorio/mappeDiBase.js',
            #'pua/pua_finestramappa.js',
        )
        #css = {
            #'all': (
                #"https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.css",
                #"https://unpkg.com/ol-layerswitcher@3.2.0/src/ol-layerswitcher.css",
                #'pua/pua_finestramappa.css',
            #)
        #}

    def get_inlines(self, request, obj):
        if not obj:
            return []
        elif obj.tipologia in ('AdPian', ):
            return [allegatiInInfo,]
        elif obj.tipologia in ('Altro', 'AdProg','PUA', ):
            return [informazioniInline, allegatiInline]
        else:
            return [informazioniInline, allegatiInline]

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return (
                ("Intestazione", {
                    'classes': ('grp-collapse grp-open',),
                    'fields': (('id_pua','curatore'), ('tipologia', 'sottocategoria'),('stato', "anno_estratto"),'ditta', 'denominazione', 'num_archiv', 'note')#,'ditta',
                }),
                ("Localizzazione", {
                    'classes': ('grp-collapse grp-open',),
                    'fields': ('the_geom',('wkt_geom','link_clipboard_wkt',), 'coordinate_catastali','the_geom_disable_edit_button') #'coordCatastali',
                }),
            )
        
        osservazioni_def = ('cat_osservazioni','link_osservazioni','link_nuova_osservazione')
        if not obj.cat_osservazioni:
                osservazioni_def = ('cat_osservazioni','link_crea_cat_osservazioni')
        elif obj.cat_osservazioni and obj.cat_osservazioni.termine and obj.cat_osservazioni.termine < date.today():
                osservazioni_def = ('cat_osservazioni_RO','link_osservazioni')

        intestazione = ("Intestazione", {
            'classes': ('grp-collapse grp-open',),
            'fields': (('id_pua','curatore'), ('tipologia', 'sottocategoria',),('stato', 'decadenza', "anno_estratto"),'ditta', 'denominazione', 'note',('num_archiv', 'link_scheda',))#,'ditta',
        })
        
        #if obj: # editing an existing object
        if obj.tipologia in ( 'PI', 'AdProg'):
            rif_amm_fields = ('Riferimenti amministrativi', {
                'classes': ('grp-collapse grp-open',),
                'fields': (
                    'dds',
                    ('delib_ado','link_delib_ado'),
                    ('delib_appr','link_delib_appr'),
                    'efficacia',
                    ('pratiche_edilizie','link_pratiche'),
                    osservazioni_def,
                    'version',
                    'qc',
                    ('elaborati','link_elaborati',),
                    ('link_lista_rifweb',),
                ),
            })
        elif obj.tipologia in ('Altro',):
            rif_amm_fields = ('Riferimenti amministrativi', {
                'classes': ('grp-collapse grp-open',),
                'fields': (
                    'pratiche_edilizie',
                    ('link_lista_rifweb',),
                ),
            })
            intestazione = ("Intestazione", {
                'classes': ('grp-collapse grp-open',),
                'fields': (('id_pua','curatore'), ('tipologia', 'sottocategoria',),'ditta', )#,'ditta',
            })
        elif obj.tipologia in ('AdPian',):
            rif_amm_fields = ('Riferimenti amministrativi', {
                'classes': ('grp-collapse grp-open',),
                'fields': (
                    ('variante_rif', 'link_variante_rif',),
                    ('delib_appr','link_delib_appr'),
                    'pratiche_edilizie',
                    ('elaborati','link_elaborati'),
                ),
            })
        else:
            rif_amm_fields = ('Riferimenti amministrativi', {
                'classes': ('grp-collapse grp-open',),
                'fields': (
                    ('delib_amb','link_delib_amb'),
                    ('delib_ado','link_delib_ado'),
                    ('delib_appr','link_delib_appr'),
                    ('delib_var','link_delib_var'),
                    osservazioni_def,
                    ('det_collaudo','link_deter_col'),
                    ('pratiche_edilizie','link_pratiche'),
                    ('elaborati','link_elaborati'),
                    ('link_lista_rifweb'),
                    ('link_convenzione','link_cert_collaudo','link_planivol_raster','link_planivol_vector'),
                ),
            })

        if request.GET.get('force_edit', ''):
            #loc_class = 'grp-collapse grp-closed'
            #loc_edit_class = 'grp-collapse grp-open'
            loc_mod_fields1 = ("Localizzazione", {
                'classes': ('grp-collapse grp-open',),
                'fields': ('the_geom',('wkt_geom','link_clipboard_wkt',), 'coordinate_catastali','the_geom_disable_edit_button') #'coordCatastali',
            })
            loc_mod_fields2 = ("Localizzazione - modifiche da abilitare", {
                'classes': ('grp-collapse grp-closed',),
                'fields': ('link_clipboard_wkt',) #'coordCatastali',
            })

        else:
            #loc_class = 'grp-collapse grp-open'
            #loc_edit_class = 'grp-collapse grp-closed'
            loc_mod_fields1 = ("Localizzazione", {
                'classes': ('grp-collapse grp-open',),
                'fields': ('mappa_sola_lettura',('link_streetview','the_geom_edit_button')) #'coordCatastali',
            })
            loc_mod_fields2 = ("Localizzazione - modifiche da abilitare", {
                'classes': ('grp-collapse grp-closed',),
                'fields': ('the_geom',) #'coordCatastali',
            })

        fs_def = [
            
            intestazione,
            ("Segnalazioni",{
                'classes': ('grp-collapse grp-closed',),
                'fields': (('segnalazione','link_segnalazione'),)
            }),
            loc_mod_fields1,
            loc_mod_fields2,
            ('Dettaglio per CDU', {
                'classes': ('grp-collapse grp-closed',),
                'fields': (
                    'pubblicato',
                    'cdu',
                ),
            }),
            rif_amm_fields,
        ]

        if obj.tipologia in ('PUA','VPL','AdPian'):
            fs_def.append(
                ('Dati quantitativi', {
                    'classes': (('grp-collapse grp-open' if obj.tipologia == 'PUA' else 'grp-collapse grp-closed'),),
                    'fields': (
                        ('sup_ambito',
                        'sup_perimetro',
                        'vol_res',),
                        ('vol_misto',
                        'slp_no_res',
                        'sup_cessioni'),
                        ('sup_privata',
                        'sup_verde',
                        'sup_park'),
                    ),
                })
            )
            fs_def.append(
                ('Riferimenti ad altre informazioni su rapper', {
                    'classes': (('grp-collapse grp-open'),),
                    'fields': ('link_riferimenti_incrociati',),
                })
            )
            fs_def.append(
                ('Indici', {
                    'classes': ('grp-collapse grp-closed',),
                    'fields': (
                        ('indice_territoriale_residenziale',
                        'indice_territoriale_non_residenziale',),
                        ('abitanti_teorici',
                        'standard_parcheggio',
                        'standard_verde'),
                    )
                })
            )
        elif obj.tipologia in ('PI',):
            fs_def.append(
            #tmp = (
                ("Destinazioni di piano",{
                    'classes': ('grp-collapse grp-closed',),
                    'fields': ('destinazioni_table',)
                }) 
            )

        return fs_def


    def destinazioni_table(self,obj):
        #urlsafe_wkt = urllib.parse.quote(obj.the_geom.wkt, safe='')
        #r = requests.get(settings.SERVER_ADDRESS+"certificati/destinazioni/", params={"json":"1","geom":urlsafe_wkt})
        #destinazioni_json = r.json()
        if obj.the_geom and not checkAreaTroppoEstesa(obj.the_geom.wkt):
            try:
                destinazioni_json = queryPRG(obj.the_geom)
                html = '<TABLE><TR><TH>destinazioni di PI</TH><TH style="text-align: right;">mq</TH></TR>'
                resti = 0.00
                for destinazione in destinazioni_json:
                    try:
                        if float(destinazione[2]) > 1:
                            html += '<TR><TD>%s</TD><TD style="text-align: right;">%.2f</TD></TR>' % (destinazione[0], float(destinazione[1]))
                        else:
                            resti += float(destinazione[1])
                    except:
                        pass
                if resti > 0.0:
                    html += '<TR><TD>Aree residuali</TD><TD style="text-align: right;">%.2f</TD></TR></TABLE>' % resti
                else:
                    html += '</TABLE>'
            except Exception as e:
                html = '<strong>ERRORE GEOMETRICO:</strong></br>'+ str(e)
            return format_html(html)
        else:
            return "Destinazioni indeterminate"

    destinazioni_table.allow_tags = True

    def wkt_desc(self, obj):
        return obj.the_geom.wkt
    wkt_desc.short_description = 'descrizione wkt della geometria del piano'

    def mappa_sola_lettura(self, obj):
        if obj.pk:
            #queryset = pua.objects.filter(pk=obj.pk)
            #features = json.dumps(JsonFromQueryset(queryset))
            #with open(os.path.join(settings.MEDIA_ROOT,obj.zonizzazione.name)) as geojson:
            #    zoning = geojson.read()
            error = "no_err"
            raster_rif = has_planivol_raster(obj)
            if raster_rif:
                warp_dataset = str(datasets.objects.get(name=obj.tipologia).pk)
            else:
                warp_dataset = "undefined"
            #try:
            #    if planivol_raster_related:
            #        raster_rif = obj.planivol_raster.split("/")[-2]
            #    else:
            #        raster_rif = None
            #except Exception as e:
            #    error = str(e)
            #    raster_rif = None

            confine_pd = GEOSGeometry(CONFINE_WKT)
            if obj.the_geom.disjoint(confine_pd):
                ro_geom = CONFINE_WKT
            else:
                ro_geom = obj.the_geom.wkt

            html= '''<div id="map"></div><script>window.onload = loadFinestraMappa('%s','%s', '%s', '%s')</script>''' % (ro_geom, obj.zonizzazione.url if obj.zonizzazione else '', warp_dataset, error)
            return format_html(html)
        else:
            return ''
    mappa_sola_lettura.short_description = ''

    def link_streetview(self, obj):
        links = '''<a href="/pua/sv/%s/" class="button" target="_blank">Streetview</a>'''
        return format_html(links % obj.pk)
    link_streetview.short_description = ''

    def link_riferimenti_incrociati(self,obj):
        if obj.the_geom:
            return format_html(riferimenti_incrociati(obj.the_geom.wkt,exclude_self={"piani":obj.pk}))
    link_riferimenti_incrociati.short_description = 'Riferimenti ad altre informazioni su rapper'
    link_riferimenti_incrociati.allow_tags = True

    def cat_osservazioni_RO(self, obj): #al momento non utilizzato
        return str(obj.cat_osservazioni)
    cat_osservazioni_RO.short_description = ''

    def link_map_esterna(self, obj): #al momento non utilizzato
        if obj.pk:
            link= '<a target="_blank" href="/pua/mappa/%d/">Mappa di dettaglio</a>' % obj.pk
            return format_html(link)
        else:
            return ''
    link_map_esterna.short_description = ''

    def link_istanze(self, obj):
        if obj.tipologia == 'AdPian' and obj.pk:
            try:
                istanze_related = repertorioIstanze.objects.filter(geom__intersects=obj.the_geom)
                print ('istanze_related', istanze_related)
            except Expception as E:
                print ('istanze_related', E)
            link = ''
            for istanza in istanze_related:
                link += '<a target="_blank" href="/admin/repertorio/repertorioistanze/%d/">%s</a>&nbsp;&nbsp;' % (istanza.pk, str(istanza))
            return format_html(link)
        else:
            return ''
    link_istanze.short_description = 'Link ad istanze correlate'

    def link_clipboard_wkt(self, obj):
        links = '''<a id="link_button" class="button" onclick="copia_clipboard('%s')">Copia negli appunti</a>'''
        return format_html(links % obj.the_geom.wkt)
    link_clipboard_wkt.short_description = ''

    def link_osservazioni(self, obj):
        if obj.cat_osservazioni:
            try:
                link = '<a target="_blank" href="/admin/repertorio/repertorioistanze/?categoriaIstanza=%d">Link ad osservazioni correlate</a>' % obj.cat_osservazioni.pk
                return format_html(link)
            except Exception as E:
                print ("link_osservazioni",E)
        else:
            return ''
    link_osservazioni.short_description = ''

    def link_lista_rifweb(self,obj):
        info_related = info.objects.filter(rif_pua__pk=obj.pk).order_by('data')
        print ("info_related",info_related)
        links = ""
        for i in info_related:
            if i.rif_web:
                links += ('<a target="_blank" href="%s">Documentazione relativa a %s - %s</a><br/>' % (i.rif_web, i.titolo, str(i.data))) #.strftime("%m/%d/%Y"))
        return format_html(links)
    link_lista_rifweb.short_description = 'Raccolta documentazione da Iter di piano'

    def link_crea_cat_osservazioni(self, obj):
        if obj and not obj.cat_osservazioni:
            link = '<a href="#" onclick="$(\'#add_id_cat_osservazioni\').click()">Nuova categoria osservazioni</a>'
            return format_html(link)
        else:
            return ''
    link_crea_cat_osservazioni.short_description = ''

    def link_nuova_osservazione(self, obj):
        if obj.cat_osservazioni:
            try:
                link = '<a target="_blank" href="/admin/repertorio/repertorioistanze/add/?_changelist_filters=categoriaIstanza=%d">Nuova osservazione</a>' % obj.cat_osservazioni.pk
                return format_html(link)
            except Exception as E:
                print ("nuova_osservazione",E)
        else:
            return ''
    link_nuova_osservazione.short_description = ''

    def link_segnalazione(self, obj):
        link = '<div id="link_segnala" style="cursor: pointer;" onclick="segnala(%d)">Invia segnalazione</div>' % ((obj.curatore.pk if obj.curatore else -1))
        return format_html(link)
    link_segnalazione.short_description = ''

    def trigger_nascondi_the_geom(self, obj):
        return format_html('''<script>window.onload = hideGeom()</script>''')
    trigger_nascondi_the_geom.short_description = ''

    def the_geom_edit_button(self, obj):
        if obj.pk:
            link= '<a href="/admin/pua/pua/%d/change/?force_edit=true">Abilita la modifica della geometria</a>' % obj.pk
            return format_html(link)
        else:
            return ''
    the_geom_edit_button.short_description = ''

    def the_geom_disable_edit_button(self, obj):
        if obj.pk:
            link= '<a href="/admin/pua/pua/%d/change/?">Disabilita la modifica della geometria</a>' % obj.pk
            return format_html(link)
        else:
            return ''
    the_geom_disable_edit_button.short_description = ''

    def link_cert_collaudo(self, obj):
        cert_collaudo_related = files_allegati.objects.filter(rif_info__rif_pua__pk=obj.pk).filter(tipo='COL').last()
        if cert_collaudo_related:
            link= '<a target="_blank" href="%s">Link a Certificato di collaudo</a>' % cert_collaudo_related.documento.url
            return format_html(link)
        else:
            return 'Certificato di collaudo non presente'
    link_cert_collaudo.short_description = ''

    def link_convenzione(self, obj):
        convenzione_related = files_allegati.objects.filter(rif_info__rif_pua__pk=obj.pk).filter(tipo='CON').last()
        if convenzione_related:
            link= '<a target="_blank" href="%s">Link a Convenzione</a>' % convenzione_related.documento.url
            return format_html(link)
        else:
            return 'Convenzione non presente'
    link_convenzione.short_description = ''

    def link_scheda(self, obj):
        link= '<a target="_blank" href="%s">Scheda PDF del Piano</a>' % get_schede_link(obj.pk, obj.the_geom)
        return format_html(link)
    link_scheda.short_description = ''

    def link_elaborati(self, obj):
        if obj.elaborati:
            link= '<a target="_blank" href="%s">Link a Elaborati di piano</a>' % (obj.elaborati,)
            return format_html(link)
        else:
            return 'Elaborati di piano non presenti'
    link_elaborati.short_description = ''

    def link_planivol_vector(self, obj):
        planivol_vector_related = files_allegati.objects.filter(rif_info__rif_pua__pk=obj.pk).filter(tipo='GEO').last()
        if planivol_vector_related:
            link= '<a target="_blank" href="%s">Link a Planivolumetrico vettoriale</a>' % planivol_vector_related.documento.url
            return format_html(link)
        else:
            return 'Planivolumetrico vettoriale non presente'
    link_planivol_vector.short_description = ''

    def link_planivol_raster(self, obj):
        planivol_raster_related = files_allegati.objects.filter(rif_info__rif_pua__pk=obj.pk).filter(tipo='PVL').last()
        if planivol_raster_related:
            link= '<a target="_blank" href="%s">Link a Planivolumetrico raster</a>' % planivol_raster_related.documento.url
            return format_html(link)
        else:
            return 'Planivolumetrico raster non presente'
    link_planivol_raster.short_description = ''

    def link_variante_rif(self, obj):
        if obj.variante_rif:
            link= '<a target="_blank" href="/admin/pua/pua/%s/change/">Link a Variante</a>' % obj.variante_rif.pk
            return format_html(link)
        else:
            return ''
    link_variante_rif.short_description = ''

    def link_delib_amb(self, obj):
        return get_delibere_link(obj.delib_amb,'Consiglio')
    link_delib_amb.short_description = ''

    def link_delib_ado(self, obj):
        return get_delibere_link(obj.delib_ado,'Giunta')
    link_delib_ado.short_description = ''

    def link_delib_appr(self, obj):
        return get_delibere_link(obj.delib_appr,'Giunta')
    link_delib_appr.short_description = ''

    def link_delib_var(self, obj):
        return get_delibere_link(obj.delib_var,'Giunta')
    link_delib_var.short_description = ''

    def link_deter_col(self, obj):
        return get_determine_link(obj.det_collaudo)
    link_deter_col.short_description = ''

    def link_pratiche(self, obj):
        return get_delibere_link(obj.pratiche_edilizie, 'Pratica')
    link_pratiche.short_description = ''

    def indice_territoriale_residenziale(self, obj):
        try:
            it = round(obj.vol_res/obj.sup_perimetro,2)
        except:
            it = None
        return it
    indice_territoriale_residenziale.short_description = 'indice terr. residenziale'

    def indice_territoriale_non_residenziale(self, obj):
        try:
            it = round(obj.slp_no_res/obj.sup_perimetro,2)
        except:
            it = None
        return it
    indice_territoriale_non_residenziale.short_description = 'indice terr. non residenziale'

    def abitanti_teorici(self, obj):
        try:
            it = round(obj.vol_res/192,2)
        except:
            it = None
        return it

    def standard_parcheggio(self, obj):
        try:
            it = round(obj.sup_park/self.abitanti_teorici(obj),2)
        except:
            it = None
        return it

    def standard_verde(self, obj):
        try:
            it = round(obj.sup_verde/self.abitanti_teorici(obj),2)
        except:
            it = None
        return it

    def save_related(self, request, form, formsets, change):
        obj = form.instance
        warp_search = datasets.objects.filter(name=obj.tipologia)
        if warp_search:
            warp_dataset = warp_search[0]
        else:
            pua_dataset = datasets.objects.get(name="PUA")
            warp_dataset = duplicate(pua_dataset,obj.tipologia)

        print('pre save_related puaAdmin --->')
        super(puaAdmin, self).save_related(request, form, formsets, change)
        for n,formset in enumerate(formsets):
            print('FORMSET %s --->' % n)
            for n,sub_form in enumerate(formset):
                print('FORM %s --->' % n)
                #print('changed:',sub_form.has_changed(), file=sys.stderr)
                #print('instance:',dir(sub_form.instance), file=sys.stderr)
                #print('model:',sub_form.cleaned_data, file=sys.stderr)
                #print(sub_form.cleaned_data, file=sys.stderr)
                model_name = sub_form.instance.__class__.__name__
                #print('model_name:',model_name, file=sys.stderr)
                #print('is deleting:',sub_form.cleaned_data['DELETE'])

                if sub_form.has_changed():
                    if model_name == 'files_allegati': # E' un nuovo allegato
                        allegati_obj = sub_form.instance
                        if sub_form.cleaned_data['DELETE']:
                            if sub_form.instance.rif_warp:
                                sub_form.instance.rif_warp.delete() 
                                build_vrt(warp_dataset.pk) #rigenera coverage su django_warp
                            if sub_form.cleaned_data['tipo'] == 'CON': # se si sta cancellando una convenzione, si cancellerà anche il campo nell'istanza principale
                                obj.convenzione = None
                                obj.save()
                            elif sub_form.cleaned_data['tipo'] == 'PVL': # se si sta cancellando una convenzione, si cancellerà anche il campo nell'istanza principale
                                obj.planivol_raster = has_planivol_raster(obj)
                                obj.save()
                            elif sub_form.cleaned_data['tipo'] == 'GEO': # se si sta cancellando una convenzione, si cancellerà anche il campo nell'istanza principale
                                obj.planivol_vector = None
                                obj.save()

                        else:
                            allegati_obj.save()
                            if sub_form.cleaned_data['tipo'] == 'GEO': # E' geometria
                                nome_file, estensione_file = os.path.splitext(sub_form.cleaned_data['documento'].name)
                                if estensione_file.upper() == '.DXF':
                                    dir_path = os.path.join(settings.MEDIA_ROOT,'pua','planivol',str(obj.pk))
                                    decoder = dxf_decode(regoleDefinizione,dir_path,srid = 3003)
                                    file_path = os.path.join(dir_path,'geometria_raw.dxf')
                                    with open(file_path, "w") as raw_geom_file:
                                        for chunk in sub_form.cleaned_data['documento'].chunks():
                                            raw_geom_file.write(chunk.decode("utf-8"))
                                    risultato = decoder.validate(file_path)
                                    if risultato['esito'] or not risultato['esito']: # estrai comunque il poligono del perimetro, anche con esito di validazione negativo
                                        
                                        obj.planivol_vector = os.path.join(settings.MEDIA_URL,'pua','planivol',str(obj.pk),'geometria_raw.dxf')
                                        geojson = json.loads(risultato.pop('geojson'))
                                        obj.zonizzazione = file_path.replace(settings.MEDIA_ROOT,"") + '.geojson'
                                        #obj.zonizzazione = file_path
                                        features = geojson['features']
                                        for feature in features:
                                            if feature['properties']['layer'] == 'PUA_PERIMETRO':

                                                geom_tmp = GEOSGeometry(json.dumps(feature['geometry']))

                                                if geom_tmp.valid:
                                                    print (geom_tmp.wkt, file=sys.stderr)
                                                else:
                                                    print (geom_tmp.valid_reason, file=sys.stderr)

                                                if geom_tmp.geom_type == 'Polygon':
                                                    geom = MultiPolygon([geom_tmp])
                                                elif geom_tmp.geom_type == 'MultiPolygon':
                                                    geom = geom_tmp
                                                else:
                                                    geom = None
                                                    #raise exception! not valid geom type

                                                print (geom, file=sys.stderr)
                                                if geom:
                                                    print (feature['geometry'], file=sys.stderr)
                                                    #estrazione della geometria del perimetro
                                                    obj.the_geom = geom
                                                    for field in ('sup_perimetro','sup_ambito','sup_servizi','sup_verde','sup_park','sup_privata','sup_cessioni','vol_res','slp_no_res'):
                                                        setattr(obj,field,round(risultato['standard'][field],0)) #assegna dati geometrici
                                                    break
                                        obj.save()
                                else:
                                    raise forms.ValidationError("E' consentito solo il formato dxf")
                                            
                            elif sub_form.cleaned_data['tipo'] == 'PVL': # E' planivolumetrico raster
                                print ("PVL",dir(sub_form.cleaned_data['documento']), file=sys.stderr)
                                print ("FILE",sub_form.instance.documento.storage, sub_form.instance.documento.name, file=sys.stderr)
                                nome_file, estensione_file = os.path.splitext(sub_form.cleaned_data['documento'].name)
                                if not estensione_file.lower() in ('.jpg', '.png', '.pdf'):
                                    raise forms.ValidationError("Sono consentiti solo i formati pdf, jpg e png")

                                file_path = os.path.join(settings.MEDIA_ROOT,sub_form.instance.documento.name)

                                if estensione_file.lower() == '.pdf':
                                    pages = convert_from_path(file_path, 200)
                                    file_path = file_path+'.png'
                                    pages[0].save(file_path,'PNG')


                                #parsed_url = urlparse(request.build_absolute_uri())
                                #host_complete_url = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_url)
                                #schema0 = 'https://' if request.is_secure else 'http://'
                                schema0 = 'https://' if "rapper" in request.META['HTTP_HOST'] else 'http://'
                                schema = request.scheme+'://'
                                host_complete_url = schema0+request.META['HTTP_HOST']
                                #pua_dataset = datasets.objects.get(name='PUA')
                                url = host_complete_url + '/warp/newimage_direct/%d/' % warp_dataset.pk
                                print ("URI",host_complete_url, url, schema0, schema)
                                files = {'sorgente': open(file_path, 'rb')} # #sub_form.cleaned_data['documento']
                                values = {
                                    'titolo': "%s_%s" % (obj.id_pua,sub_form.instance.uuid[:8]),
                                    'note': json.dumps({"id":obj.pk, "denominazione":obj.denominazione, "ditta":obj.ditta, "allegato": sub_form.instance.uuid}),
                                    'output': 'json',
                                }
                                print ("CONNECTION WARP1",files,values)
                                print ("CONNECTION WARP2",json.dumps(values))
                                print ("request",request.META. get('HTTP_AUTHORIZATION'))
                                r = requests.post(url, files=files, data=values, verify=False)
                                print (r.status_code,r.text, file=sys.stderr)
                                if r.status_code == 200:
                                    warp_rif = rasterMaps.objects.get(pk=r.json()["id"])
                                    sub_form.instance.rif_warp = warp_rif #r.json()["warp_id"]
                                    sub_form.instance.save()
                                    obj.planivol_raster = "/warp/imgset/%d/" % warp_rif.pk
                                    obj.save()
                            
                            #elif sub_form.cleaned_data['tipo'] == 'CON': # E' convenzione
                            #    obj.convenzione = sub_form.instance.documento.name
                            #    obj.save()


                    elif model_name == 'info':
                    
                        if sub_form.cleaned_data['cat'] == 'AMM': #è un atto amminstrativo quindi bisogna registrare i parametri
                            if sub_form.cleaned_data['stato'] and obj.stato != sub_form.cleaned_data['stato']:
                                obj.stato = sub_form.cleaned_data['stato']
                                obj.save()
                        elif sub_form.cleaned_data['cat'] == 'DOC': # se è documentazione di piano quello che è inserito come rif_web va come riferimento ad elaborati
                            if sub_form.cleaned_data['rif_web']:
                                obj.elaborati = sub_form.cleaned_data['rif_web']
                                obj.save()


                    print('FORM %s --->' % n, file=sys.stderr)
                    if 'documento' in form.cleaned_data.keys():
                        print(form.cleaned_data['documento'].name, file=sys.stderr)
                        print(dir(form.cleaned_data['documento']), file=sys.stderr)
                    print('<--- FORM %s' % n, file=sys.stderr)
            print('<--- FORMSET %s' % n, file=sys.stderr)

        #inserimento della geometria da un testo wkt inserito in apposito campoS
        wkt_geom = form.cleaned_data.get('wkt_geom', None)
        if wkt_geom: #!= ''
            try:
                print ('SET GEOM TO WKT')
                new_geom = GEOSGeometry(wkt_geom, srid=3003)
                if isinstance(new_geom, Polygon):
                    new_geom = MultiPolygon(new_geom)
                print ("obj.the_geom = new_geom - 930", file=sys.stderr)
                obj.the_geom = new_geom
                obj.save()
            except Exception as e:
                print(e, file=sys.stderr)
                pass

        #inserimento della geometria da coordinate catastali
        coordinate_catastali = form.cleaned_data.get('coordinate_catastali', None)
        if coordinate_catastali and coordinate_catastali != '':
            wkt_geom, cc_feedback = GeomFromCoordinateCatastali(coordinate_catastali, check=True)
            
            print ("obj.the_geom = GEOSGeometry(wkt_geom, srid=3003) - 943", file=sys.stderr)
            obj.the_geom = GEOSGeometry(wkt_geom, srid=3003)
            obj.coordinate_catastali = cc_feedback
            obj.save()

        print('<--- post save_related puaAdmin', file=sys.stderr)

    def get_form(self, request, obj=None, *args, **kwargs):
        form = super(puaAdmin, self).get_form(request, *args, **kwargs)
        #print( "GETFORM", obj, obj.anno_stato)
        if obj and obj.anno_estratto and obj.anno_stato != obj.anno_estratto:
            obj.anno_stato = int(obj.anno_estratto)
            obj.save()
            
        return form

admin.site.register(files_allegati)
#admin.site.register(User)
#admin.site.register(Group)

@admin.register(LogEntry)
class logEntryAdmin(admin.ModelAdmin):
    search_fields = ['user__username','content_type__model','object_repr']
    list_display = ('object_repr','action_time','content_type','user')
    list_filter = ('user', 'action_flag','content_type')