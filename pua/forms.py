from django import forms
from django.forms import ModelForm
from django.core.validators import RegexValidator
from bootstrap_datepicker_plus.widgets import DateTimePickerInput

from .models import pua,info,provvedimento

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


class UploadDXFForm(forms.Form):
    file_dxf = forms.FileField()

class PUAForm(ModelForm):
    class Meta:
        model = pua
        '''
        _widgets = {
            'id_pua': forms.TextInput(attrs={'class': 'form-control'}),
            'ditta': forms.TextInput(attrs={'class': 'form-control'}),
            'tipologia': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_email': forms.TextInput(attrs={'class': 'form-control'}),
            'richiedente_telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'protocollo_data': DateTimePickerInput(), 
            'protocollo_numero': forms.TextInput(attrs={'class': 'form-control'}),
            'coordinate_catastali': forms.TextInput(attrs={'class': 'form-control'}),
            'note': forms.Textarea(attrs={'class': 'form-control'}),
        }
        '''
        fields = ['id_pua','ditta', 'tipologia', 'the_geom', 'delib_amb', 'delib_appr', 'delib_var', 'det_collaudo', 'vol_res', 'slp_no_res', 'sup_servizi', 'sup_verde', 'sup_park', 'infos']


