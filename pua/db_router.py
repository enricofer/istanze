
__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

class puaRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'pua':
            return 'visio'
        return 'default'

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'pua':
            return 'visio'
        return 'default'
    
    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'pua' and obj2._meta.app_label == 'pua':
            return True
        # Allow if neither is chinook app
        elif 'pua' not in [obj1._meta.app_label, obj2._meta.app_label]: 
            return True
        return False
    
    def allow_migrate(self, db, app_label, model_name=None, **hints):
        return True