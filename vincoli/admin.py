from django.contrib.gis import admin
from django.contrib.gis import forms
from django.utils.html import format_html
from django.http import FileResponse, HttpResponse
from django.shortcuts import render
from django.contrib.gis.geos import GEOSGeometry, MultiPolygon, Polygon
from django.conf import settings
from django.contrib.admin import helpers
from dal import autocomplete

from certificati.coordinateCatastali import GeomFromCoordinateCatastali
from certificati.views import ubicazione, riferimenti_incrociati
from .models import vincoli, mibac, riflegis_choice
from .views import JsonFromQueryset

from shapely.geometry import mapping
from shapely import wkt
from fiona import collection
from fiona.crs import from_epsg
from io import BytesIO
from zipfile import ZipFile
from datetime import datetime

import shutil
import uuid
import os
import sys
import json

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

def shapefileFromQueryset(dataset, schede = False):
    """
    Returns format representation for given dataset.
    """
    attribs = {
        'FID': 'int',
        'ID_VINCOLO': 'str:12',
        'COD_ISTAT': 'str:6',
        'TIPOVINC': 'str:2',
        'N_AREAV': 'str:4',
        'DENOM': 'str:150',
        'RIFLEGIS': 'str:100',
        'DETTVINC': 'int',
        'CODICESCHE': 'str:50',
        'LINK': 'str:50',
        'SHAPE_LENG': 'float:15.2',
        'ARTICOLO': 'str:50',
        'RIFERIMENTO': 'str:50',
        'SHAPE_AREA': 'float:15.2'
    }

    schema = {'geometry': 'MultiPolygon', 'properties': attribs}
    #TODO: it would be much better to save the artifacts to an in memory buffer
    if schede == "cartaweb":
        export_dir = os.path.join(settings.MEDIA_ROOT,'vincoli_cartaweb')
    else:
        export_dir = os.path.join(settings.MEDIA_ROOT,'vincoli','tmp',str(uuid.uuid1()))
    print (settings.BASE_DEV, export_dir)
    #temp_dir_schede = os.path.join(temp_dir,'schede')
    if not os.path.exists(export_dir):
        os.makedirs(export_dir)
    temp_shp = os.path.join(export_dir,"vincoli_aggiornamento.shp")
    with collection(temp_shp, "w", "ESRI Shapefile", schema, crs=from_epsg(3003), encoding='utf-8') as output:

        for thing in dataset:

                data = dict()
                data['FID'] = thing.id_vincolo
                data['COD_ISTAT'] = '028060'
                data['TIPOVINC'] = '0'+str(thing.dettvinc)
                data['N_AREAV'] = "0000"[:4 - len(str(thing.id_vincolo))]+str(thing.id_vincolo)
                data['ID_VINCOLO'] = data['COD_ISTAT']+data['TIPOVINC']+data['N_AREAV']
                data['DENOM'] = thing.denominazione
                data['RIFLEGIS'] = dict(riflegis_choice)[thing.riflegis]
                data['DETTVINC'] = thing.dettvinc
                data['CODICESCHE'] = "S"+data['N_AREAV']
                data['LINK'] = data['CODICESCHE']
                data['ARTICOLO'] = ''
                data['RIFERIMENTO'] = thing.rifamm
                data['SHAPE_LENG'] = thing.geom.length
                data['SHAPE_AREA'] = thing.geom.area

                the_geom = wkt.loads(thing.geom.wkt)
                output.write({'properties': data, 'geometry': mapping(the_geom)})

                if schede:
                    filename, ext = os.path.splitext(thing.scheda.path)
                    shutil.copy(thing.scheda.path,os.path.join(export_dir,data['CODICESCHE']+ext))

                print (thing.scheda, thing.scheda.path, file=sys.stderr)

    if schede != "cartaweb":
        zipMemory = BytesIO()
        zip = ZipFile(zipMemory, 'w')

        for filename in os.listdir(export_dir):
            zip.write(os.path.join(export_dir,filename),filename)
            os.remove(os.path.join(export_dir,filename))

        os.rmdir(export_dir)

        # fix for Linux zip files read in Windows
        for filename in zip.filelist:
            filename.create_system = 0

        zip.close()
        zipMemory.seek(0)
        return zipMemory

def mappa_vincoli(modeladmin, request, queryset):
    return render(request, 'vincoli_mappa.html', {'features':json.dumps(JsonFromQueryset(queryset))})

def mappa_vincoli_tutto(modeladmin, request, queryset):
    return render(request, 'vincoli_mappa.html', {'features':json.dumps(JsonFromQueryset(queryset))})

def export_shp(modeladmin, request, queryset):
    return FileResponse(shapefileFromQueryset(queryset),content_type='application/zip')

def export_shp_tutto(modeladmin, request, queryset):
    shapefileFromQueryset(vincoli.objects.all().exclude(stato__in=(5,6)).exclude(dettvinc=5),schede="cartaweb")
    print (len(vincoli.objects.all()))
    print (len(vincoli.objects.exclude(stato__in=(5,6))))
    print (len(vincoli.objects.exclude(dettvinc=5)))

def export_shp_schede(modeladmin, request, queryset):
    return FileResponse(shapefileFromQueryset(queryset,schede=True),content_type='application/zip')

export_shp.short_description = "Esporta come shapefile i vincoli selezionati"
export_shp_tutto.short_description = "Esporta tutti i vincoli per cartaweb"
export_shp_tutto.acts_on_all = True
export_shp_schede.short_description = "Esporta come shapefile i vincoli selezionati con schede"
mappa_vincoli.short_description = "Riproduci in mappa i vincoli selezionati"
mappa_vincoli_tutto.short_description = "Riproduci in mappa tutti i vincoli"
mappa_vincoli_tutto.acts_on_all = True

class vincoliForm(forms.ModelForm):

    catasto = forms.CharField(required=False, widget=forms.Textarea( attrs={'rows':'1', 'cols': '10'}))
    wkt_geom = forms.CharField(required=False, widget=forms.Textarea( attrs={'rows':'2', 'cols': '10'}))
    rifmibac = forms.ModelChoiceField(required=False, queryset=mibac.objects.all(),
        widget=autocomplete.ModelSelect2(url='client-autocomplete')
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs and kwargs['instance']:  
            #print ("FORM",args, self.fields,file=sys.stderr)
            wkt_geom_txt = kwargs['instance'].geom.wkt
            self.fields['wkt_geom'].widget.attrs.update({'placeholder': wkt_geom_txt})
            rifcatasto_txt = kwargs['instance'].rifcatasto
            self.fields['catasto'].widget.attrs.update({'placeholder': rifcatasto_txt})

    def clean(self):
        data = self.cleaned_data        
        wkt_geom = self.cleaned_data.get('wkt_geom', None)
        if wkt_geom != '':
            try:
                geom = GEOSGeometry(wkt_geom, srid=3003)
            except:
                raise forms.ValidationError("il formato della geometria non è valido")

        return data

    class Meta(object):
        model = vincoli
        exclude = ('', )

class vincoliAdmin(admin.OSMGeoAdmin):#admin.OSMGeoAdmin):

    change_form_template = 'admin/vincoli/change_form.html'
    list_per_page = 10
    default_lon = 1725155
    default_lat = 5032083
    default_zoom = 13
    max_resolution = 350000
    num_zoom = 28
    max_zoom = 28
    min_zoom = 10
    map_width = 750
    map_height = 500
    map_srid = 3003
    wms_url = "https://rapper.comune.padova.it/qgisserver?MAP=/usr/lib/cgi-bin/servizi/default.qgs"
    wms_layer = 'PI,PI_CS'
    wms_name = 'PI'
    wms_options = {'transparent':"true",'format': 'image/png'}
    map_template = 'admin/vincoli/openlayers_extralayers.html'
    modifiable = True
    form = vincoliForm
    readonly_fields = (
        'id_vincolo', 'rifscheda', 'corrispondenzamibac',
        'distmibac', 'wkt_desc','link_inquadramento_mibac',
        'link_streetview', 'link_clipboard_catasto', 'link_clipboard_wkt',
        'link_esterno_mibac', 'link_interno_mibac', 'link_riferimenti_incrociati',
        'mappa_sola_lettura', 'trigger_nascondi_geom', 'geom_edit_button', 'geom_disable_edit_button',
    )
    list_display = ('id_vincolo','denominazione','descrizione','riflegis','dettvinc', "scheda", 'rifscheda', "rifmibac")
    search_fields = ['id_vincolo','denominazione','descrizione','note','rifscheda']
    list_filter = ('riflegis', 'dettvinc','stato')
    ordering = ['-id_vincolo','denominazione']
    actions = [mappa_vincoli, mappa_vincoli_tutto, export_shp, export_shp_schede, export_shp_tutto]

    class Media:
        js = (

            "certificati/js/salva_stato_layers.js",
            #'repertorio/mappeDiBase.js',
            #'pua/pua_finestramappa.js',
        )
        css = {
             'all': ('pua/pua_finestramappa.css',)
        }


    def get_fieldsets(self, request, obj=None):
        #if obj: # editing an existing object

        if request.GET.get('force_edit', '') or not obj:
            loc_mod_fields1 = ("localizzazione", {
                'classes': ('grp-collapse grp-open',),
                'fields': (('geom','link_inquadramento_mibac'), ('catasto', 'link_clipboard_catasto'), ('wkt_geom', 'link_clipboard_wkt'), 'geom_disable_edit_button')#,'ditta',
            })
            loc_mod_fields2 = ("localizzazione_mod", {
                'classes': ('grp-collapse grp-open',),
                'fields': ('trigger_nascondi_geom',)
            })

        else:
            loc_mod_fields1 = ("localizzazione", {
                'classes': ('grp-collapse grp-open',),
                'fields': (('link_inquadramento_mibac', 'link_streetview'),'mappa_sola_lettura','geom_edit_button')#,'ditta',
            })
            loc_mod_fields2 = ("localizzazione_mod", {
                'classes': ('posizione-nascosta grp-collapse grp-open',),
                'fields': ('geom','trigger_nascondi_geom')
            })

        return (
            ("intestazione", {
                'classes': ('grp-collapse grp-open',),
                'fields': ('id_vincolo', 'denominazione','stato','riflegis','rifamm','dettvinc','rifscheda','scheda')#,'ditta',
            }),
            loc_mod_fields1,
            loc_mod_fields2,
            ("altre info", {
                'classes': ('grp-collapse grp-open',),
                'fields': ('descrizione', 'note', 'immagine')#,'ditta',
            }),
            ("riferimenti", {
                'classes': ('grp-collapse grp-open',),
                'fields': ('rifscheda', 'rifcatasto68', ('rifmibac', 'corrispondenzamibac', 'distmibac', 'link_esterno_mibac', 'link_interno_mibac') )#,'ditta',
            }),
            ("riferimenti ad altre informazioni su rapper", {
                'classes': ('grp-collapse grp-open',),
                'fields': ('link_riferimenti_incrociati',) #,'ditta',
            }),
        )

    def mappa_sola_lettura(self, obj):
        if obj.pk:
            html= '''<div id="map"></div><script>window.onload = loadFinestraMappa('%s')</script>''' % obj.geom.wkt
            return format_html(html)
        else:
            return ''
    mappa_sola_lettura.short_description = ''

    def trigger_nascondi_geom(self, obj):
        return format_html('''<script>window.onload = hideGeom()</script>''')
    trigger_nascondi_geom.short_description = ''

    def geom_edit_button(self, obj):
        if obj.pk:
            link= '<a href="/admin/vincoli/vincoli/%d/change/?force_edit=true">Abilita la modifica della geometria</a>' % obj.pk
            return format_html(link)
        else:
            return ''
    geom_edit_button.short_description = ''

    def geom_disable_edit_button(self, obj):
        if obj.pk:
            link= '<a href="/admin/vincoli/vincoli/%d/change/?">Disabilita la modifica della geometria</a>' % obj.pk
            return format_html(link)
        else:
            return ''
    geom_disable_edit_button.short_description = ''

    def wkt_desc(self, obj):
        return obj.geom.wkt
    wkt_desc.short_description = 'descrizione wkt della geometria del vincolo'

    def link_riferimenti_incrociati(self,obj):
        if obj.geom:
            return format_html(riferimenti_incrociati(obj.geom.wkt,exclude_self={"vincoli":obj.pk}))
    link_riferimenti_incrociati.short_description = 'Riferimenti ad altre informazioni su rapper'
    link_riferimenti_incrociati.allow_tags = True

    def link_clipboard_wkt(self, obj):
        links = '''<a id="link_button" class="button" onclick="copia_clipboard('%s')">Copia negli appunti</a>'''
        return format_html(links % obj.geom.wkt)
    link_clipboard_wkt.short_description = ''

    def link_clipboard_catasto(self, obj):
        links = '''<a id="link_button" class="button" onclick="copia_clipboard('%s')">Copia negli appunti</a>'''
        return format_html(links % obj.rifcatasto)
    link_clipboard_catasto.short_description = ''

    def link_inquadramento_mibac(self, obj):
        links = '''<a href="/vincoli/mappavincoli/%s/" class="button" target="_blank">Mappa di inquadramento</a>'''
        return format_html(links % obj.id_vincolo)
    link_inquadramento_mibac.short_description = ''

    def link_esterno_mibac(self, obj):
        links = '''<a href="http://vincoliinrete.beniculturali.it/VincoliInRete/vir/bene/dettagliobene%s" class="button" target="_blank">Link esterno MIBAC</a>'''
        if obj.rifmibac:
            return format_html(links % obj.rifmibac.id_mibac)
        else:
            return ""
    link_esterno_mibac.short_description = ''

    def link_interno_mibac(self, obj):
        links = '''<a href="/admin/vincoli/mibac/%s/" class="button" target="_blank">Link interno MIBAC</a>'''
        if obj.rifmibac:
            return format_html(links % obj.rifmibac.id_mibac)
        else:
            return ""
    link_interno_mibac.short_description = ''

    def link_streetview(self, obj):
        links = '''<a href="/vincoli/sv/%s/" class="button" target="_blank">Streetview</a>'''
        return format_html(links % obj.id_vincolo)
    link_streetview.short_description = ''

    def changelist_view(self, request, extra_context=None):
        try:
            action = self.get_actions(request)[request.POST['action']][0]
            action_acts_on_all = action.acts_on_all
        except (KeyError, AttributeError):
            action_acts_on_all = False

        if action_acts_on_all:
            post = request.POST.copy()
            post.setlist(helpers.ACTION_CHECKBOX_NAME,
                        self.model.objects.values_list('id_vincolo', flat=True))
            request.POST = post

        return admin.OSMGeoAdmin.changelist_view(self, request, extra_context)

    def save_model(self, request, obj, form, change):

        if not obj.pk:
            tutti_vincoli = vincoli.objects.all().order_by('id_vincolo')
            primo_seq_libero = None
            for seq,vincolo_esistente in enumerate (tutti_vincoli):
                print ("CHECKID", vincolo_esistente.id_vincolo, seq + 1, file=sys.stderr)
                if vincolo_esistente.id_vincolo != seq + 1:
                    primo_seq_libero = seq + 1
                    break
            free_id = primo_seq_libero or tutti_vincoli.latest('id_vincolo').id_vincolo  + 1
            print ("NEW ID", primo_seq_libero, free_id, file=sys.stderr)
            obj.id_vincolo = free_id

        wkt_geom = form.cleaned_data.get('wkt_geom', None)
        print ("VINCOLI-1",file=sys.stderr)
        print ("obj",obj,obj.geom)
        if wkt_geom != '':
            try:
                new_geom = GEOSGeometry(wkt_geom, srid=3003)
                if isinstance(new_geom, Polygon):
                    new_geom = MultiPolygon(new_geom)
                obj.geom = new_geom
                print ("VINCOLI-1.1",file=sys.stderr)
            except Exception as e:
                print(e, file=sys.stderr)
                pass
        
        print ("VINCOLI-2",file=sys.stderr)
        coordinate_catastali = form.cleaned_data.get('catasto', None)
        print ("coordinate_catastali",coordinate_catastali)
        if coordinate_catastali != '':
            wkt_geom, cc_feedback = GeomFromCoordinateCatastali(coordinate_catastali, check=True)
            obj.geom = GEOSGeometry(wkt_geom, srid=3003)
            obj.rifcatasto = cc_feedback
            print ("VINCOLI-2.2",file=sys.stderr)
        
        print ("VINCOLI-3",file=sys.stderr)
        if not obj.geom:
            #print ("GEOMNULL",obj.geom)
            raise forms.ValidationError("il formato della geometria non è valido")

        print ("VINCOLI-4",file=sys.stderr)

        obj.autore_modifica = request.user.username
        obj.momento_modifica = datetime.now()

        obj.save()

admin.site.register(vincoli, vincoliAdmin)

class vincoliMibac(admin.OSMGeoAdmin):#admin.OSMGeoAdmin):

    change_form_template = 'admin/vincoli-change_form.html'
    default_lon = 1725155
    default_lat = 5032083
    default_zoom = 13
    max_resolution = 350000
    num_zoom = 28
    max_zoom = 28
    min_zoom = 10
    map_width = 700
    map_height = 500
    map_srid = 3003
    wms_url = "https://rapper.comune.padova.it/qgisserver?MAP=/usr/lib/cgi-bin/servizi/default.qgs"
    wms_layer = 'PI,PI_CS'
    wms_name = 'PI'
    wms_options = {'transparent':"true",'format': 'image/png'}
    map_template = 'admin/vincoli/openlayers_extralayers.html'
    modifiable = False
    readonly_fields = ('id_mibac','link_esterno_mibac','link_inquadramento_mibac','denominazione','decreto','data','num_gu','data_gu','localizzazione','indirizzo_completo','tipo_scheda','condizione_giuridica', 'presenza_vincoli', 'presente_in', 'codice_catalogo', 'keycode_sigec', 'id_benitutelati', 'id_cartarischio', 'anteprima', 'tipo_bene','immagine' )
    fieldsets = (
        ("intestazione", {
            'classes': ('grp-collapse grp-open',),
            'fields': (('id_mibac','link_esterno_mibac'), 'denominazione')#,'ditta',
        }),   
        ("localizzazione", {
            'classes': ('grp-collapse grp-open',),
            'fields': (('geom','link_inquadramento_mibac'), 'localizzazione',)#,'ditta',
        }),
        ("scheda", {
            'classes': ('grp-collapse grp-open',),
            'fields': (('tipo_bene', 'tipo_scheda','condizione_giuridica', 'presenza_vincoli'), ('presente_in', 'codice_catalogo', 'keycode_sigec', 'id_benitutelati', 'id_cartarischio'),)#,'ditta',
        }),
        ("decreto", {
            'classes': ('grp-collapse grp-open',),
            'fields': (('decreto','data','num_gu','data_gu'),)#,'ditta',
        }),
        ("altro", {
            'classes': ('grp-collapse grp-open',),
            'fields': ('immagine',)#,'ditta',
        }),
    )
    list_display = ('id_mibac','denominazione','decreto','data','localizzazione',)
    search_fields = ['id_mibac','denominazione','localizzazione']
    list_filter = ('decreto','presenza_vincoli','tipo_bene','tipo_scheda','condizione_giuridica')
    ordering = ['-id_mibac','denominazione']

    def link_esterno_mibac(self, obj):
        links = '''<a href="http://vincoliinrete.beniculturali.it/VincoliInRete/vir/bene/dettagliobene%s" class="button" target="_blank">Link esterno MIBAC</a>'''
        return format_html(links % obj.id_mibac)
    link_esterno_mibac.short_description = ''

    def link_inquadramento_mibac(self, obj):
        links = '''<a href="/vincoli/mappamibac/%s/" class="button" target="_blank">Mappa di inquadramento</a>'''
        return format_html(links % obj.id_mibac)
    link_inquadramento_mibac.short_description = ''

    def immagine(self,obj):
        if obj.anteprima:
            html= '<img src="%s"/>' % obj.anteprima
        else:
            html = '-'
        return format_html(html)


admin.site.register(mibac, vincoliMibac)