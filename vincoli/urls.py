from django.conf.urls import url,include
from . import views

from django.conf import settings
import django.contrib.auth.views

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

urlpatterns = [
    url(r'^copiaschede/$', views.copia_schede, name='copia_schede'),
    url(r'^mappavincoli/(\d+)/$', views.mappa_vincoli, name='mappa_vincoli'),
    url(r'^mappamibac/(\d+)/$', views.mappa_mibac, name='mappa_mibac'),
    url(r'^sv/(\d+)/$', views.streetview_link, name='streetview_link'),
    url(r'^geojson/$', views.as_geojson, name='as_geojson'),
    url(r'^mibac_geojson/$', views.mibac_as_geojson, name='mibac_as_geojson'),
    url(r'^client-autocomplete/$', views.ClientAutocomplete.as_view(), name='client-autocomplete',),
    url(r'^login/', django.contrib.auth.views.LoginView),
    url(r'^logout/', django.contrib.auth.views.LogoutView),
]