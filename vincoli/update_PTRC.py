from vincoli.models import vincoli
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry, MultiPolygon, Polygon

import os
import csv
from datetime import datetime

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

TABELLA_SORGENTE = '/home/urb/pub/PTRC/segnalazioni_PTRC.csv'

input_file = csv.DictReader(open(TABELLA_SORGENTE,'r'), delimiter =';')

prog = 10000

for row in input_file:
    vincolo = vincoli()
    vincolo.id_vincolo = prog
    vincolo.denominazione = row["DENOM"].upper()
    vincolo.descrizione = row["PROGETTO"] or '' + " " + row["PERIODO"] or ''
    vincolo.stato = 7
    vincolo.riflegis = 10
    nome_scheda = row["N_Amb900"][1:]
    if row["sub_Amb"] != '00':
        nome_scheda += '_' + row["sub_Amb"]
    vincolo.rifamm = nome_scheda.replace("_", ".")
    vincolo.note = "Ambito Atlante 900: %s\n\nSub Ambito: %s\n\nID segnalazione: %s" % (row["N_Amb900"], row["sub_Amb"], nome_scheda)
    vincolo.scheda = os.path.join('schede_PTRC', "%s.pdf" % nome_scheda)
    vincolo.geom = GEOSGeometry(row["WKT"])
    vincolo.dettvinc = 5
    vincolo.autore_modifica = 'efadmin'
    vincolo.save()
    prog += 1
    print (vincolo)