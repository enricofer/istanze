
from django.contrib.gis.db import models
from django.contrib.gis.geos import Polygon
from joinfield.joinfield import JoinField

import os

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Create your models here.

dettvinc_choice = (
    (1,"1  vincolo diretto"),
    (2,"2  villa veneta"),
    (3,"3  vincolo indiretto"),
    (4,"4  vincolo paesaggistico"),
    (5,"5  segnalazione PTRC"),
    (5,"6  scheda L.R.24/85"),
)

stato_choice = (
    (1,"1  notificato"),
    (2,"2  dichiarazione di interesse"),
    (3,"3  vincolo in istruttoria"),
    (4,"4  vincolo revocato"),
    (5,"5  vincolo sospeso"),
    (6,"6  vincolo sospeso"),
    (7,"7  segnalato"),
)

riflegis_choice = (
    (1,"1   364/1909"),
    (2,"2   1089/1939"),
    (3,"3   1497/1939"),
    (4,"4   490/1999 Art. 2"),
    (5,"5   490/1999 Art. 10"),
    (6,"6   490/1999 Art. 49"),
    (7,"7   42/2004 Art. 10"),
    (8,"8   42/2004 Art. 12"),
    (9,"9   42/2004 Art. 10/13"),
    (10,"10 D.G.R 62/2020"),
    (11,"11 L.R.24/85 - Art. 11"),
)

class mibac(models.Model):

    id_mibac = models.IntegerField(primary_key=True)
    denominazione = models.CharField(max_length=254)
    decreto = models.CharField(max_length=50) 
    data = models.CharField(max_length=50, blank=True, null=True) 
    num_gu = models.IntegerField(blank=True, null=True) 
    data_gu = models.CharField(max_length=50, blank=True, null=True) 
    localizzazione = models.CharField(max_length=254)
    #nome_via = models.CharField(max_length=100, blank=True, null=True) 
    #wkt = models.CharField(max_length=50, blank=True, null=True) 
    geom = models.PointField(srid=3003, blank=True, null=True)
    tipo_scheda = models.CharField(max_length=50, blank=True, null=True) 
    condizione_giuridica = models.CharField(max_length=50, blank=True, null=True) 
    presenza_vincoli = models.CharField(max_length=50, blank=True, null=True) 
    presente_in = models.CharField(max_length=50, blank=True, null=True) 
    codice_catalogo = models.CharField(max_length=50, blank=True, null=True) 
    keycode_sigec = models.CharField(max_length=50, blank=True, null=True) 
    id_benitutelati = models.CharField(max_length=50, blank=True, null=True) 
    id_cartarischio = models.CharField(max_length=50, blank=True, null=True) 
    anteprima = models.CharField(max_length=150, blank=True, null=True) 
    tipo_bene = models.CharField(max_length=50, blank=True, null=True) 
    indirizzo_completo = models.CharField(max_length=150, blank=True, null=True) 


    class Meta:
        managed = False
        db_table = 'vincoli_mibac_lista_beni'
        verbose_name_plural = "Lista beni censiti al mibac al 23/7/2019"
        verbose_name = "Lista beni mibac"

    def __str__(self):
        return str(self.id_mibac) + '-' + self.denominazione.replace(' ','_')

class vincoli(models.Model):

    def update_filename(instance, filename):
        path = "vincoli/%s" % instance.id_vincolo
        return os.path.join(path, filename)

    def update_imgname(instance, filename):
        path = "vincoli/%s" % instance.id_vincolo
        return os.path.join(path, filename)

    #id = models.IntegerField(primary_key=True, db_column='pk', default='99999999')
    id_vincolo = models.IntegerField(primary_key=True,)
    denominazione = models.CharField(max_length=254)
    descrizione = models.TextField(blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    stato = models.IntegerField(choices=stato_choice)
    riflegis = models.IntegerField(choices=riflegis_choice)
    rifamm = models.CharField(max_length=254, blank=True, null=True)
    immagine = models.ImageField(upload_to=update_imgname, blank=True, null=True)
    scheda = models.FileField(upload_to=update_filename)
    geom = models.MultiPolygonField(srid=3003, blank=True, null=True)
    rifcatasto68 = models.CharField(max_length=254, blank=True, null=True)
    rifcatasto = models.CharField(max_length=254, blank=True, null=True)
    rifmibac =  JoinField(mibac, to_field='id_mibac', blank=True, null=True, on_delete=models.SET_NULL)
    corrispondenzamibac = models.IntegerField(verbose_name=u"Corrispondenza tra denominazioni", blank=True, null=True)
    distmibac = models.FloatField(verbose_name=u"Distanza da geolocalizzazione indirizzo", blank=True, null=True)
    #models.IntegerField(blank=True, null=True) 
    tipovinc = models.IntegerField(blank=True, null=True) 
    dettvinc = models.IntegerField(choices=dettvinc_choice) 
    momento_inserimento = models.DateTimeField(auto_now_add=True)
    momento_modifica = models.DateTimeField(auto_now_add=True)
    autore_modifica = models.CharField(max_length=50) 
    rifscheda = models.CharField(verbose_name=u"Vecchio riferimento", max_length=50, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Vincoli su beni architettonici e paesaggistici"
        verbose_name = "Vincolo su beni architettonici e paesaggistici"

    def publish(self):
        self.save()

    def __str__(self):
        return str(self.id_vincolo) + '-' + self.denominazione.replace(' ','_')