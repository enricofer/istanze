from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.gis.geos import GEOSGeometry, LineString, Point
from django.db.models import Q
from dal import autocomplete

from .models import vincoli,mibac
from certificati.coordinateCatastali import SV

import os
import json
import sys

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Create your views here.

def copia_schede(request):
    SOURCE_BASE = '/home/urb/pub/schede_vincoli/'
    TARGET_BASE = '/home/urb/Dropbox/dev/istanze/documenti/'
    vincoli_tutti = vincoli.objects.all()
    vincoli_processati = []
    for vincolo in vincoli_tutti:
        if not vincolo.rifscheda:
            #print("SORGENTE NULLA!",vincolo)
            vincoli_processati.append({"id":vincolo.pk,"vincolo":str(vincolo),"esito":"SORGENTE NULLA"})  
            continue
        source_vincolo = os.path.join(SOURCE_BASE,vincolo.rifscheda+".pdf")
        target_dir = os.path.join(TARGET_BASE,'vincoli',str(vincolo.id_vincolo))
        target_vincolo = os.path.join(TARGET_BASE,vincolo.scheda.name)
        if os.path.exists(source_vincolo):
            #print("DIR",source_vincolo,target_dir,target_vincolo)
            if not os.path.exists(target_dir):
                os.makedirs(target_dir)
            if not os.path.exists(target_vincolo):
                #os.remove(target_vincolo)
                vincoli_processati.append({"id":vincolo.id_vincolo,"vincolo":str(vincolo),"esito":"SCHEDA INESISTENTE: "+vincolo.rifscheda})
            #os.rename(source_vincolo,target_vincolo)
            #print("OK!",vincolo)
            #vincoli_processati.append({"id":vincolo.pk,"vincolo":str(vincolo),"esito":"OK"})
        else:
            #vincoli_processati.append({"id":vincolo.pk,"vincolo":str(vincolo),"esito":"NON ESISTENTE: "+vincolo.rifscheda})
            print("\nSORGENTE DEL VINCOLO %s NON ESISTENTE! %s" %(str(vincolo),vincolo.rifscheda+".pdf") )
    return JsonResponse({"risultato":vincoli_processati})


def JsonFromQueryset(features):
    #features_geojson = serialize('geojson', features,geometry_field='geom',srid=3003, fields=('idIstanza','ditta',))
    features_geojson = {
        "type": "FeatureCollection",
        "crs": {"type": "name", "properties": {"name": "EPSG:3003"}},
        "features": []
    }
    
    #printf = False
    for feature in features:
        #print (feature.__dict__, file=sys.stderr)
        if feature.geom:
            geometry = json.loads(feature.geom.json)
            properties = {}
            for key,value in feature.__dict__.items():
                if type(value) in (str,int,float):
                    properties[key]=value
                #else:
                #    if not printf:
                #        print ("key",key,type(value),file=sys.stderr)
            feature = {
                "type": "Feature",
                "geometry": geometry,
                "properties": properties,
            }
            #printf = True
            features_geojson["features"].append(feature)
    return features_geojson

class ClientAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return User.objects.none()

        qs = mibac.objects.all()

        if self.q:
            qs = qs.filter(Q(denominazione__icontains=self.q) | Q(id_mibac__istartswith=self.q))

        return qs

def mappa_vincoli(request, vincolo_id):
    queryset = vincoli.objects.all()
    vincolo  = vincoli.objects.get(id_vincolo=vincolo_id)
    ext = vincolo.geom.buffer(20).extent
    return render(request, 'vincoli_mappa.html', {'id_vincolo': vincolo_id, 'extent':ext,'features':json.dumps(JsonFromQueryset(queryset))})

def mappa_mibac(request, id_mibac):
    queryset = vincoli.objects.all()
    beneMibac  = mibac.objects.get(id_mibac=id_mibac)
    ext = beneMibac.geom.buffer(75).extent
    print ("coords",beneMibac.geom.coords,beneMibac.geom.wkt,file=sys.stderr)
    return render(request, 'vincoli_mappa.html', {'mibac':beneMibac.geom.coords, 'extent':ext,'features':json.dumps(JsonFromQueryset(queryset))})

def mibac_as_geojson(request):
    json = JsonFromQueryset(mibac.objects.all())
    return JsonResponse(json)

def as_geojson(request):
    json = JsonFromQueryset(vincoli.objects.all())
    return JsonResponse(json)

def streetview_link(request,vincolo_id=None):
    vincolo  = vincoli.objects.get(id_vincolo=vincolo_id)
    return SV(vincolo.geom.centroid)