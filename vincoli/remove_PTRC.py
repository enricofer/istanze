from vincoli.models import vincoli

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

schede_PTRC = vincoli.objects.filter(riflegis=10)
for scheda in schede_PTRC:
    print ("Deleting " + str(scheda))
    scheda.delete()