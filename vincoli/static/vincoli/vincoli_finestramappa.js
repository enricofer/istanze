console.log('FINESTRAMAPPA LOADING')

var targetExtent = [1716014, 5023919, 1737137, 5038662];

var map_glob, getfeature_popup;
var polyObject, zoningObject //polyJSON, zoningJSON, 
var polyFeatures, polyOverlay, zoningFeatures, zoningOverlay;

var targetProjection = new ol.proj.Projection({
    code: 'EPSG:3003',
    // The extent is used to determine zoom level 0. Recommended values for a
    // projection's validity extent can be found at http://epsg.io/.
    extent: targetExtent,
    units: 'm'
});
ol.proj.addProjection(targetProjection);

var UTM32Extent = [719459.961,5024860.053, 733523.921,5038269.875];
proj4.defs('EPSG:32632', '+proj=utm +zone=32 +datum=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
var UTM32proj = new ol.proj.Projection({
    code: 'EPSG:32632',
    extent: UTM32Extent
});
ol.proj.addProjection(UTM32proj);

var ETRS89Extent = [719459.961,5024860.053, 733523.921,5038269.875];
proj4.defs('EPSG:25832', '+proj=utm +zone=32 +ellps=GRS80 +towgs84=128.8,17.85,0,0,0,0,0 +units=m +no_defs');
var ETRS89proj = new ol.proj.Projection({
    code: 'EPSG:25832',
    extent: ETRS89Extent
});
ol.proj.addProjection(ETRS89proj);

var geoJson_convertitore = new ol.format.GeoJSON();

var WKT_convertitore = new ol.format.WKT();

/**
 * @param {number} n The max number of characters to keep.
 * @return {string} Truncated string.
 */
String.prototype.trunc = String.prototype.trunc ||
    function(n) {
    return this.length > n ? this.substr(0, n - 1) + '...' : this.substr(0);
    };

function copia_clipboard (txt) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(txt).select();
    document.execCommand("copy");
    $temp.remove();
    alert("Testo copiato negli appunti:\n"+txt)
}

function hideGeom( ) {
    console.log("hideGeom è ORA")
    $( "h2:contains('localizzazione_mod')" ).parent().hide()
}

window.app = {};
var app = window.app;

//
// Define custom control.
//

/**
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 */
app.SVControl = function(opt_options) {

    var options = opt_options || {};

    var button = document.createElement('button');
    button.innerHTML = 'SV';

    var this_ = this;
    var handleSVdirection = function() {
        // this_.getMap().getView().setRotation(0);
        console.log("SV")
    };

    button.addEventListener('click', handleSVdirection);

    var element = document.createElement('div');
    element.className = 'streetview ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};
ol.inherits(app.SVControl, ol.control.Control);


function loadFinestraMappa( polyJSON ) { //zoningJSON, WMSRif

    console.log(polyJSON)

    console.log("ONLOAD è ORA")
    $( "h2:contains('posizione_mod')" ).parent().hide()
    
    var maxResolution = 10;

    function getText(feature, resolution) {
        var text;
        if (resolution > maxResolution) {
            text = '';
        } else {
            text = feature.get('id_pua').toString()+"\n"+feature.get('ditta').trunc(20);;
        }
        return text;
    }

    function polygonStyleFunction(feature, resolution) {
        console.log(feature.get('idIstanza'));
        console.log(resolution);
        return new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#ff0000',
                width: 3
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.3)'
            }),
        });
    }

    var layerSet = [
        mappeDiBase_supporto,
        mappeDiBase_PAT,
        mappeDiBase_PI,
        mappeDiBase_altro
    ]

    //particelle overlay
    if (polyJSON) {
        polyFeatures = new ol.Collection(new ol.format.WKT().readFeatures(polyJSON));

        polyOverlay = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: polyFeatures
            }),
            title: 'perimetro relativo alla richiesta',
            style: polygonStyleFunction
        });
    
        console.log("polyOverlay",polyOverlay.getSource().getExtent());

        layerSet.push(polyOverlay)
    } 

    map_glob = new ol.Map({
        layers: layerSet,
        controls: ol.control.defaults({
            attributionOptions: {
              collapsible: false
            }
          }).extend([
            //new ol.control.ScaleLine(),
            new ol.control.MousePosition(),
            new ol.control.OverviewMap(),
            new app.SVControl()
        ]),
        target: 'map',
        view: new ol.View({
            projection: targetProjection,
            center: ol.extent.getCenter(targetExtent),
            zoom: 2
        })
    });

    var layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Legenda' // Optional label for button
    });

    map_glob.getView().on('propertychange', function(e) {
        switch (e.key) {
            case 'resolution':
            console.log('resolution');
            console.log(e.oldValue);
            break;
        }
    });

    map_glob.addControl(layerSwitcher);

    extent = polyOverlay.getSource().getExtent();
    map_glob.getView().fit(extent, map_glob.getSize());
    
    enable_layer_state_tracking(map_glob)
}