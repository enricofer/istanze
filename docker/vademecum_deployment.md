# Deployment del server mediawiki

I dati persistenti relativi al dbms si tovano nella seguente cartella:

```
/dati/mediawiki
```

mentre i dati persistenti degli uploads al server mediawiki si trovano nella seguente cartella:

```
/dati/mediawiki_uploads
```

le necessarie immagini di docker sono conservate nell'hub di docker per mezzo del seguente utente:

```
login: ferregutie
password: zeto4sil
```

#### installazione portale docker

per installare portainer per gestire i container con interfaccia web: https://www.portainer.io/

```
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
```

#### scaricamento immagini da docker

```
docker pull ferregutie/mediawiki:latest 
docker pull ferregutie/mariadb:latest
```

#### installazione di estensioni nell'immagine personalizzata di mediawiki

nb accorgimenti da tenere per la compizaione del Dockerfile: 

- L'ordine è importante. 
- ad ogni riga di run corrisponde un layer. tutte le righe precedenti ad una modifica vengono conservate come layer, mentre dalla riga modificata in poi vengono ricalcolati i layer modificati. 
- Raggruppare agli apt-get install con gli apt-get update per evitare di installare pacchetti non aggiornati
- Raggruppare per quanto possibile i comandi che si riferiscono ad una sola modifica in una sola riga

```
docker build -t ferregutie/mediawiki ./urbmediawiki
```

#### salvataggio immagini nel repository docker

```
docker login <con le credenziali precedenti>
docker tag ferregutie/mediawiki:latest ferregutie/mediawiki:deploy_v[n]
docker push ferregutie/mediawiki:deploy_v[n]
```

#### definizione pool di esecuzione swarm

```
docker swarm init --default-addr-pool 172.171.0.0/16
```

#### deployment di mediawiki e database mariadb su swarm

```
docker stack deploy -c docker-compose.yml mediawiki
```

#### lista dei container in esecuzione

```
docker ps
```

#### per fermare l'esecuzione di un container

```
docker stop <container>
```

#### shell di comando dentro al container in funzione

```
docker exec -it <container>/bin/bash
```

#### ispezione dell'indirizzo di IP esterno container

```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <container>
```

#### ispezione dei log del container

```
docker log <container>
```

#### configurazione di mediawiki

editare il file `LocalSettings_latest.php` anche con il container in esecuzione

#### configurazione di LDAP

editare il file `ldapprovider.json`

*NB: Il mapping dei gruppi non funziona*

#### resettare network o servizi swarm

```
docker network prune

#per individuare il servizio che blocca un network
docker service ls

docker service rm <service>
```

