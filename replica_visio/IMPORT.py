import os
import sys

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

#psql = '"C:\\Program Files (x86)\\pgAdmin III\\1.22\\psql.exe"'
psql = 'C:\\OSGeo4W\\bin\\psql.exe'
shp2pgsql = '"C:\\Program Files\\shp2pgsql\\shp2pgsql.exe"'
host = '10.10.21.50'

def sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, layers, base_dir, refresh_materialized_view=None):
    #riclassificazione dei layers in mapping per unificare lo schema al nome layer mantendo la compatibilita con i comandi precedenti
    print ("EXPORT to postgis phase")
    mapping = {}
    if dbschema:
        for source,target in layers.iteritems():
            source_name = str(os.path.join(base_dir, source))
            mapping[source_name] = "%s.%s" % ( dbschema, target)
    else:
        for l in layers:
            
            source_name = str(os.path.join(base_dir, (l["source_dataset"] or "SIT").replace('\\',''), l["source_table"].replace('SIT.','')))
            mapping[source_name] = "%s.%s" % ( l["target_dataset"], l["target_table"])
    
    print ("MAPPING", mapping)

    shp2pgsql_path = os.path.join(base_dir,"shp2pg.sql")
    deleteTablessql_path = os.path.join(base_dir,"deleteTables.sql")

    try:
        os.remove(shp2pgsql_path)
    except:
        print("Can't remove shp2pgsql_path: ", shp2pgsql_path) 

    try:
        os.remove(deleteTablessql_path)
    except:
        print("Can't remove deleteTablessql_path: ", deleteTablessql_path) 

    #sys.exit()

    if update_db_layers:
        print "UPDATE:\n"
        delete_cmd = "echo "
        import_cmd = ""

        for layer_path,postgis_layer in mapping.iteritems():
            #layer_path = os.path.join(base_dir, layer)
            delete_cmd += "DELETE FROM %s;" % postgis_layer
            import_cmd = shp2pgsql + ' -a -s 3003 -g the_geom -W UTF-8 %s %s >> %s' % (layer_path +".shp",postgis_layer,shp2pgsql_path)
            print import_cmd
            os.system(import_cmd)

        delete_cmd += ' > %s' % os.path.join(base_dir,"deleteTables.sql")

        print delete_cmd
        os.system(delete_cmd)

    if create_db_layers:
        print "CREATE:\n"
        import_cmd = ""

        for layer_path,postgis_layer in mapping.iteritems():
            #layer_path = os.path.join(base_dir, layer)
            import_cmd = shp2pgsql + ' -I -d -s 3003 -g the_geom -W UTF-8 %s %s  >> %s' % (layer_path +".shp",postgis_layer,shp2pgsql_path)
            print import_cmd
            os.system(import_cmd)

    if delete_db_layers:
        print "DELETE:\n"
        delete_cmd = "echo "

        for layer_path,postgis_layer in mapping.iteritems():
            delete_cmd += "DROP TABLE %s;" % postgis_layer

        delete_cmd += ' > %sdeleteTables.sql' % base_dir

        print delete_cmd
        os.system(delete_cmd)

    if delete_db_layers or update_db_layers:
        delete_sql = '"%s" --host=%s --dbname=VISIO --username=istanze-amm < %s' % (psql,host,deleteTablessql_path)
        os.system( delete_sql )
        os.remove(deleteTablessql_path)

    if create_db_layers or update_db_layers:
        create_cmd = '"%s" --host=%s --dbname=VISIO --username=istanze-amm < %s' % (psql,host,shp2pgsql_path )
        os.system( create_cmd )
        os.remove(shp2pgsql_path)
    
    if refresh_materialized_view:
		refresh_sql = "REFRESH MATERIALIZED VIEW %s" % refresh_materialized_view
		refresh_cmd = '%s --host=%s --dbname=VISIO --username=istanze-amm -c "%s"' % (psql,host,refresh_sql )
		os.system( refresh_cmd )

