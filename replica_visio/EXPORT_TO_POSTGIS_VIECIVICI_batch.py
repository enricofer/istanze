# ---------------------------------------------------------------------------
# EXPORT CATASTO to POSTGIS
# ---------------------------------------------------------------------------

# Import arcpy module
import arceditor
import arcpy
import getpass
import os
import sys

from IMPORT import sendToPostgis, psql

# Script arguments
mergeByGeometry = "false"
alberatura_pat = "false"
Directory_di_esportazione = "D:\\Documenti\\01_LAVORO\\00-REPERTORI\\99_ESRI" 
ogr_path = 'C:\\OSGeo4W\\bin\\'
layers_mapping = [
	{
		"source_dataset": "\\SIT.PD2000",
		"source_table": "SIT.civici2000_tot",
		"target_dataset": "toponomastica",
		"target_table": "civici2000_tot_export"
	},
    {
		"source_dataset": "\\SIT.TRONCHI",
		"source_table": "SIT.FCL_VIE",
		"target_dataset": "toponomastica",
		"target_table": "vie_export"
	},
    {
		"source_dataset": "\\SIT.PD2000",
		"source_table": "SIT.VIE2000",
		"target_dataset": "toponomastica",
		"target_table": "vie_polig_export"
	},
]

update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except Exception as e:
    print ("Errore directory", e)
    pass


arcpy.env.overwriteOutput=True

if 1==0:
    for layer in layers_mapping:
        source = "Connessioni Database\\" + getpass.getuser() + ".sde\\%s" % (layer["source_dataset"], layer["source_table"])
        target = os.path.join(Directory_di_esportazione, (layer["source_dataset"] or "SIT").replace('\\',''), layer["source_table"].replace('SIT.','').replace('_Rw','').replace('_rw','').replace('_w','') +".shp") #layer.name
        print("ESPORTAZIONE DI "+source+" IN " + target)
        arcpy.CopyFeatures_management(source, target)


sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, None, layers_mapping, Directory_di_esportazione)




