# ---------------------------------------------------------------------------
# EXPORT CATASTO to POSTGIS
# ---------------------------------------------------------------------------

# Import arcpy module
import arceditor
import arcpy
import os
import sys
import getpass

from IMPORT import sendToPostgis, psql

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Script arguments
dataset_name = 'SIT.PR1000_WORK'
postgis_name = 'cs'
mergeByGeometry = "true"
alberatura_pat = "false"
Directory_di_esportazione = "D:\\Documenti\\01_LAVORO\\00-REPERTORI\\99_ESRI\\" + dataset_name
ogr_path = 'C:\\OSGeo4W\\bin\\'
layers_mapping = {
    "PR_02_01_01_fl":"PR_02_01_01_fl",
    "PR_10_00":"PR_10_00",
    "PR_ED_tutte":"PR_ED_tutte",
    "PR_avanzi_sy":"PR_avanzi_sy"
}
dbschema = "pi_cs_export"
base_dir = Directory_di_esportazione #"D:\\Documenti\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PR1000\\"
update_db_layers = True
delete_db_layers = False
create_db_layers = False
update_dataset = False

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except:
    pass

arcpy.env.workspace = "Connessioni Database\\" + getpass.getuser() + ".sde\\" + dataset_name
arcpy.env.overwriteOutput=True

fts = {'Point':'P_','Polygon':'A_','Polyline':'L_'}


for ftk,fti in fts.iteritems():
    layers = arcpy.ListFeatureClasses("*",ftk)
    print("layers", ftk, fti, layers)
    arcpy.AddMessage(layers)
    for layer in layers:
        clean_layername = layer.replace('SIT.','').replace('_Rw','').replace('_rw','').replace('_w','')
        if not update_dataset: 
            if not (clean_layername in layers_mapping):
                print (clean_layername, " export discarded")
                continue
        if mergeByGeometry == "true":
            pref = fti
        else:
            pref = ''
        source = layer#.dataSource
        
        target_dir = Directory_di_esportazione
            
        target = os.path.join(target_dir, pref+ clean_layername +".shp") #layer.name
        arcpy.AddMessage("ESPORTAZIONE DI "+source+" IN " + target)
        print("ESPORTAZIONE DI "+source+" IN " + target)
        arcpy.CopyFeatures_management(source, target)

if mergeByGeometry == "true":
    # merge_shps.py
    arcpy.AddMessage("mergeByGeometry")
    arcpy.AddMessage(mergeByGeometry)
    print ("mergeByGeometry")
    arcpy.env.workspace = Directory_di_esportazione

    path = Directory_di_esportazione #"  # path to your folder of .shp files
    types = fts.values() #["L_","A_","P_"]
    new_mapping = {}

    for type in types:
        merge = dataset_name + "_"+type+"merged"                          # this will be the name of your merged result
        new_mapping[merge] = merge.replace(dataset_name,postgis_name).lower()
        directory = os.listdir(path)
        arcpy.AddMessage(merge)
        print ("merging",merge)

        count = 0
        for filename in directory:
            clean_layername, file_extension = os.path.splitext(filename)

            if not update_dataset: 
                if not (clean_layername.replace(type,"") in layers_mapping):
                    if file_extension == ".shp":
                        print (clean_layername, " merge discarded")
                    continue
                    
            if filename.upper()[:2]==type and ".SHP" in filename.upper() and not ".XML" in filename.upper():
                
                # On the first pass, create a clone and add the filename column.
                if count == 0:
                    # Make a clone (matt wilkie)..
                    cmd = ogr_path+'ogr2ogr ' + path + '\\' + merge + '.shp ' + path + '\\' + filename + ' -where "FID < 0"'
                    arcpy.AddMessage(cmd)
                    os.system(cmd)

                    # Add the field (j03lar50n)..
                    cmd = ogr_path+'ogrinfo ' + path + '\\' + merge + '.shp -sql "ALTER TABLE ' + merge + ' ADD COLUMN filename character(50)"'
                    arcpy.AddMessage(cmd)
                    os.system(cmd)

                # Now populate the data (capooti)..
                arcpy.AddMessage( "Merging: " + str(filename))
                print("Merging: " + str(filename))

                # You'll need the filename without the .shp extension for the OGR_SQL..
                filenameNoExt = filename.replace(".shp","")

                cmd = ogr_path+'ogr2ogr -f "esri shapefile" -update -addfields -append ' + \
                        path + '\\' + merge + '.shp ' + \
                        path + '\\' + filename + \
                        ' -sql "SELECT \'' + filename[2:len(filename)-4].replace("_rw","").replace("_r","").replace("_w","") + '\' AS filename, * FROM ' + filenameNoExt + '"'

                # Uncomment this line to spit the ogr2ogr sentence to the terminal..
                arcpy.AddMessage( "\n" + cmd + "\n")

                os.system(cmd)

                arcpy.Delete_management(filename)

                count += 1
                
    layers_mapping = new_mapping


sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, layers_mapping, base_dir,refresh_materialized_view='pi_cs.cs_blob')




