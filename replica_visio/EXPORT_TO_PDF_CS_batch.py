# ---------------------------------------------------------------------------
# EXPORT CATASTO to POSTGIS
# ---------------------------------------------------------------------------

# Import basic licence
import arceditor
# Import arcpy module
import arcpy
import getpass
import os
import sys
import shutil
import datetime
now = datetime.datetime.now()

from IMPORT import sendToPostgis, psql

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Script arguments
dataset_name = 'SIT.PR1000_WORK'
mergeByGeometry = "false"
alberatura_pat = "false"
Directory_di_esportazione = "Z:\\URB-archivio\\QC_lavorazione\\00_ALBERATURA_COMUNE\\b_Progetto\\b05_PianoInterventi_var\\b0501_TematismiProg\\b0501019_tematismiPI\\1000"
ogr_path = 'C:\\OSGeo4W\\bin\\'
layers_mapping = {
    "PI_limiti":"pi_limiti",
    "PI_perimetri":"pi_perimetri",
    "PI_linee":"pi_linee",
    "PI_zoning":"pi_zoning"
}
dbschema = "pi_vigente_export"
base_dir = Directory_di_esportazione #"D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PI_WORK\\"
update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except:
    pass

arcpy.env.workspace = "Connessioni Database\\" + getpass.getuser() + ".sde\\" + dataset_name
arcpy.env.overwriteOutput=True

fts = {'Point':'P_','Polygon':'A_','Polyline':'L_'}

ws = "Z:\\URB-archivio\\Varianti PRG - PI\\Varianti PRG - PI\\_PI_VIGENTE\\_GIS"
pd = "Z:\\URB-archivio\\Varianti PRG - PI\\Varianti PRG - PI\\_PI_VIGENTE\\1000"

pageset = {
    "PI_CS_UFFICIALE.mxd": [75,76,87,88,89,99,100,101,102,116,117,118,119,133,134,135,149,150,164],
}

bakdir = "Z:\\URB-archivio\\Varianti PRG - PI\\Varianti PRG - PI\\_PI_VIGENTE\\1000 fino al\\%d%s%s" % (now.year,str(now.month).zfill(2),str(now.day).zfill(2))
print ("Backup to %s" % bakdir)
shutil.move(pd ,bakdir)
os.mkdir(pd)

for mxd in ["PI_CS_UFFICIALE.mxd", ]:
    current_mxd = arcpy.mapping.MapDocument(os.path.join(ws, mxd))
    for pageNameNum in pageset[mxd]:  
        pagename = "Foglio  %d" % pageNameNum
        current_mxd.dataDrivenPages.currentPageID = current_mxd.dataDrivenPages.getPageIDFromName (pagename)
        #print (pagename, current_mxd.dataDrivenPages.currentPageID, current_mxd.dataDrivenPages.getPageIDFromName (pagename))
        pdf_name = "PI_CS_F_%s.pdf" % str(pageNameNum).zfill(2)
        print ("printing %s" % pdf_name)
        arcpy.mapping.ExportToPDF(current_mxd, os.path.join(pd, pdf_name))

d = datetime.datetime.now()-now
print ("Total elaboration time: %d seconds" % d.total_seconds()) 
