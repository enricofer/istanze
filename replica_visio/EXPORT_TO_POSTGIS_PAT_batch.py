# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------------
# EXPORT PAT to POSTGIS
# ---------------------------------------------------------------------------

# Import arcpy module
import arceditor
import arcpy
import getpass
import os
import sys

from IMPORT import sendToPostgis, psql

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Script arguments
dataset_name = 'SIT.PAT_R_WORK'
mergeByGeometry = "true"
alberatura_pat = "false"
Directory_di_esportazione = "\\\\10.10.21.50\\dati\\archiviazione\\replica_visio\\" + dataset_name
ogr_path = 'C:\\OSGeo4W\\bin\\'
merged_layers = {
    "SIT.PAT_R_WORK_A_merged": "pat_a_merged",
    "SIT.PAT_R_WORK_P_merged": "pat_p_merged",
    "SIT.PAT_R_WORK_L_merged": "pat_l_merged"
}
dbschema = "pat_export"
base_dir = Directory_di_esportazione #"D:\\Documenti\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PAT_R_WORK\\"
update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except:
    pass

arcpy.env.workspace = "Connessioni Database\\" + getpass.getuser() + ".sde\\" + dataset_name
arcpy.env.overwriteOutput=True

dirmap = {
    "b0101": r"b_Progetto\b01_VincoliPianifTerrit\b0101_Vincoli",
    "b0102":r"b_Progetto\b01_VincoliPianifTerrit\b0102_Biodiversita",
    "b0103":r"b_Progetto\b01_VincoliPianifTerrit\b0103_PianifLivSuperiore",
    "b0104":r"b_Progetto\b01_VincoliPianifTerrit\b0104_CentriStorici",
    "b0105":r"b_Progetto\b01_VincoliPianifTerrit\b0105_GeneratoriVincolo",
    "b0201":r"b_Progetto\b02_Invarianti\b0201_InvGeologiche",
    "b0202":r"b_Progetto\b02_Invarianti\b0202_InvPaesaggistiche",
    "b0203":r"b_Progetto\b02_Invarianti\b0203_InvAmbientali",
    "b0204":r"b_Progetto\b02_Invarianti\b0204_InvStoricoMonumentali",
    "b0301":r"b_Progetto\b03_Fragilita\b0301_CompatGeologica",
    "b0302":r"b_Progetto\b03_Fragilita\b0302_DissestoIdrogeologico",
    "b0303":r"b_Progetto\b03_Fragilita\b0303_ZoneTutela",
    "b0401":r"b_Progetto\b04_Trasformabilita\b0401_ATO",
    "b0402":r"b_Progetto\b04_Trasformabilita\b0402_AzioniStrategiche",
    "b0403":r"b_Progetto\b04_Trasformabilita\b0403_ValoriTuteleCulturali",
    "b0404":r"b_Progetto\b04_Trasformabilita\b0404_ValoriTuteleNaturali",
}

fts = {'Point':'P_','Polygon':'A_','Polyline':'L_'}

allowed_fields_param='-skipfailures -select "filename, RIFLEGIS, ARTICOLO, TIPOVINC, TIPOLOGIA, DENOM, PERICOLO, AREEURBC, TIPOINT, NOTA, TIPOCOMP, QUALITA, QUALITÀ, N_AREACOMP, TIPOTUT, TIPPAES, TIPOELEM, TIPORISP, TIPAMB, TIPSTMON"'

for ftk,fti in fts.iteritems():
    layers = arcpy.ListFeatureClasses("*",ftk)
    print(layers)
    arcpy.AddMessage(layers)
    for layer in layers:
        if mergeByGeometry == "true":
            pref = fti
        else:
            pref = ''
        source = layer#.dataSource
        
        if alberatura_pat == "true":
            if layer[4:9] in dirmap:
                target_dir = os.path.join(Directory_di_esportazione, dirmap[layer[4:9]])
                print "target_dir", target_dir
                if not os.path.exists(target_dir):
                    os.makedirs(target_dir)
            else:
                target_dir = Directory_di_esportazione
        else:
            target_dir = Directory_di_esportazione
            
        target = os.path.join(target_dir, pref+layer.replace('SIT.','').replace('_Rw','').replace('_rw','').replace('_w','') +".shp") #layer.name
        #arcpy.AddMessage("ESPORTAZIONE DI "+source+" IN " + target)
        print("ESPORTAZIONE DI "+source+" IN " + target)
        arcpy.CopyFeatures_management(source, target)

if mergeByGeometry == "true":
    # merge_shps.py
    arcpy.AddMessage("mergeByGeometry")
    arcpy.AddMessage(mergeByGeometry)
    print ("mergeByGeometry")
    arcpy.env.workspace = Directory_di_esportazione

    path = Directory_di_esportazione #"  # path to your folder of .shp files
    types = fts.values() #["L_","A_","P_"]

    for type in types:
        merge = dataset_name + "_raw_"+type+"merged"                          # this will be the name of your merged result

        directory = os.listdir(path)
        arcpy.AddMessage(merge)
        print (merge)

        count = 0
        for filename in directory:
            print (filename)
            if filename.upper()[:2]==type and ".SHP" in filename.upper() and not ".XML" in filename.upper():
                
                # On the first pass, create a clone and add the filename column.
                if count == 0:
                    # Make a clone (matt wilkie)..
                    cmd = ogr_path+'ogr2ogr -lco ENCODING=UTF-8 ' + path + '\\' + merge + '.shp ' + path + '\\' + filename  + ' -where "FID < 0"'
                    arcpy.AddMessage(cmd)
                    print cmd
                    os.system(cmd)

                    # Add the field (j03lar50n)..
                    cmd = ogr_path+'ogrinfo ' + path + '\\' + merge + '.shp -sql "ALTER TABLE ' + merge + ' ADD COLUMN filename character(50)"'
                    arcpy.AddMessage(cmd)
                    os.system(cmd)

                # Now populate the data (capooti)..
                print("Merging: " + str(filename))

                # You'll need the filename without the .shp extension for the OGR_SQL..
                filenameNoExt = filename.replace(".shp","")

                cmd = ogr_path+'ogr2ogr -lco ENCODING=UTF-8 -f "esri shapefile" -update -addfields -append ' + \
                        path + '\\' + merge + '.shp ' + \
                        path + '\\' + filename + \
                        ' -sql "SELECT \'' + filename[2:len(filename)-4].replace("_rw","").replace("_r","").replace("_w","") + '\' AS filename, * FROM ' + filenameNoExt + '"'

                # Uncomment this line to spit the ogr2ogr sentence to the terminal..
                arcpy.AddMessage( "\n" + cmd + "\n")

                os.system(cmd)

                arcpy.Delete_management(filename)

                count += 1
	
    for type in types:
        raw = dataset_name + "_raw_"+type+"merged" 
        merged_layer_raw = path + '\\' + raw + '.shp'
        merge = dataset_name + "_"+type+"merged"  
        merged_layer = path + '\\' + merge + '.shp'
        cmd = ogr_path+'ogr2ogr -f "ESRI Shapefile" %s %s %s  -lco ENCODING=UTF-8' % (allowed_fields_param, merged_layer, merged_layer_raw)
        arcpy.AddMessage(cmd)
        print cmd
        os.system(cmd)

#sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, merged_layers, base_dir)

#refresh_sql = "REFRESH MATERIALIZED VIEW pat.pat_blob"
#refresh_cmd = '%s --host=10.10.21.50 --dbname=VISIO --username=istanze-amm -c "%s"' % (psql,refresh_sql )
#os.system( refresh_cmd )



