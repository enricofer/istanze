# merge_shps.py
import os    

path = "D:\\documenti\\_1_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PAT\\"  # path to your folder of .shp files
types = ["L_","A_","P_"]
ogr_path = 'C:\\Programmi\\"QGIS Essen"\\bin\\'

for type in types:
    merge = "PAT_"+type+"merged"                          # this will be the name of your merged result
    
    directory = os.listdir(path)
    
    count = 0
    for filename in directory:
        if filename.upper()[:2]==type and ".SHP" in filename.upper() and not ".XML" in filename.upper():
    
            # On the first pass, create a clone and add the filename column.
            if count == 0:
                # Make a clone (matt wilkie)..
                cmd = ogr_path+'ogr2ogr ' + path + '\\' + merge + '.shp ' + path + '\\' + filename + ' -where "FID < 0"'
                print cmd
                os.system(cmd)
    
                # Add the field (j03lar50n)..
                cmd = ogr_path+'ogrinfo ' + path + '\\' + merge + '.shp -sql "ALTER TABLE ' + merge + ' ADD COLUMN filename character(50)"'
                print cmd
                os.system(cmd)
    
            # Now populate the data (capooti)..
            print "Merging: " + str(filename)
    
            # You'll need the filename without the .shp extension for the OGR_SQL..
            filenameNoExt = filename.replace(".shp","")
    
            cmd = ogr_path+'ogr2ogr -f "esri shapefile" -update  -addfields -append ' + \
                    path + '\\' + merge + '.shp ' + \
                    path + '\\' + filename + \
                    ' -sql "SELECT \'' + filename[2:len(filename)-4] + '\' AS filename, * FROM ' + filenameNoExt + '"'
    
            # Uncomment this line to spit the ogr2ogr sentence to the terminal..
            print "\n" + cmd + "\n"
    
            os.system(cmd)
    
            count += 1