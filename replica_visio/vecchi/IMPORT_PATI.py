import os
import sys

update_db_layers = None
delete_db_layers = None
create_db_layers = None

help_msg = "\nUTILIZZO: python IMPORT_PATI.py [options]\n\noptions (default: --update):\n\n--update Aggiorna layer esistenti\n--create Crea nuovi layers\n--delete Cancella layers"

args = sys.argv
print len(args)

if len(args) == 1:
    print help_msg
    sys.exit()

for arg in args:
    print arg
    if arg == '--update':
        update_db_layers = True
    elif arg == '--delete':
        delete_db_layers = True
    elif arg == '--create':
        create_db_layers = True
    elif arg == '--help':
        print help_msg
        sys.exit()
    

layers = ["PATI_A_merged","PATI_P_merged","PATI_L_merged"]

dbschema = "pati_export"
base_dir = "D:\\documenti\\_1_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PATI\\"

psql = os.path.join("C:\\","Programmi","PostgreSQL","9.3","bin","psql.exe")

#sys.exit()

try:
    os.remove(base_dir+"shp2pg.sql")
except:
    pass
    
try:
    os.remove(base_dir+"deleteTables.sql")
except:
    pass

#sys.exit()

if update_db_layers:
    print "UPDATE:\n"
    delete_cmd = "echo "
    import_cmd = ""
    
    for layer in layers:
        delete_cmd += "DELETE FROM %s.%s;" % (dbschema,layer)
        import_cmd = '"C:\\Programmi\\PostgreSQL\\9.3\\bin\\shp2pgsql.exe" -a -s 3003 -g the_geom -W LATIN1 %s %s.%s >> %sshp2pg.sql' % (base_dir+layer+".shp",dbschema,layer,base_dir)
        print import_cmd
        os.system(import_cmd)
    
    delete_cmd += ' > %sdeleteTables.sql' % base_dir
    
    print delete_cmd
    os.system(delete_cmd)
    
if create_db_layers:
    print "CREATE:\n"
    import_cmd = ""
    
    for layer in layers:
        import_cmd = '"C:\\Programmi\\PostgreSQL\\9.3\\bin\\shp2pgsql.exe" -I -d -s 3003 -g the_geom -W LATIN1 %s %s.%s >> %sshp2pg.sql' % (base_dir+layer+".shp",dbschema,layer,base_dir)
        print import_cmd
        os.system(import_cmd)
    
if delete_db_layers:
    print "DELETE:\n"
    delete_cmd = "echo "
    
    for layer in layers:
        delete_cmd += "DROP TABLE %s.%s;" % (dbschema,layer)
    
    delete_cmd += ' > %sdeleteTables.sql' % base_dir
    
    print delete_cmd
    os.system(delete_cmd)
    
if delete_db_layers or update_db_layers:
    os.system( '"%s" --host=10.10.21.50 --dbname=VISIO --username=istanze-amm < %sdeleteTables.sql' % (psql,base_dir) )
    os.remove(base_dir+"deleteTables.sql")
    
if create_db_layers or update_db_layers:
    os.system( '"%s" --host=10.10.21.50 --dbname=VISIO --username=istanze-amm < %sshp2pg.sql' % (psql,base_dir ))
    os.remove(base_dir+"shp2pg.sql")

