import os
import sys
from shutil import copy2

suffix = '_22102012'
source_dir = "D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PRG_22102012\\"
target_dir = "U:\\PRG221012\\"

if not os.path.exists(target_dir):
    os.makedirs(target_dir)

layers_name_map = {
    "aree_servizi_no_dest_w":"PRG_AreeAServiziSenzaDestinazioneSpecifica",
    "aree_rispetto_w":"PRG_AreeDiRispetto",
    "centro_storico_w":"PRG_CentroStorico",
    "corridoio_tav_w":"PRG_CorridoioTAV",
    "dir_comm_ind_w":"PRG_DirezioneCommercialeIndustriale",
    "edilizia_economica_pop_w":"PRG_EdiliziaEconomicaEPopolare",
    "infra_viabilita_aree_w":"PRG_InfrastruttureEVIabilitaA",
    "infra_viabilita_linee_w":"PRG_InfrastruttureViabilitaL",
    "perimetri_w":"PRG_Perimetri",
    "residenza_w":"PRG_Residenza",
    "servizi_generale_w":"PRG_ServiziDiInteresseGenerale",
    "servizi_quartiere_w":"PRG_ServiziDiQuartiere",
    "tutele_w":"PRG_Tutele",
    "verde_w":"PRG_Verde",
    "zone_agricole_w":"PRG_ZoneAgricole",
}

extensions_allowed = ['.shp','.shx','.dbf','.prj']

for filename in os.listdir(source_dir):
    source = os.path.join(source_dir,filename)
    print ("SOURCE:",source)
    basename, extension = os.path.splitext(filename)
    basename = basename.replace(suffix,'_w')
    if extension in extensions_allowed and basename in layers_name_map:
        target = os.path.join(target_dir,layers_name_map[basename]+extension)
        print ("TARGET:",target)
        copy2(source,target)
