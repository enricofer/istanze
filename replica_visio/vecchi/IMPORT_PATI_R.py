import os
import sys
from IMPORT import sendToPostgis

update_db_layers = None
delete_db_layers = None
create_db_layers = None

help_msg = "\nUTILIZZO: python IMPORT_PATI.py [options]\n\noptions (default: --update):\n\n--update Aggiorna layer esistenti\n--create Crea nuovi layers\n--delete Cancella layers"

args = sys.argv
print len(args)

if len(args) == 1:
    print help_msg
    sys.exit()

for arg in args:
    print arg
    if arg == '--update':
        update_db_layers = True
    elif arg == '--delete':
        delete_db_layers = True
    elif arg == '--create':
        create_db_layers = True
    elif arg == '--help':
        print help_msg
        sys.exit()
    

layers = {
    "SIT.PATI_R_A_merged":"PATI_A_merged",
    "SIT.PATI_R_P_merged":"PATI_P_merged",
    "SIT.PATI_R_L_merged":"PATI_L_merged"
}

dbschema = "pati_export"
base_dir = "D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PATI_R\\"

sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, layers, base_dir)