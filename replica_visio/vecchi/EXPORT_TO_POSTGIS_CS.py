# ---------------------------------------------------------------------------
# EXPORT CATASTO to POSTGIS
# ---------------------------------------------------------------------------

# Import arcpy module
import arcpy

import os
import sys

from IMPORT import sendToPostgis, psql

# Script arguments
dataset_name = 'SIT.PR1000_WORK'
mergeByGeometry = "true"
alberatura_pat = "false"
Directory_di_esportazione = "D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\" + dataset_name
ogr_path = 'C:\\OSGeo4W64\\bin\\'
layers_mapping = {
    dataset_name+"_A_merged":"CS_A_merged",
    dataset_name+"_P_merged":"CS_P_merged",
    dataset_name+"_L_merged":"CS_L_merged"
}
rep = {
    'Polygon':["SIT.PR_02_01_01_fl_w","SIT.PR_10_00_w"],
    'Point':["SIT.PR_avanzi_sy_w",],
    'Polyline':["SIT.PR_ED_tutte_w",],
}

dbschema = "pi_cs_export"
base_dir = Directory_di_esportazione #"D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PI_WORK\\"
update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------


try:
    os.makedirs(Directory_di_esportazione)
except:
    pass

arcpy.env.workspace = "Connessioni Database\\ferregutie.sde\\" + dataset_name
arcpy.env.overwriteOutput=True

fts = {'Point':'P_','Polygon':'A_','Polyline':'L_'}


for ftk,fti in fts.iteritems():
    layers = rep[ftk]
    print(layers)
    arcpy.AddMessage(layers)
    for layer in layers:
        if mergeByGeometry == "true":
            pref = fti
        else:
            pref = ''
        source = layer#.dataSource
        
        target_dir = Directory_di_esportazione
            
        target = os.path.join(target_dir, pref+layer.replace('SIT.','').replace('_Rw','').replace('_rw','').replace('_w','') +".shp") #layer.name
        arcpy.AddMessage("ESPORTAZIONE DI "+source+" IN " + target)
        print("ESPORTAZIONE DI "+source+" IN " + target)
        arcpy.CopyFeatures_management(source, target)

if mergeByGeometry == "true":
    # merge_shps.py
    arcpy.AddMessage("mergeByGeometry")
    arcpy.AddMessage(mergeByGeometry)
    print ("mergeByGeometry")
    arcpy.env.workspace = Directory_di_esportazione

    path = Directory_di_esportazione #"  # path to your folder of .shp files
    types = fts.values() #["L_","A_","P_"]

    for type in types:
        merge = dataset_name + "_"+type+"merged"                          # this will be the name of your merged result

        directory = os.listdir(path)
        arcpy.AddMessage(merge)
        print (merge)

        count = 0
        for filename in directory:
            if filename.upper()[:2]==type and ".SHP" in filename.upper() and not ".XML" in filename.upper():

                # On the first pass, create a clone and add the filename column.
                if count == 0:
                    # Make a clone (matt wilkie)..
                    cmd = ogr_path+'ogr2ogr ' + path + '\\' + merge + '.shp ' + path + '\\' + filename + ' -where "FID < 0"'
                    arcpy.AddMessage(cmd)
                    os.system(cmd)

                    # Add the field (j03lar50n)..
                    cmd = ogr_path+'ogrinfo ' + path + '\\' + merge + '.shp -sql "ALTER TABLE ' + merge + ' ADD COLUMN filename character(50)"'
                    arcpy.AddMessage(cmd)
                    os.system(cmd)

                # Now populate the data (capooti)..
                arcpy.AddMessage( "Merging: " + str(filename))
                print("Merging: " + str(filename))

                # You'll need the filename without the .shp extension for the OGR_SQL..
                filenameNoExt = filename.replace(".shp","")

                cmd = ogr_path+'ogr2ogr -f "esri shapefile" -update -addfields -append ' + \
                        path + '\\' + merge + '.shp ' + \
                        path + '\\' + filename + \
                        ' -sql "SELECT \'' + filename[2:len(filename)-4].replace("_rw","").replace("_r","").replace("_w","") + '\' AS filename, * FROM ' + filenameNoExt + '"'

                # Uncomment this line to spit the ogr2ogr sentence to the terminal..
                arcpy.AddMessage( "\n" + cmd + "\n")

                os.system(cmd)

                arcpy.Delete_management(filename)

                count += 1


sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, layers_mapping, base_dir)



refresh_sql = "REFRESH MATERIALIZED VIEW pi_cs.cs_blob"
refresh_cmd = '%s --host=10.10.21.50 --dbname=VISIO --username=istanze-amm -c "%s"' % (psql,refresh_sql )
os.system( refresh_cmd )

