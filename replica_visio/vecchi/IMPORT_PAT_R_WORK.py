import os
import sys
from IMPORT import sendToPostgis, psql

update_db_layers = None
delete_db_layers = None
create_db_layers = None

help_msg = "\nUTILIZZO: python IMPORT_PAT.py [options]\n\noptions (default: --update):\n\n--update Aggiorna layer esistenti\n--create Crea nuovi layers\n--delete Cancella layers"

args = sys.argv
print len(args)

if len(args) == 1:
    print help_msg
    sys.exit()

for arg in args:
    print arg
    if arg == '--update':
        update_db_layers = True
    elif arg == '--delete':
        delete_db_layers = True
    elif arg == '--create':
        create_db_layers = True
    elif arg == '--help':
        print help_msg
        sys.exit()
    

layers = {
    "SIT.PAT_R_WORK_A_merged": "pat_a_merged",
    "SIT.PAT_R_WORK_P_merged": "pat_p_merged",
    "SIT.PAT_R_WORK_L_merged": "pat_l_merged"
}

#allowed_fields=["the_geom", "riflegis", "articolo", "tipovinc", "tipologia", "denom", "pericolo", "areeurbc", "tipoint", "nota", "tipocomp", "qualitÀ", "n_areacomp", "tipotut", "tippaes", "tipoelem", "tiporisp", "tipamb", "tipstmon"]
allowed_fields="RIFLEGIS, ARTICOLO, TIPOVINC, TIPOLOGIA, DENOM, PERICOLO, AREEURBC, TIPOINT, NOTA, TIPOCOMP, QUALITA, QUALITÀ, N_AREACOMP, TIPOTUT, TIPPAES, TIPOELEM, TIPORISP, TIPAMB, TIPSTMON"

dbschema = "pat_export"
base_dir = "D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PAT_R_WORK\\"

sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, layers, base_dir)

refresh_sql = "REFRESH MATERIALIZED VIEW pat.pat_blob"

refresh_cmd = '%s --host=10.10.21.50 --dbname=VISIO --username=istanze-amm -c "%s"' % (psql,refresh_sql )
os.system( refresh_cmd )

# "C:\\Program Files (x86)\\pgAdmin III\\1.22\\psql.exe" --host=10.10.21.50 --dbname=VISIO --username=istanze-amm -c "REFRESH MATERIALIZED VIEW pat.pat_blob"
