import os
import sys

update_db_layers = None
delete_db_layers = None
create_db_layers = None

help_msg = "\nUTILIZZO: python IMPORT_CATASTO.py [options]\n\noptions (default: --update):\n\n--update Aggiorna layer esistenti\n--create Crea nuovi layers\n--delete Cancella layers"

args = sys.argv
print len(args)

if len(args) == 1:
    print help_msg
    sys.exit()

for arg in args:
    print arg
    if arg == '--update':
        update_db_layers = True
    elif arg == '--delete':
        delete_db_layers = True
    elif arg == '--create':
        create_db_layers = True
    elif arg == '--help':
        print help_msg
        sys.exit()
    

layers = [
    {
        "source":"A_brebac_giugno12_pd.shp",
        "target":"pai_pericolosita_idraulica",
        "dbschema": "altro",
        "base_dir":"D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PAI\\",
    },
    {
        "source":"A_PTCP_VENETO_pd.shp",
        "target":"pai_bacino_scolante_laguna",
        "dbschema": "altro",
        "base_dir":"D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PAI\\",
    },
    {
        "source":"A_aree_sottoposte_a_bonifica.shp",
        "target":"aree_sottoposte_a_bonifica",
        "dbschema": "altro",
        "base_dir":"D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.AREE_INQUINATE\\",
    },
]

sql_directive = "D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\shp2pg.sql"

shp2pgsql_cmd = "C:\Program Files\shp2pgsql\shp2pgsql.exe"

psql_cmd = "psql.exe"

try:
    os.remove(sql_directive)
except:
    pass
    

#sys.exit()
    
if delete_db_layers or update_db_layers:
    print "DELETE:\n"
    delete_cmd = "echo "
    
    for layer in layers:
        source = layer["source"]
        target = layer["target"]
        dbschema = layer["dbschema"]
        base_dir = layer["base_dir"]
        delete_cmd += "DROP TABLE %s.%s;" % (dbschema,target)
    
    delete_cmd += ' > %s' % sql_directive
    
    print delete_cmd
    os.system(delete_cmd)

if update_db_layers:
    print "UPDATE:\n"
    import_cmd = ""
    
    for layer in layers:
        source = layer["source"]
        target = layer["target"]
        dbschema = layer["dbschema"]
        base_dir = layer["base_dir"]
        import_cmd = '"%s" -a -s 3003 -g the_geom -W LATIN1 %s %s.%s >> %s' % (shp2pgsql_cmd,base_dir+source,dbschema,target,sql_directive)
        print import_cmd
        os.system(import_cmd)
    
if create_db_layers:
    print "CREATE:\n"
    import_cmd = ""
    
    for layer in layers:
        source = layer["source"]
        target = layer["target"]
        dbschema = layer["dbschema"]
        base_dir = layer["base_dir"]
        import_cmd = '"%s" -I -d -s 3003 -g the_geom -W LATIN1 %s %s.%s >> %s' % (shp2pgsql_cmd,base_dir+source,dbschema,target,sql_directive)
        print import_cmd
        os.system(import_cmd)
        
os.system( '"%s" --host=10.10.21.50 --dbname=VISIO --username=istanze-amm < %s' % (psql_cmd,sql_directive ))
os.remove(sql_directive)

