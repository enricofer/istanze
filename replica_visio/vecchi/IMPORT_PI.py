import os
import sys
from IMPORT import sendToPostgis

update_db_layers = None
delete_db_layers = None
create_db_layers = None

help_msg = "\nUTILIZZO: python IMPORT_PI.py [options]\n\noptions (default: --update):\n\n--update Aggiorna layer esistenti\n--create Crea nuovi layers\n--delete Cancella layers"

args = sys.argv
print len(args)

if len(args) == 1:
    print help_msg
    sys.exit()

for arg in args:
    print arg
    if arg == '--update':
        update_db_layers = True
    elif arg == '--delete':
        delete_db_layers = True
    elif arg == '--create':
        create_db_layers = True
    elif arg == '--help':
        print help_msg
        sys.exit()

layers = {
    "PI_limiti":"pi_limiti",
    "PI_perimetri":"pi_perimetri",
    "PI_linee":"pi_linee",
    "PI_zoning":"pi_zoning"
}

dbschema = "pi_vigente_export"
base_dir = "D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PI_WORK\\"

sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, layers, base_dir)
