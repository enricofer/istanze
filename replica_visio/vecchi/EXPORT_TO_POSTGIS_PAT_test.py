# ---------------------------------------------------------------------------
# EXPORT PAT to POSTGIS
# ---------------------------------------------------------------------------

# Import arcpy module
import arceditor
import arcpy

import os
import sys

from IMPORT import sendToPostgis, psql

# Script arguments
dataset_name = 'SIT.PAT_R_WORK'
mergeByGeometry = "true"
alberatura_pat = "false"
Directory_di_esportazione = "D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\" + dataset_name
ogr_path = 'C:\\OSGeo4W64\\bin\\'
merged_layers = {
    "SIT.PAT_R_WORK_A_merged": "pat_a_merged",
    "SIT.PAT_R_WORK_P_merged": "pat_p_merged",
    "SIT.PAT_R_WORK_L_merged": "pat_l_merged"
}
dbschema = "pat_export"
base_dir = Directory_di_esportazione #"D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PAT_R_WORK\\"
update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except:
    pass

arcpy.env.workspace = "Connessioni Database\\ferregutie.sde\\" + dataset_name
arcpy.env.overwriteOutput=True

dirmap = {
    "b0101": r"b_Progetto\b01_VincoliPianifTerrit\b0101_Vincoli",
    "b0102":r"b_Progetto\b01_VincoliPianifTerrit\b0102_Biodiversita",
    "b0103":r"b_Progetto\b01_VincoliPianifTerrit\b0103_PianifLivSuperiore",
    "b0104":r"b_Progetto\b01_VincoliPianifTerrit\b0104_CentriStorici",
    "b0105":r"b_Progetto\b01_VincoliPianifTerrit\b0105_GeneratoriVincolo",
    "b0201":r"b_Progetto\b02_Invarianti\b0201_InvGeologiche",
    "b0202":r"b_Progetto\b02_Invarianti\b0202_InvPaesaggistiche",
    "b0203":r"b_Progetto\b02_Invarianti\b0203_InvAmbientali",
    "b0204":r"b_Progetto\b02_Invarianti\b0204_InvStoricoMonumentali",
    "b0301":r"b_Progetto\b03_Fragilita\b0301_CompatGeologica",
    "b0302":r"b_Progetto\b03_Fragilita\b0302_DissestoIdrogeologico",
    "b0303":r"b_Progetto\b03_Fragilita\b0303_ZoneTutela",
    "b0401":r"b_Progetto\b04_Trasformabilita\b0401_ATO",
    "b0402":r"b_Progetto\b04_Trasformabilita\b0402_AzioniStrategiche",
    "b0403":r"b_Progetto\b04_Trasformabilita\b0403_ValoriTuteleCulturali",
    "b0404":r"b_Progetto\b04_Trasformabilita\b0404_ValoriTuteleNaturali",
}


sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, merged_layers, base_dir)

refresh_sql = "REFRESH MATERIALIZED VIEW pat.pat_blob"
refresh_cmd = '%s --host=10.10.21.50 --dbname=VISIO --username=istanze-amm -c "%s"' % (psql,refresh_sql )
os.system( refresh_cmd )



