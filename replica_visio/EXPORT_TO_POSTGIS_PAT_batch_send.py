# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------------
# EXPORT PAT to POSTGIS
# ---------------------------------------------------------------------------

# Import arcpy module
import arceditor
import arcpy
import getpass
import os
import sys

from IMPORT import sendToPostgis, psql

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Script arguments
dataset_name = 'SIT.PAT_R_WORK'
mergeByGeometry = "true"
alberatura_pat = "false"
Directory_di_esportazione = "\\\\10.10.21.50\\dati\\archiviazione\\replica_visio\\" + dataset_name
ogr_path = 'C:\\OSGeo4W\\bin\\'
merged_layers = {
    "SIT.PAT_R_WORK_A_merged": "pat_a_merged",
    "SIT.PAT_R_WORK_P_merged": "pat_p_merged",
    "SIT.PAT_R_WORK_L_merged": "pat_l_merged"
}
dbschema = "pat_export"
base_dir = Directory_di_esportazione #"D:\\Documenti\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PAT_R_WORK\\"
update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except:
    pass

arcpy.env.workspace = "Connessioni Database\\" + getpass.getuser() + ".sde\\" + dataset_name
arcpy.env.overwriteOutput=True

dirmap = {
    "b0101": r"b_Progetto\b01_VincoliPianifTerrit\b0101_Vincoli",
    "b0102":r"b_Progetto\b01_VincoliPianifTerrit\b0102_Biodiversita",
    "b0103":r"b_Progetto\b01_VincoliPianifTerrit\b0103_PianifLivSuperiore",
    "b0104":r"b_Progetto\b01_VincoliPianifTerrit\b0104_CentriStorici",
    "b0105":r"b_Progetto\b01_VincoliPianifTerrit\b0105_GeneratoriVincolo",
    "b0201":r"b_Progetto\b02_Invarianti\b0201_InvGeologiche",
    "b0202":r"b_Progetto\b02_Invarianti\b0202_InvPaesaggistiche",
    "b0203":r"b_Progetto\b02_Invarianti\b0203_InvAmbientali",
    "b0204":r"b_Progetto\b02_Invarianti\b0204_InvStoricoMonumentali",
    "b0301":r"b_Progetto\b03_Fragilita\b0301_CompatGeologica",
    "b0302":r"b_Progetto\b03_Fragilita\b0302_DissestoIdrogeologico",
    "b0303":r"b_Progetto\b03_Fragilita\b0303_ZoneTutela",
    "b0401":r"b_Progetto\b04_Trasformabilita\b0401_ATO",
    "b0402":r"b_Progetto\b04_Trasformabilita\b0402_AzioniStrategiche",
    "b0403":r"b_Progetto\b04_Trasformabilita\b0403_ValoriTuteleCulturali",
    "b0404":r"b_Progetto\b04_Trasformabilita\b0404_ValoriTuteleNaturali",
}

fts = {'Point':'P_','Polygon':'A_','Polyline':'L_'}

allowed_fields_param='-skipfailures -select "filename, RIFLEGIS, ARTICOLO, TIPOVINC, TIPOLOGIA, DENOM, PERICOLO, AREEURBC, TIPOINT, NOTA, TIPOCOMP, QUALITA, QUALITÀ, N_AREACOMP, TIPOTUT, TIPPAES, TIPOELEM, TIPORISP, TIPAMB, TIPSTMON"'



sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, dbschema, merged_layers, base_dir, refresh_materialized_view='pat.pat_blob')




