# ---------------------------------------------------------------------------
# EXPORT CATASTO to POSTGIS
# ---------------------------------------------------------------------------

# Import arcpy module
import arceditor
import arcpy

import os
import sys
import getpass

from IMPORT import sendToPostgis, psql

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Script arguments
dataset_name = 'SIT.PI_WORK'
mergeByGeometry = "false"
alberatura_pat = "false"
Directory_di_esportazione = "Z:\\URB-archivio\\QC_lavorazione\\00_ALBERATURA_COMUNE\\b_Progetto\\b05_PianoInterventi_var\\b0501_TematismiProg\\b0501019_tematismiPI\\5000"
ogr_path = 'C:\\OSGeo4W64\\bin\\'
layers_mapping = {
    "PI_limiti":"pi_limiti",
    "PI_perimetri":"pi_perimetri",
    "PI_linee":"pi_linee",
    "PI_zoning":"pi_zoning"
}
dbschema = "pi_vigente_export"
base_dir = Directory_di_esportazione #"D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PI_WORK\\"
update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except:
    pass

arcpy.env.workspace = "Connessioni Database\\" + getpass.getuser() + ".sde\\" + dataset_name
arcpy.env.overwriteOutput=True

fts = {'Point':'P_','Polygon':'A_','Polyline':'L_'}

for ftk,fti in fts.iteritems():
    layers = arcpy.ListFeatureClasses("*",ftk)
    print(layers)
    arcpy.AddMessage(layers)
    for layer in layers:
        if mergeByGeometry == "true":
            pref = fti
        else:
            pref = ''
        source = layer#.dataSource
        
        target_dir = Directory_di_esportazione
            
        target = os.path.join(target_dir, pref+layer.replace('SIT.','').replace('_Rw','').replace('_rw','').replace('_w','') +".shp") #layer.name
        arcpy.AddMessage("ESPORTAZIONE DI "+source+" IN " + target)
        print("ESPORTAZIONE DI "+source+" IN " + target)
        arcpy.CopyFeatures_management(source, target)
