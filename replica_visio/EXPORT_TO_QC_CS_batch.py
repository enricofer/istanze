# ---------------------------------------------------------------------------
# EXPORT CS to QC
# ---------------------------------------------------------------------------

# Import arcpy module
import arceditor
import arcpy

import os
import sys
import getpass

from IMPORT import sendToPostgis, psql

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

# Script arguments
dataset_name = 'SIT.PR1000_WORK'
mergeByGeometry = "false"
alberatura_pat = "false"
Directory_di_esportazione = "Z:\\URB-archivio\\QC_lavorazione\\00_ALBERATURA_COMUNE\\b_Progetto\\b05_PianoInterventi_var\\b0501_TematismiProg\\b0501019_tematismiPI\\1000"
ogr_path = 'C:\\OSGeo4W\\bin\\'
layers_mapping = {
    "IS07":"IS07",
    "PR_00_01_01_sy":"PR_00_01_01_sy",
    "PR_00_01_01_tx":"PR_00_01_01_tx",
    "PR_00_01_02_sy":"PR_00_01_02_sy",
    "PR_00_01_02_tx":"PR_00_01_02_tx",
    "PR_01_01_01":"PR_01_01_01",
    "PR_01_01_16":"PR_01_01_16",
    "PR_01_02_02":"PR_01_02_02",
    "PR_02_01_01_fl":"PR_02_01_01_fl",
    "PR_02_01_01_flale":"PR_02_01_01_flale",
    "PR_02_01_01_flori":"PR_02_01_01_flori",
    "PR_02_01_01_ly":"PR_02_01_01_ly",
    "PR_02_01_02":"PR_02_01_02",
    "PR_02_01_03":"PR_02_01_03",
    "PR_02_01_04":"PR_02_01_04",
    "PR_02_02_01_ly":"PR_02_02_01_ly",
    "PR_02_02_01_sy":"PR_02_02_01_sy",
    "PR_02_02_09":"PR_02_02_09",
    "PR_02_02_10_fl":"PR_02_02_10_fl",
    "PR_02_02_10_ly":"PR_02_02_10_ly",
    "PR_02_03_01":"PR_02_03_01",
    "PR_02_05_02":"PR_02_05_02",
    "PR_03_01_01":"PR_03_01_01",
    "PR_03_01_02":"PR_03_01_02",
    "PR_04_04_02":"PR_04_04_02",
    "PR_05_01_02_sy":"PR_05_01_02_sy",
    "PR_05_01_02_tx":"PR_05_01_02_tx",
    "PR_05_03_02_fl":"PR_05_03_02_fl",
    "PR_05_03_02_ly":"PR_05_03_02_ly",
    "PR_05_03_02_ly_1":"PR_05_03_02_ly_1",
    "PR_06_01_06_ly":"PR_06_01_06_ly",
    "PR_06_01_06_sy":"PR_06_01_06_sy",
    "PR_06_04_01":"PR_06_04_01",
    "PR_06_04_02":"PR_06_04_02",
    "PR_07_03_01":"PR_07_03_01",
    "PR_07_03_02":"PR_07_03_02",
    "PR_09_01_01":"PR_09_01_01",
    "PR_09_01_01_sy":"PR_09_01_01_sy",
    "PR_10_00":"PR_10_00",
    "PR_10_00ale":"PR_10_00ale",
    "PR_10_00old":"PR_10_00old",
    "PR_avanzi_ly":"PR_avanzi_ly",
    "PR_avanzi_sy":"PR_avanzi_sy",
    "PR_ED_tutte":"PR_ED_tutte"
}
dbschema = "pi_cs_export"
base_dir = Directory_di_esportazione #"D:\\Documents\\01_LAVORO\\00-REPERTORI\\99_ESRI\\SIT.PR1000_WORK\\"
update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except:
    pass

arcpy.env.workspace = "Connessioni Database\\" + getpass.getuser() + ".sde\\" + dataset_name
arcpy.env.overwriteOutput=True

fts = {'Point':'P_','Polygon':'A_','Polyline':'L_'}

for ftk,fti in fts.iteritems():
    layers = arcpy.ListFeatureClasses("*",ftk)
    print(layers)
    arcpy.AddMessage(layers)
    for layer in layers:
        if mergeByGeometry == "true":
            pref = fti
        else:
            pref = ''
        source = layer#.dataSource
        
        target_dir = Directory_di_esportazione
            
        target = os.path.join(target_dir, pref+layer.replace('SIT.','').replace('_Rw','').replace('_rw','').replace('_w','') +".shp") #layer.name
        arcpy.AddMessage("ESPORTAZIONE DI "+source+" IN " + target)
        print("ESPORTAZIONE DI "+source+" IN " + target)
        arcpy.CopyFeatures_management(source, target)
