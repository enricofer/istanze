# ---------------------------------------------------------------------------
# EXPORT CATASTO to POSTGIS
# ---------------------------------------------------------------------------

# Import arcpy module
import arceditor
import arcpy

import os
import sys

from IMPORT import sendToPostgis, psql

# Script arguments
mergeByGeometry = "false"
alberatura_pat = "false"
Directory_di_esportazione = "D:\\Documenti\\01_LAVORO\\00-REPERTORI\\99_ESRI" 
ogr_path = 'C:\\OSGeo4W64\\bin\\'
layers_mapping = [
	{
		"source_dataset": "\\SIT.ZONOM",
		"source_table": "SIT.FCL_ZONEOM",
		"target_dataset": "pi_vigente_export",
		"target_table": "zoneomogenee_export"
	},
]

update_db_layers = True
delete_db_layers = None
create_db_layers = None

# ---------------------------------------------------------------------------

try:
    os.makedirs(Directory_di_esportazione)
except Exception as e:
    print ("Errore directory", e)
    pass


arcpy.env.overwriteOutput=True

if 1==0:
    for layer in layers_mapping:
        source = "Connessioni Database\\ferregutie.sde%s\\%s" % (layer["source_dataset"], layer["source_table"])
        target = os.path.join(Directory_di_esportazione, (layer["source_dataset"] or "SIT").replace('\\',''), layer["source_table"].replace('SIT.','').replace('_Rw','').replace('_rw','').replace('_w','') +".shp") #layer.name
        print("ESPORTAZIONE DI "+source+" IN " + target)
        arcpy.CopyFeatures_management(source, target)


sendToPostgis (update_db_layers, create_db_layers, delete_db_layers, None, layers_mapping, Directory_di_esportazione)




