from django.apps import AppConfig

from django_mailbox.signals import message_received
from django.dispatch import receiver
from django.conf import settings
import vobject
import re
import uuid
import os
from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.core.mail import EmailMessage

class ProcessMailConfig(AppConfig):
    name = 'process_mail'

TEST = False

@receiver(message_received)
def email_callback(sender, message, **args):
    print ("I just recieved a message titled %s from a mailbox named %s" % (message.subject , message.mailbox.name))
    if TEST:
        pref = "TEST "
    else:
        pref = ''
    if message.subject.startswith(pref +'Nuovo appuntamento CUP'):
        r_nome = capture(message.text," di "," per ")
        r_data = capture(message.text," data "," alle ")
        r_ora = capture(message.text," ore ","\.")
        r_email = capture(message.text,"utente è: ","$")[:-2].lower()
        r_mobile = capture(message.text,"cellulare dell'utente è: ","$")

        print (message.subject)
        print (message.text)
        print (r_nome)
        print (r_data)
        print (r_ora)
        print (r_mobile)
        
        r_from_tmp = datetime.strptime(r_data+" CET "+r_ora, '%d/%m/%Y %Z %H:%M')
        r_from = datetime.fromtimestamp(datetime.timestamp(r_from_tmp), tz=vobject.icalendar.utc)
        r_to = r_from + timedelta(minutes=30)
        schedule_uid = str(uuid.uuid4()).replace("-","").upper()
        conference_link = 'https://open.meet.garr.it/'+schedule_uid
        conference_link_alt = 'https://meet.jit.si/'+schedule_uid
        subject = "Sportello Urbanistica: Ricevimento programmato %s del %s h %s" % (r_email, r_data, r_ora)
        desc = """
Prenotazione del ricevimento dello sportello urbanistica del %s h %s
Da parte dell'utente %s mobile: %s email: %s 
Predisposizione conferenza online: %s
    """ % (
            r_data,
            r_ora,
            r_nome,
            r_mobile,
            r_email,
            conference_link_alt
        )
        if TEST:
            gruppo_invio = ["ferregutie@comune.padova.it"]
        else:
            gruppo_invio = [u.email for u in User.objects.filter(groups__name='ricevimento')]
        gruppo_invio.append(r_email)

        print ("INVIO",gruppo_invio)

        for e in gruppo_invio:
            crea_conferenza(r_nome, e, subject, desc, r_from, r_to, schedule_uid)


def capture(txt,s,e):
    re_def = "(?<=(%s))(.*?)(?=(%s))" % (s,e)
    return re.search(re_def, txt, re.MULTILINE).group(0) or ""


def crea_conferenza(attendee_name, attendee_email,sbj, msg, from_time, to_time, schedule_uid):
    cal = vobject.newFromBehavior('vcalendar')
    cal.add('calscale').value = "GREGORIAN"
    cal.add('method').value = "REQUEST"
    cal.add('vevent')
    cal.vevent.add('summary').value = sbj
    utc = vobject.icalendar.utc
    print ("utc",utc)
    attendee = cal.vevent.add('ATTENDEE')
    attendee.role_param = 'REQ-PARTICIPANT'
    attendee.partstat_param = 'NEEDS-ACTION'
    attendee.rsvp_param = 'TRUE'
    attendee.cn_param = attendee_name
    attendee.value = 'mailto:' + attendee_email
    cal.vevent.add('X-MOZ-SEND-INVITATIONS').value = "TRUE"
    organizer = cal.vevent.add('ORGANIZER')
    organizer.cn_param = 'Comune Padova'
    organizer.value = "mailto:rapper1@comune.padova.it"
    start = cal.vevent.add('dtstart')
    #start.value = datetime.datetime(2021, 4, 30, tzinfo = utc)
    start.value = from_time #datetime.datetime.fromisoformat('2021-04-29 15:30:00')
    end = cal.vevent.add('dtend')
    end.value = to_time #datetime.datetime.fromisoformat('2021-04-29 16:00:00')
    cal.vevent.add('class').value = 'PUBLIC'
    cal.vevent.add('uid').value = schedule_uid
    cal.vevent.add('description').value = msg

    print (cal.serialize())

    emsg = EmailMessage(
        sbj,
        msg,
        "noreply@comune.padova.it",
        [attendee_email],
        attachments = [
            ("", cal.serialize(), "text/calendar; method=REQUEST; charset=utf-8")
        ]
    )
    res = emsg.send(fail_silently=False)
    if res == 1:
        logdir = os.path.join(settings.MEDIA_ROOT,'log')
        logfile = os.path.join(logdir,'inviti_ricevimento.log')
        if not os.path.exists(logdir):
            os.makedirs(logdir)
        with open(logfile,'a') as log:
            log.write(sbj +'\n'+ attendee_email +'\n'+ msg + '\n\n')

    print ("MSG SENT", res)