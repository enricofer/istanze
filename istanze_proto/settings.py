"""
Django settings for istanze project.

Generated by 'django-admin startproject' using Django 1.8.6.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import datetime

import ldap
from django_auth_ldap.config import LDAPSearch

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '' # INSERIRE CREDENZIALI CORRETTE

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEST_DEPLOYMENT = False #

ALLOWED_HOSTS = ['*']

#EMAIL_HOST = 'posta.comune.padova.it'
#EMAIL_PORT = 587
#EMAIL_HOST_USER = '' # INSERIRE CREDENZIALI CORRETTE
#EMAIL_HOST_PASSWORD = '' # INSERIRE CREDENZIALI CORRETTE
#EMAIL_USE_TLS = True
#EMAIL_USE_SSL = False

EMAIL_HOST = 'smtp.office365.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'applicativi.smtp@comunepadovait.onmicrosoft.com'
EMAIL_HOST_PASSWORD = '@ASK_GRANZIERO' 
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False

# Application definition

INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'dal',
    'dal_select2',
    #'admin_view_permission',
    'grappelli',
    
    "taggit",
    'taggit_labels',
    'taggit_bulk',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django_tables2',
    'floppyforms',
    'selectable',
    'django_filters',
    'ajax_select',
    'httpproxy',
    'nested_admin',
    'simple_history',
    'django_extensions', 
    'dbbackup',
    'django_json_widget',
    'django_mailbox',
    'bootstrap_datepicker_plus',
    'django_q',
    'massadmin',
    
    #USERAPP
    'repertorio',
    'vincoli',
    'certificati',
    'pua',
    'hp',
    'django_warp',
    'version',
    'process_mail'
)

MIDDLEWARE = (
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'admin_reorder.middleware.ModelAdminReorder',
    #'whitenoise.middleware.WhiteNoiseMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',
)

#LOGIN_REDIRECT_URL = ('..')

#LOGOUT_REDIRECT_URL = ('..')

DEFAULT_AUTO_FIELD='django.db.models.AutoField'

ROOT_URLCONF = 'istanze.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        #'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django_settings_export.settings_export',
                'hp.context_processors.admin_media',
            ],
            'loaders': ['django.template.loaders.app_directories.Loader']#,'admin_tools.template_loaders.Loader',],
        },
    },
]

WSGI_APPLICATION = 'istanze.wsgi.application'

BASE_DEV = "/home/ferregutie/"

DATABASES_PROD = {
	'default': {
		'ENGINE': 'django.contrib.gis.db.backends.postgis',
		'NAME': 'istanze',
		'USER': '',# INSERIRE CREDENZIALI CORRETTE
		'PASSWORD' : '',# INSERIRE CREDENZIALI CORRETTE
		'HOST' : 'localhost' ,
		'PORT' : '5432',
	},
	'visio': {
		'ENGINE': 'django.contrib.gis.db.backends.postgis',
		'NAME': 'VISIO',
		'USER': '',# INSERIRE CREDENZIALI CORRETTE
		'PASSWORD' : '',# INSERIRE CREDENZIALI CORRETTE
		'HOST' : 'localhost' ,
		'PORT' : '5432',
	},
	'gdbt': {
		'ENGINE': 'django.db.backends.oracle',
		'NAME': 'gdbt',
		'USER': '',# INSERIRE CREDENZIALI CORRETTE
		'PASSWORD' : '',# INSERIRE CREDENZIALI CORRETTE
		'HOST' : '10.10.20.232' ,
		'PORT' : '1521',
	}
}

DATABASES_TEST = {
	'default': {
		'ENGINE': 'django.contrib.gis.db.backends.postgis',
		'NAME': 'istanze',
		'USER': '',# INSERIRE CREDENZIALI CORRETTE
		'PASSWORD' : '',# INSERIRE CREDENZIALI CORRETTE
		'HOST' : 'localhost' ,
		'PORT' : '5433',
	},
	'visio': {
		'ENGINE': 'django.contrib.gis.db.backends.postgis',
		'NAME': 'VISIO',
		'USER': '',# INSERIRE CREDENZIALI CORRETTE
		'PASSWORD' : '',# INSERIRE CREDENZIALI CORRETTE
		'HOST' : 'localhost' ,
		'PORT' : '5433',
	}
}

if TEST_DEPLOYMENT:
    DATABASES = DATABASES_TEST
else:
    DATABASES = DATABASES_PROD

# Internationalization

LANGUAGE_CODE = 'it-it'

TIME_ZONE = 'Europe/Rome'

USE_I18N = True

USE_L10N = True

USE_TZ = True

ADMIN_TOOLS_MENU = 'repertorio.menu.CustomMenu'

JWT_ALLOW_REFRESH = True
JWT_EXPIRATION_DELTA = datetime.timedelta(hours=6)

DATA_UPLOAD_MAX_MEMORY_SIZE = 26214400

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

MEDIA_URL = '/documenti/'
MEDIA_ROOT = BASE_DEV + "dev/rapper/documenti/"

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DEV + 'dev/rapper/static/'

CORS_ORIGIN_ALLOW_ALL = True

GRAPPELLI_ADMIN_TITLE = 'Settore urbanistica e servizi catastali\nProcedure Intranet'

DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
DBBACKUP_STORAGE_OPTIONS = {'location': '/dati/archiviazione/BACKUP'}

WMS_SERVICE = 'http://172.25.193.167:8080/service'

SETTINGS_EXPORT = [
    'STATIC_URL',
    'STATIC_ROOT',
    'MEDIA_URL',
    'MEDIA_ROOT',
    'BASE_DEV',
    'WMS_SERVICE',
]

AUTHENTICATION_BACKENDS = [
    "django_auth_ldap.backend.LDAPBackend",
    'django.contrib.auth.backends.ModelBackend'
]

AUTH_LDAP_SERVER_URI = "ldap://10.10.20.87:7389"

AUTH_LDAP_BIND_DN = "uid=rapper,cn=users,dc=comune,dc=padova,dc=it"
AUTH_LDAP_BIND_PASSWORD = "" #INSERIRE CREDENZIALI ask @granzierof
AUTH_LDAP_USER_DN_TEMPLATE = "uid=%(user)s,cn=users,dc=comune,dc=padova,dc=it"
AUTH_LDAP_USER_SEARCH = LDAPSearch(
    "cn=users,dc=comune,dc=padova,dc=it", ldap.SCOPE_SUBTREE, "(uid=%(user)s)"
)

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {"console": {"class": "logging.StreamHandler"}},
    "loggers": {"django_auth_ldap": {"level": "DEBUG", "handlers": ["console"]}},
}

ADMIN_REORDER = (
    # Keep original label and models
    #'sites',

    # Rename app
    #{'app': 'auth', 'label': 'Authorisation'},

    # Reorder app models
    {'app': 'repertorio', 'models': ('repertorio.repertorioIstanze','repertorio.corrispondenza','repertorio.sottoscrittori','repertorio.catIstanze','repertorio.valutazione','repertorio.modelli','repertorio.istruttorie','repertorio.catIstruttoria')},
    {'app': 'certificati', 'models': ('certificati.CDU','certificati.individuazioni','certificati.log_analisi',)},
    {'app': 'pua', 'models': ('pua.pua','pua.info','pua.provvedimento')},
    {'app': 'django_warp', 'models': ('django_warp.datasets','django_warp.rasterMaps',)},
    {'app': 'vincoli', 'models': ('vincoli.vincoli','vincoli.mibac',)},
    {'app': 'version', 'models': ('version.Version',)},
    {'app': 'admin', 'models': ('admin.LogEntry',)},
    {'app': 'hp', 'models': ('hp.applicazioni',)},
    {'app': 'auth', 'models': ('auth.User','auth.Group', )},
    {'app': 'django_mailbox', 'models': ('django_mailbox.Mailbox', 'django_mailbox.Message', 'django_mailbox.MessageAttachment')},

    # Exclude models
    #{'app': 'auth', 'models': ('auth.User', )},

    # Cross-linked models
    #{'app': 'auth', 'models': ('auth.User', 'sites.Site')},

    # models with custom name
    #{'app': 'auth', 'models': (
    #    'auth.Group',
    #    {'model': 'auth.User', 'label': 'Staff'},
    #)},
)

#DJANGO taggit
TAGGIT_CASE_INSENSITIVE = True
TAGGIT_STRIP_UNICODE_WHEN_SLUGIFYING = True

TAGGIT_TAGS_FROM_STRING = 'taggit_selectize.utils.parse_tags'
TAGGIT_STRING_FROM_TAGS = 'taggit_selectize.utils.join_tags'

TAGGIT_AUTOSUGGEST_CSS_FILENAME = 'autoSuggest-grappelli.css'

MASSEDIT = {
    'ADD_ACTION_GLOBALLY': False,
    'MASS_USERS_GROUP': 'amministrazione'
}