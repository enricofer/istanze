"""istanze URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url, path
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from ajax_select import urls as ajax_select_urls
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as auth_views

from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

import hp.views

import django.views.static

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

admin.autodiscover()

urlpatterns = [
    url(r'^$', hp.views.home_page),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^taggit/', include('taggit_bulk.urls')),
    url(r'^admin/', include('massadmin.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^selectable/', include('selectable.urls')),
    url(r'^warp/', include('django_warp.urls')),
    url(r'^version/', include('version.urls')),
    url(r'^repertorio/', include('repertorio.urls')),
    url(r'^certificati/', include('certificati.urls')),
    url(r'^pua/', include('pua.urls')),
    url(r'^hp/', include('hp.urls')),
    url(r'^vincoli/', include('vincoli.urls')),
    url(r'^ajax_select/', include(ajax_select_urls)),
    url(r'^_nested_admin/', include('nested_admin.urls')),
    url('^accounts/', include('django.contrib.auth.urls')),
    # nell'attuale configurazione con DEBUG=False gunicorn serve in maniera errata i files statici binaria (mimetype sbagliato ??)
    # nell'attesa di configurare opportunamente nginx o apache django serve direttamente i files statici
    url(r'^static/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.STATIC_ROOT, 'show_indexes': settings.DEBUG}),
    url(r'^documenti/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.MEDIA_ROOT, 'show_indexes': settings.DEBUG}),
    path("favicon.ico",  RedirectView.as_view(url=staticfiles_storage.url("favicon.ico"))),
] #+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
