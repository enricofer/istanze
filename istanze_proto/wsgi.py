"""
WSGI config for istanze project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

import os

from django.core.wsgi import get_wsgi_application
#from whitenoise.django import DjangoWhiteNoise
from .settings import STATIC_ROOT, MEDIA_ROOT

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "istanze.settings")

application = get_wsgi_application()
#application = DjangoWhiteNoise(application)
#application.add_files(MEDIA_ROOT, prefix='documenti/')
