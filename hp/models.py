from django.contrib.gis.db import models
from django.conf import settings

import os
from datetime import datetime


__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


tipo_problema_choice = [
    ("badinfo",'Informazione inesatta'),
    ("noinfo",'Informazione mancante'),
    ("errore",'Errore applicazione'),
    ("feature",'Richiesta di nuova funzione'),
]

# Create your models here.
class applicazioni(models.Model):

    def upload_img(instance, filename):
        path = "hp/%d/" % instance.pk
        name,extension = os.path.splitext (filename)
        return os.path.join(path, filename)

    class Meta:
        verbose_name_plural = "Applicazioni"
        verbose_name = "Applicazione"

    catasto_sintassi_validator = "^((((\d+)\:)?(\d+)([a-zA-Z]{1})?\/((\d+)\-)*?(\d+))\s)*(((\d+)\:)?(\d+)([a-zA-Z]{1})?\/((\d+)\-)*?(\d+))$"

    nome = models.CharField(max_length=50)
    link_aiuto = models.CharField(max_length=250, blank=True, null=True)
    link_app = models.CharField(max_length=250)
    sfondo = models.ImageField(upload_to=upload_img, blank=True, null=True)
    descrizione = models.CharField(max_length=250, blank=True, null=True)
    hero = models.BooleanField(default=False)
    ordine = models.IntegerField(default=0)
    referente = models.ForeignKey(settings.AUTH_USER_MODEL,verbose_name=u"Referente interno",blank=True,null=True, on_delete=models.SET_NULL,limit_choices_to={'groups__name': 'settore'},)

    def __str__(self):
        return self.nome

class segnalazioni(models.Model):

    class Meta:
        verbose_name_plural = "Segnalazioni"
        verbose_name = "Segnalazione"

    stato_choice = (
        (1,'aperto'),
        (0,'chiuso'),
    )

    tipo_problema = models.CharField(max_length=10, choices=tipo_problema_choice)
    stato = models.IntegerField(default=1, choices=stato_choice)
    applicazione = models.ForeignKey('applicazioni', limit_choices_to={'hero': False}, on_delete=models.CASCADE)
    descrizione = models.CharField(max_length=1000)
    link = models.CharField(max_length=200)
    segnalazione = models.DateTimeField( verbose_name=u"Data segnalazione", default=datetime.now)
    segnalato_da = models.CharField(help_text="inserito automaticamente", max_length=30, blank=True, null=True)
    risoluzione = models.DateTimeField( verbose_name=u"Data risoluzione", blank=True, null=True)
    risolto_da = models.CharField( verbose_name=u"inserito automaticamente", max_length=30, blank=True, null=True)
    fix = models.CharField( verbose_name=u"fissaggio", max_length=200, blank=True, null=True)

    def __str__(self):
        return '%d-%s' % (self.pk,self.descrizione)

class news(models.Model):

    claim = models.CharField(max_length=30)
    contenuto = models.CharField(max_length=250)
    link = models.CharField(max_length=50)


