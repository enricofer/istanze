from django.shortcuts import render
from .models import applicazioni as modello_applicazioni, segnalazioni, news, tipo_problema_choice
from pua.models import pua
from .forms import segnalazioneForm
from django.http import HttpResponse, HttpResponseRedirect, StreamingHttpResponse, Http404
from django.core.mail import EmailMessage
from django.contrib.auth.models import User

import sys
import os
import requests
#CERTIFI NON FUNZIONA
import certifi

from datetime import datetime
# Create your views here.

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

tipo_problema_dict = dict(tipo_problema_choice)

admin = 'efadmin'

#MEDIAWIKI_BASE_URL="https://rapper.comune.padova.it:8443"
MEDIAWIKI_BASE_URL="http://10.10.21.50:8889"
MEDIAWIKI_ELENCO = "Norme_Tecniche_Operative_(N.T.O)"
#Rapper come nome utente e rapper@gn76v32sj0j1hlfd359p07s7djllr5la 
BOT_LOGIN = 'Rapper'
BOT_PASSWORD = 'rapper@gn76v32sj0j1hlfd359p07s7djllr5la'

def wiki_norme(request):
    try:
        return StreamingHttpResponse( rebuild_norme() )
    except:
        return HttpResponseRedirect("https://rapper.comune.padova.it:8443/index.php/Testo_Corrente_NTO")

def outdate_norme(request):
    articolo_modificato = request.GET.get("page","")
    utente = request.GET.get("user","")
    is_sysop = "sysop" in request.GET.get("groups","")
    if not articolo_modificato:
        raise Http404("ERRORE: SPECIFICARE PAGINA")

    url_nto = MEDIAWIKI_BASE_URL+"/rest.php/v1/page/Testo_Corrente_NTO"
    response = requests.get(url_nto)
    _json = response.json()
    testo = _json["source"]
    tags = _json.get("tags")
    print("TESTO OUTDATED",testo[:100],"...")
    if testo.startswith("*'''LA PAGINA"):
        suff = ""
    else:
        #url = MEDIAWIKI_BASE_URL + "/hp/norme/" [[Testo Corrente NTO|'''Testo Corrente N.T.O.''']]
        url = "Rigenerazione_in_corso_testo_corrente_NTO"
        suff = "'''[%s %s]'''\n\n" % (url, "RIGENERAZIONE TESTO CORRENTE NTO")

    S = requests.Session()
    #S.verify = certifi.where()

    URL = MEDIAWIKI_BASE_URL + "/api.php"

    PARAMS_0 = {
        'action':"query",
        'meta':"tokens",
        'type':"login",
        'format':"json"
    }
    R = S.get(url=URL, params=PARAMS_0)
    DATA = R.json()
    LOGIN_TOKEN = DATA['query']['tokens']['logintoken']
    print("LOGIN_TOKEN",DATA)

    PARAMS_1 = {
        'action':"login",
        'lgname': BOT_LOGIN,
        'lgpassword': BOT_PASSWORD,
        'lgtoken':LOGIN_TOKEN,
        'format':"json"
    }
    R = S.post(URL, data=PARAMS_1)
    DATA = R.json()
    print("LOGIN_DATA",DATA)

    PARAMS_0 = {
        'action':"query",
        'meta':"tokens",
        'type':"csrf",
        'format':"json"
    }
    R = S.get(url=URL, params=PARAMS_0)
    DATA = R.json()
    CSRF_TOKEN = DATA['query']['tokens']['csrftoken']
    print("LOGIN_TOKEN",DATA)

    if is_sysop:
        ptxt = "*'''LA PAGINA E'SUPERATA DALLA MODIFICA DELL'ARTICOLO '%s' DA PARTE DELL'UTENTE '%s'.'''\n%s"
    else:
        ptxt = "*'''L'ARTICOLO '%s' E' INTERESSATO DA MODIFICHE PROPOSTE DALL'UTENTE '%s', IN FASE DI REVISIONE.'''\n%s"

    PARAMS_2 = {
        'action':"edit",
        'pageid': 68,
        'prependtext': "*'''LA PAGINA E'SUPERATA DALLA MODIFICA DEL SEGUENTE ARTICOLO: '%s'.'''\n%s" % (articolo_modificato, utente, suff),
        'token':CSRF_TOKEN,
        'format':"json"
    }
    R = S.post(URL, data=PARAMS_2)
    DATA = R.json()
    print("EDIT DATA",DATA)

    PARAMS_3 = {
        'action':"edit",
        'pageid': 68,
        'tags': "outdated",
        'token':CSRF_TOKEN,
        'format':"json"
    }
    R = S.post(URL, data=PARAMS_3)
    DATA = R.json()
    print("EDIT DATA",DATA)
    return HttpResponse("PAGINA MARCATA COME SUPERATA")

def rebuild_norme():
    url_elenco = MEDIAWIKI_BASE_URL+"/rest.php/v1/page/"+MEDIAWIKI_ELENCO
    response = requests.get(url_elenco)
    elenco_wiki = response.json()
    print (url_elenco, response)
    print (elenco_wiki)
    yield("RIGENERAZIONE NTO</br></br>")
    merge_txt = ""
    for row in elenco_wiki["source"].split("\n"):
        
        if row.startswith(":"):
            page_tit = row.split('|')[0][4:].replace(" ","_")
            url_page = MEDIAWIKI_BASE_URL+"/rest.php/v1/page/"+page_tit
            response = requests.get(url_page)
            print (response)
            if response.status_code == 200:
                yield(page_tit.replace("_"," ")+"</br>")
                page_wiki = response.json()
                merge_txt += page_wiki["source"] + "\n"
            else:
                yield("PAGINA '" + page_tit+"' NON TROVATA</br>")
        elif not row.startswith("__NOTOC__"):
            yield(row+"</br>")
            startrow = row[:3]
            if startrow in ("TIT","ALL","ELE",):
                merge_txt += ("="+ row + "=\n")
            elif startrow in ("CAP",):
                merge_txt += ("=="+ row + "==\n")


    S = requests.Session()
    S.verify = certifi.where()

    URL = MEDIAWIKI_BASE_URL + "/api.php"

    # Retrieve login token first
    PARAMS_0 = {
        'action':"query",
        'meta':"tokens",
        'type':"login",
        'format':"json"
    }
    R = S.get(url=URL, params=PARAMS_0)
    DATA = R.json()
    LOGIN_TOKEN = DATA['query']['tokens']['logintoken']
    print("LOGIN_TOKEN",DATA)

    # Send a post request to login. Using the main account for login is not
    # supported. Obtain credentials via Special:BotPasswords
    # (https://www.mediawiki.org/wiki/Special:BotPasswords) for lgname & lgpassword

    PARAMS_1 = {
        'action':"login",
        'lgname': BOT_LOGIN,
        'lgpassword': BOT_PASSWORD,
        'lgtoken':LOGIN_TOKEN,
        'format':"json"
    }
    R = S.post(URL, data=PARAMS_1)
    DATA = R.json()
    print("LOGIN_DATA",DATA)

    # Retrieve login token first
    PARAMS_0 = {
        'action':"query",
        'meta':"tokens",
        'type':"csrf",
        'format':"json"
    }
    R = S.get(url=URL, params=PARAMS_0)
    DATA = R.json()
    CSRF_TOKEN = DATA['query']['tokens']['csrftoken']
    print("LOGIN_TOKEN",DATA)

    PARAMS_2 = {
        'action':"edit",
        'pageid': 68,
        'text': merge_txt,
        'token':CSRF_TOKEN,
        'format':"json"
    }
    R = S.post(URL, data=PARAMS_2)
    DATA = R.json()
    print("EDIT DATA",DATA)

    PARAMS_3 = {
        'action':"edit",
        'pageid': 68,
        'tags': "",
        'token':CSRF_TOKEN,
        'format':"json"
    }
    R = S.post(URL, data=PARAMS_3)
    DATA = R.json()
    print("EDIT DATA",DATA)

    #"https://rapper.comune.padova.it:8443/index.php/Testo_Corrente_NTO"
    yield( "</br></br>" + "RIGENERAZIONE TERMINATA" )
    raise ValueError('')


def get_notifiche (segnalazione):
    print ("get_notifiche")
    admin_user = User.objects.get(username=admin)
    piani_app = modello_applicazioni.objects.get(nome='Pianificazione comunale')
    notifiche = []
    if segnalazione.tipo_problema == "errore":
        notifiche.append(admin_user.email)
        print(1)
    else:
        print(2)
        if segnalazione.applicazione == piani_app:
            print(3)
            path_elems = segnalazione.link.split('/')
            try:
                piano_pk = None
                for i in range(0, len(path_elems)):
                    elem = path_elems[-(i+1)]
                    print (i, -(i+1), elem)
                    if elem.isnumeric():
                        piano_pk = int(elem)
                        break
                print ("piano_pk",piano_pk)
                if piano_pk:
                    piano = pua.objects.get(pk=piano_pk)
                    print(4)
                    print("curatore",piano.curatore) 
                    if piano and piano.curatore:
                        print(5)
                        notifiche.append(piano.curatore.email)
            except Exception as e:
                print ("Applicazione piani: Piano non trovato in link", segnalazione.link, e)
            notifiche.append(segnalazione.applicazione.referente.email)
            print(6)
        else:
            notifiche.append(segnalazione.applicazione.referente.email)
            print(7)
    print(notifiche)
    return notifiche


def home_page(request):
    fields = {
        "applicazione": None,
        "tipo_problema": None,
        "descrizione": None,
        "link": None,
    }
    for field in fields:
        print (field, getattr(request, request.method), request.method)
        fields[field] = getattr(request, request.method).get(field,None)

    print (fields)

    if all(value != None for value in fields.values()):

        app = modello_applicazioni.objects.get(pk=fields["applicazione"])
        segnalazione = segnalazioni()
        segnalazione.tipo_problema = fields["tipo_problema"]
        segnalazione.descrizione = fields["descrizione"]
        segnalazione.link = fields["link"]
        segnalazione.applicazione = app
        segnalazione.segnalato_da = request.user.username
        segnalazione.save()

        email = EmailMessage(
            'Settore Urbanistica e Servizi catastali - procedure informatiche: notifica di segnalazione %d di %s in %s' % (segnalazione.pk, tipo_problema_dict[segnalazione.tipo_problema], segnalazione.applicazione),
            """
Ricevuta dall'utente %s la seguente segnalazione di %s in %s:

%s

rif: %s

Notifica automatica di segnalazione, non rispondere a questo messaggio
            """ % (
                segnalazione.segnalato_da, 
                tipo_problema_dict[segnalazione.tipo_problema], 
                segnalazione.applicazione,
                segnalazione.descrizione,
                segnalazione.link
            ),
            'rapper1@comune.padova.it',
            [request.user.email],
            get_notifiche(segnalazione),
            reply_to=['no_reply@comune.padova.it'],
        )

        email.send(fail_silently=False) 

    intestazione = modello_applicazioni.objects.filter(hero=True).first()
    applicazioni = modello_applicazioni.objects.filter(hero=False).order_by("ordine")
    if request.user.groups.all().first() and request.user.groups.all().first().name != "ospiti" :
        segnalazioni_aperte = segnalazioni.objects.filter(stato=1).order_by("-segnalazione")
        segnalazioni_chiuse = segnalazioni.objects.filter(stato=0).order_by("-risoluzione")
    else:
        segnalazioni_aperte = []
        segnalazioni_chiuse = []
    print("segnalazioni_chiuse",segnalazioni_chiuse)
    notizie = news.objects.all().order_by('-id')[:5]
    nuova_segnalazione = segnalazioneForm()
    return render(request, 'hp.html', {
        "intestazione":intestazione,
        "applicazioni":applicazioni,
        "notizie": notizie,
        "segnalazioni_aperte": segnalazioni_aperte,
        "segnalazioni_chiuse": segnalazioni_chiuse,
        "nuova_segnalazione": nuova_segnalazione,
    })

def chiudi(request):
    if request.method == 'POST':
        print (request.POST)
        segnalazione = segnalazioni.objects.get(pk=int(request.POST.get("pk",0)))
        segnalazione.stato = 0
        segnalazione.fix = request.POST.get("fix",'')
        segnalazione.risolto_da = request.user.username
        segnalazione.risoluzione = datetime.now()
        print (segnalazione)
        segnalazione.save()

        email = EmailMessage(
            'Settore Urbanistica e Servizi catastali - procedure informatiche: notifica di risoluzione problema %d di %s in %s' % (segnalazione.pk, tipo_problema_dict[segnalazione.tipo_problema], segnalazione.applicazione),
            """
Il problema di %s in %s segnalato dall'utente %s in data %s: %s

è stato risolto in data %s dall'utente %s: %s 

rif: %s

Notifica automatica di segnalazione, non rispondere a questo messaggio
            """ % (
                tipo_problema_dict[segnalazione.tipo_problema], 
                segnalazione.applicazione,
                segnalazione.segnalato_da, 
                segnalazione.segnalazione.strftime('%d/%m/%Y %H:%M:%S'),
                segnalazione.descrizione,
                segnalazione.risoluzione.strftime('%d/%m/%Y %H:%M:%S'),
                segnalazione.risolto_da, 
                segnalazione.fix, 
                segnalazione.link
            ),
            'rapper1@comune.padova.it',
            [request.user.email],
            get_notifiche(segnalazione),
            reply_to=['no_reply@comune.padova.it'],
        )

        email.send(fail_silently=False) 

    return HttpResponseRedirect('/')

def riapri(request):
    if request.method == 'POST':
        print (request.POST)
        segnalazione = segnalazioni.objects.get(pk=int(request.POST.get("pk",0)))
        segnalazione.stato = 1
        segnalazione.segnalato_da = request.user.username
        segnalazione.fix = ""
        segnalazione.risolto_da = ""
        segnalazione.risoluzione = None
        print (segnalazione)
        segnalazione.save()
        
        email = EmailMessage(
            'Settore Urbanistica e Servizi catastali - procedure informatiche: notifica di segnalazione %d di %s in %s' % (segnalazione.pk, tipo_problema_dict[segnalazione.tipo_problema], segnalazione.applicazione),
            """
Ricevuta dall'utente %s la seguente segnalazione di %s in %s:

%s

rif: %s

Notifica automatica di segnalazione, non rispondere a questo messaggio
            """ % (
                segnalazione.segnalato_da, 
                tipo_problema_dict[segnalazione.tipo_problema], 
                segnalazione.applicazione,
                segnalazione.descrizione,
                segnalazione.link
            ),
            'rapper1@comune.padova.it',
            [request.user.email],
            get_notifiche(segnalazione),
            reply_to=['no_reply@comune.padova.it'],
        )

        email.send(fail_silently=False) 

    return HttpResponseRedirect('/')

def oauth(request):
    return HttpResponseRedirect('/')