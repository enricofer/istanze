from django import forms
#import floppyforms as forms
from django.forms import ModelForm

from .models import segnalazioni, tipo_problema_choice, applicazioni

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"


class segnalazioneForm(forms.ModelForm):

    tipo_problema = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    applicazione = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    descrizione = forms.CharField(max_length=1000,widget=forms.Textarea(attrs={'class': 'form-control', "rows":"4"}))
    link = forms.CharField(max_length=200,widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = segnalazioni
        fields = ['tipo_problema', 'applicazione', 'descrizione', 'link']



    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        apps = applicazioni.objects.filter(hero=False)
        choices = [[None,'------']]
        for app in apps:
            choices.append([app.pk,app.nome])
        self.fields['applicazione'] = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=choices)
        new_problema_choices = [(None,'------')] + tipo_problema_choice
        self.fields['tipo_problema'] = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=new_problema_choices)
        

