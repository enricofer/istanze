from django.conf.urls import url,include
from django.urls import path
#from django.contrib.auth.views import login, logout, password_change, password_change_done
import django.contrib.auth.views

from . import views

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2017, Enrico Ferreguti"
__license__ = "GPL3"

urlpatterns = [
    url(r'^chiudi/$', views.chiudi, name='chiudi'),
    url(r'^riapri/$', views.riapri, name='chiudi'),
    url(r'^oauth/$', views.oauth, name='oauth'),
    url(r'^norme/$', views.wiki_norme, name='norme'),
    url(r'^outdate/$', views.outdate_norme, name='outdate'),
    url(r'^login/', django.contrib.auth.views.LoginView.as_view(), name='auth_login'),
    url(r'^logout/', django.contrib.auth.views.LogoutView.as_view(next_page=None), name='auth_logout'),
    #url(r'^change-password/$', password_change,{'template_name': 'registration/password_change_form.html', 'post_change_redirect': 'cdu_password_change_done'},name='cdu_change-password'),
    #url(r'^password-changed/$', password_change_done,{'template_name': 'registration/password_change_done.html'},name='cdu_password_change_done'),
]
