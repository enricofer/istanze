from django.contrib import admin
from .models import applicazioni, segnalazioni, news

# Register your models here.
admin.site.register(applicazioni)
admin.site.register(segnalazioni)
admin.site.register(news)