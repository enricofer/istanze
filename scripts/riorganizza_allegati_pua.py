from django.conf import settings
from pua.models import pua, info, files_allegati, tipo_allegato_choice

tipo_allegato_dict = dict(tipo_allegato_choice)
#PUA = pua.objects.filter(tipologia='PUA').order_by('id_pua')
#PUA = pua.objects.filter(tipologia='VPL').order_by('id_pua')#.filter(id_pua__startswith='V')
PUA = pua.objects.filter(id_pua__startswith='ZZZ')
n = 1
for piano in PUA:
    allegati_ref = files_allegati.objects.filter(rif_pua=piano)
    if allegati_ref:
        print(n,piano, allegati_ref )
        n += 1
        #aggiungere l'allegato come iter
        for allegato in allegati_ref:
            nuovo_iter = info()
            nuovo_iter.cat = 'DOC'
            nuovo_iter.titolo = tipo_allegato_dict[allegato.tipo]
            nuovo_iter.data = allegato.data
            nuovo_iter.descrizione = "Allegato unito al piano come iter - procedura automatica"
            nuovo_iter.rif_pua = piano
            #nuovo_iter.files_allegati_set.add(allegato)
            print (nuovo_iter)
            nuovo_iter.save()
            allegato.rif_info = nuovo_iter
            allegato.rif_pua = None
            allegato.save()
            #piano.info_set.add(nuovo_iter)
            #piano.files_allegati.remove(allegato)
            piano.save()