from repertorio.models import repertorioIstanze, corrispondenza, catIstanze
from repertorio.views import nomina_istanza

avvisi = repertorioIstanze.objects.filter(categoriaIstanza__pk__in=range(24,30)).order_by('pk')


for avviso in avvisi:
    avviso.ditta = nomina_istanza(avviso)
    print (avviso, avviso.pk, avviso.ditta, nomina_istanza(avviso))
    avviso.save()