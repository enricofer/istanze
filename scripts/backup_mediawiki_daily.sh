#!/bin/bash -x
DIR="/dati/backup/mediawiki"
FILE="$DIR/rapper_mediawiki_mariadb_daily_$(date +%Y%m%d).tar.gz"
tar -czvf "$FILE" /dati/mediawiki
find $DIR/rapper_mediawiki_mariadb_daily_* -mtime +7 -exec rm {} \;

