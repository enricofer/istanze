import vobject
import datetime
import uuid
from django.core.mail import EmailMessage


schedule_uid = str(uuid.uuid4()).replace("-","").upper()
cal = vobject.newFromBehavior('vcalendar')
cal.add('calscale').value = "GREGORIAN"
cal.add('method').value = "REQUEST"
cal.add('vevent')
cal.vevent.add('summary').value = "This is a note2"
utc = vobject.icalendar.utc
print ("utc",utc)
attendee = cal.vevent.add('ATTENDEE')
attendee.role_param = 'REQ-PARTICIPANT'
attendee.partstat_param = 'NEEDS-ACTION'
attendee.rsvp_param = 'TRUE'
attendee.cn_param = 'Enrico Ferreguti'
attendee.value = 'mailto:ferregutie@comune.padova.it'
cal.vevent.add('X-MOZ-SEND-INVITATIONS').value = "TRUE"
organizer = cal.vevent.add('ORGANIZER')
organizer.cn_param = 'Comune Padova'
organizer.value = "mailto:rapper1@comune.padova.it"
start = cal.vevent.add('dtstart')
#start.value = datetime.datetime(2021, 4, 30, tzinfo = utc)
start.value = datetime.datetime.fromisoformat('2021-04-29 15:30:00')
end = cal.vevent.add('dtend')
end.value = datetime.datetime.fromisoformat('2021-04-29 16:00:00')
cal.vevent.add('class').value = 'PUBLIC'
cal.vevent.add('uid').value = schedule_uid
cal.vevent.add('description').value = 'Sample DESCRIPTION2: https://meet.jit.si/'+schedule_uid

print (cal.serialize())

msg = EmailMessage(
    "soggetto: nuovo evento",
    'Collegamento a conferenza: https://meet.jit.si/'+schedule_uid,
    "Settore Urbanistica e Servizi Catastali - Comune di Padova <rapper1@comune.padova.it>",
    ['enricofer@gmail.com','ferregutie@comune.padova.it'],
    attachments = [
         ("", cal.serialize(), "text/calendar; method=REQUEST; charset=utf-8")
    ]
)
#msg.content_subtype = "html"  
msg.send()

