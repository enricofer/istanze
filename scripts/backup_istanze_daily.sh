#!/bin/bash -x
DIR="/dati/backup/db"
FILE="$DIR/rapper_istanze_postgres__daily_$(date +%Y%m%d).sql"
pg_dump istanze > $FILE
zip -9 $FILE.zip $FILE
rm $FILE
find $DIR/rapper_istanze_postgres__daily_* -mtime +30 -exec rm {} \;

