#!/bin/bash -x
DIR="/dati/backup/db"
FILE="$DIR/rapper_visio_postgres_weekly_$(date +%Y%m%d).sql"
pg_dump VISIO > $FILE
zip -9 $FILE.zip $FILE
rm $FILE
find $DIR/rapper_visio_postgres_weekly_* -mtime +30 -exec rm {} \;

