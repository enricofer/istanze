from pnrr.models import progetto

progetti = progetto.objects.all()

print (progetti)

for obj in progetti:

        if obj.the_geom and obj.the_geom.valid and not obj.the_geom.empty:
            max_extent_x = obj.the_geom.extent[2]-obj.the_geom.extent[0]
            max_extent_y = obj.the_geom.extent[3]-obj.the_geom.extent[1]
            if max_extent_x < 507 and max_extent_y<507:
                scale_base = 564/2
                scale = 2000
            elif max_extent_x < 1269 and max_extent_y<1269:
                scale_base = 1410/2
                scale = 5000
            elif max_extent_x < 1900 and max_extent_y<1900:
                scale_base = 2115/2
                scale = 7500
            elif max_extent_x < 2538 and max_extent_y<2538:
                scale_base = 2820/2
                scale = 10000
            elif max_extent_x < 5070 and max_extent_y<5070:
                scale_base = 5640/2
                scale = 20000
            elif max_extent_x < 12690 and max_extent_y<12690:
                scale_base = 14100/2
                scale = 50000
            elif max_extent_x < 19000 and max_extent_y<19000:
                scale_base = 21150/2
                scale = 75000
            else:
                scale_base = 28200/2
                scale = 100000
            print (obj, obj.scala_rappresentazione, scale)
            obj.scala_rappresentazione = scale
            obj.save()