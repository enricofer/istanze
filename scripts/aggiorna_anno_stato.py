from pua.models import pua, info, files_allegati, tipo_allegato_choice

pua = pua.objects.all()

for piano in pua:
    if piano.anno_estratto:
        piano.anno_stato = int(piano.anno_estratto)
    if piano.anno_stato != piano.anno_estratto:
        print (piano, piano.anno_stato, piano.anno_estratto)
    piano.save()
