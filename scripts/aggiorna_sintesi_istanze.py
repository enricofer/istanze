from repertorio.models import repertorioIstanze,istruttorie

osservazioni = repertorioIstanze.objects.filter(categoriaIstanza__pk=45).order_by('pk')

print (osservazioni)

for osservazione in osservazioni:
    
    def update_valutazione_istanza(istanza_obj,istruttorie_model):
        ultima_sintesi = ""
        ultima_valutazione = ""
        #istanza_obj = repertorioIstanze.objects.get(pk=istanza_id)
        for istruttoria in istruttorie_model.objects.filter(istanza_id=istanza_obj.pk).order_by('pk'):
            ultima_sintesi = istruttoria.sintesi
            ultima_valutazione = istruttoria.dettaglio
        if ultima_sintesi or ultima_valutazione:
            istanza_obj.valutazione = (ultima_sintesi or "") + ('\n\n' if ultima_sintesi and ultima_valutazione else '') + (ultima_valutazione or "")
        print (istanza_obj.pk,istanza_obj.valutazione)
        #istanza_obj.save()


    def update_istanza(istanza_obj,istruttorie_model):
        riassunto_note = ""
        ultima_sintesi = ""
        ultima_valutazione = ""
        ultima_destAssegnata = ""
        ultima_destRichiesta = ""
        ultimo_stato = ""
        #istanza_obj = repertorioIstanze.objects.get(pk=istanza_id)
        for istruttoria in istruttorie_model.objects.filter(istanza_id=istanza_obj.pk).order_by('pk'):
            istruttoria.sintesi = istruttoria.note
            istruttoria.save()
            if istruttoria.rapporto_istruttoria:
                riassunto_note += istruttoria.rapporto_istruttoria
                riassunto_note += '\n'
            ultima_sintesi = istruttoria.sintesi
            ultima_valutazione = istruttoria.dettaglio
            ultima_destAssegnata = istruttoria.destAssegnata
            ultima_destRichiesta = istruttoria.destRichiesta
            ultimo_stato = istruttoria.accoglimento if istruttoria.accoglimento != 'nv' else 'in'
        istanza_obj.note = riassunto_note
        if ultima_sintesi or ultima_valutazione:
            istanza_obj.valutazione = (ultima_sintesi or "") + ('\n\n' if ultima_sintesi and ultima_valutazione else '') + (ultima_valutazione or "")
        if ultima_destAssegnata:
            istanza_obj.destAssegnata = ultima_destAssegnata
        if ultima_destRichiesta:
            istanza_obj.destRichiesta = ultima_destRichiesta
        if ultimo_stato:
            istanza_obj.stato = ultimo_stato
        else:
            istanza_obj.stato = "nv"
        print (istanza_obj.pk,istanza_obj.valutazione)
        istanza_obj.save()

    print ("------\n", )
    update_valutazione_istanza(osservazione, istruttorie)
    print (osservazione.pk,osservazione.pk,osservazione.ditta, "\n------\n\n")
    #osservazione.save()