#!/bin/bash -x
source /home/rapper/.VENV/rapper_prod/bin/activate
DIR="/home/rapper/prod/rapper/backup"
FILE="$DIR/rapper_backup_daily_$(date +%Y%m%d).json"
python /home/rapper/prod/rapper/manage.py dumpdata > $FILE
zip -9 $FILE.zip $FILE
rm $FILE
find $DIR/rapper_backup_daily_* -mtime +30 -exec rm {} \;
