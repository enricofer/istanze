from repertorio.models import repertorioIstanze,istruttorie

avvisi = repertorioIstanze.objects.filter(categoriaIstanza__pk__in=range(24,30)).order_by('pk')

print (avvisi)

for avviso in avvisi:
    

    def update_istanza(istanza_obj,istruttorie_model):
        riassunto_note = ""
        ultima_valutazione = ""
        ultima_destAssegnata = ""
        ultima_destRichiesta = ""
        ultimo_stato = ""
        #istanza_obj = repertorioIstanze.objects.get(pk=istanza_id)
        for istruttoria in istruttorie_model.objects.filter(istanza_id=istanza_obj.pk).order_by('pk'):
            if istruttoria.rapporto_istruttoria:
                riassunto_note += istruttoria.rapporto_istruttoria
                riassunto_note += '\n'
            ultima_valutazione = istruttoria.dettaglio
            ultima_destAssegnata = istruttoria.destAssegnata
            ultima_destRichiesta = istruttoria.destRichiesta
            ultimo_stato = istruttoria.accoglimento if istruttoria.accoglimento != 'nv' else 'in'
        istanza_obj.note = riassunto_note
        if ultima_valutazione:
            istanza_obj.valutazione = ultima_valutazione
        if ultima_destAssegnata:
            istanza_obj.destAssegnata = ultima_destAssegnata
        if ultima_destRichiesta:
            istanza_obj.destRichiesta = ultima_destRichiesta
        if ultimo_stato:
            istanza_obj.stato = ultimo_stato
        else:
            istanza_obj.stato = "nv"
        istanza_obj.save()
    print ("------\n",avviso.pk)
    update_istanza(avviso, istruttorie)
    print (avviso.ditta, avviso.stato,"------\n")
    avviso.save()