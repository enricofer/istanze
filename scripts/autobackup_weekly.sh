#!/bin/bash
BEFOREAWEEK=$(date --date "7 days ago" '+%Y-%m-%d')
DIR="/home/rapper/prod/rapper/backup"
SOURCEFILE="$DIR/rapper_backup_daily_$BEFOREAWEEK.zip"
TARGETFILE="$DIR/rapper_backup_weekly_$BEFOREAWEEK.zip"
mv $SOURCEFILE $TARGETFILE
find $DIR/rapper_backup_weekly_* -mtime +120 -exec rm {} \;

