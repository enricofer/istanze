#!/bin/bash -x
DIR="/dati/backup/mediawiki"
FILE="$FILE/rapper_mediawiki_mariadb_weekly_$(date +%Y%m%d).tar.gz"
tar -czvf "$FILE" /dati/mediawiki
find $DIR/rapper_mediawiki_mariadb_weekly_* -mtime +60 -exec rm {} \;

