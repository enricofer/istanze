from repertorio.models import repertorioIstanze, istruttorie, valutazione

for istanza in repertorioIstanze.objects.all():
    matrice = valutazione.objects.filter(catIstanza__pk=istanza.categoriaIstanza.pk)
    if matrice:
        istruttorie_related = istruttorie.objects.filter(istanza__pk=istanza.pk).order_by("pk")
        for istruttoria in istruttorie_related:
            if istruttoria.matrice_valutazione:
                istanza.matrice_valutazione = istruttoria.matrice_valutazione
                print (istanza,istanza.categoriaIstanza)
                print (istruttoria.matrice_valutazione)
                #istanza.save()
                break
