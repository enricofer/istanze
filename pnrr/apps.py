from django.apps import AppConfig


class PnrrConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pnrr'
