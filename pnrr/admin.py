from django import forms
from django.contrib.gis import admin
from django.contrib.admin import helpers
from django.utils.html import format_html
from django.shortcuts import render
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry,MultiPolygon,Polygon
from django.http import FileResponse, HttpResponse, HttpResponseRedirect

import nested_admin
import os
import json
import xlsxwriter
from datetime import datetime

from .models import categoria,progetto,files_allegati

from pua.dxf_decode import dxf_decode

regoleDefinizione = {
"CONFINE":"Polygon ((1719950.38655980210751295 5045167.14118372369557619, 1739277.01541306986473501 5042557.34093711525201797, 1740052.90197287243790925 5022878.03637484647333622, 1735891.32860665791667998 5017940.57644882891327143, 1714589.71578298346139491 5019421.81442663446068764, 1711556.70468557276763022 5036843.99445129465311766, 1714237.04007398220710456 5045237.67632552422583103, 1719950.38655980210751295 5045167.14118372369557619))",
"LIVELLI":"""{"PERIMETRO":{"obbligatorio":true,"geometria":"POLYGON","min":0,"max":99999999,"discontinuo": true,"info_tipo":"string", "info":"","db":"sup_ambito"}}"""
}

#admin.site.disable_action('delete_selected')

class allegatiInlineFormset(forms.models.BaseInlineFormSet):

    def save_new(self, form, commit=True):
        
        obj = super().save_new(form, commit=False)
        print ("OBJ", obj.documento.path)
        #setattr(obj, self.date_field, datetime.now())
        if commit:
            obj.save()
        return obj
    

    def process_dxf(self):
        for form in self.forms:
            print (form.changed_data)
            if (not form.cleaned_data.get('id') or 'documento' in form.changed_data or 'tipo' in form.changed_data) and form.cleaned_data.get('tipo') == 'DXF':
                dir_path = os.path.join(settings.MEDIA_ROOT,"tmp","dxf")
                decoder = dxf_decode(regoleDefinizione,dir_path,srid = 3003)
                if 'tipo' in form.changed_data and not 'documento' in form.changed_data:
                    file_path = form.instance.documento.path
                else:
                    #file_path = os.path.join(settings.MEDIA_ROOT,str(form.cleaned_data.get('documento')))
                    file_path = os.path.join(dir_path,str(form.cleaned_data.get('documento')))
                    with open(file_path, "w") as raw_geom_file:
                        for chunk in form.cleaned_data['documento'].chunks():
                            raw_geom_file.write(chunk.decode("utf-8"))
                return decoder.validate(file_path)
        return {"esito": True, "mesg":["nessun dxf processato"]}


    def clean(self):
        clean_risultato = self.process_dxf()
        print ("DXF", json.dumps(clean_risultato))
        if not clean_risultato["esito"]:
            raise forms.ValidationError("File DXF non valido: %s" % "; ".join(clean_risultato["mesg"]) )


class allegatiFormsetMixin(object):
    formset = allegatiInlineFormset

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        formset.request = request
        return formset

class allegatiInline(allegatiFormsetMixin,nested_admin.NestedStackedInline):

    model = files_allegati
    extra = 0
    readonly_fields = ()
    fields = (('tipo','titolo','data','documento'),)


class progettoForm(forms.ModelForm):

    class Meta(object):
        model = progetto
        fields = '__all__'

    wkt_geom = forms.CharField(required=False, widget=forms.Textarea( attrs={'rows':'2', 'cols': '10'}), initial='')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'instance' in kwargs and kwargs['instance']:  
            if kwargs['instance'].the_geom:
                wkt_geom_txt = kwargs['instance'].the_geom.wkt
                self.fields['wkt_geom'].widget.attrs.update({'placeholder': wkt_geom_txt})

    def clean_wkt_geom(self):
        wkt_geom = self.cleaned_data['wkt_geom']
        if wkt_geom != '':
            if wkt_geom == "CANCELLA":
                self.instance.the_geom = None
                self.instance.save()
                return ""
            try:
                print ('SET GEOM TO WKT')
                new_geom = GEOSGeometry(wkt_geom, srid=3003)
                if isinstance(new_geom, Polygon):
                    new_geom = MultiPolygon(new_geom)
                self.instance.the_geom = new_geom
                self.instance.save()
            except Exception as e:
                print ("GEOM NON VALIDA", e)
                raise forms.ValidationError("il formato della geometria non è valido")
        return wkt_geom

def get_schede_link(template, pks='*'):

    return """https://rapper.comune.padova.it/qgisserver
?MAP=/dati/archiviazione/servizi/DEFAULT/PROGETTI.qgs
&SERVICE=WMS
&REQUEST=GetPrint
&TEMPLATE=%s
&version=1.3.0
&FORMAT=pdf
&DPI=150
&CRS=EPSG:3003
&ATLAS_PK=%s
    """ % (template,pks)

def export_tutte_schede(modeladmin, request, queryset):
    service_link = get_schede_link('SCHEDA_PROGETTO_A3')
    return HttpResponseRedirect(service_link.replace("\n", ""))

export_tutte_schede.short_description = "Esporta il plico in A3 di tutti i progetti"
export_tutte_schede.acts_on_all = True

def export_schede(modeladmin, request, queryset):
    ids = ''
    for item in queryset:
        ids += str(item.pk) + ','
    ids = ids[:-1]
    service_link = get_schede_link('SCHEDA_PROGETTO_A3',ids)
    return HttpResponseRedirect(service_link.replace("\n", ""))

export_schede.short_description = "Esporta il plico dei progetti selezionati"

def export_xlsx(modeladmin, request, queryset):
    def writerow(sheet, rowcount, row):
        for i, cell in enumerate(row):
            sheet.write(rowcount,i,cell)
        return rowcount + 1

    xlsx_file = os.path.join(settings.MEDIA_ROOT,'pnrr','progetti_%s.xlsx' % datetime.now().strftime('%y%m%d'))
    pnrr_workbook = xlsxwriter.Workbook(xlsx_file)

    euro_format = pnrr_workbook.add_format({'num_format': '#.### €'})
    euro_format.set_bottom()
    header_format = pnrr_workbook.add_format({'font_name': 'Arial', 'font_size': 10,'bold': True, 'bg_color': '#D7DFD5'})
    header_format.set_top(2)
    header_format.set_bottom(2)
    cat_format = pnrr_workbook.add_format({'font_name': 'Arial', 'font_size': 9,'italic': True, 'bg_color': '#e7efe4'})
    cat_format.set_text_wrap()
    cat_format.set_bottom()
    cell_format = pnrr_workbook.add_format({'font_name': 'Arial', 'font_size': 9})
    cell_format.set_text_wrap()
    cell_format.set_bottom()

    progetti_worksheet = pnrr_workbook.add_worksheet("progetti")
    
    #for col in range(0,21):


    count = writerow(progetti_worksheet,0,[
        'categoria', 
        'categoria_id', 
        'id',
        'id_progetto', 
        'priorita',
        'codice_opera',
        'cup',
        'oggetto',
        'importo_progetto',
        'finanziamento_ministero',
        'finanziamento_altro',
        'determina_rup',
        'rup',
        'stato_finanziamento',
        'stato_avanzamento',
        'avanzamento_stimato',
        'importo_lavori',
        'inizio_lavori',
        'fine_lavori',
        'extracosti',
        'note'
    ])

    for item in queryset:

        count = writerow(progetti_worksheet, count,[
            item.categoria.titolo, 
            item.categoria.pk, 
            item.pk,
            item.id_progetto or ' ', 
            item.priorita or ' ',
            item.codice_opera or ' ',
            item.cup or ' ',
            item.oggetto or ' ',
            item.importo_progetto or ' ',
            item.finanziamento_ministero or ' ',
            item.finanziamento_altro or ' ',
            item.determina_rup or ' ',
            item.rup or ' ',
            'FINANZIATO' if item.stato_finanziamento else 'NON_FINANZIATO',
            item.stato_avanzamento.label if item.stato_avanzamento else ' ',
            item.avanzamento_stimato or ' ',
            item.importo_lavori or ' ',
            item.inizio_lavori or ' ',
            item.fine_lavori or ' ',
            item.extracosti or ' ',
            item.note or ' '
        ])

    progetti_worksheet.set_row(0, 20, header_format)
    progetti_worksheet.set_column('A:B', 25, cat_format)
    progetti_worksheet.set_column('C:Z', 25, cell_format)
    progetti_worksheet.set_column('I:K', 25, euro_format)
    progetti_worksheet.set_column('T:U', 25, euro_format)
    progetti_worksheet.set_column('U:Z', 25, cell_format)

    pnrr_workbook.close()
    return FileResponse(open(xlsx_file, 'rb'), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')

export_xlsx.short_description = "Esporta tutti i dati in foglio xlsx"
export_xlsx.acts_on_all = True

def mappa_tutto(modeladmin, request, queryset):
    return render(request, 'pnrr_mappa.html', {})

mappa_tutto.short_description = "Mappa dei progetti"
mappa_tutto.acts_on_all = True

@admin.register(progetto)
class progettoAdmin(nested_admin.NestedModelAdmin, admin.OSMGeoAdmin):#admin.OSMGeoAdmin):
    #change_form_template = 'admin/pua/change_form.html'
    #change_list_template = 'admin/pua/change_list.html'
    list_per_page = 14
    default_lon = 1725155
    default_lat = 5032083
    default_zoom = 13
    max_resolution = 350000
    num_zoom = 28
    max_zoom = 28
    min_zoom = 10
    map_width = 750
    map_height = 500
    map_srid = 3003
    wms_url = "https://rapper.comune.padova.it/mapproxy/"
    wms_layer = 'PI2030,ORTOFOTO2021,PAT_1_vincoli'
    wms_name = 'PI2030'
    wms_options = {'transparent':"true",'format': 'image/png'}
    map_template = 'admin/pua/openlayers_extralayers.html'
    modifiable = False
    readonly_fields = ('mappa_sola_lettura','ha_geometria')
    exclude = []
    form = progettoForm
    inlines = [allegatiInline, ] #informazioniInline,
    list_display = ('id_progetto','categoria','cup','oggetto', 'stato_finanziamento','stato_avanzamento','rup','ha_geometria')
    search_fields = ['id_progetto','cup','oggetto','rup','codice_opera']
    list_filter = ('categoria', 'stato_finanziamento','stato_avanzamento')
    ordering = ['id_progetto','oggetto','categoria']
    actions = [mappa_tutto, export_schede, export_xlsx]
    class Media:
        js = (
            "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js",
            "https://cdnjs.cloudflare.com/ajax/libs/ol3/4.6.5/ol.js",
            "https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.2.1/proj4.js",
            "https://unpkg.com/ol-layerswitcher@3.2.0",
            "https://unpkg.com/ol-popup@2.0.0",
            "repertorio/js/my_admin.js",
            "certificati/js/salva_stato_layers.js",
            "repertorio/mappeDiBase.js",
            "repertorio/repertorio_finestramappa.js",
        )
        css = {
            'all': (
                'https://cdnjs.cloudflare.com/ajax/libs/ol3/4.6.5/ol.css', 
                'https://unpkg.com/ol-popup@2.0.0/src/ol-popup.css',
                'https://unpkg.com/ol-layerswitcher@3.2.0/src/ol-layerswitcher.css',
                #'repertorio/css/my_admin.css',
                'repertorio/repertorio_finestramappa.css'
            ) #'pua/pua_finestramappa.css'
        }

    def mappa_sola_lettura(self, obj):
        if obj.pk:

            error = "no_err"
            ro_geom = obj.the_geom.wkt
            warp_dataset = "undefined" #inserire il riferimento ad un dataset di warp per averlo in mappa
            zonizzazione_vector = ''

            html= '''<div id="map"></div><script>window.onload = loadFinestraMappa('%s','%s', '%s', '%s')</script>''' % (ro_geom, zonizzazione_vector, warp_dataset, error)
            return format_html(html)
        else:
            return ''
    mappa_sola_lettura.short_description = ''

    def ha_geometria(self,obj):
        return obj.ha_geometria
    ha_geometria.boolean = True

    def get_fieldsets(self, request, obj=None):
            return (
                ("Intestazione", {
                    'classes': ('grp-collapse grp-open',),
                    'fields': (
                        ('id_progetto', 'priorita','categoria'),
                        ('oggetto',),
                        ('codice_opera', 'capitolo', 'cup',), 
                    )
                }),
                ("importi", {
                    'classes': ('grp-collapse grp-open',),
                    'fields': (
                        ('importo_progetto', 'finanziamento_ministero','finanziamento_altro','extracosti'),
                    )
                }),
                ("dati", {
                    'classes': ('grp-collapse grp-open',),
                    'fields': (
                        ('rup','determina_rup',),
                        ('inizio_lavori', 'fine_lavori', ), 
                        'note',
                    )
                }),
                ("Localizzazione", {
                    'classes': ('grp-collapse grp-open',),
                    'fields': ('mappa_sola_lettura','wkt_geom','scala_rappresentazione') #'coordCatastali',
                }),
            )

    def changelist_view(self, request, extra_context=None):
        try:
            action = self.get_actions(request)[request.POST['action']][0]
            action_acts_on_all = action.acts_on_all
        except (KeyError, AttributeError):
            action_acts_on_all = False

        if action_acts_on_all:
            post = request.POST.copy()
            post.setlist(helpers.ACTION_CHECKBOX_NAME,
                        self.model.objects.values_list('id', flat=True))
            request.POST = post

        return admin.ModelAdmin.changelist_view(self, request, extra_context)

    def save_related(self, request, form, formsets, change):
        obj = form.instance
        super().save_related(request, form, formsets, change)
        for n,formset in enumerate(formsets):
            print('FORMSET %s --->' % n)
            for n,sub_form in enumerate(formset):
                model_name = sub_form.instance.__class__.__name__
                if sub_form.has_changed():
                    if model_name == 'files_allegati': # E' un nuovo allegato
                        if sub_form.cleaned_data['DELETE']:
                            continue
                        allegati_obj = sub_form.instance
                        allegati_obj.save()
                        if sub_form.cleaned_data['tipo'] == 'DXF':
                            dir_path = os.path.join(settings.MEDIA_ROOT,"tmp","dxf")
                            decoder = dxf_decode(regoleDefinizione,dir_path,srid = 3003)
                            res = decoder.validate(allegati_obj.documento.path)
                            geojson_feats = json.loads(res["geojson"])
                            print ("RISULTATO SALVATAGGIO",res)
                            new_geom = GEOSGeometry(json.dumps(geojson_feats["features"][0]["geometry"]))
                            if new_geom.geom_type == 'Polygon':
                                new_geom = MultiPolygon([new_geom])
                            obj.the_geom = new_geom
                            obj.save()
                            print (obj.the_geom.wkt)

admin.site.register(categoria)
admin.site.register(files_allegati)