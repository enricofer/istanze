# Generated by Django 3.2.11 on 2022-08-16 11:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pnrr', '0002_alter_categoria_titolo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='progetto',
            name='allegati',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pnrr.files_allegati', verbose_name='Files allegati'),
        ),
        migrations.AlterField(
            model_name='progetto',
            name='stato_finanziamento',
            field=models.BooleanField(choices=[(True, 'finanziato'), (False, 'non finanziato')], default=False),
        ),
    ]
