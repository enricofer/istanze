from django.contrib.gis.db import models
from datetime import date
from django.conf import settings

from django.db.models.signals import pre_delete, pre_save, post_save
from django.dispatch.dispatcher import receiver

import os
import re
import sys
import shutil
import uuid
import base64

# Create your models here.

__author__ = "Enrico Ferreguti"
__email__ = "enricofer@gmail.com"
__copyright__ = "Copyright 2022, Enrico Ferreguti"
__license__ = "GPL3"

priorita_choice = [
    (None,'---------------'),
    (1,'priorità 1'),
    (2,'priorità 2'),
    (3,'priorità 3')
]

stato_finanziamento_choice = [
    (True,'finanziato'),
    (False,'non finanziato'),
]

stato_avanzamento_choice = [
    (None,'---------------'),
    ('01','01 - Programmazione'),
    ('11','01 - Studio di Fattibilità'),
    ('12','02 - Progetto Definitivo'),
    ('13','03 - Progetto Esecutivo'),
    ('21','11 - Determina a contrarre'),
    ('22','12 - Procedura di affidamento'),
    ('31','21 - Lavori in corso'),
    ('32','22 - In attesa di collaudo'),
    ('41','41 - Completato'),
]

stato_avanzamento_choice = [
    (None,'---------------'),
    ('00','0 - Programmazione'),
    ('10','1 - Progettazione'),
    ('20','2 - Affidamento'),
    ('30','3 - Realizzazione'),
    ('40','4 - Completato'),
]

tipo_allegato_choice = [
    ('promemoria','promemoria'),
    ('cronoprogramma','cronoprogramma'),
    ('relazione','relazione'),
    ('elaborato','elaborato di progetto'),
    ('determina','determina'),
    ('contratto','contratto'),
    ('DXF','Perimetro dxf'),
    ('altro','altra documentazione'),
]


class categoria(models.Model):

    class Meta:
        verbose_name_plural = "Categorie di progetto"
        verbose_name = "Categoria di progetto"

    titolo = models.CharField(max_length=60)
    titolo_esteso = models.CharField(max_length=150, blank=True, null=True)
    scadenze = models.TextField(blank=True, null=True)
    note = models.TextField(blank=True, null=True)

    def __str__(self):
        return '%d-%s' % (self.pk,self.titolo)
    

class progetto(models.Model):

    class Meta:
        verbose_name_plural = "Progetti"
        verbose_name = "Progetto"

    categoria = models.ForeignKey('categoria', verbose_name=u"Categoria di progetto", on_delete=models.PROTECT )
    id_progetto = models.CharField(max_length=5, blank=True, null=True)
    priorita = models.IntegerField(blank=True, null=True, choices=priorita_choice)
    codice_opera = models.CharField(max_length=30, blank=True, null=True)
    capitolo = models.CharField(max_length=8, blank=True, null=True)
    cup = models.CharField(max_length=15, blank=True, null=True)
    oggetto = models.CharField(max_length=150, blank=True, null=True)
    importo_progetto = models.IntegerField(blank=True, null=True)
    finanziamento_ministero = models.IntegerField(verbose_name=u"Ammontare finanziamento ministero",blank=True, null=True)
    finanziamento_altro = models.IntegerField(blank=True, null=True)
    determina_rup = models.CharField(max_length=50, verbose_name=u"Delibera di nomina RUP", blank=True, null=True)
    rup = models.CharField(max_length=50, verbose_name=u"RUP", blank=True, null=True)
    stato_finanziamento = models.BooleanField(default=False, verbose_name=u"Progetto finanziato",)
    stato_avanzamento = models.CharField(max_length=2, blank=True, null=True, choices=stato_avanzamento_choice)
    avanzamento_stimato = models.IntegerField(blank=True, null=True)
    importo_lavori = models.IntegerField(blank=True, null=True)
    inizio_lavori = models.DateField( blank=True, null=True)
    fine_lavori = models.DateField( blank=True, null=True)
    extracosti = models.IntegerField(blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    the_geom = models.MultiPolygonField(srid=3003, blank=True, null=True)
    scala_rappresentazione = models.IntegerField(blank=True, null=True, default=2000)

    @property
    def ha_geometria(self):
        return not not self.the_geom

    @property
    def scala_calcolata(self): #scala calcolata rispetto una finestra di visualizzazione di 282x282 mm
        if self.the_geom and self.the_geom.valid and not self.the_geom.empty:
            max_extent_x = self.the_geom.extent[2]-self.the_geom.extent[0]
            max_extent_y = self.the_geom.extent[3]-self.the_geom.extent[1]
            if max_extent_x < 507 and max_extent_y<507:
                return 2000
            elif max_extent_x < 1269 and max_extent_y<1269:
                return 5000
            elif max_extent_x < 1900 and max_extent_y<1900:
                return 7500
            elif max_extent_x < 2538 and max_extent_y<2538:
                return 10000
            elif max_extent_x < 5070 and max_extent_y<5070:
                return 20000
            elif max_extent_x < 12690 and max_extent_y<12690:
                return 50000
            elif max_extent_x < 19000 and max_extent_y<19000:
                return 75000
            else:
                return 100000

    def __str__(self):
        return '%d-%s-%s' % (self.pk,self.categoria.titolo,self.id_progetto or self.oggetto,)


class files_allegati(models.Model):

    class Meta:
        verbose_name_plural = "Files allegati"
        verbose_name = "File allegato"

    def update_filename(instance, filename):
        path = "pnrr"
        if not os.path.exists(path):
            os.makedirs(path)
        name,extension = os.path.splitext (filename)
        uuid_to_b64str = base64.urlsafe_b64encode(uuid.uuid1().bytes).decode('utf8').rstrip('=\n')
        format = str(instance.data) +'_' + uuid_to_b64str + '_' + str(instance.tipo) + '_'+ str(instance.titolo) + extension
        return os.path.join(path, format)

    tipo = models.CharField(max_length=15, choices=tipo_allegato_choice)
    titolo = models.CharField(max_length=50)
    data = models.DateField( default=date.today() )
    documento = models.FileField(upload_to=update_filename)
    progetto = models.ForeignKey('progetto', verbose_name=u"progetto", on_delete=models.CASCADE )

    def __str__(self):
        return '%d-%s-%s' % (self.pk or -1,self.tipo or "non specificato",self.titolo or "non specificato")

@receiver(pre_delete, sender=files_allegati)
def files_allegati_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.documento.delete(False)

@receiver(post_save, sender=progetto)
def update_scala_rappresentazione(sender, instance, **kwargs):
    if instance.scala_rappresentazione != instance.scala_calcolata:
        print (instance)
        instance.scala_rappresentazione = instance.scala_calcolata
        instance.save()